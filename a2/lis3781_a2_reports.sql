mysql> create database ooa19a;
Query OK, 1 row affected (0.01 sec)

mysql> show databases
    -> ;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| ooa19a             |
| performance_schema |
+--------------------+
4 rows in set (0.00 sec)

mysql> use ooa19a;
Database changed
mysql> show tables;
Empty set (0.00 sec)

mysql> create table if not exists company
    -> (
    -> 
    -> ;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '' at line 2
mysql> create table if not exists company    (    cmp_id int unsigned not null auto_increment,   cmp_type enum('C-Corp','S-Corp','Non-Profit-Corp','LLC','Partnership'),   cmp_street varchar(30) not null,   cmp_city varchar(30) not null,   cmp_state char (2) not null,   cmp_zip int (9) unsigned zerofill NOT NULL,   cmp_phone bigint unsigned not null,   cmp_ytd_sales decimal (10,2) unsigned not null,   cmp_url varchar(100) null,   cmp_notes  varchar(100) null,      primary key(cmd_id)   );
ERROR 1072 (42000): Key column 'cmd_id' doesn't exist in table
mysql> create table if not exists company    (    cmp_id int unsigned not null auto_increment,   cmp_type enum('C-Corp','S-Corp','Non-Profit-Corp','LLC','Partnership'),   cmp_street varchar(30) not null,   cmp_city varchar(30) not null,   cmp_state char (2) not null,   cmp_zip int (9) unsigned zerofill NOT NULL,   cmp_phone bigint unsigned not null,   cmp_ytd_sales decimal (10,2) unsigned not null,   cmp_url varchar(100) null,   cmp_notes  varchar(100) null,      primary key(cmp_id)   );
Query OK, 0 rows affected, 3 warnings (0.04 sec)

mysql> show warnings;
+---------+------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Level   | Code | Message                                                                                                                                                                   |
+---------+------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Warning | 1681 | The ZEROFILL attribute is deprecated and will be removed in a future release. Use the LPAD function to zero-pad numbers, or store the formatted numbers in a CHAR column. |
| Warning | 1681 | Integer display width is deprecated and will be removed in a future release.                                                                                              |
| Warning | 1681 | UNSIGNED for decimal and floating point data types is deprecated and support for it will be removed in a future release.                                                  |
+---------+------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
3 rows in set (0.00 sec)

mysql> show tables
    -> ;
+------------------+
| Tables_in_ooa19a |
+------------------+
| company          |
+------------------+
1 row in set (0.00 sec)

mysql> insert into company values(null, 'S-corp','839 - 20th Ave.', 'London','NM', '0909876543','201324294.00', null, 'www.google.com','company note 1');
ERROR 1048 (23000): Column 'cmp_ytd_sales' cannot be null
mysql> insert into company values(null, 'S-corp','839 - 20th Ave.', 'London','NM', '0909876543','201324294.00','45435560', null, 'www.google.com','company note 1');
ERROR 1136 (21S01): Column count doesn't match value count at row 1
mysql> insert into company values(null, 'S-corp','839 - 20th Ave.', 'London','NM', '0909876543','201324294.00','45435560', 'www.google.com','company note 1');
Query OK, 1 row affected (0.01 sec)

mysql> select * from company;
+--------+----------+-----------------+----------+-----------+-----------+-----------+---------------+----------------+----------------+
| cmp_id | cmp_type | cmp_street      | cmp_city | cmp_state | cmp_zip   | cmp_phone | cmp_ytd_sales | cmp_url        | cmp_notes      |
+--------+----------+-----------------+----------+-----------+-----------+-----------+---------------+----------------+----------------+
|      1 | S-Corp   | 839 - 20th Ave. | London   | NM        | 909876543 | 201324294 |   45435560.00 | www.google.com | company note 1 |
+--------+----------+-----------------+----------+-----------+-----------+-----------+---------------+----------------+----------------+
1 row in set (0.00 sec)

mysql> alter table company
    -> add cmp_email varchar (100) null
    -> after cmp_ytd_sales;
Query OK, 0 rows affected (0.16 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> select * from company;
+--------+----------+-----------------+----------+-----------+-----------+-----------+---------------+-----------+----------------+----------------+
| cmp_id | cmp_type | cmp_street      | cmp_city | cmp_state | cmp_zip   | cmp_phone | cmp_ytd_sales | cmp_email | cmp_url        | cmp_notes      |
+--------+----------+-----------------+----------+-----------+-----------+-----------+---------------+-----------+----------------+----------------+
|      1 | S-Corp   | 839 - 20th Ave. | London   | NM        | 909876543 | 201324294 |   45435560.00 | NULL      | www.google.com | company note 1 |
+--------+----------+-----------------+----------+-----------+-----------+-----------+---------------+-----------+----------------+----------------+
1 row in set (0.00 sec)

mysql> update company 
    -> set cmp_email = 'scorp@scorp.com'
    -> where cmp_id = 1;
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> select * from company;
+--------+----------+-----------------+----------+-----------+-----------+-----------+---------------+-----------------+----------------+----------------+
| cmp_id | cmp_type | cmp_street      | cmp_city | cmp_state | cmp_zip   | cmp_phone | cmp_ytd_sales | cmp_email       | cmp_url        | cmp_notes      |
+--------+----------+-----------------+----------+-----------+-----------+-----------+---------------+-----------------+----------------+----------------+
|      1 | S-Corp   | 839 - 20th Ave. | London   | NM        | 909876543 | 201324294 |   45435560.00 | scorp@scorp.com | www.google.com | company note 1 |
+--------+----------+-----------------+----------+-----------+-----------+-----------+---------------+-----------------+----------------+----------------+
1 row in set (0.00 sec)

mysql> insert into company values(null, 'C-corp','Johnson Ave.', 'Southy','MY', '94947933','64774783.00','46747338', 'ccorp@xyz.com', 'www.okay.com','company note 2'),(null, 'C-corp','Magic Ave.', 'Best','NJ', '0398585','95049339.00','74784538', 'ccorp@xyz.com', 'www.school.com','company note 3'),(null, 'Parthnership','Kobe Ave.', 'Jotter','Colorado', '46743839','6834874.00','68483794', 'npfcorp@xyz.com', 'www.servlet.com','company note 4'),(null, 'Non-Profit-Corp','LeBron Ave.', 'Oak','NY', '48799385','95489830.00','8377493', 'npf@xyz.com', 'www.bottle.com','company note 5'),(null, 'LLC','Steph Ave.', 'Cali','CL', '8749344','84794739.00','3847392', 'LLC@xyz.com', 'www.lamp.com','company note 6'),(null, 'Parthnership','Curry Ave.', 'Sunny Vale','SF', '9374932','0438538.00','85373874', 'parthnership@xyz.com', 'www.apple.com','company note 7');
ERROR 1265 (01000): Data truncated for column 'cmp_type' at row 3
mysql> insert into company values(null, 'C-corp','Johnson Ave.', 'Southy','MY', '94947933','64774783.00','46747338', 'ccorp@xyz.com', 'www.okay.com','company note 2'),(null, 'S-corp','Magic Ave.', 'Best','NJ', '0398585','95049339.00','74784538', 'ccorp@xyz.com', 'www.school.com','company note 3'),(null, 'Parthnership','Kobe Ave.', 'Jotter','Colorado', '46743839','6834874.00','68483794', 'npfcorp@xyz.com', 'www.servlet.com','company note 4'),(null, 'Non-Profit-Corp','LeBron Ave.', 'Oak','NY', '48799385','95489830.00','8377493', 'npf@xyz.com', 'www.bottle.com','company note 5'),(null, 'LLC','Steph Ave.', 'Cali','CL', '8749344','84794739.00','3847392', 'LLC@xyz.com', 'www.lamp.com','company note 6'),(null, 'Parthnership','Curry Ave.', 'Sunny Vale','SF', '9374932','0438538.00','85373874', 'parthnership@xyz.com', 'www.apple.com','company note 7');
ERROR 1265 (01000): Data truncated for column 'cmp_type' at row 3
mysql> insert into company values(null, 'C-corp','Johnson Ave.', 'Southy','MY', '94947933','64774783.00','46747338', 'ccorp@xyz.com', 'www.okay.com','company note 2'),(null, 'S-corp','Magic Ave.', 'Best','NJ', '0398585','95049339.00','74784538', 'scorp@xyz.com', 'www.school.com','company note 3'),(null, 'Partnership','Kobe Ave.', 'Jotter','Colorado', '46743839','6834874.00','68483794', 'npfcorp@xyz.com', 'www.servlet.com','company note 4'),(null, 'Non-Profit-Corp','LeBron Ave.', 'Oak','NY', '48799385','95489830.00','8377493', 'npf@xyz.com', 'www.bottle.com','company note 5'),(null, 'LLC','Steph Ave.', 'Cali','CL', '8749344','84794739.00','3847392', 'LLC@xyz.com', 'www.lamp.com','company note 6'),(null, 'Partnership','Curry Ave.', 'Sunny Vale','SF', '9374932','0438538.00','85373874', 'Partnership@xyz.com', 'www.apple.com','company note 7');
ERROR 1406 (22001): Data too long for column 'cmp_state' at row 3
mysql> insert into company values(null, 'C-corp','Johnson Ave.', 'Southy','MY', '94947933','64774783.00','46747338', 'ccorp@xyz.com', 'www.okay.com','company note 2'),(null, 'S-corp','Magic Ave.', 'Best','NJ', '0398585','95049339.00','74784538', 'scorp@xyz.com', 'www.school.com','company note 3'),(null, 'Partnership','Kobe Ave.', 'Jotter','CL', '46743839','6834874.00','68483794', 'npfcorp@xyz.com', 'www.servlet.com','company note 4'),(null, 'Non-Profit-Corp','LeBron Ave.', 'Oak','NY', '48799385','95489830.00','8377493', 'npf@xyz.com', 'www.bottle.com','company note 5'),(null, 'LLC','Steph Ave.', 'Cali','CL', '8749344','84794739.00','3847392', 'LLC@xyz.com', 'www.lamp.com','company note 6'),(null, 'Partnership','Curry Ave.', 'Sunny Vale','SF', '9374932','0438538.00','85373874', 'Partnership@xyz.com', 'www.apple.com','company note 7');
Query OK, 6 rows affected (0.01 sec)
Records: 6  Duplicates: 0  Warnings: 0

mysql> select * from company;
+--------+-----------------+-----------------+------------+-----------+-----------+-----------+---------------+---------------------+-----------------+----------------+
| cmp_id | cmp_type        | cmp_street      | cmp_city   | cmp_state | cmp_zip   | cmp_phone | cmp_ytd_sales | cmp_email           | cmp_url         | cmp_notes      |
+--------+-----------------+-----------------+------------+-----------+-----------+-----------+---------------+---------------------+-----------------+----------------+
|      1 | S-Corp          | 839 - 20th Ave. | London     | NM        | 909876543 | 201324294 |   45435560.00 | scorp@scorp.com     | www.google.com  | company note 1 |
|     20 | C-Corp          | Johnson Ave.    | Southy     | MY        | 094947933 |  64774783 |   46747338.00 | ccorp@xyz.com       | www.okay.com    | company note 2 |
|     21 | S-Corp          | Magic Ave.      | Best       | NJ        | 000398585 |  95049339 |   74784538.00 | scorp@xyz.com       | www.school.com  | company note 3 |
|     22 | Partnership     | Kobe Ave.       | Jotter     | CL        | 046743839 |   6834874 |   68483794.00 | npfcorp@xyz.com     | www.servlet.com | company note 4 |
|     23 | Non-Profit-Corp | LeBron Ave.     | Oak        | NY        | 048799385 |  95489830 |    8377493.00 | npf@xyz.com         | www.bottle.com  | company note 5 |
|     24 | LLC             | Steph Ave.      | Cali       | CL        | 008749344 |  84794739 |    3847392.00 | LLC@xyz.com         | www.lamp.com    | company note 6 |
|     25 | Partnership     | Curry Ave.      | Sunny Vale | SF        | 009374932 |    438538 |   85373874.00 | Partnership@xyz.com | www.apple.com   | company note 7 |
+--------+-----------------+-----------------+------------+-----------+-----------+-----------+---------------+---------------------+-----------------+----------------+
7 rows in set (0.00 sec)

mysql> drop table company;
Query OK, 0 rows affected (0.02 sec)

mysql> create table if not exists company    (    cmp_id int unsigned not null auto_increment,   cmp_type enum('C-Corp','S-Corp','Non-Profit-Corp','LLC','Partnership'),   cmp_street varchar(30) not null,   cmp_city varchar(30) not null,   cmp_state char (2) not null,   cmp_zip int (9) unsigned zerofill NOT NULL,   cmp_phone bigint unsigned not null,   cmp_ytd_sales decimal (10,2) unsigned not null,   cmp_url varchar(100) null,   cmp_notes  varchar(100) null,      primary key(cmp_id)   );
Query OK, 0 rows affected, 3 warnings (0.03 sec)

mysql> alter table company
    -> add cmp_email varchar (100) null
    -> after cmp_ytd_sales;
Query OK, 0 rows affected (0.04 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> select * from tables;
ERROR 1146 (42S02): Table 'ooa19a.tables' doesn't exist
mysql> select * from company;
Empty set (0.00 sec)

mysql> insert into company values(null, 'C-corp','Johnson Ave.', 'Southy','MY', '94947933','64774783.00','46747338', 'ccorp@xyz.com', 'www.okay.com','company note 2'),(null, 'S-corp','Magic Ave.', 'Best','NJ', '0398585','95049339.00','74784538', 'scorp@xyz.com', 'www.school.com','company note 3'),(null, 'Partnership','Kobe Ave.', 'Jotter','CL', '46743839','6834874.00','68483794', 'npfcorp@xyz.com', 'www.servlet.com','company note 4'),(null, 'Non-Profit-Corp','LeBron Ave.', 'Oak','NY', '48799385','95489830.00','8377493', 'npf@xyz.com', 'www.bottle.com','company note 5'),(null, 'LLC','Steph Ave.', 'Cali','CL', '8749344','84794739.00','3847392', 'LLC@xyz.com', 'www.lamp.com','company note 6'),(null, 'Partnership','Curry Ave.', 'Sunny Vale','SF', '9374932','0438538.00','85373874', 'Partnership@xyz.com', 'www.apple.com','company note 7');
Query OK, 6 rows affected (0.01 sec)
Records: 6  Duplicates: 0  Warnings: 0

mysql> select * from company;
+--------+-----------------+--------------+------------+-----------+-----------+-----------+---------------+---------------------+-----------------+----------------+
| cmp_id | cmp_type        | cmp_street   | cmp_city   | cmp_state | cmp_zip   | cmp_phone | cmp_ytd_sales | cmp_email           | cmp_url         | cmp_notes      |
+--------+-----------------+--------------+------------+-----------+-----------+-----------+---------------+---------------------+-----------------+----------------+
|      1 | C-Corp          | Johnson Ave. | Southy     | MY        | 094947933 |  64774783 |   46747338.00 | ccorp@xyz.com       | www.okay.com    | company note 2 |
|      2 | S-Corp          | Magic Ave.   | Best       | NJ        | 000398585 |  95049339 |   74784538.00 | scorp@xyz.com       | www.school.com  | company note 3 |
|      3 | Partnership     | Kobe Ave.    | Jotter     | CL        | 046743839 |   6834874 |   68483794.00 | npfcorp@xyz.com     | www.servlet.com | company note 4 |
|      4 | Non-Profit-Corp | LeBron Ave.  | Oak        | NY        | 048799385 |  95489830 |    8377493.00 | npf@xyz.com         | www.bottle.com  | company note 5 |
|      5 | LLC             | Steph Ave.   | Cali       | CL        | 008749344 |  84794739 |    3847392.00 | LLC@xyz.com         | www.lamp.com    | company note 6 |
|      6 | Partnership     | Curry Ave.   | Sunny Vale | SF        | 009374932 |    438538 |   85373874.00 | Partnership@xyz.com | www.apple.com   | company note 7 |
+--------+-----------------+--------------+------------+-----------+-----------+-----------+---------------+---------------------+-----------------+----------------+
6 rows in set (0.00 sec)

mysql> create table if not exists custoemr;(cus_id int unsigned not null auto_increment,cmp_id int unsigned not null,cus_ssn binary(64) not null,cus_salt binary(64) not null,cus_type enum('Loyal','Discount','Impulse','Need-Based','Wandering'),cus_first varchar (30) not null,cus_last varchar (30) not null,cus_street varchar (100) not null,cus_city varchar (20) not null,cus_state char (2) not null,cus_zip int(9) unsigned zerofill not null, cus_phone bigint unsigned not null,cus_email varchar(100) not null,cus_balance decimal (10,2) not null unsigned null,cus_tot_sales decimal (10,2) not null unsigned null,cus_notes varchar(255) null,primary key (cus_id),unique index ux_cus_ssn (cus_ssn ASC),index idx_cmp_id (cmp_id ASC),constraint fk_company_customerforeign key (cmp_id)reference company (cmp_id)on delete no action on update cascade);
ERROR 1113 (42000): A table must have at least 1 column
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'cus_id int unsigned not null auto_increment,cmp_id int unsigned not null,cus_ssn' at line 1
mysql> create table if not exists customer(cus_id int unsigned not null auto_increment,cmp_id int unsigned not null,cus_ssn binary(64) not null,cus_salt binary(64) not null,cus_type enum('Loyal','Discount','Impulse','Need-Based','Wandering'),cus_first varchar (30) not null,cus_last varchar (30) not null,cus_street varchar (100) not null,cus_city varchar (20) not null,cus_state char (2) not null,cus_zip int(9) unsigned zerofill not null, cus_phone bigint unsigned not null,cus_email varchar(100) not null,cus_balance decimal (10,2) not null unsigned null,cus_tot_sales decimal (10,2) not null unsigned null,cus_notes varchar(255) null,primary key (cus_id),unique index ux_cus_ssn (cus_ssn ASC),index idx_cmp_id (cmp_id ASC),constraint fk_company_customerforeign key (cmp_id)reference company (cmp_id)on delete no action on update cascade);
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'unsigned null,cus_tot_sales decimal (10,2) not null unsigned null,cus_notes varc' at line 1
mysql> create table if not exists customer(cus_id int unsigned not null auto_increment,cmp_id int unsigned not null,cus_ssn binary(64) not null,cus_salt binary(64) not null,cus_type enum('Loyal','Discount','Impulse','Need-Based','Wandering'),cus_first varchar (30) not null,cus_last varchar (30) not null,cus_street varchar (100) not null,cus_city varchar (20) not null,cus_state char (2) not null,cus_zip int(9) unsigned zerofill not null, cus_phone bigint unsigned not null,cus_email varchar(100) not null,cus_balance decimal (10,2) unsigned not null ,cus_tot_sales decimal (10,2) unsigned not null ,cus_notes varchar(255) null,primary key (cus_id),unique index ux_cus_ssn (cus_ssn ASC),index idx_cmp_id (cmp_id ASC),constraint fk_company_customerforeign key (cmp_id)references company (cmp_id)on delete no action on update cascade);
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'key (cmp_id)references company (cmp_id)on delete no action on update cascade)' at line 1
mysql> create table if not exists customer(cus_id int unsigned not null auto_increment,cmp_id int unsigned not null,cus_ssn binary(64) not null,cus_salt binary(64) not null,cus_type enum('Loyal','Discount','Impulse','Need-Based','Wandering'),cus_first varchar (30) not null,cus_last varchar (30) not null,cus_street varchar (100) not null,cus_city varchar (20) not null,cus_state char (2) not null,cus_zip int(9) unsigned zerofill not null, cus_phone bigint unsigned not null,cus_email varchar(100) not null,cus_balance decimal (10,2) unsigned not null ,cus_tot_sales decimal (10,2) unsigned not null ,cus_notes varchar(255) null,primary key (cus_id),unique index ux_cus_ssn (cus_ssn ASC),index idx_cmp_id (cmp_id ASC),CONSTRAINT fk_company_customerforeign key (cmp_id)REFERENCES company(cmp_id)ON DELETE NO ACTION ON UPDATE CASCADE);
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'key (cmp_id)REFERENCES company(cmp_id)ON DELETE NO ACTION ON UPDATE CASCADE)' at line 1
mysql> create table if not exists customer(cus_id int unsigned not null auto_increment,cmp_id int unsigned not null,cus_ssn binary(64) not null,cus_salt binary(64) not null,cus_type enum('Loyal','Discount','Impulse','Need-Based','Wandering'),cus_first varchar (30) not null,cus_last varchar (30) not null,cus_street varchar (100) not null,cus_city varchar (20) not null,cus_state char (2) not null,cus_zip int(9) unsigned zerofill not null, cus_phone bigint unsigned not null,cus_email varchar(100) not null,cus_balance decimal (10,2) unsigned not null ,cus_tot_sales decimal (10,2) unsigned not null ,cus_notes varchar(255) null,primary key (cus_id),unique index ux_cus_ssn (cus_ssn ASC),index idx_cmp_id (cmp_id ASC),CONSTRAINT fk_company_customerforeign key (cmp_id)REFERENCES company(cmp_id)ON DELETE NO ACTION ON UPDATE CASCADE)ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'key (cmp_id)REFERENCES company(cmp_id)ON DELETE NO ACTION ON UPDATE CASCADE)ENGI' at line 1
mysql> select * from customer;
Empty set (0.00 sec)

mysql> describe customer;
+---------------+-------------------------------------------------------------+------+-----+---------+----------------+
| Field         | Type                                                        | Null | Key | Default | Extra          |
+---------------+-------------------------------------------------------------+------+-----+---------+----------------+
| cus_id        | int(10) unsigned                                            | NO   | PRI | NULL    | auto_increment |
| cmp_id        | int(10) unsigned                                            | NO   | MUL | NULL    |                |
| cus_ssn       | binary(64)                                                  | NO   | UNI | NULL    |                |
| cus_salt      | binary(64)                                                  | NO   |     | NULL    |                |
| cus_type      | enum('Loyal','Discount','Impulse','Need-Based','Wandering') | YES  |     | NULL    |                |
| cus_first     | varchar(30)                                                 | NO   |     | NULL    |                |
| cus_last      | varchar(30)                                                 | NO   |     | NULL    |                |
| cus_street    | varchar(100)                                                | NO   |     | NULL    |                |
| cus_city      | varchar(20)                                                 | NO   |     | NULL    |                |
| cus_state     | char(2)                                                     | NO   |     | NULL    |                |
| cus_zip       | int(9) unsigned zerofill                                    | NO   |     | NULL    |                |
| cus_phone     | bigint(20) unsigned                                         | NO   |     | NULL    |                |
| cus_email     | varchar(100)                                                | NO   |     | NULL    |                |
| cus_balance   | decimal(10,2) unsigned                                      | NO   |     | NULL    |                |
| cus_tot_sales | decimal(10,2) unsigned                                      | NO   |     | NULL    |                |
| cus_notes     | varchar(255)                                                | YES  |     | NULL    |                |
+---------------+-------------------------------------------------------------+------+-----+---------+----------------+
16 rows in set (0.01 sec)

mysql> set @ salt = random_bytes(64);
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'salt = random_bytes(64)' at line 1
mysql> set @salt=random_bytes(64);
Query OK, 0 rows affected (0.00 sec)

mysql> insert into customer
    -> values(
    -> null, 2, unhex(sha2(concat(@salt,00001234),512)),@salt,'Discount','Mulan', 'Li','El paso','Paso','TX','0807656','686799','mulan@li.com','847593.00','76444.00','customer note 1');
Query OK, 1 row affected (0.01 sec)

mysql> select * from customer;
+--------+--------+------------------------------------------------------------------+------------------------------------------------------------------+----------+-----------+----------+------------+----------+-----------+-----------+-----------+--------------+-------------+---------------+-----------------+
| cus_id | cmp_id | cus_ssn                                                          | cus_salt                                                         | cus_type | cus_first | cus_last | cus_street | cus_city | cus_state | cus_zip   | cus_phone | cus_email    | cus_balance | cus_tot_sales | cus_notes       |
+--------+--------+------------------------------------------------------------------+------------------------------------------------------------------+----------+-----------+----------+------------+----------+-----------+-----------+-----------+--------------+-------------+---------------+-----------------+
|      1 |      2 | ����7{���=��P���s=l���8�G�������[����t�x7��G���I�,��Z                                      | r�_�I��O�ԖU��E�|�3�`c|V�+zn ꥒ����Z ��z�Yw�F�_�B�|�H=J$�                            | Discount | Mulan     | Li       | El paso    | Paso     | TX        | 000807656 |    686799 | mulan@li.com |   847593.00 |      76444.00 | customer note 1 |
+--------+--------+------------------------------------------------------------------+------------------------------------------------------------------+----------+-----------+----------+------------+----------+-----------+-----------+-----------+--------------+-------------+---------------+-----------------+
1 row in set (0.00 sec)

mysql> select * from customer;
+--------+--------+------------------------------------------------------------------+------------------------------------------------------------------+----------+-----------+----------+------------+----------+-----------+-----------+-----------+--------------+-------------+---------------+-----------------+
| cus_id | cmp_id | cus_ssn                                                          | cus_salt                                                         | cus_type | cus_first | cus_last | cus_street | cus_city | cus_state | cus_zip   | cus_phone | cus_email    | cus_balance | cus_tot_sales | cus_notes       |
+--------+--------+------------------------------------------------------------------+------------------------------------------------------------------+----------+-----------+----------+------------+----------+-----------+-----------+-----------+--------------+-------------+---------------+-----------------+
|      1 |      2 | ����7{���=��P���s=l���8�G�������[����t�x7��G���I�,��Z                                      | r�_�I��O�ԖU��E�|�3�`c|V�+zn ꥒ����Z ��z�Yw�F�_�B�|�H=J$�                            | Discount | Mulan     | Li       | El paso    | Paso     | TX        | 000807656 |    686799 | mulan@li.com |   847593.00 |      76444.00 | customer note 1 |
+--------+--------+------------------------------------------------------------------+------------------------------------------------------------------+----------+-----------+----------+------------+----------+-----------+-----------+-----------+--------------+-------------+---------------+-----------------+
1 row in set (0.01 sec)

mysql> insert into customer values (null, 3, unhex(sha2(concat(@salt,00006754),512)),@salt,'Impulse','Lewis', 'Tola','Ben','West Orange','NJ','075565','0897864','oba@olx.com','886545.00','757654.00','customer note 2');
Query OK, 1 row affected (0.01 sec)

mysql> select * from customer;
+--------+--------+------------------------------------------------------------------+------------------------------------------------------------------+----------+-----------+----------+------------+-------------+-----------+-----------+-----------+--------------+-------------+---------------+-----------------+
| cus_id | cmp_id | cus_ssn                                                          | cus_salt                                                         | cus_type | cus_first | cus_last | cus_street | cus_city    | cus_state | cus_zip   | cus_phone | cus_email    | cus_balance | cus_tot_sales | cus_notes       |
+--------+--------+------------------------------------------------------------------+------------------------------------------------------------------+----------+-----------+----------+------------+-------------+-----------+-----------+-----------+--------------+-------------+---------------+-----------------+
|      1 |      2 | ����7{���=��P���s=l���8�G�������[����t�x7��G���I�,��Z                                      | r�_�I��O�ԖU��E�|�3�`c|V�+zn ꥒ����Z ��z�Yw�F�_�B�|�H=J$�                            | Discount | Mulan     | Li       | El paso    | Paso        | TX        | 000807656 |    686799 | mulan@li.com |   847593.00 |      76444.00 | customer note 1 |
|      2 |      3 | XMUV�Z�G5��µ%����J2�A�؎D�����.�a7�Q���|�����"dq�                               | r�_�I��O�ԖU��E�|�3�`c|V�+zn ꥒ����Z ��z�Yw�F�_�B�|�H=J$�                            | Impulse  | Lewis     | Tola     | Ben        | West Orange | NJ        | 000075565 |    897864 | oba@olx.com  |   886545.00 |     757654.00 | customer note 2 |
+--------+--------+------------------------------------------------------------------+------------------------------------------------------------------+----------+-----------+----------+------------+-------------+-----------+-----------+-----------+--------------+-------------+---------------+-----------------+
2 rows in set (0.00 sec)

mysql> insert into customer values (null, 4, unhex(sha2(concat(@salt,000056743),512)),@salt,'Need-Based ','Tunde', 'Sanya','Balt','Baltimore','WD','0230756','834229','jinn@jiji.com','33232.00','47382.00','customer note 3');
Query OK, 1 row affected (0.01 sec)

mysql> select * from customer;
+--------+--------+------------------------------------------------------------------+------------------------------------------------------------------+------------+-----------+----------+------------+-------------+-----------+-----------+-----------+---------------+-------------+---------------+-----------------+
| cus_id | cmp_id | cus_ssn                                                          | cus_salt                                                         | cus_type   | cus_first | cus_last | cus_street | cus_city    | cus_state | cus_zip   | cus_phone | cus_email     | cus_balance | cus_tot_sales | cus_notes       |
+--------+--------+------------------------------------------------------------------+------------------------------------------------------------------+------------+-----------+----------+------------+-------------+-----------+-----------+-----------+---------------+-------------+---------------+-----------------+
|      1 |      2 | ����7{���=��P���s=l���8�G�������[����t�x7��G���I�,��Z                                      | r�_�I��O�ԖU��E�|�3�`c|V�+zn ꥒ����Z ��z�Yw�F�_�B�|�H=J$�                            | Discount   | Mulan     | Li       | El paso    | Paso        | TX        | 000807656 |    686799 | mulan@li.com  |   847593.00 |      76444.00 | customer note 1 |
|      2 |      3 | XMUV�Z�G5��µ%����J2�A�؎D�����.�a7�Q���|�����"dq�                               | r�_�I��O�ԖU��E�|�3�`c|V�+zn ꥒ����Z ��z�Yw�F�_�B�|�H=J$�                            | Impulse    | Lewis     | Tola     | Ben        | West Orange | NJ        | 000075565 |    897864 | oba@olx.com   |   886545.00 |     757654.00 | customer note 2 |
|      3 |      4 | �H�������^̳ϙG5�X'�%��6oNbG�/i�Ex��o���n��WkvT.kg&G|h�                          | r�_�I��O�ԖU��E�|�3�`c|V�+zn ꥒ����Z ��z�Yw�F�_�B�|�H=J$�                            | Need-Based | Tunde     | Sanya    | Balt       | Baltimore   | WD        | 000230756 |    834229 | jinn@jiji.com |    33232.00 |      47382.00 | customer note 3 |
+--------+--------+------------------------------------------------------------------+------------------------------------------------------------------+------------+-----------+----------+------------+-------------+-----------+-----------+-----------+---------------+-------------+---------------+-----------------+
3 rows in set (0.00 sec)

mysql> use mysql;
Database changed
mysql> show tables;
+---------------------------+
| Tables_in_mysql           |
+---------------------------+
| columns_priv              |
| component                 |
| db                        |
| default_roles             |
| engine_cost               |
| func                      |
| general_log               |
| global_grants             |
| gtid_executed             |
| help_category             |
| help_keyword              |
| help_relation             |
| help_topic                |
| innodb_index_stats        |
| innodb_table_stats        |
| password_history          |
| plugin                    |
| procs_priv                |
| proxies_priv              |
| role_edges                |
| server_cost               |
| servers                   |
| slave_master_info         |
| slave_relay_log_info      |
| slave_worker_info         |
| slow_log                  |
| tables_priv               |
| time_zone                 |
| time_zone_leap_second     |
| time_zone_name            |
| time_zone_transition      |
| time_zone_transition_type |
| user                      |
+---------------------------+
33 rows in set (0.01 sec)

mysql> select * from user;
+-----------+------------------+-------------+-------------+-------------+-------------+-------------+-----------+-------------+---------------+--------------+-----------+------------+-----------------+------------+------------+--------------+------------+-----------------------+------------------+--------------+-----------------+------------------+------------------+----------------+---------------------+--------------------+------------------+------------+--------------+------------------------+----------+------------+-------------+--------------+---------------+-------------+-----------------+----------------------+-----------------------+------------------------------------------------------------------------+------------------+-----------------------+-------------------+----------------+------------------+----------------+------------------------+---------------------+--------------------------+-----------------+
| Host      | User             | Select_priv | Insert_priv | Update_priv | Delete_priv | Create_priv | Drop_priv | Reload_priv | Shutdown_priv | Process_priv | File_priv | Grant_priv | References_priv | Index_priv | Alter_priv | Show_db_priv | Super_priv | Create_tmp_table_priv | Lock_tables_priv | Execute_priv | Repl_slave_priv | Repl_client_priv | Create_view_priv | Show_view_priv | Create_routine_priv | Alter_routine_priv | Create_user_priv | Event_priv | Trigger_priv | Create_tablespace_priv | ssl_type | ssl_cipher | x509_issuer | x509_subject | max_questions | max_updates | max_connections | max_user_connections | plugin                | authentication_string                                                  | password_expired | password_last_changed | password_lifetime | account_locked | Create_role_priv | Drop_role_priv | Password_reuse_history | Password_reuse_time | Password_require_current | User_attributes |
+-----------+------------------+-------------+-------------+-------------+-------------+-------------+-----------+-------------+---------------+--------------+-----------+------------+-----------------+------------+------------+--------------+------------+-----------------------+------------------+--------------+-----------------+------------------+------------------+----------------+---------------------+--------------------+------------------+------------+--------------+------------------------+----------+------------+-------------+--------------+---------------+-------------+-----------------+----------------------+-----------------------+------------------------------------------------------------------------+------------------+-----------------------+-------------------+----------------+------------------+----------------+------------------------+---------------------+--------------------------+-----------------+
| locahost  | user3            | N           | N           | N           | N           | N           | N         | N           | N             | N            | N         | N          | N               | N          | N          | N            | N          | N                     | N                | N            | N               | N                | N                | N              | N                   | N                  | N                | N          | N            | N                      |          |            |             |              |             0 |           0 |               0 |                    0 | mysql_native_password | *F357E78CABAD76FD3F1018EF85D78499B6ACC431                              | N                | 2020-02-03 16:44:39   |              NULL | N              | N                | N              |                   NULL |                NULL | NULL                     | NULL            |
| localhost | mysql.infoschema | Y           | N           | N           | N           | N           | N         | N           | N             | N            | N         | N          | N               | N          | N          | N            | N          | N                     | N                | N            | N               | N                | N                | N              | N                   | N                  | N                | N          | N            | N                      |          |            |             |              |             0 |           0 |               0 |                    0 | caching_sha2_password | $A$005$THISISACOMBINATIONOFINVALIDSALTANDPASSWORDTHATMUSTNEVERBRBEUSED | N                | 2019-11-19 05:46:52   |              NULL | Y              | N                | N              |                   NULL |                NULL | NULL                     | NULL            |
| localhost | mysql.session    | N           | N           | N           | N           | N           | N         | N           | Y             | N            | N         | N          | N               | N          | N          | N            | Y          | N                     | N                | N            | N               | N                | N                | N              | N                   | N                  | N                | N          | N            | N                      |          |            |             |              |             0 |           0 |               0 |                    0 | caching_sha2_password | $A$005$THISISACOMBINATIONOFINVALIDSALTANDPASSWORDTHATMUSTNEVERBRBEUSED | N                | 2019-11-19 05:46:52   |              NULL | Y              | N                | N              |                   NULL |                NULL | NULL                     | NULL            |
| localhost | mysql.sys        | N           | N           | N           | N           | N           | N         | N           | N             | N            | N         | N          | N               | N          | N          | N            | N          | N                     | N                | N            | N               | N                | N                | N              | N                   | N                  | N                | N          | N            | N                      |          |            |             |              |             0 |           0 |               0 |                    0 | caching_sha2_password | $A$005$THISISACOMBINATIONOFINVALIDSALTANDPASSWORDTHATMUSTNEVERBRBEUSED | N                | 2019-11-19 05:46:52   |              NULL | Y              | N                | N              |                   NULL |                NULL | NULL                     | NULL            |
| localhost | root             | Y           | Y           | Y           | Y           | Y           | Y         | Y           | Y             | Y            | Y         | Y          | Y               | Y          | Y          | Y            | Y          | Y                     | Y                | Y            | Y               | Y                | Y                | Y              | Y                   | Y                  | Y                | Y          | Y            | Y                      |          |            |             |              |             0 |           0 |               0 |                    0 | mysql_native_password | *E74858DB86EBA20BC33D0AECAE8A8108C56B17FA                              | N                | 2019-11-19 09:04:09   |              NULL | N              | Y                | Y              |                   NULL |                NULL | NULL                     | NULL            |
+-----------+------------------+-------------+-------------+-------------+-------------+-------------+-----------+-------------+---------------+--------------+-----------+------------+-----------------+------------+------------+--------------+------------+-----------------------+------------------+--------------+-----------------+------------------+------------------+----------------+---------------------+--------------------+------------------+------------+--------------+------------------------+----------+------------+-------------+--------------+---------------+-------------+-----------------+----------------------+-----------------------+------------------------------------------------------------------------+------------------+-----------------------+-------------------+----------------+------------------+----------------+------------------------+---------------------+--------------------------+-----------------+
5 rows in set (0.00 sec)

mysql> create user user4@'localhost' IDENTIFIED BY 'user4';
Query OK, 0 rows affected (0.01 sec)

mysql> select User, authentication_string from user;
+------------------+------------------------------------------------------------------------+
| User             | authentication_string                                                  |
+------------------+------------------------------------------------------------------------+
| user3            | *F357E78CABAD76FD3F1018EF85D78499B6ACC431                              |
| mysql.infoschema | $A$005$THISISACOMBINATIONOFINVALIDSALTANDPASSWORDTHATMUSTNEVERBRBEUSED |
| mysql.session    | $A$005$THISISACOMBINATIONOFINVALIDSALTANDPASSWORDTHATMUSTNEVERBRBEUSED |
| mysql.sys        | $A$005$THISISACOMBINATIONOFINVALIDSALTANDPASSWORDTHATMUSTNEVERBRBEUSED |
| root             | *E74858DB86EBA20BC33D0AECAE8A8108C56B17FA                              |
| user4            | *9246DFDBF8341B128B1B132A4626D3AFFEF03F0C                              |
+------------------+------------------------------------------------------------------------+
6 rows in set (0.00 sec)

mysql> create user 'user5'@'localhost' IDENTIFIED BY 'user5';
Query OK, 0 rows affected (0.01 sec)

mysql> select User, authentication_string from user;
+------------------+------------------------------------------------------------------------+
| User             | authentication_string                                                  |
+------------------+------------------------------------------------------------------------+
| user3            | *F357E78CABAD76FD3F1018EF85D78499B6ACC431                              |
| mysql.infoschema | $A$005$THISISACOMBINATIONOFINVALIDSALTANDPASSWORDTHATMUSTNEVERBRBEUSED |
| mysql.session    | $A$005$THISISACOMBINATIONOFINVALIDSALTANDPASSWORDTHATMUSTNEVERBRBEUSED |
| mysql.sys        | $A$005$THISISACOMBINATIONOFINVALIDSALTANDPASSWORDTHATMUSTNEVERBRBEUSED |
| root             | *E74858DB86EBA20BC33D0AECAE8A8108C56B17FA                              |
| user4            | *9246DFDBF8341B128B1B132A4626D3AFFEF03F0C                              |
| user5            | *EE0AEA25B21B2D11C36B82B27BF18794AAC3861E                              |
+------------------+------------------------------------------------------------------------+
7 rows in set (0.00 sec)

mysql> delete from user where User = 'user3'
    -> ;
Query OK, 1 row affected (0.01 sec)

mysql> select * from user;
+-----------+------------------+-------------+-------------+-------------+-------------+-------------+-----------+-------------+---------------+--------------+-----------+------------+-----------------+------------+------------+--------------+------------+-----------------------+------------------+--------------+-----------------+------------------+------------------+----------------+---------------------+--------------------+------------------+------------+--------------+------------------------+----------+------------+-------------+--------------+---------------+-------------+-----------------+----------------------+-----------------------+------------------------------------------------------------------------+------------------+-----------------------+-------------------+----------------+------------------+----------------+------------------------+---------------------+--------------------------+-----------------+
| Host      | User             | Select_priv | Insert_priv | Update_priv | Delete_priv | Create_priv | Drop_priv | Reload_priv | Shutdown_priv | Process_priv | File_priv | Grant_priv | References_priv | Index_priv | Alter_priv | Show_db_priv | Super_priv | Create_tmp_table_priv | Lock_tables_priv | Execute_priv | Repl_slave_priv | Repl_client_priv | Create_view_priv | Show_view_priv | Create_routine_priv | Alter_routine_priv | Create_user_priv | Event_priv | Trigger_priv | Create_tablespace_priv | ssl_type | ssl_cipher | x509_issuer | x509_subject | max_questions | max_updates | max_connections | max_user_connections | plugin                | authentication_string                                                  | password_expired | password_last_changed | password_lifetime | account_locked | Create_role_priv | Drop_role_priv | Password_reuse_history | Password_reuse_time | Password_require_current | User_attributes |
+-----------+------------------+-------------+-------------+-------------+-------------+-------------+-----------+-------------+---------------+--------------+-----------+------------+-----------------+------------+------------+--------------+------------+-----------------------+------------------+--------------+-----------------+------------------+------------------+----------------+---------------------+--------------------+------------------+------------+--------------+------------------------+----------+------------+-------------+--------------+---------------+-------------+-----------------+----------------------+-----------------------+------------------------------------------------------------------------+------------------+-----------------------+-------------------+----------------+------------------+----------------+------------------------+---------------------+--------------------------+-----------------+
| localhost | mysql.infoschema | Y           | N           | N           | N           | N           | N         | N           | N             | N            | N         | N          | N               | N          | N          | N            | N          | N                     | N                | N            | N               | N                | N                | N              | N                   | N                  | N                | N          | N            | N                      |          |            |             |              |             0 |           0 |               0 |                    0 | caching_sha2_password | $A$005$THISISACOMBINATIONOFINVALIDSALTANDPASSWORDTHATMUSTNEVERBRBEUSED | N                | 2019-11-19 05:46:52   |              NULL | Y              | N                | N              |                   NULL |                NULL | NULL                     | NULL            |
| localhost | mysql.session    | N           | N           | N           | N           | N           | N         | N           | Y             | N            | N         | N          | N               | N          | N          | N            | Y          | N                     | N                | N            | N               | N                | N                | N              | N                   | N                  | N                | N          | N            | N                      |          |            |             |              |             0 |           0 |               0 |                    0 | caching_sha2_password | $A$005$THISISACOMBINATIONOFINVALIDSALTANDPASSWORDTHATMUSTNEVERBRBEUSED | N                | 2019-11-19 05:46:52   |              NULL | Y              | N                | N              |                   NULL |                NULL | NULL                     | NULL            |
| localhost | mysql.sys        | N           | N           | N           | N           | N           | N         | N           | N             | N            | N         | N          | N               | N          | N          | N            | N          | N                     | N                | N            | N               | N                | N                | N              | N                   | N                  | N                | N          | N            | N                      |          |            |             |              |             0 |           0 |               0 |                    0 | caching_sha2_password | $A$005$THISISACOMBINATIONOFINVALIDSALTANDPASSWORDTHATMUSTNEVERBRBEUSED | N                | 2019-11-19 05:46:52   |              NULL | Y              | N                | N              |                   NULL |                NULL | NULL                     | NULL            |
| localhost | root             | Y           | Y           | Y           | Y           | Y           | Y         | Y           | Y             | Y            | Y         | Y          | Y               | Y          | Y          | Y            | Y          | Y                     | Y                | Y            | Y               | Y                | Y                | Y              | Y                   | Y                  | Y                | Y          | Y            | Y                      |          |            |             |              |             0 |           0 |               0 |                    0 | mysql_native_password | *E74858DB86EBA20BC33D0AECAE8A8108C56B17FA                              | N                | 2019-11-19 09:04:09   |              NULL | N              | Y                | Y              |                   NULL |                NULL | NULL                     | NULL            |
| localhost | user4            | N           | N           | N           | N           | N           | N         | N           | N             | N            | N         | N          | N               | N          | N          | N            | N          | N                     | N                | N            | N               | N                | N                | N              | N                   | N                  | N                | N          | N            | N                      |          |            |             |              |             0 |           0 |               0 |                    0 | mysql_native_password | *9246DFDBF8341B128B1B132A4626D3AFFEF03F0C                              | N                | 2020-02-05 15:48:24   |              NULL | N              | N                | N              |                   NULL |                NULL | NULL                     | NULL            |
| localhost | user5            | N           | N           | N           | N           | N           | N         | N           | N             | N            | N         | N          | N               | N          | N          | N            | N          | N                     | N                | N            | N               | N                | N                | N              | N                   | N                  | N                | N          | N            | N                      |          |            |             |              |             0 |           0 |               0 |                    0 | mysql_native_password | *EE0AEA25B21B2D11C36B82B27BF18794AAC3861E                              | N                | 2020-02-05 15:50:19   |              NULL | N              | N                | N              |                   NULL |                NULL | NULL                     | NULL            |
+-----------+------------------+-------------+-------------+-------------+-------------+-------------+-----------+-------------+---------------+--------------+-----------+------------+-----------------+------------+------------+--------------+------------+-----------------------+------------------+--------------+-----------------+------------------+------------------+----------------+---------------------+--------------------+------------------+------------+--------------+------------------------+----------+------------+-------------+--------------+---------------+-------------+-----------------+----------------------+-----------------------+------------------------------------------------------------------------+------------------+-----------------------+-------------------+----------------+------------------+----------------+------------------------+---------------------+--------------------------+-----------------+
6 rows in set (0.00 sec)

mysql> create user 'user3'@'localhost' IDENTIFIED BY 'user3';
Query OK, 0 rows affected (0.02 sec)

mysql> select * from user;
+-----------+------------------+-------------+-------------+-------------+-------------+-------------+-----------+-------------+---------------+--------------+-----------+------------+-----------------+------------+------------+--------------+------------+-----------------------+------------------+--------------+-----------------+------------------+------------------+----------------+---------------------+--------------------+------------------+------------+--------------+------------------------+----------+------------+-------------+--------------+---------------+-------------+-----------------+----------------------+-----------------------+------------------------------------------------------------------------+------------------+-----------------------+-------------------+----------------+------------------+----------------+------------------------+---------------------+--------------------------+-----------------+
| Host      | User             | Select_priv | Insert_priv | Update_priv | Delete_priv | Create_priv | Drop_priv | Reload_priv | Shutdown_priv | Process_priv | File_priv | Grant_priv | References_priv | Index_priv | Alter_priv | Show_db_priv | Super_priv | Create_tmp_table_priv | Lock_tables_priv | Execute_priv | Repl_slave_priv | Repl_client_priv | Create_view_priv | Show_view_priv | Create_routine_priv | Alter_routine_priv | Create_user_priv | Event_priv | Trigger_priv | Create_tablespace_priv | ssl_type | ssl_cipher | x509_issuer | x509_subject | max_questions | max_updates | max_connections | max_user_connections | plugin                | authentication_string                                                  | password_expired | password_last_changed | password_lifetime | account_locked | Create_role_priv | Drop_role_priv | Password_reuse_history | Password_reuse_time | Password_require_current | User_attributes |
+-----------+------------------+-------------+-------------+-------------+-------------+-------------+-----------+-------------+---------------+--------------+-----------+------------+-----------------+------------+------------+--------------+------------+-----------------------+------------------+--------------+-----------------+------------------+------------------+----------------+---------------------+--------------------+------------------+------------+--------------+------------------------+----------+------------+-------------+--------------+---------------+-------------+-----------------+----------------------+-----------------------+------------------------------------------------------------------------+------------------+-----------------------+-------------------+----------------+------------------+----------------+------------------------+---------------------+--------------------------+-----------------+
| localhost | mysql.infoschema | Y           | N           | N           | N           | N           | N         | N           | N             | N            | N         | N          | N               | N          | N          | N            | N          | N                     | N                | N            | N               | N                | N                | N              | N                   | N                  | N                | N          | N            | N                      |          |            |             |              |             0 |           0 |               0 |                    0 | caching_sha2_password | $A$005$THISISACOMBINATIONOFINVALIDSALTANDPASSWORDTHATMUSTNEVERBRBEUSED | N                | 2019-11-19 05:46:52   |              NULL | Y              | N                | N              |                   NULL |                NULL | NULL                     | NULL            |
| localhost | mysql.session    | N           | N           | N           | N           | N           | N         | N           | Y             | N            | N         | N          | N               | N          | N          | N            | Y          | N                     | N                | N            | N               | N                | N                | N              | N                   | N                  | N                | N          | N            | N                      |          |            |             |              |             0 |           0 |               0 |                    0 | caching_sha2_password | $A$005$THISISACOMBINATIONOFINVALIDSALTANDPASSWORDTHATMUSTNEVERBRBEUSED | N                | 2019-11-19 05:46:52   |              NULL | Y              | N                | N              |                   NULL |                NULL | NULL                     | NULL            |
| localhost | mysql.sys        | N           | N           | N           | N           | N           | N         | N           | N             | N            | N         | N          | N               | N          | N          | N            | N          | N                     | N                | N            | N               | N                | N                | N              | N                   | N                  | N                | N          | N            | N                      |          |            |             |              |             0 |           0 |               0 |                    0 | caching_sha2_password | $A$005$THISISACOMBINATIONOFINVALIDSALTANDPASSWORDTHATMUSTNEVERBRBEUSED | N                | 2019-11-19 05:46:52   |              NULL | Y              | N                | N              |                   NULL |                NULL | NULL                     | NULL            |
| localhost | root             | Y           | Y           | Y           | Y           | Y           | Y         | Y           | Y             | Y            | Y         | Y          | Y               | Y          | Y          | Y            | Y          | Y                     | Y                | Y            | Y               | Y                | Y                | Y              | Y                   | Y                  | Y                | Y          | Y            | Y                      |          |            |             |              |             0 |           0 |               0 |                    0 | mysql_native_password | *E74858DB86EBA20BC33D0AECAE8A8108C56B17FA                              | N                | 2019-11-19 09:04:09   |              NULL | N              | Y                | Y              |                   NULL |                NULL | NULL                     | NULL            |
| localhost | user3            | N           | N           | N           | N           | N           | N         | N           | N             | N            | N         | N          | N               | N          | N          | N            | N          | N                     | N                | N            | N               | N                | N                | N              | N                   | N                  | N                | N          | N            | N                      |          |            |             |              |             0 |           0 |               0 |                    0 | mysql_native_password | *4570676E59FAC04669A75B74C31338296F688A44                              | N                | 2020-02-05 15:54:24   |              NULL | N              | N                | N              |                   NULL |                NULL | NULL                     | NULL            |
| localhost | user4            | N           | N           | N           | N           | N           | N         | N           | N             | N            | N         | N          | N               | N          | N          | N            | N          | N                     | N                | N            | N               | N                | N                | N              | N                   | N                  | N                | N          | N            | N                      |          |            |             |              |             0 |           0 |               0 |                    0 | mysql_native_password | *9246DFDBF8341B128B1B132A4626D3AFFEF03F0C                              | N                | 2020-02-05 15:48:24   |              NULL | N              | N                | N              |                   NULL |                NULL | NULL                     | NULL            |
| localhost | user5            | N           | N           | N           | N           | N           | N         | N           | N             | N            | N         | N          | N               | N          | N          | N            | N          | N                     | N                | N            | N               | N                | N                | N              | N                   | N                  | N                | N          | N            | N                      |          |            |             |              |             0 |           0 |               0 |                    0 | mysql_native_password | *EE0AEA25B21B2D11C36B82B27BF18794AAC3861E                              | N                | 2020-02-05 15:50:19   |              NULL | N              | N                | N              |                   NULL |                NULL | NULL                     | NULL            |
+-----------+------------------+-------------+-------------+-------------+-------------+-------------+-----------+-------------+---------------+--------------+-----------+------------+-----------------+------------+------------+--------------+------------+-----------------------+------------------+--------------+-----------------+------------------+------------------+----------------+---------------------+--------------------+------------------+------------+--------------+------------------------+----------+------------+-------------+--------------+---------------+-------------+-----------------+----------------------+-----------------------+------------------------------------------------------------------------+------------------+-----------------------+-------------------+----------------+------------------+----------------+------------------------+---------------------+--------------------------+-----------------+
7 rows in set (0.00 sec)

mysql> select version ();
+------------+
| version () |
+------------+
| 8.0.18     |
+------------+
1 row in set (0.00 sec)

mysql> show grants;
+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Grants for root@localhost                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, RELOAD, SHUTDOWN, PROCESS, FILE, REFERENCES, INDEX, ALTER, SHOW DATABASES, SUPER, CREATE TEMPORARY TABLES, LOCK TABLES, EXECUTE, REPLICATION SLAVE, REPLICATION CLIENT, CREATE VIEW, SHOW VIEW, CREATE ROUTINE, ALTER ROUTINE, CREATE USER, EVENT, TRIGGER, CREATE TABLESPACE, CREATE ROLE, DROP ROLE ON *.* TO `root`@`localhost` WITH GRANT OPTION                                                                                                           |
| GRANT APPLICATION_PASSWORD_ADMIN,AUDIT_ADMIN,BACKUP_ADMIN,BINLOG_ADMIN,BINLOG_ENCRYPTION_ADMIN,CLONE_ADMIN,CONNECTION_ADMIN,ENCRYPTION_KEY_ADMIN,GROUP_REPLICATION_ADMIN,INNODB_REDO_LOG_ARCHIVE,PERSIST_RO_VARIABLES_ADMIN,REPLICATION_APPLIER,REPLICATION_SLAVE_ADMIN,RESOURCE_GROUP_ADMIN,RESOURCE_GROUP_USER,ROLE_ADMIN,SERVICE_CONNECTION_ADMIN,SESSION_VARIABLES_ADMIN,SET_USER_ID,SYSTEM_USER,SYSTEM_VARIABLES_ADMIN,TABLE_ENCRYPTION_ADMIN,XA_RECOVER_ADMIN ON *.* TO `root`@`localhost` WITH GRANT OPTION |
| GRANT PROXY ON ''@'' TO 'root'@'localhost' WITH GRANT OPTION                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
3 rows in set (0.01 sec)

mysql> show grant for user4@'localhost';
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'grant for user4@'localhost'' at line 1
mysql> show grant for 'user4'@'localhost';
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'grant for 'user4'@'localhost'' at line 1
mysql> show grants for user4@'localhost';
+-------------------------------------------+
| Grants for user4@localhost                |
+-------------------------------------------+
| GRANT USAGE ON *.* TO `user4`@`localhost` |
+-------------------------------------------+
1 row in set (0.00 sec)

mysql> grant select, update, delete
    -> on ooa19a.company
    -> to user3@'localhost';
Query OK, 0 rows affected (0.01 sec)

mysql> show warnings;
Empty set (0.00 sec)

mysql> flush privileges;
Query OK, 0 rows affected (0.01 sec)

mysql> exit
mysql> notee
mysql> notee
