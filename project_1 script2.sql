mysql> decribe court;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'decribe court' at line 1
mysql> describe court;
+------------+--------------------------+------+-----+---------+----------------+
| Field      | Type                     | Null | Key | Default | Extra          |
+------------+--------------------------+------+-----+---------+----------------+
| crt_id     | tinyint(3) unsigned      | NO   | PRI | NULL    | auto_increment |
| crt_name   | varchar(40)              | NO   |     | NULL    |                |
| crt_street | varchar(30)              | NO   |     | NULL    |                |
| crt_city   | varchar(30)              | NO   |     | NULL    |                |
| crt_state  | char(2)                  | NO   |     | NULL    |                |
| crt_zip    | int(9) unsigned zerofill | YES  |     | NULL    |                |
| crt_phone  | bigint(20)               | NO   |     | NULL    |                |
| crt_email  | varchar(100)             | NO   |     | NULL    |                |
| crt_url    | varchar(100)             | NO   |     | NULL    |                |
| crt_notes  | varchar(255)             | YES  |     | NULL    |                |
+------------+--------------------------+------+-----+---------+----------------+
10 rows in set (0.00 sec)

mysql> INSERT INTO `ooa19a`.`court` (`crt_id`, `crt_name`, `crt_street`, `crt_city`, `crt_state`, `crt_zip`, `crt_phone`, `crt_email`, `crt_url`, `crt_notes`) VALUES ('', 'Leon County', 'West Pole', 'Tallahassee', 'Florida', '32301', '3423243', 'leon@county.com', 'www.leoncounty.com', 'court 1');INSERT INTO `ooa19a`.`court` (`crt_id`, `crt_name`, `crt_street`, `crt_city`, `crt_state`, `crt_zip`, `crt_phone`, `crt_email`, `crt_url`, `crt_notes`) VALUES ('', 'Supreme court', 'South Pole', 'Tallahassee', 'Florida', '32304', '45556553', 'supreme@court.com', 'www.supremecourt.com', 'court 2');INSERT INTO `ooa19a`.`court` (`crt_id`, `crt_name`, `crt_street`, `crt_city`, `crt_state`, `crt_zip`, `crt_phone`, `crt_email`, `crt_url`, `crt_notes`) VALUES ('', 'High court', 'East pole', 'Tallahasse', 'Florida', '32342', '34540124', 'high@court.com', 'www.highcourt.com', 'court 3');INSERT INTO `ooa19a`.`court` (`crt_id`, `crt_name`, `crt_street`, `crt_city`, `crt_state`, `crt_zip`, `crt_phone`, `crt_email`, `crt_url`, `crt_notes`) VALUES ('', 'Tribunal', 'North Pole', 'Tallahassee', 'Florida', '32345', '67892422', 'Tribunal@court.com', 'www.tribunal.com', 'court 4');INSERT INTO `ooa19a`.`court` (`crt_name`, `crt_street`, `crt_city`, `crt_state`, `crt_zip`, `crt_phone`, `crt_email`, `crt_url`, `crt_notes`) VALUES ('Snowden', 'Orange', 'Orlando', 'Florida', '30922', '24364642', 'snowden@court.com', 'www.snowden.com', 'court 5'); show warnings;
ERROR 1366 (HY000): Incorrect integer value: '' for column 'crt_id' at row 1
ERROR 1366 (HY000): Incorrect integer value: '' for column 'crt_id' at row 1
ERROR 1366 (HY000): Incorrect integer value: '' for column 'crt_id' at row 1
ERROR 1366 (HY000): Incorrect integer value: '' for column 'crt_id' at row 1
ERROR 1406 (22001): Data too long for column 'crt_state' at row 1
+-------+------+-----------------------------------------------+
| Level | Code | Message                                       |
+-------+------+-----------------------------------------------+
| Error | 1406 | Data too long for column 'crt_state' at row 1 |
+-------+------+-----------------------------------------------+
1 row in set (0.00 sec)

mysql> INSERT INTO `ooa19a`.`court` (`crt_id`, `crt_name`, `crt_street`, `crt_city`, `crt_state`, `crt_zip`, `crt_phone`, `crt_email`, `crt_url`, `crt_notes`) VALUES ('1', 'Leon County', 'West Pole', 'Tallahassee', 'FL', '32301', '3423243', 'leon@county.com', 'www.leoncounty.com', 'court 1');INSERT INTO `ooa19a`.`court` (`crt_id`, `crt_name`, `crt_street`, `crt_city`, `crt_state`, `crt_zip`, `crt_phone`, `crt_email`, `crt_url`, `crt_notes`) VALUES ('2', 'Supreme court', 'South Pole', 'Tallahassee', 'FL', '32304', '45556553', 'supreme@court.com', 'www.supremecourt.com', 'court 2');INSERT INTO `ooa19a`.`court` (`crt_id`, `crt_name`, `crt_street`, `crt_city`, `crt_state`, `crt_zip`, `crt_phone`, `crt_email`, `crt_url`, `crt_notes`) VALUES ('3', 'High court', 'East pole', 'Tallahasse', 'FL', '32342', '34540124', 'high@court.com', 'www.highcourt.com', 'court 3');INSERT INTO `ooa19a`.`court` (`crt_id`, `crt_name`, `crt_street`, `crt_city`, `crt_state`, `crt_zip`, `crt_phone`, `crt_email`, `crt_url`, `crt_notes`) VALUES ('4', 'Tribunal', 'North Pole', 'Tallahassee', 'FL', '32345', '67892422', 'Tribunal@court.com', 'www.tribunal.com', 'court 4');INSERT INTO `ooa19a`.`court` (`crt_id`, `crt_name`, `crt_street`, `crt_city`, `crt_state`, `crt_zip`, `crt_phone`, `crt_email`, `crt_url`, `crt_notes`) VALUES ('5', 'Snowden', 'Orange', 'Orlando', 'FL', '30922', '24364642', 'snowden@court.com', 'www.snowden.com', 'court 5'); show warnings;
Query OK, 1 row affected (0.01 sec)

Query OK, 1 row affected (0.01 sec)

Query OK, 1 row affected (0.00 sec)

Query OK, 1 row affected (0.00 sec)

Query OK, 1 row affected (0.00 sec)

Empty set (0.00 sec)

mysql> select * from court;
+--------+---------------+------------+-------------+-----------+-----------+-----------+--------------------+----------------------+-----------+
| crt_id | crt_name      | crt_street | crt_city    | crt_state | crt_zip   | crt_phone | crt_email          | crt_url              | crt_notes |
+--------+---------------+------------+-------------+-----------+-----------+-----------+--------------------+----------------------+-----------+
|      1 | Leon County   | West Pole  | Tallahassee | FL        | 000032301 |   3423243 | leon@county.com    | www.leoncounty.com   | court 1   |
|      2 | Supreme court | South Pole | Tallahassee | FL        | 000032304 |  45556553 | supreme@court.com  | www.supremecourt.com | court 2   |
|      3 | High court    | East pole  | Tallahasse  | FL        | 000032342 |  34540124 | high@court.com     | www.highcourt.com    | court 3   |
|      4 | Tribunal      | North Pole | Tallahassee | FL        | 000032345 |  67892422 | Tribunal@court.com | www.tribunal.com     | court 4   |
|      5 | Snowden       | Orange     | Orlando     | FL        | 000030922 |  24364642 | snowden@court.com  | www.snowden.com      | court 5   |
+--------+---------------+------------+-------------+-----------+-----------+-----------+--------------------+----------------------+-----------+
5 rows in set (0.00 sec)

mysql> update court 
    -> set 
    -> crt_phone = 34242469
    -> where crt_id = 1;
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> select * from court;
+--------+---------------+------------+-------------+-----------+-----------+-----------+--------------------+----------------------+-----------+
| crt_id | crt_name      | crt_street | crt_city    | crt_state | crt_zip   | crt_phone | crt_email          | crt_url              | crt_notes |
+--------+---------------+------------+-------------+-----------+-----------+-----------+--------------------+----------------------+-----------+
|      1 | Leon County   | West Pole  | Tallahassee | FL        | 000032301 |  34242469 | leon@county.com    | www.leoncounty.com   | court 1   |
|      2 | Supreme court | South Pole | Tallahassee | FL        | 000032304 |  45556553 | supreme@court.com  | www.supremecourt.com | court 2   |
|      3 | High court    | East pole  | Tallahasse  | FL        | 000032342 |  34540124 | high@court.com     | www.highcourt.com    | court 3   |
|      4 | Tribunal      | North Pole | Tallahassee | FL        | 000032345 |  67892422 | Tribunal@court.com | www.tribunal.com     | court 4   |
|      5 | Snowden       | Orange     | Orlando     | FL        | 000030922 |  24364642 | snowden@court.com  | www.snowden.com      | court 5   |
+--------+---------------+------------+-------------+-----------+-----------+-----------+--------------------+----------------------+-----------+
5 rows in set (0.00 sec)

mysql> describe attorney;
+-----------------------+-----------------------+------+-----+---------+-------+
| Field                 | Type                  | Null | Key | Default | Extra |
+-----------------------+-----------------------+------+-----+---------+-------+
| per_id                | smallint(5) unsigned  | NO   | PRI | NULL    |       |
| aty_start_date        | date                  | NO   |     | NULL    |       |
| aty_end_date          | date                  | YES  |     | NULL    |       |
| aty_hourly_rate       | decimal(5,2) unsigned | NO   |     | NULL    |       |
| aty_years_in_practice | tinyint(4)            | NO   |     | NULL    |       |
| aty_notes             | varchar(255)          | YES  |     | NULL    |       |
+-----------------------+-----------------------+------+-----+---------+-------+
6 rows in set (0.00 sec)

mysql> INSERT INTO `ooa19a`.`judge` (`per_id`, `crt_id`, `jud_salary`, `jud_years_in_practice`, `jud_notes`) VALUES ('11', '1', '150000', '12', 'Judge 1');INSERT INTO `ooa19a`.`judge` (`per_id`, `crt_id`, `jud_salary`, `jud_years_in_practice`, `jud_notes`) VALUES ('12', '2', '140000', '10', 'Judge 2');INSERT INTO `ooa19a`.`judge` (`per_id`, `crt_id`, `jud_salary`, `jud_years_in_practice`, `jud_notes`) VALUES ('13', '3', '100000', '15', 'Judge 3');INSERT INTO `ooa19a`.`judge` (`per_id`, `crt_id`, `jud_salary`, `jud_years_in_practice`, `jud_notes`) VALUES ('14', '4', '95000', '12', 'Judge 4');INSERT INTO `ooa19a`.`judge` (`per_id`, `crt_id`, `jud_salary`, `jud_years_in_practice`, `jud_notes`) VALUES ('15', '5', '120000', '11', 'Judge 5'); show warnings;
Query OK, 1 row affected (0.01 sec)

Query OK, 1 row affected (0.00 sec)

Query OK, 1 row affected (0.00 sec)

Query OK, 1 row affected (0.00 sec)

Query OK, 1 row affected (0.00 sec)

Empty set (0.00 sec)

mysql> selecr * from attorney;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'selecr * from attorney' at line 1
mysql> select * from attorney;
+--------+----------------+--------------+-----------------+-----------------------+------------+
| per_id | aty_start_date | aty_end_date | aty_hourly_rate | aty_years_in_practice | aty_notes  |
+--------+----------------+--------------+-----------------+-----------------------+------------+
|      6 | 1995-02-03     | 2019-01-12   |           40.00 |                    24 | attorney 1 |
|      7 | 1980-06-05     | 2018-09-12   |           45.00 |                    38 | attorney 2 |
|      8 | 1975-05-04     | 2017-12-01   |           39.00 |                    42 | attorney 3 |
|      9 | 1970-09-08     | 2012-12-03   |           45.00 |                    42 | attorney 4 |
|     10 | 2000-08-12     | NULL         |           50.00 |                    20 | attorney 5 |
+--------+----------------+--------------+-----------------+-----------------------+------------+
5 rows in set (0.00 sec)

mysql> select * from person;
+--------+---------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
| per_id | per_ssn | per_fname | per_lname  | per_street             | per_city    | per_state | per_zip    | per_email               | per_dob    | per_type | per_notes |
+--------+---------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
|      1 | NULL    | Steve     | Rogers     | 437 Southern Drive     | Rochester   | NY        |  032443022 | srogers@comcast.net     | 1923-10-03 | c        | NULL      |
|      2 | NULL    | Bruce     | Wayne      | 1007 Mountain Drive    | Gotham      | NY        |  003243440 | bwayne@knology.net      | 1968-03-20 | c        | NULL      |
|      3 | NULL    | Peter     | Parker     | 20 Ingram Street       | New York    | NY        |  102862341 | pparker@msn.net         | 1923-10-03 | c        | NULL      |
|      4 | NULL    | Jane      | Thompson   | 13563 Ocean View Drive | Seattle     | WA        |  032084409 | jthompson@gmail.net     | 1973-05-08 | c        | NULL      |
|      5 | NULL    | Debra     | Steele     | 543 Oak Ln             | Milwaukee   | WI        | 2862374478 | dsteele@verizon.net     | 1994-07-19 | c        | NULL      |
|      6 | NULL    | Tony      | Stark      | 332 Palm Avenue        | Malibu      | CA        |  902638832 | tstark@yahoo.com        | 1972-05-04 | a        | NULL      |
|      7 | NULL    | Hank      | Pymi       | 2355 Brown Street      | Cleveland   | OH        |  022342390 | hpym@aol.net            | 1980-08-28 | a        | NULL      |
|      8 | NULL    | Bob       | Best       | 4902 Avendale Ave.     | Scottsdale  | AZ        |  087263912 | bbest@yahoo.com         | 1992-02-10 | a        | NULL      |
|      9 | NULL    | Sandra    | Dole       | 8732 Lawrence Ave.     | Atlanta     | GA        |  002345390 | sdole@gmail.net         | 1990-01-26 | a        | NULL      |
|     10 | NULL    | Ben       | Avery      | 6432 Thunderbird Ln    | Sioux Falls | SD        |  005637932 | bavery@hotmail.net      | 1983-12-24 | a        | NULL      |
|     11 | NULL    | Arthur    | Curry      | 3302 Euclid Avenue     | Miami       | FL        |  000219932 | acurry@gmail.com        | 1975-12-15 | j        | NULL      |
|     12 | NULL    | Diana     | Price      | 944 Green Street       | Las vegas   | NV        |  033235223 | dprice@aol.net          | 1980-08-22 | j        | NULL      |
|     13 | NULL    | Adam      | Jurris     | 98435 Valencia Dr.     | Gulf Shore  | AL        |  870639912 | ajurris@yahoo.com       | 1995-01-31 | j        | NULL      |
|     14 | NULL    | Judy      | Sleen      | 56343 Rover Ct.        | Billings    | MT        |  067204823 | jsleen@symaptico.com    | 1995-01-31 | j        | NULL      |
|     15 | NULL    | Bill      | Neiderheim | 43567 Netherland Blvd  | South Bend  | IN        |  320219932 | bneiderheim@comcast.net | 1982-03-24 | j        | NULL      |
+--------+---------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
15 rows in set (0.00 sec)

mysql> select per_fname, per_name as Name;
ERROR 1054 (42S22): Unknown column 'per_fname' in 'field list'
mysql> select per_fname, per_name as Name from person;
ERROR 1054 (42S22): Unknown column 'per_name' in 'field list'
mysql> select per_fname, per_lname as Name from person;
+-----------+------------+
| per_fname | Name       |
+-----------+------------+
| Steve     | Rogers     |
| Bruce     | Wayne      |
| Peter     | Parker     |
| Jane      | Thompson   |
| Debra     | Steele     |
| Tony      | Stark      |
| Hank      | Pymi       |
| Bob       | Best       |
| Sandra    | Dole       |
| Ben       | Avery      |
| Arthur    | Curry      |
| Diana     | Price      |
| Adam      | Jurris     |
| Judy      | Sleen      |
| Bill      | Neiderheim |
+-----------+------------+
15 rows in set (0.00 sec)

mysql> select (concat(per_lname, ',' per_fname), per_type from person;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'from person' at line 1
mysql> select (concat(per_lname, ',' per_fname)), per_type from person;
ERROR 1583 (42000): Incorrect parameters in the call to native function 'concat'
mysql> select concat(per_lname, ',' per_fname), per_type from person;
ERROR 1583 (42000): Incorrect parameters in the call to native function 'concat'
mysql> select concat(per_lname, ',', per_fname), per_type from person;
+-----------------------------------+----------+
| concat(per_lname, ',', per_fname) | per_type |
+-----------------------------------+----------+
| Rogers,Steve                      | c        |
| Wayne,Bruce                       | c        |
| Parker,Peter                      | c        |
| Thompson,Jane                     | c        |
| Steele,Debra                      | c        |
| Stark,Tony                        | a        |
| Pymi,Hank                         | a        |
| Best,Bob                          | a        |
| Dole,Sandra                       | a        |
| Avery,Ben                         | a        |
| Curry,Arthur                      | j        |
| Price,Diana                       | j        |
| Jurris,Adam                       | j        |
| Sleen,Judy                        | j        |
| Neiderheim,Bill                   | j        |
+-----------------------------------+----------+
15 rows in set (0.00 sec)

mysql> select concat(per_lname, ', ', per_fname), per_type from person;
+------------------------------------+----------+
| concat(per_lname, ', ', per_fname) | per_type |
+------------------------------------+----------+
| Rogers, Steve                      | c        |
| Wayne, Bruce                       | c        |
| Parker, Peter                      | c        |
| Thompson, Jane                     | c        |
| Steele, Debra                      | c        |
| Stark, Tony                        | a        |
| Pymi, Hank                         | a        |
| Best, Bob                          | a        |
| Dole, Sandra                       | a        |
| Avery, Ben                         | a        |
| Curry, Arthur                      | j        |
| Price, Diana                       | j        |
| Jurris, Adam                       | j        |
| Sleen, Judy                        | j        |
| Neiderheim, Bill                   | j        |
+------------------------------------+----------+
15 rows in set (0.00 sec)

mysql> select concat(per_lname, ', ', per_fname) as Name, per_type from person;
+------------------+----------+
| Name             | per_type |
+------------------+----------+
| Rogers, Steve    | c        |
| Wayne, Bruce     | c        |
| Parker, Peter    | c        |
| Thompson, Jane   | c        |
| Steele, Debra    | c        |
| Stark, Tony      | a        |
| Pymi, Hank       | a        |
| Best, Bob        | a        |
| Dole, Sandra     | a        |
| Avery, Ben       | a        |
| Curry, Arthur    | j        |
| Price, Diana     | j        |
| Jurris, Adam     | j        |
| Sleen, Judy      | j        |
| Neiderheim, Bill | j        |
+------------------+----------+
15 rows in set (0.00 sec)

mysql> select count(*) from person;
+----------+
| count(*) |
+----------+
|       15 |
+----------+
1 row in set (0.01 sec)

mysql> select distinct count(*) from person;
+----------+
| count(*) |
+----------+
|       15 |
+----------+
1 row in set (0.00 sec)

mysql> select distinct count(per_type) from person ;
+-----------------+
| count(per_type) |
+-----------------+
|              15 |
+-----------------+
1 row in set (0.00 sec)

mysql> select distinct count(distinct per_type) from person ;
+--------------------------+
| count(distinct per_type) |
+--------------------------+
|                        3 |
+--------------------------+
1 row in set (0.01 sec)

mysql> select count(distinct per_type) from person ;
+--------------------------+
| count(distinct per_type) |
+--------------------------+
|                        3 |
+--------------------------+
1 row in set (0.00 sec)

mysql> alter table person 
    -> add per_sslt binary(64) null comment 'only for demo purposes - do not use the word SALT in the naming!'
    -> after per_ssn;
Query OK, 0 rows affected (0.18 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> select * from person;
+--------+---------+----------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
| per_id | per_ssn | per_sslt | per_fname | per_lname  | per_street             | per_city    | per_state | per_zip    | per_email               | per_dob    | per_type | per_notes |
+--------+---------+----------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
|      1 | NULL    | NULL     | Steve     | Rogers     | 437 Southern Drive     | Rochester   | NY        |  032443022 | srogers@comcast.net     | 1923-10-03 | c        | NULL      |
|      2 | NULL    | NULL     | Bruce     | Wayne      | 1007 Mountain Drive    | Gotham      | NY        |  003243440 | bwayne@knology.net      | 1968-03-20 | c        | NULL      |
|      3 | NULL    | NULL     | Peter     | Parker     | 20 Ingram Street       | New York    | NY        |  102862341 | pparker@msn.net         | 1923-10-03 | c        | NULL      |
|      4 | NULL    | NULL     | Jane      | Thompson   | 13563 Ocean View Drive | Seattle     | WA        |  032084409 | jthompson@gmail.net     | 1973-05-08 | c        | NULL      |
|      5 | NULL    | NULL     | Debra     | Steele     | 543 Oak Ln             | Milwaukee   | WI        | 2862374478 | dsteele@verizon.net     | 1994-07-19 | c        | NULL      |
|      6 | NULL    | NULL     | Tony      | Stark      | 332 Palm Avenue        | Malibu      | CA        |  902638832 | tstark@yahoo.com        | 1972-05-04 | a        | NULL      |
|      7 | NULL    | NULL     | Hank      | Pymi       | 2355 Brown Street      | Cleveland   | OH        |  022342390 | hpym@aol.net            | 1980-08-28 | a        | NULL      |
|      8 | NULL    | NULL     | Bob       | Best       | 4902 Avendale Ave.     | Scottsdale  | AZ        |  087263912 | bbest@yahoo.com         | 1992-02-10 | a        | NULL      |
|      9 | NULL    | NULL     | Sandra    | Dole       | 8732 Lawrence Ave.     | Atlanta     | GA        |  002345390 | sdole@gmail.net         | 1990-01-26 | a        | NULL      |
|     10 | NULL    | NULL     | Ben       | Avery      | 6432 Thunderbird Ln    | Sioux Falls | SD        |  005637932 | bavery@hotmail.net      | 1983-12-24 | a        | NULL      |
|     11 | NULL    | NULL     | Arthur    | Curry      | 3302 Euclid Avenue     | Miami       | FL        |  000219932 | acurry@gmail.com        | 1975-12-15 | j        | NULL      |
|     12 | NULL    | NULL     | Diana     | Price      | 944 Green Street       | Las vegas   | NV        |  033235223 | dprice@aol.net          | 1980-08-22 | j        | NULL      |
|     13 | NULL    | NULL     | Adam      | Jurris     | 98435 Valencia Dr.     | Gulf Shore  | AL        |  870639912 | ajurris@yahoo.com       | 1995-01-31 | j        | NULL      |
|     14 | NULL    | NULL     | Judy      | Sleen      | 56343 Rover Ct.        | Billings    | MT        |  067204823 | jsleen@symaptico.com    | 1995-01-31 | j        | NULL      |
|     15 | NULL    | NULL     | Bill      | Neiderheim | 43567 Netherland Blvd  | South Bend  | IN        |  320219932 | bneiderheim@comcast.net | 1982-03-24 | j        | NULL      |
+--------+---------+----------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
15 rows in set (0.00 sec)

mysql> drop procedure if exists CreatePersonSSN;
Query OK, 0 rows affected, 1 warning (0.02 sec)

mysql> show warnings;
+-------+------+-------------------------------------------------+
| Level | Code | Message                                         |
+-------+------+-------------------------------------------------+
| Note  | 1305 | PROCEDURE ooa19a.CreatePersonSSN does not exist |
+-------+------+-------------------------------------------------+
1 row in set (0.00 sec)

mysql> COMMENT 'Testing comments'
    -> ;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'COMMENT 'Testing comments'' at line 1
mysql> drop procedure if exists CreatePersonSSN;delimiter $$create procedure CreatePersonSSN()BEGINCOMMENT '--DECLARE VARIABLE IN MSQL' DECLARE x, y INT; SET X = 1;  COMMENT 'DYNAMICALLY SET LOOP ENDING VALUE (TOTAL NUMBER OF PERSONS)'select count(*) into y from person comment 'count the total number of records on the table and put them in Y'while x <= y doset @salt = random_bytes(64); set @ran_num = floor(rand() * (999999999 - 111111111 + 1)) + 111111111; set @ssn = unhex(sha2(concat(@salt, @ran_num), 512 ));update personset per_ssn = @ssn, per_sslt = @saltwhere per_id =x;set x = x + 1;end while;end $$Delimiter ;call createPersonSSN();
Query OK, 0 rows affected, 1 warning (0.01 sec)

    -> drop procedure if exists CreatePersonSSN;delimiter $$create procedure CreatePersonSSN()BEGINCOMMENT '--DECLARE VARIABLE IN MSQL' DECLARE x, y INT; SET X = 1;  COMMENT 'DYNAMICALLY SET LOOP ENDING VALUE (TOTAL NUMBER OF PERSONS)'select count(*) into y from person comment 'count the total number of records on the table and put them in Y'while x <= y doset @salt = random_bytes(64); set @ran_num = floor(rand() * (999999999 - 111111111 + 1)) + 111111111; set @ssn = unhex(sha2(concat(@salt, @ran_num), 512 ));update personset per_ssn = @ssn, per_sslt = @saltwhere per_id =x;set x = x + 1;end while;end $$Delimiter ;call createPersonSSN();
ERROR 1193 (HY000): Unknown system variable 'X'
    -> ;
    -> exit
    -> select * from person;
    -> 
    -> 
    -> 
    -> xxXq!
    -> q!
    -> bye
    -> ;
    -> \c
mysql> select * from person;
    -> \c
mysql> drop procedure if exists CreatePersonSSN;
    -> \c
mysql> \c
mysql> use ooa19a;
ERROR 1049 (42000): Unknown database 'ooa19a;'
mysql> use ooa19a
Database changed
mysql> select * from person;
    -> \c
mysql> describe person;
    -> \c
mysql> \c
mysql> select * from `ooa19a`.`person`;
    -> \c
mysql> select * from `ooa19a`.`client`;
    -> \c
mysql> exit
mysql> select * from `ooa19a`.`person`;
+--------+---------+----------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
| per_id | per_ssn | per_sslt | per_fname | per_lname  | per_street             | per_city    | per_state | per_zip    | per_email               | per_dob    | per_type | per_notes |
+--------+---------+----------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
|      1 | NULL    | NULL     | Steve     | Rogers     | 437 Southern Drive     | Rochester   | NY        |  032443022 | srogers@comcast.net     | 1923-10-03 | c        | NULL      |
|      2 | NULL    | NULL     | Bruce     | Wayne      | 1007 Mountain Drive    | Gotham      | NY        |  003243440 | bwayne@knology.net      | 1968-03-20 | c        | NULL      |
|      3 | NULL    | NULL     | Peter     | Parker     | 20 Ingram Street       | New York    | NY        |  102862341 | pparker@msn.net         | 1923-10-03 | c        | NULL      |
|      4 | NULL    | NULL     | Jane      | Thompson   | 13563 Ocean View Drive | Seattle     | WA        |  032084409 | jthompson@gmail.net     | 1973-05-08 | c        | NULL      |
|      5 | NULL    | NULL     | Debra     | Steele     | 543 Oak Ln             | Milwaukee   | WI        | 2862374478 | dsteele@verizon.net     | 1994-07-19 | c        | NULL      |
|      6 | NULL    | NULL     | Tony      | Stark      | 332 Palm Avenue        | Malibu      | CA        |  902638832 | tstark@yahoo.com        | 1972-05-04 | a        | NULL      |
|      7 | NULL    | NULL     | Hank      | Pymi       | 2355 Brown Street      | Cleveland   | OH        |  022342390 | hpym@aol.net            | 1980-08-28 | a        | NULL      |
|      8 | NULL    | NULL     | Bob       | Best       | 4902 Avendale Ave.     | Scottsdale  | AZ        |  087263912 | bbest@yahoo.com         | 1992-02-10 | a        | NULL      |
|      9 | NULL    | NULL     | Sandra    | Dole       | 8732 Lawrence Ave.     | Atlanta     | GA        |  002345390 | sdole@gmail.net         | 1990-01-26 | a        | NULL      |
|     10 | NULL    | NULL     | Ben       | Avery      | 6432 Thunderbird Ln    | Sioux Falls | SD        |  005637932 | bavery@hotmail.net      | 1983-12-24 | a        | NULL      |
|     11 | NULL    | NULL     | Arthur    | Curry      | 3302 Euclid Avenue     | Miami       | FL        |  000219932 | acurry@gmail.com        | 1975-12-15 | j        | NULL      |
|     12 | NULL    | NULL     | Diana     | Price      | 944 Green Street       | Las vegas   | NV        |  033235223 | dprice@aol.net          | 1980-08-22 | j        | NULL      |
|     13 | NULL    | NULL     | Adam      | Jurris     | 98435 Valencia Dr.     | Gulf Shore  | AL        |  870639912 | ajurris@yahoo.com       | 1995-01-31 | j        | NULL      |
|     14 | NULL    | NULL     | Judy      | Sleen      | 56343 Rover Ct.        | Billings    | MT        |  067204823 | jsleen@symaptico.com    | 1995-01-31 | j        | NULL      |
|     15 | NULL    | NULL     | Bill      | Neiderheim | 43567 Netherland Blvd  | South Bend  | IN        |  320219932 | bneiderheim@comcast.net | 1982-03-24 | j        | NULL      |
+--------+---------+----------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
15 rows in set (0.00 sec)

mysql> drop procedure if exists CreatePersonSSN;delimiter $$create procedure CreatePersonSSN()BEGINCOMMENT '--DECLARE VARIABLE IN MSQL' DECLARE x, y INT; SET X = 1;  COMMENT 'DYNAMICALLY SET LOOP ENDING VALUE (TOTAL NUMBER OF PERSONS)'select count(*) into y from person comment 'count the total number of records on the table and put them in Y'while x <= y docomment 'give each person a unique randomized salt, and hashed salted randomized ssn'comment 'this demo is only for showing how to include salted and hashed randomized values for testing purposes!'set @salt = random_bytes(64); set @ran_num = floor(rand() * (999999999 - 111111111 + 1)) + 111111111; set @ssn = unhex(sha2(concat(@salt, @ran_num), 512 ));comment 'rand([N]): Returns random floating oint value v in the range 0 < = v 1.0'comment 'randomize ssn between 111111111-999999999 (note: using value 000000000 for ex. 4)'update personset per_ssn = @ssn, per_sslt = @saltwhere per_id =x;set x = x + 1;end while;end $$Delimiter ;call CreatePersonSSN();
ERROR 1046 (3D000): No database selected
    -> use ooa19a
    -> ;
    -> \c
mysql> use ooa19a;
ERROR 1049 (42000): Unknown database 'ooa19a;'
mysql> use ooa19a
Database changed
mysql> drop procedure if exists CreatePersonSSN;delimiter $$create procedure CreatePersonSSN()BEGINCOMMENT '--DECLARE VARIABLE IN MSQL' DECLARE x, y INT; SET X = 1;  COMMENT 'DYNAMICALLY SET LOOP ENDING VALUE (TOTAL NUMBER OF PERSONS)'select count(*) into y from person comment 'count the total number of records on the table and put them in Y'while x <= y docomment 'give each person a unique randomized salt, and hashed salted randomized ssn'comment 'this demo is only for showing how to include salted and hashed randomized values for testing purposes!'set @salt = random_bytes(64); set @ran_num = floor(rand() * (999999999 - 111111111 + 1)) + 111111111; set @ssn = unhex(sha2(concat(@salt, @ran_num), 512 ));comment 'rand([N]): Returns random floating oint value v in the range 0 < = v 1.0'comment 'randomize ssn between 111111111-999999999 (note: using value 000000000 for ex. 4)'update personset per_ssn = @ssn, per_sslt = @saltwhere per_id =x;set x = x + 1;end while;end $$Delimiter ;call CreatePersonSSN();
Query OK, 0 rows affected, 1 warning (0.00 sec)

ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'delimiter' at line 1
    -> \C
ERROR: 
Usage: \C charset_name | charset charset_name
    -> \c
mysql> select * from `ooa19a`.`person`;
    -> \c
mysql> exit
mysql> drop procedure if exists CreatePersonSSN;
ERROR 1046 (3D000): No database selected
mysql> use ooa19a;
Database changed
mysql> drop procedure if exists CreatePersonSSN;
Query OK, 0 rows affected, 1 warning (0.01 sec)

mysql> show warnings;
+-------+------+-------------------------------------------------+
| Level | Code | Message                                         |
+-------+------+-------------------------------------------------+
| Note  | 1305 | PROCEDURE ooa19a.CreatePersonSSN does not exist |
+-------+------+-------------------------------------------------+
1 row in set (0.00 sec)

mysql> Delimiter $$create procedure CreatePersonSSN()BEGINCOMMENT '--DECLARE VARIABLE IN MSQL' DECLARE x, y INT; SET X = 1;  COMMENT 'DYNAMICALLY SET LOOP ENDING VALUE (TOTAL NUMBER OF PERSONS)'select count(*) into y from person comment 'count the total number of records on the table and put them in Y'while x <= y docomment 'give each person a unique randomized salt, and hashed salted randomized ssn'comment 'this demo is only for showing how to include salted and hashed randomized values for testing purposes!'set @salt = random_bytes(64); set @ran_num = floor(rand() * (999999999 - 111111111 + 1)) + 111111111; set @ssn = unhex(sha2(concat(@salt, @ran_num), 512 ));comment 'rand([N]): Returns random floating oint value v in the range 0 < = v 1.0'comment 'randomize ssn between 111111111-999999999 (note: using value 000000000 for ex. 4)'update personset per_ssn = @ssn, per_sslt = @saltwhere per_id =x;set x = x + 1;end while;end $$Delimiter ;
mysql> call CreatePersonSSN();
    -> \c
mysql> call CreatePersonSSN();
    -> \c
mysql> use ooa19a
Database changed
mysql> call CreatePersonSSN();
    -> \C
ERROR: 
Usage: \C charset_name | charset charset_name
    -> \c
mysql> select * from person;
    -> \c
mysql> bye
    -> ;
    -> exit;
    -> \c
mysql> exit
mysql> drop procedure if exists CreatePersonSSN;
Query OK, 0 rows affected, 1 warning (0.01 sec)

mysql> Delimiter $$
mysql> create procedure CreatePersonSSN() BEGIN
    -> COMMENT '--DECLARE VARIABLE IN MSQL'
    ->  DECLARE x, y INT;
    -> SET x = 1;
    ->  COMMENT 'DYNAMICALLY SET LOOP ENDING VALUE (TOTAL NUMBER OF PERSONS)'
    -> select count(*) into y from person   comment 'count the total number of records on the table and put them in Y'
    -> ^C
mysql> while x <= y do
    -> comment 'give each person a unique randomized salt, and hashed salted randomized ssn' comment 'this demo is only for showing how to include salted and hashed randomized values for testing purposes!'
    -> set @salt = random_bytes(64); set @ran_num = floor(rand() * (999999999 - 111111111 + 1)) + 111111111; set @ssn = unhex(sha2(concat(@salt, @ran_num), 512 ));
    -> comment 'rand([N]): Returns random floating oint value v in the range 0 < = v 1.0'
    -> comment 'randomize ssn between 111111111-999999999 (note: using value 000000000 for ex. 4)'
    -> update person set per_ssn = @ssn, per_sslt = @salt where per_id =x; set x = x + 1; end while; end $$ Delimiter ;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'while x <= y do
comment 'give each person a unique randomized salt, and hashed s' at line 1
    -> end $$
mysql> drop procedure if exists CreatePersonSSN;
    -> Delimiter $$
    -> create procedure CreatePersonSSN() 
    -> BEGIN
    -> COMMENT '--DECLARE VARIABLE IN MSQL'
    ->  DECLARE x, y INT;
    -> SET x = 1;
    ->  COMMENT 'DYNAMICALLY SET LOOP ENDING VALUE (TOTAL NUMBER OF PERSONS)'
    -> select count(*) into y from person   comment 'count the total number of records on the table and put them in Y'
    -> while x <= y DO
    -> comment 'give each person a unique randomized salt, and hashed salted randomized ssn' comment 'this demo is only for showing how to include salted and hashed randomized values for testing purposes!'
    -> set @salt = random_bytes(64); set @ran_num = FLOOR(RAND() * (999999999 - 111111111 + 1)) + 111111111; set @ssn = unhex(sha2(concat(@salt, @ran_num), 512 ));
    -> comment 'rand([N]): Returns random floating oint value v in the range 0 < = v 1.0'
    -> comment 'randomize ssn between 111111111-999999999 (note: using value 000000000 for ex. 4)'
    -> update person set per_ssn = @ssn, per_sslt = @salt where per_id =x; set x = x + 1; end while; end$$ Delimiter ;
    -> end$$ Delimiter ;
    -> call CreatePersonSSN();
    -> \c
mysql> \c
mysql> exit
mysql> use ooa19a;drop procedure if exists CreatePersonSSN;        Delimiter $$    create procedure CreatePersonSSN()     BEGIN   -- DECLARE VARIABLE IN MSQL     DECLARE x, y INT;    SET x = 1;   --  COMMENT 'DYNAMICALLY SET LOOP ENDING VALUE (TOTAL NUMBER OF PERSONS)'    select count(*) into y from person;   -- comment 'count the total number of records on the table and put them in Y'    while x <= y DO    -- comment 'give each person a unique randomized salt, and hashed salted randomized ssn' -- comment 'this demo is only for showing how to include salted and hashed randomized values for testing purposes!'    set @salt = random_bytes(64); set @ran_num = FLOOR(RAND() * (999999999 - 111111111 + 1)) + 111111111; set @ssn = unhex(sha2(concat(@salt, @ran_num), 512 ));    -- comment 'rand([N]): Returns random floating oint value v in the range 0 < = v 1.0'    -- comment 'randomize ssn between 111111111-999999999 (note: using value 000000000 for ex. 4)'    update person set per_ssn = @ssn, per_sslt = @salt     where per_id =x; set x = x + 1; end while; end$$ Delimiter ;
Database changed
Query OK, 0 rows affected, 1 warning (0.01 sec)

    -> \c
mysql> show warnings;
+-------+------+-------------------------------------------------+
| Level | Code | Message                                         |
+-------+------+-------------------------------------------------+
| Note  | 1305 | PROCEDURE ooa19a.CreatePersonSSN does not exist |
+-------+------+-------------------------------------------------+
1 row in set (0.00 sec)

mysql> call CreatePersonSSN();
ERROR 1305 (42000): PROCEDURE ooa19a.CreatePersonSSN does not exist
mysql> use ooa19a
Database changed
mysql> use ooa19a;drop procedure if exists CreatePersonSSN;        Delimiter $$    create procedure CreatePersonSSN()     BEGIN   -- DECLARE VARIABLE IN MSQL     DECLARE x, y INT;    SET x = 1;   --  COMMENT 'DYNAMICALLY SET LOOP ENDING VALUE (TOTAL NUMBER OF PERSONS)'    select count(*) into y from person;   -- comment 'count the total number of records on the table and put them in Y'    while x <= y DO    -- comment 'give each person a unique randomized salt, and hashed salted randomized ssn' -- comment 'this demo is only for showing how to include salted and hashed randomized values for testing purposes!'    set @salt = random_bytes(64); set @ran_num = FLOOR(RAND() * (999999999 - 111111111 + 1)) + 111111111; set @ssn = unhex(sha2(concat(@salt, @ran_num), 512 ));    -- comment 'rand([N]): Returns random floating oint value v in the range 0 < = v 1.0'    -- comment 'randomize ssn between 111111111-999999999 (note: using value 000000000 for ex. 4)'    update person set per_ssn = @ssn, per_sslt = @salt     where per_id =x; set x = x + 1; end while; end$$ Delimiter ;
Database changed
Query OK, 0 rows affected, 1 warning (0.01 sec)

    -> \c
mysql> select * from person;
+--------+---------+----------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
| per_id | per_ssn | per_sslt | per_fname | per_lname  | per_street             | per_city    | per_state | per_zip    | per_email               | per_dob    | per_type | per_notes |
+--------+---------+----------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
|      1 | NULL    | NULL     | Steve     | Rogers     | 437 Southern Drive     | Rochester   | NY        |  032443022 | srogers@comcast.net     | 1923-10-03 | c        | NULL      |
|      2 | NULL    | NULL     | Bruce     | Wayne      | 1007 Mountain Drive    | Gotham      | NY        |  003243440 | bwayne@knology.net      | 1968-03-20 | c        | NULL      |
|      3 | NULL    | NULL     | Peter     | Parker     | 20 Ingram Street       | New York    | NY        |  102862341 | pparker@msn.net         | 1923-10-03 | c        | NULL      |
|      4 | NULL    | NULL     | Jane      | Thompson   | 13563 Ocean View Drive | Seattle     | WA        |  032084409 | jthompson@gmail.net     | 1973-05-08 | c        | NULL      |
|      5 | NULL    | NULL     | Debra     | Steele     | 543 Oak Ln             | Milwaukee   | WI        | 2862374478 | dsteele@verizon.net     | 1994-07-19 | c        | NULL      |
|      6 | NULL    | NULL     | Tony      | Stark      | 332 Palm Avenue        | Malibu      | CA        |  902638832 | tstark@yahoo.com        | 1972-05-04 | a        | NULL      |
|      7 | NULL    | NULL     | Hank      | Pymi       | 2355 Brown Street      | Cleveland   | OH        |  022342390 | hpym@aol.net            | 1980-08-28 | a        | NULL      |
|      8 | NULL    | NULL     | Bob       | Best       | 4902 Avendale Ave.     | Scottsdale  | AZ        |  087263912 | bbest@yahoo.com         | 1992-02-10 | a        | NULL      |
|      9 | NULL    | NULL     | Sandra    | Dole       | 8732 Lawrence Ave.     | Atlanta     | GA        |  002345390 | sdole@gmail.net         | 1990-01-26 | a        | NULL      |
|     10 | NULL    | NULL     | Ben       | Avery      | 6432 Thunderbird Ln    | Sioux Falls | SD        |  005637932 | bavery@hotmail.net      | 1983-12-24 | a        | NULL      |
|     11 | NULL    | NULL     | Arthur    | Curry      | 3302 Euclid Avenue     | Miami       | FL        |  000219932 | acurry@gmail.com        | 1975-12-15 | j        | NULL      |
|     12 | NULL    | NULL     | Diana     | Price      | 944 Green Street       | Las vegas   | NV        |  033235223 | dprice@aol.net          | 1980-08-22 | j        | NULL      |
|     13 | NULL    | NULL     | Adam      | Jurris     | 98435 Valencia Dr.     | Gulf Shore  | AL        |  870639912 | ajurris@yahoo.com       | 1995-01-31 | j        | NULL      |
|     14 | NULL    | NULL     | Judy      | Sleen      | 56343 Rover Ct.        | Billings    | MT        |  067204823 | jsleen@symaptico.com    | 1995-01-31 | j        | NULL      |
|     15 | NULL    | NULL     | Bill      | Neiderheim | 43567 Netherland Blvd  | South Bend  | IN        |  320219932 | bneiderheim@comcast.net | 1982-03-24 | j        | NULL      |
+--------+---------+----------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
15 rows in set (0.00 sec)

mysql> drop procedure if exists CreatePersonSSN; show warnings;
Query OK, 0 rows affected, 1 warning (0.01 sec)

+-------+------+-------------------------------------------------+
| Level | Code | Message                                         |
+-------+------+-------------------------------------------------+
| Note  | 1305 | PROCEDURE ooa19a.CreatePersonSSN does not exist |
+-------+------+-------------------------------------------------+
1 row in set (0.00 sec)

mysql> select * from person;
+--------+---------+----------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
| per_id | per_ssn | per_sslt | per_fname | per_lname  | per_street             | per_city    | per_state | per_zip    | per_email               | per_dob    | per_type | per_notes |
+--------+---------+----------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
|      1 | NULL    | NULL     | Steve     | Rogers     | 437 Southern Drive     | Rochester   | NY        |  032443022 | srogers@comcast.net     | 1923-10-03 | c        | NULL      |
|      2 | NULL    | NULL     | Bruce     | Wayne      | 1007 Mountain Drive    | Gotham      | NY        |  003243440 | bwayne@knology.net      | 1968-03-20 | c        | NULL      |
|      3 | NULL    | NULL     | Peter     | Parker     | 20 Ingram Street       | New York    | NY        |  102862341 | pparker@msn.net         | 1923-10-03 | c        | NULL      |
|      4 | NULL    | NULL     | Jane      | Thompson   | 13563 Ocean View Drive | Seattle     | WA        |  032084409 | jthompson@gmail.net     | 1973-05-08 | c        | NULL      |
|      5 | NULL    | NULL     | Debra     | Steele     | 543 Oak Ln             | Milwaukee   | WI        | 2862374478 | dsteele@verizon.net     | 1994-07-19 | c        | NULL      |
|      6 | NULL    | NULL     | Tony      | Stark      | 332 Palm Avenue        | Malibu      | CA        |  902638832 | tstark@yahoo.com        | 1972-05-04 | a        | NULL      |
|      7 | NULL    | NULL     | Hank      | Pymi       | 2355 Brown Street      | Cleveland   | OH        |  022342390 | hpym@aol.net            | 1980-08-28 | a        | NULL      |
|      8 | NULL    | NULL     | Bob       | Best       | 4902 Avendale Ave.     | Scottsdale  | AZ        |  087263912 | bbest@yahoo.com         | 1992-02-10 | a        | NULL      |
|      9 | NULL    | NULL     | Sandra    | Dole       | 8732 Lawrence Ave.     | Atlanta     | GA        |  002345390 | sdole@gmail.net         | 1990-01-26 | a        | NULL      |
|     10 | NULL    | NULL     | Ben       | Avery      | 6432 Thunderbird Ln    | Sioux Falls | SD        |  005637932 | bavery@hotmail.net      | 1983-12-24 | a        | NULL      |
|     11 | NULL    | NULL     | Arthur    | Curry      | 3302 Euclid Avenue     | Miami       | FL        |  000219932 | acurry@gmail.com        | 1975-12-15 | j        | NULL      |
|     12 | NULL    | NULL     | Diana     | Price      | 944 Green Street       | Las vegas   | NV        |  033235223 | dprice@aol.net          | 1980-08-22 | j        | NULL      |
|     13 | NULL    | NULL     | Adam      | Jurris     | 98435 Valencia Dr.     | Gulf Shore  | AL        |  870639912 | ajurris@yahoo.com       | 1995-01-31 | j        | NULL      |
|     14 | NULL    | NULL     | Judy      | Sleen      | 56343 Rover Ct.        | Billings    | MT        |  067204823 | jsleen@symaptico.com    | 1995-01-31 | j        | NULL      |
|     15 | NULL    | NULL     | Bill      | Neiderheim | 43567 Netherland Blvd  | South Bend  | IN        |  320219932 | bneiderheim@comcast.net | 1982-03-24 | j        | NULL      |
+--------+---------+----------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
15 rows in set (0.00 sec)

mysql> call CreatePersonSSN();
Query OK, 1 row affected (0.07 sec)

mysql> select * from person;
+--------+------------------------------------------------------------------+------------------------------------------------------------------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
| per_id | per_ssn                                                          | per_sslt                                                         | per_fname | per_lname  | per_street             | per_city    | per_state | per_zip    | per_email               | per_dob    | per_type | per_notes |
+--------+------------------------------------------------------------------+------------------------------------------------------------------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
|      1 | V������KvF�6�4*��A��-����`*��ғ�e���+m�g[Ai�������                                    | Q<�/�N;1�) �B_.!��R^�+7J�$[�v��W�p�/��7����wm�,�`���1                         | Steve     | Rogers     | 437 Southern Drive     | Rochester   | NY        |  032443022 | srogers@comcast.net     | 1923-10-03 | c        | NULL      |
|      2 | 6\ 5����/7gڃ-G_+ػ+�i+�0�yZۭ��{�]�,��|�A؛Gıqk,��-���b� B                          | �+�[�����=�+UTcS
+�|KlRW�V��Mr%Έ���}0��p?�w�*���AMe��UQ                           | Bruce     | Wayne      | 1007 Mountain Drive    | Gotham      | NY        |  003243440 | bwayne@knology.net      | 1968-03-20 | c        | NULL      |
|      3 | 982���+D֘L����`B�&�JԘFҍ9TVum��X �`")��^�u�jf�ul����j�R                          | �6����d��ˉ���F�f���h�3M���/��s�x%7����fz��Ba�|`���                                   | Peter     | Parker     | 20 Ingram Street       | New York    | NY        |  102862341 | pparker@msn.net         | 1923-10-03 | c        | NULL      |
|      4 | ~/c���/�)�f�w�`���3|�ᠰ%ȫVs�Rq���٪�#cuob�)���d�W��{                              | �Ӭ9��m�:��K.V\5{<7>�P��nΑ.1*����'k�o���B��Q�GeB� tI�P��                            | Jane      | Thompson   | 13563 Ocean View Drive | Seattle     | WA        |  032084409 | jthompson@gmail.net     | 1973-05-08 | c        | NULL      |
|      5 | ���:�����c��y�;Ҷ�WL��e7;��I���9�Z��j�O��5�)�>�a���\�                                    | *��K�{Y����F�L>�r	,3)y"{��M6��[��f�a��d{`3���O?lg�4��ݧU��                            | Debra     | Steele     | 543 Oak Ln             | Milwaukee   | WI        | 2862374478 | dsteele@verizon.net     | 1994-07-19 | c        | NULL      |
|      6 | Jb����Z,r���@Kg� @!'�&o0P�nk��7�a4��%�&�Zm�������;VӺ&                           | }�8Z���t$�3D 	�
Ε�iv3j+�cq�訞p`c����� E�4�W��oK� ��6��A�/�                            | Tony      | Stark      | 332 Palm Avenue        | Malibu      | CA        |  902638832 | tstark@yahoo.com        | 1972-05-04 | a        | NULL      |
|      7 | "*��r�dG�dV�.�Dy�ÖKѾ��y��Q�^����|�����tMHS��-~V5�y��z�z���Y                                 | �@cJ�koZb�]�p=\�=�M���,��9�#\�Q�N\�5�<�%�G��PU����Z���                            | Hank      | Pymi       | 2355 Brown Street      | Cleveland   | OH        |  022342390 | hpym@aol.net            | 1980-08-28 | a        | NULL      |
|      8 | �D��`-QB��e� e|*��8��tI!/�m���	OG�tO�8<�q�G 	�1PH�}��� �                         | ���ȃ��{�[�'�)�e#��\�z9�z�})�׼��Z/ sE��3Åȹ�r�N��?�r<K�                                | Bob       | Best       | 4902 Avendale Ave.     | Scottsdale  | AZ        |  087263912 | bbest@yahoo.com         | 1992-02-10 | a        | NULL      |
|      9 | v���;�������Q�t���T7�f��3]�G�
� �<�}�9�'����E���������v�>                                         | �7�/P������S�LM�����
Ӎ�H#����	�vp!WӁZ��@�vm�tu&����G��                                   | Sandra    | Dole       | 8732 Lawrence Ave.     | Atlanta     | GA        |  002345390 | sdole@gmail.net         | 1990-01-26 | a        | NULL      |
|     10 | ޳{�@��7�e}��4�?a�E,vD]����r��2�1�ů��9f�j�_���p�$3��                              | uˈܻ��Y1C�d��L麳�s�Mʳ���lJ#	~����T�d&!S�y��/x�]xZ��!                           | Ben       | Avery      | 6432 Thunderbird Ln    | Sioux Falls | SD        |  005637932 | bavery@hotmail.net      | 1983-12-24 | a        | NULL      |
|     11 | �:0��Z�Z���,�6��� V���}�DA�a�f�7�b;�ɛ��m'J52��9DS��c                             | ���E�jJ3�`���A݊k�A�F��x�b����MDO�_�7x��Y	+�-��>�%��*h�g���0                                   | Arthur    | Curry      | 3302 Euclid Avenue     | Miami       | FL        |  000219932 | acurry@gmail.com        | 1975-12-15 | j        | NULL      |
|     12 | �N�>�50W!��z�; =�ɒ`�3��ek��~q�/�u�A_���
J�~�{*T�A��nF\�j                           | k��#+�0��
�ԪTG�9�;�5IR�m�q9*�:�L��)B�!��k�2�tH�7A��������                                | Diana     | Price      | 944 Green Street       | Las vegas   | NV        |  033235223 | dprice@aol.net          | 1980-08-22 | j        | NULL      |
|     13 | +�%r��6dS�KVTD�;}~�t�L�F��_�4�j�g��S7�L�L�S}޸��`h-#l;���(�                           | ��'������V��
#.�y��$�v�+8�l�P�S���R)��������9�oE�/��[[�AdG                                     | Adam      | Jurris     | 98435 Valencia Dr.     | Gulf Shore  | AL        |  870639912 | ajurris@yahoo.com       | 1995-01-31 | j        | NULL      |
|     14 | �Rb�������XT���p�c������Қ��Oq�K�By�b,���O��g�}1ޅK                                 | ���z��=Py	��£�\@���X���\���r�d.��r"�:����F9��dn��a                                | Judy      | Sleen      | 56343 Rover Ct.        | Billings    | MT        |  067204823 | jsleen@symaptico.com    | 1995-01-31 | j        | NULL      |
|     15 | �T*o�1�L�����n��0��K��z$�@'N��x2 ��e���Vp��?H��	�O�{c                             | e1b��=�{Ф���(!EH�ē���%U#���t)�!=��Mr�	'G�%����2���R$��D                                | Bill      | Neiderheim | 43567 Netherland Blvd  | South Bend  | IN        |  320219932 | bneiderheim@comcast.net | 1982-03-24 | j        | NULL      |
+--------+------------------------------------------------------------------+------------------------------------------------------------------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
15 rows in set (0.00 sec)

mysql> select * from client;
+--------+-----------+
| per_id | cli_notes |
+--------+-----------+
|      1 | NULL      |
|      2 | NULL      |
|      3 | NULL      |
|      4 | NULL      |
|      5 | NULL      |
+--------+-----------+
5 rows in set (0.00 sec)

mysql> select * from judge;
+--------+--------+------------+-----------------------+-----------+
| per_id | crt_id | jud_salary | jud_years_in_practice | jud_notes |
+--------+--------+------------+-----------------------+-----------+
|     11 |      1 |  150000.00 |                    12 | Judge 1   |
|     12 |      2 |  140000.00 |                    10 | Judge 2   |
|     13 |      3 |  100000.00 |                    15 | Judge 3   |
|     14 |      4 |   95000.00 |                    12 | Judge 4   |
|     15 |      5 |  120000.00 |                    11 | Judge 5   |
+--------+--------+------------+-----------------------+-----------+
5 rows in set (0.00 sec)

mysql> select 'Create a triggerthat automatically adds a record to the judge history table for every record addedto the judge table.' as ' '';
    '> do sleep(5);
    '> '
    -> '';
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '''' at line 4
mysql> select 'Create a triggerthat automatically adds a record to the judge history table for every record addedto the judge table.' as '';
+-----------------------------------------------------------------------------------------------------------------------+
|                                                                                                                       |
+-----------------------------------------------------------------------------------------------------------------------+
| Create a triggerthat automatically adds a record to the judge history table for every record addedto the judge table. |
+-----------------------------------------------------------------------------------------------------------------------+
1 row in set (0.00 sec)

mysql> do sleep(5);
Query OK, 0 rows affected (5.00 sec)

mysql> select 'Create a triggerthat automatically adds a record to the judge history table for every record addedto the judge table.' as ''; do sleep(5);
+-----------------------------------------------------------------------------------------------------------------------+
|                                                                                                                       |
+-----------------------------------------------------------------------------------------------------------------------+
| Create a triggerthat automatically adds a record to the judge history table for every record addedto the judge table. |
+-----------------------------------------------------------------------------------------------------------------------+
1 row in set (0.00 sec)

Query OK, 0 rows affected (5.00 sec)

mysql> use ooa19a;drop procedure if exists CreatePersonSSN;        Delimiter $$    create procedure CreatePersonSSN()     BEGIN   -- DECLARE VARIABLE IN MSQL     DECLARE x, y INT;    SET x = 1;   --  COMMENT 'DYNAMICALLY SET LOOP ENDING VALUE (TOTAL NUMBER OF PERSONS)'    select count(*) into y from person;   -- comment 'count the total number of records on the table and put them in Y'    while x <= y DO    -- comment 'give each person a unique randomized salt, and hashed salted randomized ssn' -- comment 'this demo is only for showing how to include salted and hashed randomized values for testing purposes!'    set @salt = random_bytes(64); set @ran_num = FLOOR(RAND() * (999999999 - 111111111 + 1)) + 111111111; set @ssn = unhex(sha2(concat(@salt, @ran_num), 512 ));    -- comment 'rand([N]): Returns random floating oint value v in the range 0 < = v 1.0'    -- comment 'randomize ssn between 111111111-999999999 (note: using value 000000000 for ex. 4)'    update person set per_ssn = @ssn, per_sslt = @salt     where per_id =x; set x = x + 1; end while; end$$ Delimiter ;
Database changed
Query OK, 0 rows affected (0.03 sec)

    -> \c
mysql> -
    -> -;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '-
-' at line 1
mysql> select * from person;
+--------+------------------------------------------------------------------+------------------------------------------------------------------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
| per_id | per_ssn                                                          | per_sslt                                                         | per_fname | per_lname  | per_street             | per_city    | per_state | per_zip    | per_email               | per_dob    | per_type | per_notes |
+--------+------------------------------------------------------------------+------------------------------------------------------------------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
|      1 | V������KvF�6�4*��A��-����`*��ғ�e���+m�g[Ai�������                                    | Q<�/�N;1�) �B_.!��R^�+7J�$[�v��W�p�/��7����wm�,�`���1                         | Steve     | Rogers     | 437 Southern Drive     | Rochester   | NY        |  032443022 | srogers@comcast.net     | 1923-10-03 | c        | NULL      |
|      2 | 6\ 5����/7gڃ-G_+ػ+�i+�0�yZۭ��{�]�,��|�A؛Gıqk,��-���b� B                          | �+�[�����=�+UTcS
+�|KlRW�V��Mr%Έ���}0��p?�w�*���AMe��UQ                           | Bruce     | Wayne      | 1007 Mountain Drive    | Gotham      | NY        |  003243440 | bwayne@knology.net      | 1968-03-20 | c        | NULL      |
|      3 | 982���+D֘L����`B�&�JԘFҍ9TVum��X �`")��^�u�jf�ul����j�R                          | �6����d��ˉ���F�f���h�3M���/��s�x%7����fz��Ba�|`���                                   | Peter     | Parker     | 20 Ingram Street       | New York    | NY        |  102862341 | pparker@msn.net         | 1923-10-03 | c        | NULL      |
|      4 | ~/c���/�)�f�w�`���3|�ᠰ%ȫVs�Rq���٪�#cuob�)���d�W��{                              | �Ӭ9��m�:��K.V\5{<7>�P��nΑ.1*����'k�o���B��Q�GeB� tI�P��                            | Jane      | Thompson   | 13563 Ocean View Drive | Seattle     | WA        |  032084409 | jthompson@gmail.net     | 1973-05-08 | c        | NULL      |
|      5 | ���:�����c��y�;Ҷ�WL��e7;��I���9�Z��j�O��5�)�>�a���\�                                    | *��K�{Y����F�L>�r	,3)y"{��M6��[��f�a��d{`3���O?lg�4��ݧU��                            | Debra     | Steele     | 543 Oak Ln             | Milwaukee   | WI        | 2862374478 | dsteele@verizon.net     | 1994-07-19 | c        | NULL      |
|      6 | Jb����Z,r���@Kg� @!'�&o0P�nk��7�a4��%�&�Zm�������;VӺ&                           | }�8Z���t$�3D 	�
Ε�iv3j+�cq�訞p`c����� E�4�W��oK� ��6��A�/�                            | Tony      | Stark      | 332 Palm Avenue        | Malibu      | CA        |  902638832 | tstark@yahoo.com        | 1972-05-04 | a        | NULL      |
|      7 | "*��r�dG�dV�.�Dy�ÖKѾ��y��Q�^����|�����tMHS��-~V5�y��z�z���Y                                 | �@cJ�koZb�]�p=\�=�M���,��9�#\�Q�N\�5�<�%�G��PU����Z���                            | Hank      | Pymi       | 2355 Brown Street      | Cleveland   | OH        |  022342390 | hpym@aol.net            | 1980-08-28 | a        | NULL      |
|      8 | �D��`-QB��e� e|*��8��tI!/�m���	OG�tO�8<�q�G 	�1PH�}��� �                         | ���ȃ��{�[�'�)�e#��\�z9�z�})�׼��Z/ sE��3Åȹ�r�N��?�r<K�                                | Bob       | Best       | 4902 Avendale Ave.     | Scottsdale  | AZ        |  087263912 | bbest@yahoo.com         | 1992-02-10 | a        | NULL      |
|      9 | v���;�������Q�t���T7�f��3]�G�
� �<�}�9�'����E���������v�>                                         | �7�/P������S�LM�����
Ӎ�H#����	�vp!WӁZ��@�vm�tu&����G��                                   | Sandra    | Dole       | 8732 Lawrence Ave.     | Atlanta     | GA        |  002345390 | sdole@gmail.net         | 1990-01-26 | a        | NULL      |
|     10 | ޳{�@��7�e}��4�?a�E,vD]����r��2�1�ů��9f�j�_���p�$3��                              | uˈܻ��Y1C�d��L麳�s�Mʳ���lJ#	~����T�d&!S�y��/x�]xZ��!                           | Ben       | Avery      | 6432 Thunderbird Ln    | Sioux Falls | SD        |  005637932 | bavery@hotmail.net      | 1983-12-24 | a        | NULL      |
|     11 | �:0��Z�Z���,�6��� V���}�DA�a�f�7�b;�ɛ��m'J52��9DS��c                             | ���E�jJ3�`���A݊k�A�F��x�b����MDO�_�7x��Y	+�-��>�%��*h�g���0                                   | Arthur    | Curry      | 3302 Euclid Avenue     | Miami       | FL        |  000219932 | acurry@gmail.com        | 1975-12-15 | j        | NULL      |
|     12 | �N�>�50W!��z�; =�ɒ`�3��ek��~q�/�u�A_���
J�~�{*T�A��nF\�j                           | k��#+�0��
�ԪTG�9�;�5IR�m�q9*�:�L��)B�!��k�2�tH�7A��������                                | Diana     | Price      | 944 Green Street       | Las vegas   | NV        |  033235223 | dprice@aol.net          | 1980-08-22 | j        | NULL      |
|     13 | +�%r��6dS�KVTD�;}~�t�L�F��_�4�j�g��S7�L�L�S}޸��`h-#l;���(�                           | ��'������V��
#.�y��$�v�+8�l�P�S���R)��������9�oE�/��[[�AdG                                     | Adam      | Jurris     | 98435 Valencia Dr.     | Gulf Shore  | AL        |  870639912 | ajurris@yahoo.com       | 1995-01-31 | j        | NULL      |
|     14 | �Rb�������XT���p�c������Қ��Oq�K�By�b,���O��g�}1ޅK                                 | ���z��=Py	��£�\@���X���\���r�d.��r"�:����F9��dn��a                                | Judy      | Sleen      | 56343 Rover Ct.        | Billings    | MT        |  067204823 | jsleen@symaptico.com    | 1995-01-31 | j        | NULL      |
|     15 | �T*o�1�L�����n��0��K��z$�@'N��x2 ��e���Vp��?H��	�O�{c                             | e1b��=�{Ф���(!EH�ē���%U#���t)�!=��Mr�	'G�%����2���R$��D                                | Bill      | Neiderheim | 43567 Netherland Blvd  | South Bend  | IN        |  320219932 | bneiderheim@comcast.net | 1982-03-24 | j        | NULL      |
+--------+------------------------------------------------------------------+------------------------------------------------------------------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
15 rows in set (0.00 sec)

mysql> select * `information_schema`.`tables` where table_schema = ooa19a;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '`information_schema`.`tables` where table_schema = ooa19a' at line 1
mysql> select * from `information_schema`.`tables` where table_schema = ooa19a;
ERROR 1054 (42S22): Unknown column 'ooa19a' in 'where clause'
mysql> use information_schema
Database changed
mysql> show tables;
+---------------------------------------+
| Tables_in_information_schema          |
+---------------------------------------+
| CHARACTER_SETS                        |
| CHECK_CONSTRAINTS                     |
| COLLATION_CHARACTER_SET_APPLICABILITY |
| COLLATIONS                            |
| COLUMN_PRIVILEGES                     |
| COLUMN_STATISTICS                     |
| COLUMNS                               |
| ENGINES                               |
| EVENTS                                |
| FILES                                 |
| INNODB_BUFFER_PAGE                    |
| INNODB_BUFFER_PAGE_LRU                |
| INNODB_BUFFER_POOL_STATS              |
| INNODB_CACHED_INDEXES                 |
| INNODB_CMP                            |
| INNODB_CMP_PER_INDEX                  |
| INNODB_CMP_PER_INDEX_RESET            |
| INNODB_CMP_RESET                      |
| INNODB_CMPMEM                         |
| INNODB_CMPMEM_RESET                   |
| INNODB_COLUMNS                        |
| INNODB_DATAFILES                      |
| INNODB_FIELDS                         |
| INNODB_FOREIGN                        |
| INNODB_FOREIGN_COLS                   |
| INNODB_FT_BEING_DELETED               |
| INNODB_FT_CONFIG                      |
| INNODB_FT_DEFAULT_STOPWORD            |
| INNODB_FT_DELETED                     |
| INNODB_FT_INDEX_CACHE                 |
| INNODB_FT_INDEX_TABLE                 |
| INNODB_INDEXES                        |
| INNODB_METRICS                        |
| INNODB_SESSION_TEMP_TABLESPACES       |
| INNODB_TABLES                         |
| INNODB_TABLESPACES                    |
| INNODB_TABLESPACES_BRIEF              |
| INNODB_TABLESTATS                     |
| INNODB_TEMP_TABLE_INFO                |
| INNODB_TRX                            |
| INNODB_VIRTUAL                        |
| KEY_COLUMN_USAGE                      |
| KEYWORDS                              |
| OPTIMIZER_TRACE                       |
| PARAMETERS                            |
| PARTITIONS                            |
| PLUGINS                               |
| PROCESSLIST                           |
| PROFILING                             |
| REFERENTIAL_CONSTRAINTS               |
| RESOURCE_GROUPS                       |
| ROUTINES                              |
| SCHEMA_PRIVILEGES                     |
| SCHEMATA                              |
| ST_GEOMETRY_COLUMNS                   |
| ST_SPATIAL_REFERENCE_SYSTEMS          |
| ST_UNITS_OF_MEASURE                   |
| STATISTICS                            |
| TABLE_CONSTRAINTS                     |
| TABLE_PRIVILEGES                      |
| TABLES                                |
| TABLESPACES                           |
| TRIGGERS                              |
| USER_PRIVILEGES                       |
| VIEW_ROUTINE_USAGE                    |
| VIEW_TABLE_USAGE                      |
| VIEWS                                 |
+---------------------------------------+
67 rows in set (0.00 sec)

mysql> select * from tables;
+---------------+--------------------+------------------------------------------------------+-------------+--------------------+---------+------------+------------+----------------+-------------+-----------------+--------------+-----------+----------------+---------------------+-------------+------------+--------------------+----------+---------------------------------------+-----------------------------------------+
| TABLE_CATALOG | TABLE_SCHEMA       | TABLE_NAME                                           | TABLE_TYPE  | ENGINE             | VERSION | ROW_FORMAT | TABLE_ROWS | AVG_ROW_LENGTH | DATA_LENGTH | MAX_DATA_LENGTH | INDEX_LENGTH | DATA_FREE | AUTO_INCREMENT | CREATE_TIME         | UPDATE_TIME | CHECK_TIME | TABLE_COLLATION    | CHECKSUM | CREATE_OPTIONS                        | TABLE_COMMENT                           |
+---------------+--------------------+------------------------------------------------------+-------------+--------------------+---------+------------+------------+----------------+-------------+-----------------+--------------+-----------+----------------+---------------------+-------------+------------+--------------------+----------+---------------------------------------+-----------------------------------------+
| def           | mysql              | innodb_table_stats                                   | BASE TABLE  | InnoDB             |      10 | Dynamic    |         16 |           1024 |       16384 |               0 |            0 |   5242880 |           NULL | 2019-11-19 00:16:27 | NULL        | NULL       | utf8_bin           |     NULL | row_format=DYNAMIC stats_persistent=0 |                                         |
| def           | mysql              | innodb_index_stats                                   | BASE TABLE  | InnoDB             |      10 | Dynamic    |        122 |            134 |       16384 |               0 |            0 |   5242880 |           NULL | 2019-11-19 00:16:27 | NULL        | NULL       | utf8_bin           |     NULL | row_format=DYNAMIC stats_persistent=0 |                                         |
| def           | information_schema | CHARACTER_SETS                                       | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | CHECK_CONSTRAINTS                                    | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | COLLATIONS                                           | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | COLLATION_CHARACTER_SET_APPLICABILITY                | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | COLUMNS                                              | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | COLUMN_STATISTICS                                    | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | EVENTS                                               | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | FILES                                                | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | INNODB_DATAFILES                                     | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | INNODB_FOREIGN                                       | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | INNODB_FOREIGN_COLS                                  | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | INNODB_FIELDS                                        | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | INNODB_TABLESPACES_BRIEF                             | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | KEY_COLUMN_USAGE                                     | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | KEYWORDS                                             | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | PARAMETERS                                           | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | PARTITIONS                                           | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | REFERENTIAL_CONSTRAINTS                              | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | RESOURCE_GROUPS                                      | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | ROUTINES                                             | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | SCHEMATA                                             | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | ST_SPATIAL_REFERENCE_SYSTEMS                         | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | ST_UNITS_OF_MEASURE                                  | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | ST_GEOMETRY_COLUMNS                                  | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | STATISTICS                                           | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | TABLE_CONSTRAINTS                                    | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | TABLES                                               | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | TRIGGERS                                             | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | VIEW_ROUTINE_USAGE                                   | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | VIEW_TABLE_USAGE                                     | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | VIEWS                                                | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | COLUMN_PRIVILEGES                                    | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | ENGINES                                              | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | OPTIMIZER_TRACE                                      | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | PLUGINS                                              | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | PROCESSLIST                                          | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | PROFILING                                            | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | SCHEMA_PRIVILEGES                                    | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | TABLESPACES                                          | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | TABLE_PRIVILEGES                                     | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | USER_PRIVILEGES                                      | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:38 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | performance_schema | cond_instances                                       | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |        256 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_waits_current                                 | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |       1536 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_waits_history                                 | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |       2560 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_waits_history_long                            | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |       1000 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_waits_summary_by_host_by_event_name           | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |      70912 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_waits_summary_by_instance                     | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |       9728 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_waits_summary_by_thread_by_event_name         | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |     141824 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_waits_summary_by_user_by_event_name           | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |      70912 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_waits_summary_by_account_by_event_name        | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |      70912 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_waits_summary_global_by_event_name            | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |        554 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | file_instances                                       | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |       4096 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | file_summary_by_event_name                           | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |         80 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | file_summary_by_instance                             | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |       4096 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | host_cache                                           | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |        279 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | mutex_instances                                      | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |       3072 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | objects_summary_global_by_type                       | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |       5120 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | performance_timers                                   | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Fixed      |          4 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | rwlock_instances                                     | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |       2048 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | setup_actors                                         | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Fixed      |        128 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | setup_consumers                                      | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |         15 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | setup_instruments                                    | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |       1399 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | setup_objects                                        | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |        128 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | setup_threads                                        | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |        100 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | table_io_waits_summary_by_index_usage                | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |       8192 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | table_io_waits_summary_by_table                      | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |       4096 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | table_lock_waits_summary_by_table                    | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |       4096 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | threads                                              | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |        256 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_stages_current                                | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |        256 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_stages_history                                | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |       2560 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_stages_history_long                           | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |       1000 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_stages_summary_by_thread_by_event_name        | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |      44800 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_stages_summary_by_account_by_event_name       | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |      22400 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_stages_summary_by_user_by_event_name          | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |      22400 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_stages_summary_by_host_by_event_name          | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |      22400 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_stages_summary_global_by_event_name           | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |        175 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_statements_current                            | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |       2560 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_statements_history                            | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |       2560 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_statements_history_long                       | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |       1000 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_statements_summary_by_thread_by_event_name    | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |      55808 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_statements_summary_by_account_by_event_name   | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |      27904 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_statements_summary_by_user_by_event_name      | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |      27904 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_statements_summary_by_host_by_event_name      | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |      27904 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_statements_summary_global_by_event_name       | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |        218 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_statements_summary_by_digest                  | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |       5000 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_statements_summary_by_program                 | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |       1024 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_statements_histogram_global                   | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Fixed      |        450 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_statements_histogram_by_digest                | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |       5000 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_transactions_current                          | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |        256 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_transactions_history                          | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |       2560 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_transactions_history_long                     | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |       1000 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_transactions_summary_by_thread_by_event_name  | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |        256 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_transactions_summary_by_account_by_event_name | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |        128 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_transactions_summary_by_user_by_event_name    | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |        128 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_transactions_summary_by_host_by_event_name    | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |        128 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_transactions_summary_global_by_event_name     | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |          1 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_errors_summary_by_user_by_error               | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |     587904 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_errors_summary_by_host_by_error               | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |     587904 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_errors_summary_by_account_by_error            | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |     587904 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_errors_summary_by_thread_by_error             | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |    1175808 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | events_errors_summary_global_by_error                | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |       4593 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | users                                                | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Fixed      |        128 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | accounts                                             | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Fixed      |        128 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | hosts                                                | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Fixed      |        128 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | socket_instances                                     | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |        256 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | socket_summary_by_instance                           | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |        256 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | socket_summary_by_event_name                         | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |         10 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | session_connect_attrs                                | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |     131072 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_bin        |     NULL |                                       |                                         |
| def           | performance_schema | session_account_connect_attrs                        | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |     131072 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_bin        |     NULL |                                       |                                         |
| def           | performance_schema | keyring_keys                                         | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |         96 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_bin        |     NULL |                                       |                                         |
| def           | performance_schema | memory_summary_global_by_event_name                  | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |        450 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | memory_summary_by_account_by_event_name              | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |      57600 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | memory_summary_by_host_by_event_name                 | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |      57600 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | memory_summary_by_thread_by_event_name               | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |     115200 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | memory_summary_by_user_by_event_name                 | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |      57600 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | table_handles                                        | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |       1024 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | metadata_locks                                       | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |       1024 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | data_locks                                           | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |      99999 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | data_lock_waits                                      | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |      99999 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | replication_connection_configuration                 | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |        256 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | replication_group_members                            | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Fixed      |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | replication_connection_status                        | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |        256 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | replication_applier_configuration                    | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |        256 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | replication_applier_status                           | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Fixed      |        256 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | replication_applier_status_by_coordinator            | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |        256 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | replication_applier_status_by_worker                 | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |       8192 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | replication_group_member_stats                       | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | replication_applier_filters                          | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | replication_applier_global_filters                   | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | log_status                                           | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |          1 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | prepared_statements_instances                        | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |       1024 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | user_variables_by_thread                             | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |       2560 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | status_by_account                                    | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |      30976 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | status_by_host                                       | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |      30976 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | status_by_thread                                     | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |      61952 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | status_by_user                                       | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |      30976 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | global_status                                        | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |        242 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | session_status                                       | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |        242 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | variables_by_thread                                  | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |     151808 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | global_variables                                     | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |        593 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | session_variables                                    | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |        593 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | variables_info                                       | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |        593 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | persisted_variables                                  | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | performance_schema | user_defined_functions                               | BASE TABLE  | PERFORMANCE_SCHEMA |      10 | Dynamic    |          5 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:39 | NULL        | NULL       | utf8mb4_0900_ai_ci |     NULL |                                       |                                         |
| def           | mysql              | db                                                   | BASE TABLE  | InnoDB             |      10 | Dynamic    |          2 |           8192 |       16384 |               0 |        16384 |   5242880 |           NULL | 2019-11-19 05:46:48 | NULL        | NULL       | utf8_bin           |     NULL | row_format=DYNAMIC stats_persistent=0 | Database privileges                     |
| def           | mysql              | user                                                 | BASE TABLE  | InnoDB             |      10 | Dynamic    |          7 |           2340 |       16384 |               0 |            0 |   5242880 |           NULL | 2019-11-19 05:46:48 | NULL        | NULL       | utf8_bin           |     NULL | row_format=DYNAMIC stats_persistent=0 | Users and global privileges             |
| def           | mysql              | default_roles                                        | BASE TABLE  | InnoDB             |      10 | Dynamic    |          0 |              0 |       16384 |               0 |            0 |   5242880 |           NULL | 2019-11-19 05:46:48 | NULL        | NULL       | utf8_bin           |     NULL | row_format=DYNAMIC stats_persistent=0 | Default roles                           |
| def           | mysql              | role_edges                                           | BASE TABLE  | InnoDB             |      10 | Dynamic    |          0 |              0 |       16384 |               0 |            0 |   5242880 |           NULL | 2019-11-19 05:46:48 | NULL        | NULL       | utf8_bin           |     NULL | row_format=DYNAMIC stats_persistent=0 | Role hierarchy and role grants          |
| def           | mysql              | global_grants                                        | BASE TABLE  | InnoDB             |      10 | Dynamic    |         30 |            546 |       16384 |               0 |            0 |   5242880 |           NULL | 2019-11-19 05:46:48 | NULL        | NULL       | utf8_bin           |     NULL | row_format=DYNAMIC stats_persistent=0 | Extended global grants                  |
| def           | mysql              | password_history                                     | BASE TABLE  | InnoDB             |      10 | Dynamic    |          0 |              0 |       16384 |               0 |            0 |   5242880 |           NULL | 2019-11-19 05:46:48 | NULL        | NULL       | utf8_bin           |     NULL | row_format=DYNAMIC stats_persistent=0 | Password history for user accounts      |
| def           | mysql              | func                                                 | BASE TABLE  | InnoDB             |      10 | Dynamic    |          0 |              0 |       16384 |               0 |            0 |   5242880 |           NULL | 2019-11-19 05:46:48 | NULL        | NULL       | utf8_bin           |     NULL | row_format=DYNAMIC stats_persistent=0 | User defined functions                  |
| def           | mysql              | plugin                                               | BASE TABLE  | InnoDB             |      10 | Dynamic    |          0 |              0 |       16384 |               0 |            0 |   5242880 |           NULL | 2019-11-19 05:46:48 | NULL        | NULL       | utf8_general_ci    |     NULL | row_format=DYNAMIC stats_persistent=0 | MySQL plugins                           |
| def           | mysql              | help_topic                                           | BASE TABLE  | InnoDB             |      10 | Dynamic    |        847 |           1876 |     1589248 |               0 |       131072 |   5242880 |           NULL | 2019-11-19 05:46:48 | NULL        | NULL       | utf8_general_ci    |     NULL | row_format=DYNAMIC stats_persistent=0 | help topics                             |
| def           | mysql              | help_category                                        | BASE TABLE  | InnoDB             |      10 | Dynamic    |         44 |            372 |       16384 |               0 |        16384 |   5242880 |           NULL | 2019-11-19 05:46:49 | NULL        | NULL       | utf8_general_ci    |     NULL | row_format=DYNAMIC stats_persistent=0 | help categories                         |
| def           | mysql              | help_relation                                        | BASE TABLE  | InnoDB             |      10 | Dynamic    |       1416 |             57 |       81920 |               0 |            0 |   5242880 |           NULL | 2019-11-19 05:46:49 | NULL        | NULL       | utf8_general_ci    |     NULL | row_format=DYNAMIC stats_persistent=0 | keyword-topic relation                  |
| def           | mysql              | servers                                              | BASE TABLE  | InnoDB             |      10 | Dynamic    |          0 |              0 |       16384 |               0 |            0 |   5242880 |           NULL | 2019-11-19 05:46:49 | NULL        | NULL       | utf8_general_ci    |     NULL | row_format=DYNAMIC stats_persistent=0 | MySQL Foreign Servers table             |
| def           | mysql              | tables_priv                                          | BASE TABLE  | InnoDB             |      10 | Dynamic    |          4 |           4096 |       16384 |               0 |        16384 |   5242880 |           NULL | 2019-11-19 05:46:49 | NULL        | NULL       | utf8_bin           |     NULL | row_format=DYNAMIC stats_persistent=0 | Table privileges                        |
| def           | mysql              | columns_priv                                         | BASE TABLE  | InnoDB             |      10 | Dynamic    |          0 |              0 |       16384 |               0 |            0 |   5242880 |           NULL | 2019-11-19 05:46:49 | NULL        | NULL       | utf8_bin           |     NULL | row_format=DYNAMIC stats_persistent=0 | Column privileges                       |
| def           | mysql              | help_keyword                                         | BASE TABLE  | InnoDB             |      10 | Dynamic    |        816 |            120 |       98304 |               0 |        98304 |   5242880 |           NULL | 2019-11-19 05:46:49 | NULL        | NULL       | utf8_general_ci    |     NULL | row_format=DYNAMIC stats_persistent=0 | help keywords                           |
| def           | mysql              | time_zone_name                                       | BASE TABLE  | InnoDB             |      10 | Dynamic    |          0 |              0 |       16384 |               0 |            0 |   5242880 |           NULL | 2019-11-19 05:46:49 | NULL        | NULL       | utf8_general_ci    |     NULL | row_format=DYNAMIC stats_persistent=0 | Time zone names                         |
| def           | mysql              | time_zone                                            | BASE TABLE  | InnoDB             |      10 | Dynamic    |          0 |              0 |       16384 |               0 |            0 |   5242880 |              1 | 2019-11-19 05:46:49 | NULL        | NULL       | utf8_general_ci    |     NULL | row_format=DYNAMIC stats_persistent=0 | Time zones                              |
| def           | mysql              | time_zone_transition                                 | BASE TABLE  | InnoDB             |      10 | Dynamic    |          0 |              0 |       16384 |               0 |            0 |   5242880 |           NULL | 2019-11-19 05:46:49 | NULL        | NULL       | utf8_general_ci    |     NULL | row_format=DYNAMIC stats_persistent=0 | Time zone transitions                   |
| def           | mysql              | time_zone_transition_type                            | BASE TABLE  | InnoDB             |      10 | Dynamic    |          0 |              0 |       16384 |               0 |            0 |   5242880 |           NULL | 2019-11-19 05:46:49 | NULL        | NULL       | utf8_general_ci    |     NULL | row_format=DYNAMIC stats_persistent=0 | Time zone transition types              |
| def           | mysql              | time_zone_leap_second                                | BASE TABLE  | InnoDB             |      10 | Dynamic    |          0 |              0 |       16384 |               0 |            0 |   5242880 |           NULL | 2019-11-19 05:46:49 | NULL        | NULL       | utf8_general_ci    |     NULL | row_format=DYNAMIC stats_persistent=0 | Leap seconds information for time zones |
| def           | mysql              | procs_priv                                           | BASE TABLE  | InnoDB             |      10 | Dynamic    |          0 |              0 |       16384 |               0 |        16384 |   5242880 |           NULL | 2019-11-19 05:46:49 | NULL        | NULL       | utf8_bin           |     NULL | row_format=DYNAMIC stats_persistent=0 | Procedure privileges                    |
| def           | mysql              | general_log                                          | BASE TABLE  | CSV                |      10 | Dynamic    |          2 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:49 | NULL        | NULL       | utf8_general_ci    |     NULL |                                       | General log                             |
| def           | mysql              | slow_log                                             | BASE TABLE  | CSV                |      10 | Dynamic    |          2 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:46:49 | NULL        | NULL       | utf8_general_ci    |     NULL |                                       | Slow log                                |
| def           | mysql              | component                                            | BASE TABLE  | InnoDB             |      10 | Dynamic    |          0 |              0 |       16384 |               0 |            0 |   5242880 |              1 | 2019-11-19 05:46:49 | NULL        | NULL       | utf8_general_ci    |     NULL | row_format=DYNAMIC                    | Components                              |
| def           | mysql              | slave_relay_log_info                                 | BASE TABLE  | InnoDB             |      10 | Dynamic    |          0 |              0 |       16384 |               0 |            0 |   5242880 |           NULL | 2019-11-19 05:46:49 | NULL        | NULL       | utf8_general_ci    |     NULL | row_format=DYNAMIC stats_persistent=0 | Relay Log Information                   |
| def           | mysql              | slave_master_info                                    | BASE TABLE  | InnoDB             |      10 | Dynamic    |          0 |              0 |       16384 |               0 |            0 |   5242880 |           NULL | 2019-11-19 05:46:49 | NULL        | NULL       | utf8_general_ci    |     NULL | row_format=DYNAMIC stats_persistent=0 | Master Information                      |
| def           | mysql              | slave_worker_info                                    | BASE TABLE  | InnoDB             |      10 | Dynamic    |          0 |              0 |       16384 |               0 |            0 |   5242880 |           NULL | 2019-11-19 05:46:49 | NULL        | NULL       | utf8_general_ci    |     NULL | row_format=DYNAMIC stats_persistent=0 | Worker Information                      |
| def           | mysql              | gtid_executed                                        | BASE TABLE  | InnoDB             |      10 | Dynamic    |          0 |              0 |       16384 |               0 |            0 |   5242880 |           NULL | 2019-11-19 05:46:49 | NULL        | NULL       | utf8mb4_general_ci |     NULL | row_format=DYNAMIC                    |                                         |
| def           | mysql              | server_cost                                          | BASE TABLE  | InnoDB             |      10 | Dynamic    |          6 |           2730 |       16384 |               0 |            0 |   5242880 |           NULL | 2019-11-19 05:46:49 | NULL        | NULL       | utf8_general_ci    |     NULL | row_format=DYNAMIC stats_persistent=0 |                                         |
| def           | mysql              | engine_cost                                          | BASE TABLE  | InnoDB             |      10 | Dynamic    |          2 |           8192 |       16384 |               0 |            0 |   5242880 |           NULL | 2019-11-19 05:46:49 | NULL        | NULL       | utf8_general_ci    |     NULL | row_format=DYNAMIC stats_persistent=0 |                                         |
| def           | mysql              | proxies_priv                                         | BASE TABLE  | InnoDB             |      10 | Dynamic    |          1 |          16384 |       16384 |               0 |        16384 |   5242880 |           NULL | 2019-11-19 05:46:49 | NULL        | NULL       | utf8_bin           |     NULL | row_format=DYNAMIC stats_persistent=0 | User proxy privileges                   |
| def           | information_schema | INNODB_CMP                                           | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:47:21 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | INNODB_CMPMEM                                        | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:47:21 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | INNODB_TRX                                           | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:47:21 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | INNODB_CMP_PER_INDEX                                 | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:47:21 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | INNODB_CMP_RESET                                     | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:47:21 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | INNODB_FT_DELETED                                    | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:47:21 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | INNODB_BUFFER_POOL_STATS                             | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:47:21 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | INNODB_CMPMEM_RESET                                  | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:47:21 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | INNODB_BUFFER_PAGE_LRU                               | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:47:21 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | INNODB_FT_INDEX_CACHE                                | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:47:21 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | INNODB_CMP_PER_INDEX_RESET                           | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:47:21 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | INNODB_FT_CONFIG                                     | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:47:21 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | INNODB_BUFFER_PAGE                                   | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:47:21 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | INNODB_TABLESPACES                                   | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:47:21 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | INNODB_FT_BEING_DELETED                              | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:47:21 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | INNODB_TEMP_TABLE_INFO                               | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:47:21 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | INNODB_FT_DEFAULT_STOPWORD                           | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:47:21 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | INNODB_METRICS                                       | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:47:21 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | INNODB_FT_INDEX_TABLE                                | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:47:21 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | INNODB_TABLES                                        | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:47:21 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | INNODB_TABLESTATS                                    | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:47:21 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | INNODB_INDEXES                                       | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:47:21 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | INNODB_COLUMNS                                       | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:47:21 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | INNODB_VIRTUAL                                       | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:47:21 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | INNODB_CACHED_INDEXES                                | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:47:21 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | information_schema | INNODB_SESSION_TEMP_TABLESPACES                      | SYSTEM VIEW | NULL               |      10 | NULL       |          0 |              0 |           0 |               0 |            0 |         0 |           NULL | 2019-11-19 05:47:21 | NULL        | NULL       | NULL               |     NULL |                                       |                                         |
| def           | pet_system         | customer                                             | BASE TABLE  | InnoDB             |      10 | Dynamic    |          5 |           3276 |       16384 |               0 |        16384 |         0 |              5 | 2020-02-17 12:38:07 | NULL        | NULL       | utf8_general_ci    |     NULL |                                       |                                         |
| def           | pet_system         | petstore                                             | BASE TABLE  | InnoDB             |      10 | Dynamic    |          5 |           3276 |       16384 |               0 |            0 |         0 |              5 | 2020-02-17 12:39:37 | NULL        | NULL       | utf8_general_ci    |     NULL |                                       |                                         |
| def           | pet_system         | pet                                                  | BASE TABLE  | InnoDB             |      10 | Dynamic    |          5 |           3276 |       16384 |               0 |        81920 |         0 |              5 | 2020-02-17 12:40:31 | NULL        | NULL       | utf8_general_ci    |     NULL |                                       |                                         |
| def           | ooa19a             | attorney                                             | BASE TABLE  | InnoDB             |      10 | Dynamic    |          0 |              0 |       16384 |               0 |        16384 |         0 |           NULL | 2020-02-24 15:42:48 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                                       |                                         |
| def           | ooa19a             | court                                                | BASE TABLE  | InnoDB             |      10 | Dynamic    |          0 |              0 |       16384 |               0 |            0 |         0 |           NULL | 2020-02-24 15:44:59 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                                       |                                         |
| def           | ooa19a             | judge                                                | BASE TABLE  | InnoDB             |      10 | Dynamic    |          0 |              0 |       16384 |               0 |        32768 |         0 |           NULL | 2020-02-24 15:45:22 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                                       |                                         |
| def           | ooa19a             | judge_hist                                           | BASE TABLE  | InnoDB             |      10 | Dynamic    |          0 |              0 |       16384 |               0 |        16384 |         0 |           NULL | 2020-02-24 15:46:04 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                                       |                                         |
| def           | ooa19a             | case                                                 | BASE TABLE  | InnoDB             |      10 | Dynamic    |          0 |              0 |       16384 |               0 |        16384 |         0 |           NULL | 2020-02-24 15:46:42 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                                       |                                         |
| def           | ooa19a             | bar                                                  | BASE TABLE  | InnoDB             |      10 | Dynamic    |          0 |              0 |       16384 |               0 |        16384 |         0 |           NULL | 2020-02-24 15:47:01 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                                       |                                         |
| def           | ooa19a             | speciality                                           | BASE TABLE  | InnoDB             |      10 | Dynamic    |          0 |              0 |       16384 |               0 |        16384 |         0 |           NULL | 2020-02-24 15:47:13 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                                       |                                         |
| def           | ooa19a             | client                                               | BASE TABLE  | InnoDB             |      10 | Dynamic    |          5 |           3276 |       16384 |               0 |        16384 |         0 |           NULL | 2020-02-24 15:50:49 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                                       |                                         |
| def           | ooa19a             | assignment                                           | BASE TABLE  | InnoDB             |      10 | Dynamic    |          0 |              0 |       16384 |               0 |        65536 |         0 |           NULL | 2020-02-24 15:52:11 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                                       |                                         |
| def           | ooa19a             | phone                                                | BASE TABLE  | InnoDB             |      10 | Dynamic    |         13 |           1260 |       16384 |               0 |        16384 |         0 |             13 | 2020-02-24 15:53:18 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                                       |                                         |
| def           | ooa19a             | person                                               | BASE TABLE  | InnoDB             |      10 | Dynamic    |         15 |           1092 |       16384 |               0 |        16384 |         0 |             15 | 2020-02-29 13:48:33 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                                       |                                         |
+---------------+--------------------+------------------------------------------------------+-------------+--------------------+---------+------------+------------+----------------+-------------+-----------------+--------------+-----------+----------------+---------------------+-------------+------------+--------------------+----------+---------------------------------------+-----------------------------------------+
217 rows in set (0.13 sec)

mysql> select * from tables where table_schema = ooa19a;
ERROR 1054 (42S22): Unknown column 'ooa19a' in 'where clause'
mysql> select * from tables where table_name = ooa19a;
ERROR 1054 (42S22): Unknown column 'ooa19a' in 'where clause'
mysql> select * from tables where table_name = client;
ERROR 1054 (42S22): Unknown column 'client' in 'where clause'
mysql> select * from tables where table_name = 'ooa19a';
Empty set (0.00 sec)

mysql> select * from tables where table_name = 'client';
+---------------+--------------+------------+------------+--------+---------+------------+------------+----------------+-------------+-----------------+--------------+-----------+----------------+---------------------+-------------+------------+--------------------+----------+----------------+---------------+
| TABLE_CATALOG | TABLE_SCHEMA | TABLE_NAME | TABLE_TYPE | ENGINE | VERSION | ROW_FORMAT | TABLE_ROWS | AVG_ROW_LENGTH | DATA_LENGTH | MAX_DATA_LENGTH | INDEX_LENGTH | DATA_FREE | AUTO_INCREMENT | CREATE_TIME         | UPDATE_TIME | CHECK_TIME | TABLE_COLLATION    | CHECKSUM | CREATE_OPTIONS | TABLE_COMMENT |
+---------------+--------------+------------+------------+--------+---------+------------+------------+----------------+-------------+-----------------+--------------+-----------+----------------+---------------------+-------------+------------+--------------------+----------+----------------+---------------+
| def           | ooa19a       | client     | BASE TABLE | InnoDB |      10 | Dynamic    |          5 |           3276 |       16384 |               0 |        16384 |         0 |           NULL | 2020-02-24 15:50:49 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
+---------------+--------------+------------+------------+--------+---------+------------+------------+----------------+-------------+-----------------+--------------+-----------+----------------+---------------------+-------------+------------+--------------------+----------+----------------+---------------+
1 row in set (0.00 sec)

mysql> select * from tables where table_schema = 'ooa19a';
+---------------+--------------+------------+------------+--------+---------+------------+------------+----------------+-------------+-----------------+--------------+-----------+----------------+---------------------+-------------+------------+--------------------+----------+----------------+---------------+
| TABLE_CATALOG | TABLE_SCHEMA | TABLE_NAME | TABLE_TYPE | ENGINE | VERSION | ROW_FORMAT | TABLE_ROWS | AVG_ROW_LENGTH | DATA_LENGTH | MAX_DATA_LENGTH | INDEX_LENGTH | DATA_FREE | AUTO_INCREMENT | CREATE_TIME         | UPDATE_TIME | CHECK_TIME | TABLE_COLLATION    | CHECKSUM | CREATE_OPTIONS | TABLE_COMMENT |
+---------------+--------------+------------+------------+--------+---------+------------+------------+----------------+-------------+-----------------+--------------+-----------+----------------+---------------------+-------------+------------+--------------------+----------+----------------+---------------+
| def           | ooa19a       | assignment | BASE TABLE | InnoDB |      10 | Dynamic    |          0 |              0 |       16384 |               0 |        65536 |         0 |           NULL | 2020-02-24 15:52:11 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
| def           | ooa19a       | attorney   | BASE TABLE | InnoDB |      10 | Dynamic    |          0 |              0 |       16384 |               0 |        16384 |         0 |           NULL | 2020-02-24 15:42:48 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
| def           | ooa19a       | bar        | BASE TABLE | InnoDB |      10 | Dynamic    |          0 |              0 |       16384 |               0 |        16384 |         0 |           NULL | 2020-02-24 15:47:01 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
| def           | ooa19a       | case       | BASE TABLE | InnoDB |      10 | Dynamic    |          0 |              0 |       16384 |               0 |        16384 |         0 |           NULL | 2020-02-24 15:46:42 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
| def           | ooa19a       | client     | BASE TABLE | InnoDB |      10 | Dynamic    |          5 |           3276 |       16384 |               0 |        16384 |         0 |           NULL | 2020-02-24 15:50:49 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
| def           | ooa19a       | court      | BASE TABLE | InnoDB |      10 | Dynamic    |          0 |              0 |       16384 |               0 |            0 |         0 |           NULL | 2020-02-24 15:44:59 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
| def           | ooa19a       | judge      | BASE TABLE | InnoDB |      10 | Dynamic    |          0 |              0 |       16384 |               0 |        32768 |         0 |           NULL | 2020-02-24 15:45:22 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
| def           | ooa19a       | judge_hist | BASE TABLE | InnoDB |      10 | Dynamic    |          0 |              0 |       16384 |               0 |        16384 |         0 |           NULL | 2020-02-24 15:46:04 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
| def           | ooa19a       | person     | BASE TABLE | InnoDB |      10 | Dynamic    |         15 |           1092 |       16384 |               0 |        16384 |         0 |             15 | 2020-02-29 13:48:33 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
| def           | ooa19a       | phone      | BASE TABLE | InnoDB |      10 | Dynamic    |         13 |           1260 |       16384 |               0 |        16384 |         0 |             13 | 2020-02-24 15:53:18 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
| def           | ooa19a       | speciality | BASE TABLE | InnoDB |      10 | Dynamic    |          0 |              0 |       16384 |               0 |        16384 |         0 |           NULL | 2020-02-24 15:47:13 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
+---------------+--------------+------------+------------+--------+---------+------------+------------+----------------+-------------+-----------------+--------------+-----------+----------------+---------------------+-------------+------------+--------------------+----------+----------------+---------------+
11 rows in set (0.00 sec)

mysql> use ooa19a
Database changed
mysql> select * from judge;
+--------+--------+------------+-----------------------+-----------+
| per_id | crt_id | jud_salary | jud_years_in_practice | jud_notes |
+--------+--------+------------+-----------------------+-----------+
|     11 |      1 |  150000.00 |                    12 | Judge 1   |
|     12 |      2 |  140000.00 |                    10 | Judge 2   |
|     13 |      3 |  100000.00 |                    15 | Judge 3   |
|     14 |      4 |   95000.00 |                    12 | Judge 4   |
|     15 |      5 |  120000.00 |                    11 | Judge 5   |
+--------+--------+------------+-----------------------+-----------+
5 rows in set (0.00 sec)

mysql> start transaction;
Query OK, 0 rows affected (0.00 sec)

mysql> 
mysql> Insert into judge_hist
    -> values
    -> (null, 11,3,'2009-1-16','i',130000,null),
    -> (null, 12,3,'2009-1-16','i',140000,null),
    -> (null, 13,3,'2009-1-16','i',165000,null),
    -> (null, 11,3,'2009-1-16','i',135000,'freshman justice'),
    -> (null, 14,3,'2009-1-16','i',165000, 'became chief justice'),
    -> (null, 15,3,'2009-4-18','i',170000, 'reassigned to court based upon local area population');
Query OK, 6 rows affected (0.01 sec)
Records: 6  Duplicates: 0  Warnings: 0

mysql> commit;
Query OK, 0 rows affected (0.00 sec)

mysql> select * from judge_hist;
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
| jhs_id | per_id | jhs_crt_id | jhs_date            | jhs_type | jhs_salary | jhs_notes                                            |
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
|      1 |     11 |          3 | 2009-01-16 00:00:00 | i        |  130000.00 | NULL                                                 |
|      2 |     12 |          3 | 2009-01-16 00:00:00 | i        |  140000.00 | NULL                                                 |
|      3 |     13 |          3 | 2009-01-16 00:00:00 | i        |  165000.00 | NULL                                                 |
|      4 |     11 |          3 | 2009-01-16 00:00:00 | i        |  135000.00 | freshman justice                                     |
|      5 |     14 |          3 | 2009-01-16 00:00:00 | i        |  165000.00 | became chief justice                                 |
|      6 |     15 |          3 | 2009-04-18 00:00:00 | i        |  170000.00 | reassigned to court based upon local area population |
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
6 rows in set (0.00 sec)

mysql> decribe `case`;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'decribe `case`' at line 1
mysql> describe `case`;
+-----------------+----------------------+------+-----+---------+----------------+
| Field           | Type                 | Null | Key | Default | Extra          |
+-----------------+----------------------+------+-----+---------+----------------+
| cse_id          | smallint(5) unsigned | NO   | PRI | NULL    | auto_increment |
| per_id          | smallint(5) unsigned | NO   | MUL | NULL    |                |
| cse_type        | varchar(45)          | NO   |     | NULL    |                |
| cse_description | text                 | NO   |     | NULL    |                |
| cse_start_date  | date                 | NO   |     | NULL    |                |
| cse_end_date    | date                 | YES  |     | NULL    |                |
| cse_notes       | varchar(255)         | YES  |     | NULL    |                |
+-----------------+----------------------+------+-----+---------+----------------+
7 rows in set (0.00 sec)

mysql> start transaction;
Query OK, 0 rows affected (0.00 sec)

mysql> 
mysql> insert into `case`
    -> values
    -> (null, 13, 'civil', 'client says that his logo is being used without his content to promote a rival business', '2010-09-28',null,'copyright infringement'),
    -> (null, 14, 'criminal', 'client is charged of assulting her husband', '2010-12-28',null,'Assult'),
    -> (null, 11, 'criminal', 'client broke an ankle while shopping at a local grocery store. No wet floor sign was posted although the floor had jsut being mopped', '2010-09-28','2010-12-24','slip and fall'),
    -> (null, 12, 'civil', 'client is charged with stealing of several television from his former place of employment. Client has a solid alibi', '2011-09-28',null,'grand theft'),
    -> (null, 13, 'civil', 'client says that his logo is being used without his content to promote a rival business', '2010-09-28',null,'copyright infringement'),
    -> (null, 15, 'criminal', 'client is charged with the murder of his co-worker over a lovers fued. Client has no alibi', '2010-03-12',null,'murder'),
    -> (null, 13, 'civil', 'client had chosen a degree other than IT and had to decleared bankruptcy.', '2010-09-28','2013-02-12','Bankruptcy');
Query OK, 7 rows affected (0.01 sec)
Records: 7  Duplicates: 0  Warnings: 0

mysql> 
mysql> commit;
Query OK, 0 rows affected (0.00 sec)

mysql> select * from `case`;
+--------+--------+----------+--------------------------------------------------------------------------------------------------------------------------------------+----------------+--------------+------------------------+
| cse_id | per_id | cse_type | cse_description                                                                                                                      | cse_start_date | cse_end_date | cse_notes              |
+--------+--------+----------+--------------------------------------------------------------------------------------------------------------------------------------+----------------+--------------+------------------------+
|      1 |     13 | civil    | client says that his logo is being used without his content to promote a rival business                                              | 2010-09-28     | NULL         | copyright infringement |
|      2 |     14 | criminal | client is charged of assulting her husband                                                                                           | 2010-12-28     | NULL         | Assult                 |
|      3 |     11 | criminal | client broke an ankle while shopping at a local grocery store. No wet floor sign was posted although the floor had jsut being mopped | 2010-09-28     | 2010-12-24   | slip and fall          |
|      4 |     12 | civil    | client is charged with stealing of several television from his former place of employment. Client has a solid alibi                  | 2011-09-28     | NULL         | grand theft            |
|      5 |     13 | civil    | client says that his logo is being used without his content to promote a rival business                                              | 2010-09-28     | NULL         | copyright infringement |
|      6 |     15 | criminal | client is charged with the murder of his co-worker over a lovers fued. Client has no alibi                                           | 2010-03-12     | NULL         | murder                 |
|      7 |     13 | civil    | client had chosen a degree other than IT and had to decleared bankruptcy.                                                            | 2010-09-28     | 2013-02-12   | Bankruptcy             |
+--------+--------+----------+--------------------------------------------------------------------------------------------------------------------------------------+----------------+--------------+------------------------+
7 rows in set (0.00 sec)

mysql> update table `case`
    -> set cse_description = 'Client is charged with posession of 10 grams of cocaine, allgegedly found in his glove box by state police'
    -> where cse_id = 5;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'table `case`
set cse_description = 'Client is charged with posession of 10 grams' at line 1
mysql> update table `case`
    -> set cse_description = 'Client is charged with posession of 10 grams of cocaine, allgegedly found in his glove box by state police',
    -> where cse_id = 5;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'table `case`
set cse_description = 'Client is charged with posession of 10 grams' at line 1
mysql> update `case`
    -> set cse_description = 'Client is charged with posession of 10 grams of cocaine, allgegedly found in his glove box by state police',
    -> where cse_id = 5;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'where cse_id = 5' at line 3
mysql> select * from `case`;
+--------+--------+----------+--------------------------------------------------------------------------------------------------------------------------------------+----------------+--------------+------------------------+
| cse_id | per_id | cse_type | cse_description                                                                                                                      | cse_start_date | cse_end_date | cse_notes              |
+--------+--------+----------+--------------------------------------------------------------------------------------------------------------------------------------+----------------+--------------+------------------------+
|      1 |     13 | civil    | client says that his logo is being used without his content to promote a rival business                                              | 2010-09-28     | NULL         | copyright infringement |
|      2 |     14 | criminal | client is charged of assulting her husband                                                                                           | 2010-12-28     | NULL         | Assult                 |
|      3 |     11 | criminal | client broke an ankle while shopping at a local grocery store. No wet floor sign was posted although the floor had jsut being mopped | 2010-09-28     | 2010-12-24   | slip and fall          |
|      4 |     12 | civil    | client is charged with stealing of several television from his former place of employment. Client has a solid alibi                  | 2011-09-28     | NULL         | grand theft            |
|      5 |     13 | civil    | client says that his logo is being used without his content to promote a rival business                                              | 2010-09-28     | NULL         | copyright infringement |
|      6 |     15 | criminal | client is charged with the murder of his co-worker over a lovers fued. Client has no alibi                                           | 2010-03-12     | NULL         | murder                 |
|      7 |     13 | civil    | client had chosen a degree other than IT and had to decleared bankruptcy.                                                            | 2010-09-28     | 2013-02-12   | Bankruptcy             |
+--------+--------+----------+--------------------------------------------------------------------------------------------------------------------------------------+----------------+--------------+------------------------+
7 rows in set (0.00 sec)

mysql> update `case`
    -> set 
    -> cse_description = 'client is charged with posession of 10 grams of cocaine, allegedely found in his glove box by state police'
    -> where
    -> cse_id = 5;
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> select * from `client`;
+--------+-----------+
| per_id | cli_notes |
+--------+-----------+
|      1 | NULL      |
|      2 | NULL      |
|      3 | NULL      |
|      4 | NULL      |
|      5 | NULL      |
+--------+-----------+
5 rows in set (0.00 sec)

mysql> select * from `case`;
+--------+--------+----------+--------------------------------------------------------------------------------------------------------------------------------------+----------------+--------------+------------------------+
| cse_id | per_id | cse_type | cse_description                                                                                                                      | cse_start_date | cse_end_date | cse_notes              |
+--------+--------+----------+--------------------------------------------------------------------------------------------------------------------------------------+----------------+--------------+------------------------+
|      1 |     13 | civil    | client says that his logo is being used without his content to promote a rival business                                              | 2010-09-28     | NULL         | copyright infringement |
|      2 |     14 | criminal | client is charged of assulting her husband                                                                                           | 2010-12-28     | NULL         | Assult                 |
|      3 |     11 | criminal | client broke an ankle while shopping at a local grocery store. No wet floor sign was posted although the floor had jsut being mopped | 2010-09-28     | 2010-12-24   | slip and fall          |
|      4 |     12 | civil    | client is charged with stealing of several television from his former place of employment. Client has a solid alibi                  | 2011-09-28     | NULL         | grand theft            |
|      5 |     13 | civil    | client is charged with posession of 10 grams of cocaine, allegedely found in his glove box by state police                           | 2010-09-28     | NULL         | copyright infringement |
|      6 |     15 | criminal | client is charged with the murder of his co-worker over a lovers fued. Client has no alibi                                           | 2010-03-12     | NULL         | murder                 |
|      7 |     13 | civil    | client had chosen a degree other than IT and had to decleared bankruptcy.                                                            | 2010-09-28     | 2013-02-12   | Bankruptcy             |
+--------+--------+----------+--------------------------------------------------------------------------------------------------------------------------------------+----------------+--------------+------------------------+
7 rows in set (0.00 sec)

mysql> update `case`
    -> cse_notes = 'posession of narcotics'
    -> where
    -> cse_id = 5;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '= 'posession of narcotics'
where
cse_id = 5' at line 2
mysql> update `case`
    -> set
    -> cse_notes = 'posession of narcotics'
    -> where
    -> cse_id = 5;
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> select * from `case`;
+--------+--------+----------+--------------------------------------------------------------------------------------------------------------------------------------+----------------+--------------+------------------------+
| cse_id | per_id | cse_type | cse_description                                                                                                                      | cse_start_date | cse_end_date | cse_notes              |
+--------+--------+----------+--------------------------------------------------------------------------------------------------------------------------------------+----------------+--------------+------------------------+
|      1 |     13 | civil    | client says that his logo is being used without his content to promote a rival business                                              | 2010-09-28     | NULL         | copyright infringement |
|      2 |     14 | criminal | client is charged of assulting her husband                                                                                           | 2010-12-28     | NULL         | Assult                 |
|      3 |     11 | criminal | client broke an ankle while shopping at a local grocery store. No wet floor sign was posted although the floor had jsut being mopped | 2010-09-28     | 2010-12-24   | slip and fall          |
|      4 |     12 | civil    | client is charged with stealing of several television from his former place of employment. Client has a solid alibi                  | 2011-09-28     | NULL         | grand theft            |
|      5 |     13 | civil    | client is charged with posession of 10 grams of cocaine, allegedely found in his glove box by state police                           | 2010-09-28     | NULL         | posession of narcotics |
|      6 |     15 | criminal | client is charged with the murder of his co-worker over a lovers fued. Client has no alibi                                           | 2010-03-12     | NULL         | murder                 |
|      7 |     13 | civil    | client had chosen a degree other than IT and had to decleared bankruptcy.                                                            | 2010-09-28     | 2013-02-12   | Bankruptcy             |
+--------+--------+----------+--------------------------------------------------------------------------------------------------------------------------------------+----------------+--------------+------------------------+
7 rows in set (0.00 sec)

mysql> select * from `information_schema`.`tables` where table_schema = 'ooa19a';
+---------------+--------------+------------+------------+--------+---------+------------+------------+----------------+-------------+-----------------+--------------+-----------+----------------+---------------------+-------------+------------+--------------------+----------+----------------+---------------+
| TABLE_CATALOG | TABLE_SCHEMA | TABLE_NAME | TABLE_TYPE | ENGINE | VERSION | ROW_FORMAT | TABLE_ROWS | AVG_ROW_LENGTH | DATA_LENGTH | MAX_DATA_LENGTH | INDEX_LENGTH | DATA_FREE | AUTO_INCREMENT | CREATE_TIME         | UPDATE_TIME | CHECK_TIME | TABLE_COLLATION    | CHECKSUM | CREATE_OPTIONS | TABLE_COMMENT |
+---------------+--------------+------------+------------+--------+---------+------------+------------+----------------+-------------+-----------------+--------------+-----------+----------------+---------------------+-------------+------------+--------------------+----------+----------------+---------------+
| def           | ooa19a       | assignment | BASE TABLE | InnoDB |      10 | Dynamic    |          0 |              0 |       16384 |               0 |        65536 |         0 |           NULL | 2020-02-24 15:52:11 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
| def           | ooa19a       | attorney   | BASE TABLE | InnoDB |      10 | Dynamic    |          0 |              0 |       16384 |               0 |        16384 |         0 |           NULL | 2020-02-24 15:42:48 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
| def           | ooa19a       | bar        | BASE TABLE | InnoDB |      10 | Dynamic    |          0 |              0 |       16384 |               0 |        16384 |         0 |           NULL | 2020-02-24 15:47:01 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
| def           | ooa19a       | case       | BASE TABLE | InnoDB |      10 | Dynamic    |          0 |              0 |       16384 |               0 |        16384 |         0 |           NULL | 2020-02-24 15:46:42 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
| def           | ooa19a       | client     | BASE TABLE | InnoDB |      10 | Dynamic    |          5 |           3276 |       16384 |               0 |        16384 |         0 |           NULL | 2020-02-24 15:50:49 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
| def           | ooa19a       | court      | BASE TABLE | InnoDB |      10 | Dynamic    |          0 |              0 |       16384 |               0 |            0 |         0 |           NULL | 2020-02-24 15:44:59 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
| def           | ooa19a       | judge      | BASE TABLE | InnoDB |      10 | Dynamic    |          0 |              0 |       16384 |               0 |        32768 |         0 |           NULL | 2020-02-24 15:45:22 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
| def           | ooa19a       | judge_hist | BASE TABLE | InnoDB |      10 | Dynamic    |          0 |              0 |       16384 |               0 |        16384 |         0 |           NULL | 2020-02-24 15:46:04 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
| def           | ooa19a       | person     | BASE TABLE | InnoDB |      10 | Dynamic    |         15 |           1092 |       16384 |               0 |        16384 |         0 |             15 | 2020-02-29 13:48:33 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
| def           | ooa19a       | phone      | BASE TABLE | InnoDB |      10 | Dynamic    |         13 |           1260 |       16384 |               0 |        16384 |         0 |             13 | 2020-02-24 15:53:18 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
| def           | ooa19a       | speciality | BASE TABLE | InnoDB |      10 | Dynamic    |          0 |              0 |       16384 |               0 |        16384 |         0 |           NULL | 2020-02-24 15:47:13 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
+---------------+--------------+------------+------------+--------+---------+------------+------------+----------------+-------------+-----------------+--------------+-----------+----------------+---------------------+-------------+------------+--------------------+----------+----------------+---------------+
11 rows in set (0.00 sec)

mysql> start transaction;
Query OK, 0 rows affected (0.00 sec)

mysql> insert into assignment 
    -> values
    -> (null, 1,6,4, null),
    -> (null, 2,7,1, null),
    -> (null, 3,9,2, null),
    -> (null, 1,10,3, null),
    -> (null, 4,7,1, null),
    -> (null, 3,8,5, null),
    -> (null, 5,7,7, null),
    -> (null, 2,9,6, null),
    -> (null, 2,9,6, null),
    -> (null, 1,8,7, null);
ERROR 1062 (23000): Duplicate entry '2-9-6' for key 'ux_per_cid_per_aid_cse_id'
mysql> commit;
Query OK, 0 rows affected (0.00 sec)

mysql> start transaction;
Query OK, 0 rows affected (0.00 sec)

mysql> insert into assignment 
    -> values
    -> (null, 1,6,4, null),
    -> (null, 2,7,1, null),
    -> (null, 3,9,2, null),
    -> (null, 1,10,3, null),
    -> (null, 4,7,1, null),
    -> (null, 3,8,5, null),
    -> (null, 5,7,7, null),
    -> (null, 1,9,6, null),
    -> (null, 2,9,6, null),
    -> (null, 1,8,7, null);
Query OK, 10 rows affected (0.00 sec)
Records: 10  Duplicates: 0  Warnings: 0

mysql> commit;
Query OK, 0 rows affected (0.00 sec)

mysql> select * from assignment;
+--------+---------+---------+--------+-----------+
| asn_id | per_cid | per_aid | cse_id | asn_notes |
+--------+---------+---------+--------+-----------+
|     11 |       1 |       6 |      4 | NULL      |
|     12 |       2 |       7 |      1 | NULL      |
|     13 |       3 |       9 |      2 | NULL      |
|     14 |       1 |      10 |      3 | NULL      |
|     15 |       4 |       7 |      1 | NULL      |
|     16 |       3 |       8 |      5 | NULL      |
|     17 |       5 |       7 |      7 | NULL      |
|     18 |       1 |       9 |      6 | NULL      |
|     19 |       2 |       9 |      6 | NULL      |
|     20 |       1 |       8 |      7 | NULL      |
+--------+---------+---------+--------+-----------+
10 rows in set (0.00 sec)

mysql> select * from person;
+--------+------------------------------------------------------------------+------------------------------------------------------------------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
| per_id | per_ssn                                                          | per_sslt                                                         | per_fname | per_lname  | per_street             | per_city    | per_state | per_zip    | per_email               | per_dob    | per_type | per_notes |
+--------+------------------------------------------------------------------+------------------------------------------------------------------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
|      1 | V������KvF�6�4*��A��-����`*��ғ�e���+m�g[Ai�������                                    | Q<�/�N;1�) �B_.!��R^�+7J�$[�v��W�p�/��7����wm�,�`���1                         | Steve     | Rogers     | 437 Southern Drive     | Rochester   | NY        |  032443022 | srogers@comcast.net     | 1923-10-03 | c        | NULL      |
|      2 | 6\ 5����/7gڃ-G_+ػ+�i+�0�yZۭ��{�]�,��|�A؛Gıqk,��-���b� B                          | �+�[�����=�+UTcS
+�|KlRW�V��Mr%Έ���}0��p?�w�*���AMe��UQ                           | Bruce     | Wayne      | 1007 Mountain Drive    | Gotham      | NY        |  003243440 | bwayne@knology.net      | 1968-03-20 | c        | NULL      |
|      3 | 982���+D֘L����`B�&�JԘFҍ9TVum��X �`")��^�u�jf�ul����j�R                          | �6����d��ˉ���F�f���h�3M���/��s�x%7����fz��Ba�|`���                                   | Peter     | Parker     | 20 Ingram Street       | New York    | NY        |  102862341 | pparker@msn.net         | 1923-10-03 | c        | NULL      |
|      4 | ~/c���/�)�f�w�`���3|�ᠰ%ȫVs�Rq���٪�#cuob�)���d�W��{                              | �Ӭ9��m�:��K.V\5{<7>�P��nΑ.1*����'k�o���B��Q�GeB� tI�P��                            | Jane      | Thompson   | 13563 Ocean View Drive | Seattle     | WA        |  032084409 | jthompson@gmail.net     | 1973-05-08 | c        | NULL      |
|      5 | ���:�����c��y�;Ҷ�WL��e7;��I���9�Z��j�O��5�)�>�a���\�                                    | *��K�{Y����F�L>�r	,3)y"{��M6��[��f�a��d{`3���O?lg�4��ݧU��                            | Debra     | Steele     | 543 Oak Ln             | Milwaukee   | WI        | 2862374478 | dsteele@verizon.net     | 1994-07-19 | c        | NULL      |
|      6 | Jb����Z,r���@Kg� @!'�&o0P�nk��7�a4��%�&�Zm�������;VӺ&                           | }�8Z���t$�3D 	�
Ε�iv3j+�cq�訞p`c����� E�4�W��oK� ��6��A�/�                            | Tony      | Stark      | 332 Palm Avenue        | Malibu      | CA        |  902638832 | tstark@yahoo.com        | 1972-05-04 | a        | NULL      |
|      7 | "*��r�dG�dV�.�Dy�ÖKѾ��y��Q�^����|�����tMHS��-~V5�y��z�z���Y                                 | �@cJ�koZb�]�p=\�=�M���,��9�#\�Q�N\�5�<�%�G��PU����Z���                            | Hank      | Pymi       | 2355 Brown Street      | Cleveland   | OH        |  022342390 | hpym@aol.net            | 1980-08-28 | a        | NULL      |
|      8 | �D��`-QB��e� e|*��8��tI!/�m���	OG�tO�8<�q�G 	�1PH�}��� �                         | ���ȃ��{�[�'�)�e#��\�z9�z�})�׼��Z/ sE��3Åȹ�r�N��?�r<K�                                | Bob       | Best       | 4902 Avendale Ave.     | Scottsdale  | AZ        |  087263912 | bbest@yahoo.com         | 1992-02-10 | a        | NULL      |
|      9 | v���;�������Q�t���T7�f��3]�G�
� �<�}�9�'����E���������v�>                                         | �7�/P������S�LM�����
Ӎ�H#����	�vp!WӁZ��@�vm�tu&����G��                                   | Sandra    | Dole       | 8732 Lawrence Ave.     | Atlanta     | GA        |  002345390 | sdole@gmail.net         | 1990-01-26 | a        | NULL      |
|     10 | ޳{�@��7�e}��4�?a�E,vD]����r��2�1�ů��9f�j�_���p�$3��                              | uˈܻ��Y1C�d��L麳�s�Mʳ���lJ#	~����T�d&!S�y��/x�]xZ��!                           | Ben       | Avery      | 6432 Thunderbird Ln    | Sioux Falls | SD        |  005637932 | bavery@hotmail.net      | 1983-12-24 | a        | NULL      |
|     11 | �:0��Z�Z���,�6��� V���}�DA�a�f�7�b;�ɛ��m'J52��9DS��c                             | ���E�jJ3�`���A݊k�A�F��x�b����MDO�_�7x��Y	+�-��>�%��*h�g���0                                   | Arthur    | Curry      | 3302 Euclid Avenue     | Miami       | FL        |  000219932 | acurry@gmail.com        | 1975-12-15 | j        | NULL      |
|     12 | �N�>�50W!��z�; =�ɒ`�3��ek��~q�/�u�A_���
J�~�{*T�A��nF\�j                           | k��#+�0��
�ԪTG�9�;�5IR�m�q9*�:�L��)B�!��k�2�tH�7A��������                                | Diana     | Price      | 944 Green Street       | Las vegas   | NV        |  033235223 | dprice@aol.net          | 1980-08-22 | j        | NULL      |
|     13 | +�%r��6dS�KVTD�;}~�t�L�F��_�4�j�g��S7�L�L�S}޸��`h-#l;���(�                           | ��'������V��
#.�y��$�v�+8�l�P�S���R)��������9�oE�/��[[�AdG                                     | Adam      | Jurris     | 98435 Valencia Dr.     | Gulf Shore  | AL        |  870639912 | ajurris@yahoo.com       | 1995-01-31 | j        | NULL      |
|     14 | �Rb�������XT���p�c������Қ��Oq�K�By�b,���O��g�}1ޅK                                 | ���z��=Py	��£�\@���X���\���r�d.��r"�:����F9��dn��a                                | Judy      | Sleen      | 56343 Rover Ct.        | Billings    | MT        |  067204823 | jsleen@symaptico.com    | 1995-01-31 | j        | NULL      |
|     15 | �T*o�1�L�����n��0��K��z$�@'N��x2 ��e���Vp��?H��	�O�{c                             | e1b��=�{Ф���(!EH�ē���%U#���t)�!=��Mr�	'G�%����2���R$��D                                | Bill      | Neiderheim | 43567 Netherland Blvd  | South Bend  | IN        |  320219932 | bneiderheim@comcast.net | 1982-03-24 | j        | NULL      |
+--------+------------------------------------------------------------------+------------------------------------------------------------------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
15 rows in set (0.00 sec)

mysql> select * from client;
+--------+-----------+
| per_id | cli_notes |
+--------+-----------+
|      1 | NULL      |
|      2 | NULL      |
|      3 | NULL      |
|      4 | NULL      |
|      5 | NULL      |
+--------+-----------+
5 rows in set (0.00 sec)

mysql> select * from `case`;
+--------+--------+----------+--------------------------------------------------------------------------------------------------------------------------------------+----------------+--------------+------------------------+
| cse_id | per_id | cse_type | cse_description                                                                                                                      | cse_start_date | cse_end_date | cse_notes              |
+--------+--------+----------+--------------------------------------------------------------------------------------------------------------------------------------+----------------+--------------+------------------------+
|      1 |     13 | civil    | client says that his logo is being used without his content to promote a rival business                                              | 2010-09-28     | NULL         | copyright infringement |
|      2 |     14 | criminal | client is charged of assulting her husband                                                                                           | 2010-12-28     | NULL         | Assult                 |
|      3 |     11 | criminal | client broke an ankle while shopping at a local grocery store. No wet floor sign was posted although the floor had jsut being mopped | 2010-09-28     | 2010-12-24   | slip and fall          |
|      4 |     12 | civil    | client is charged with stealing of several television from his former place of employment. Client has a solid alibi                  | 2011-09-28     | NULL         | grand theft            |
|      5 |     13 | civil    | client is charged with posession of 10 grams of cocaine, allegedely found in his glove box by state police                           | 2010-09-28     | NULL         | posession of narcotics |
|      6 |     15 | criminal | client is charged with the murder of his co-worker over a lovers fued. Client has no alibi                                           | 2010-03-12     | NULL         | murder                 |
|      7 |     13 | civil    | client had chosen a degree other than IT and had to decleared bankruptcy.                                                            | 2010-09-28     | 2013-02-12   | Bankruptcy             |
+--------+--------+----------+--------------------------------------------------------------------------------------------------------------------------------------+----------------+--------------+------------------------+
7 rows in set (0.00 sec)

mysql> select * from speciality;
Empty set (0.01 sec)

mysql> select * from `information_schema`.`tables` where table_schema = 'ooa19a';
+---------------+--------------+------------+------------+--------+---------+------------+------------+----------------+-------------+-----------------+--------------+-----------+----------------+---------------------+-------------+------------+--------------------+----------+----------------+---------------+
| TABLE_CATALOG | TABLE_SCHEMA | TABLE_NAME | TABLE_TYPE | ENGINE | VERSION | ROW_FORMAT | TABLE_ROWS | AVG_ROW_LENGTH | DATA_LENGTH | MAX_DATA_LENGTH | INDEX_LENGTH | DATA_FREE | AUTO_INCREMENT | CREATE_TIME         | UPDATE_TIME | CHECK_TIME | TABLE_COLLATION    | CHECKSUM | CREATE_OPTIONS | TABLE_COMMENT |
+---------------+--------------+------------+------------+--------+---------+------------+------------+----------------+-------------+-----------------+--------------+-----------+----------------+---------------------+-------------+------------+--------------------+----------+----------------+---------------+
| def           | ooa19a       | assignment | BASE TABLE | InnoDB |      10 | Dynamic    |          0 |              0 |       16384 |               0 |        65536 |         0 |           NULL | 2020-02-24 15:52:11 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
| def           | ooa19a       | attorney   | BASE TABLE | InnoDB |      10 | Dynamic    |          0 |              0 |       16384 |               0 |        16384 |         0 |           NULL | 2020-02-24 15:42:48 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
| def           | ooa19a       | bar        | BASE TABLE | InnoDB |      10 | Dynamic    |          0 |              0 |       16384 |               0 |        16384 |         0 |           NULL | 2020-02-24 15:47:01 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
| def           | ooa19a       | case       | BASE TABLE | InnoDB |      10 | Dynamic    |          0 |              0 |       16384 |               0 |        16384 |         0 |           NULL | 2020-02-24 15:46:42 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
| def           | ooa19a       | client     | BASE TABLE | InnoDB |      10 | Dynamic    |          5 |           3276 |       16384 |               0 |        16384 |         0 |           NULL | 2020-02-24 15:50:49 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
| def           | ooa19a       | court      | BASE TABLE | InnoDB |      10 | Dynamic    |          0 |              0 |       16384 |               0 |            0 |         0 |           NULL | 2020-02-24 15:44:59 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
| def           | ooa19a       | judge      | BASE TABLE | InnoDB |      10 | Dynamic    |          0 |              0 |       16384 |               0 |        32768 |         0 |           NULL | 2020-02-24 15:45:22 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
| def           | ooa19a       | judge_hist | BASE TABLE | InnoDB |      10 | Dynamic    |          0 |              0 |       16384 |               0 |        16384 |         0 |           NULL | 2020-02-24 15:46:04 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
| def           | ooa19a       | person     | BASE TABLE | InnoDB |      10 | Dynamic    |         15 |           1092 |       16384 |               0 |        16384 |         0 |             15 | 2020-02-29 13:48:33 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
| def           | ooa19a       | phone      | BASE TABLE | InnoDB |      10 | Dynamic    |         13 |           1260 |       16384 |               0 |        16384 |         0 |             13 | 2020-02-24 15:53:18 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
| def           | ooa19a       | speciality | BASE TABLE | InnoDB |      10 | Dynamic    |          0 |              0 |       16384 |               0 |        16384 |         0 |           NULL | 2020-02-24 15:47:13 | NULL        | NULL       | utf8mb4_general_ci |     NULL |                |               |
+---------------+--------------+------------+------------+--------+---------+------------+------------+----------------+-------------+-----------------+--------------+-----------+----------------+---------------------+-------------+------------+--------------------+----------+----------------+---------------+
11 rows in set (0.00 sec)

mysql>  select per_id, length(per_ssn) from person;
+--------+-----------------+
| per_id | length(per_ssn) |
+--------+-----------------+
|      7 |              64 |
|     13 |              64 |
|      2 |              64 |
|      3 |              64 |
|      6 |              64 |
|      1 |              64 |
|      9 |              64 |
|      4 |              64 |
|     11 |              64 |
|     14 |              64 |
|     15 |              64 |
|     12 |              64 |
|     10 |              64 |
|      5 |              64 |
|      8 |              64 |
+--------+-----------------+
15 rows in set (0.00 sec)

mysql>  select per_id, length(per_ssn) from person order by per_id asc;
+--------+-----------------+
| per_id | length(per_ssn) |
+--------+-----------------+
|      1 |              64 |
|      2 |              64 |
|      3 |              64 |
|      4 |              64 |
|      5 |              64 |
|      6 |              64 |
|      7 |              64 |
|      8 |              64 |
|      9 |              64 |
|     10 |              64 |
|     11 |              64 |
|     12 |              64 |
|     13 |              64 |
|     14 |              64 |
|     15 |              64 |
+--------+-----------------+
15 rows in set (0.00 sec)

mysql> describe speciality;
+-----------+----------------------+------+-----+---------+----------------+
| Field     | Type                 | Null | Key | Default | Extra          |
+-----------+----------------------+------+-----+---------+----------------+
| spc_id    | tinyint(3) unsigned  | NO   | PRI | NULL    | auto_increment |
| per_id    | smallint(5) unsigned | NO   | MUL | NULL    |                |
| spc_type  | varchar(45)          | NO   |     | NULL    |                |
| spc_notes | varchar(255)         | YES  |     | NULL    |                |
+-----------+----------------------+------+-----+---------+----------------+
4 rows in set (0.00 sec)

mysql> insert into speciality 
    -> values
    -> (null,6,'criminal','expert in criminal cases'),
    -> (null,7,'real estate','expert in real estate cases'),
    -> (null,8,'insurance','expert in insurance cases'),
    -> (null,9,'business','expert in business cases'),
    -> (null,10,'bankruptcy','expert in bankruptcy cases'),
    -> (null,4,'traffic','expert in traffic cases');
ERROR 1452 (23000): Cannot add or update a child row: a foreign key constraint fails (`ooa19a`.`speciality`, CONSTRAINT `fk_specialty_attorney` FOREIGN KEY (`per_id`) REFERENCES `attorney` (`per_id`) ON UPDATE CASCADE)
mysql> select * from attorney;
+--------+----------------+--------------+-----------------+-----------------------+------------+
| per_id | aty_start_date | aty_end_date | aty_hourly_rate | aty_years_in_practice | aty_notes  |
+--------+----------------+--------------+-----------------+-----------------------+------------+
|      6 | 1995-02-03     | 2019-01-12   |           40.00 |                    24 | attorney 1 |
|      7 | 1980-06-05     | 2018-09-12   |           45.00 |                    38 | attorney 2 |
|      8 | 1975-05-04     | 2017-12-01   |           39.00 |                    42 | attorney 3 |
|      9 | 1970-09-08     | 2012-12-03   |           45.00 |                    42 | attorney 4 |
|     10 | 2000-08-12     | NULL         |           50.00 |                    20 | attorney 5 |
+--------+----------------+--------------+-----------------+-----------------------+------------+
5 rows in set (0.00 sec)

mysql> insert into speciality 
    -> values
    -> (null,6,'criminal','expert in criminal cases'),
    -> (null,7,'real estate','expert in real estate cases'),
    -> (null,8,'insurance','expert in insurance cases'),
    -> (null,9,'business','expert in business cases'),
    -> (null,10,'bankruptcy','expert in bankruptcy cases');
Query OK, 5 rows affected (0.01 sec)
Records: 5  Duplicates: 0  Warnings: 0

mysql> select * from speciality;
+--------+--------+-------------+-----------------------------+
| spc_id | per_id | spc_type    | spc_notes                   |
+--------+--------+-------------+-----------------------------+
|      7 |      6 | criminal    | expert in criminal cases    |
|      8 |      7 | real estate | expert in real estate cases |
|      9 |      8 | insurance   | expert in insurance cases   |
|     10 |      9 | business    | expert in business cases    |
|     11 |     10 | bankruptcy  | expert in bankruptcy cases  |
+--------+--------+-------------+-----------------------------+
5 rows in set (0.00 sec)

mysql> drop table speciality;
Query OK, 0 rows affected (0.03 sec)

mysql> insert into speciality 
    -> values
    -> (null,6,'criminal','expert in criminal cases'),
    -> (null,7,'real estate','expert in real estate cases'),
    -> (null,8,'insurance','expert in insurance cases'),
    -> (null,9,'business','expert in business cases'),
    -> (null,10,'bankruptcy','expert in bankruptcy cases');
ERROR 1146 (42S02): Table 'ooa19a.speciality' doesn't exist
mysql> create table if not exists speciality 
    -> (
    -> spc_id tinyint unsigned not null auto_increment,
    -> per_id smallint unsigned not null,
    -> spc_type varchar (45) not null, 
    -> spc_notes varchar (255) null,
    -> primary key (spc_id),
    -> 
    -> index idx_per_id(per_id asc),
    -> 
    -> constraint fk_specialty_attorney
    -> foreign key (per_id)
    -> references attorney (per_id)
    -> on delete no action
    -> on update cascade
    -> );
Query OK, 0 rows affected (0.04 sec)

mysql> insert into speciality 
    -> values
    -> (null,6,'criminal','expert in criminal cases'),
    -> (null,7,'real estate','expert in real estate cases'),
    -> (null,8,'insurance','expert in insurance cases'),
    -> (null,9,'business','expert in business cases'),
    -> (null,10,'bankruptcy','expert in bankruptcy cases');
Query OK, 5 rows affected (0.01 sec)
Records: 5  Duplicates: 0  Warnings: 0

mysql> select * from speciality;
+--------+--------+-------------+-----------------------------+
| spc_id | per_id | spc_type    | spc_notes                   |
+--------+--------+-------------+-----------------------------+
|      1 |      6 | criminal    | expert in criminal cases    |
|      2 |      7 | real estate | expert in real estate cases |
|      3 |      8 | insurance   | expert in insurance cases   |
|      4 |      9 | business    | expert in business cases    |
|      5 |     10 | bankruptcy  | expert in bankruptcy cases  |
+--------+--------+-------------+-----------------------------+
5 rows in set (0.00 sec)

mysql> describe bar;
+-----------+----------------------+------+-----+---------+----------------+
| Field     | Type                 | Null | Key | Default | Extra          |
+-----------+----------------------+------+-----+---------+----------------+
| bar_id    | tinyint(3) unsigned  | NO   | PRI | NULL    | auto_increment |
| per_id    | smallint(5) unsigned | NO   | MUL | NULL    |                |
| bar_name  | varchar(45)          | NO   |     | NULL    |                |
| bar_notes | varchar(255)         | YES  |     | NULL    |                |
+-----------+----------------------+------+-----+---------+----------------+
4 rows in set (0.00 sec)

mysql> select * from bar;
Empty set (0.01 sec)

mysql> select 'Inserting into Bar table' as'';
+--------------------------+
|                          |
+--------------------------+
| Inserting into Bar table |
+--------------------------+
1 row in set (0.00 sec)

mysql> do sleep (3); 
Query OK, 0 rows affected (3.00 sec)

mysql> 
mysql> insert into bar
    -> values
    -> (null,6,'Florida Bar',null),
    -> (null,7, 'Alabama Bar',null),
    -> (null,8, 'Texas Bar',null),
    -> (null,9, 'New Orleans Bar',null),
    -> (null,10, 'New York Bar',null);
Query OK, 5 rows affected (0.01 sec)
Records: 5  Duplicates: 0  Warnings: 0

mysql> select * from bar;
+--------+--------+-----------------+-----------+
| bar_id | per_id | bar_name        | bar_notes |
+--------+--------+-----------------+-----------+
|      1 |      6 | Florida Bar     | NULL      |
|      2 |      7 | Alabama Bar     | NULL      |
|      3 |      8 | Texas Bar       | NULL      |
|      4 |      9 | New Orleans Bar | NULL      |
|      5 |     10 | New York Bar    | NULL      |
+--------+--------+-----------------+-----------+
5 rows in set (0.00 sec)

mysql> select 'VIEWS DISPLAY' as '';
+---------------+
|               |
+---------------+
| VIEWS DISPLAY |
+---------------+
1 row in set (0.00 sec)

mysql> do sleep(4);
Query OK, 0 rows affected (4.00 sec)

mysql> 
mysql> drop view if exists v_attorney_info;
Query OK, 0 rows affected, 1 warning (0.01 sec)

mysql> create view v_attorney_info AS
    -> 
    -> select 
    -> concat(per_lnamem,", ", per_fname) as name,
    -> concat(per_street, ", ", per_city, ", ", per_state, ", ", per_zip) as address,
    -> timestampdiff(year, per_dob, now()) as age,
    -> concat('$', format(aty_hourly_rate,2)) as hourly_rate,
    -> bar_name. spc_type
    -> from person
    -> natural join attorney
    -> natural join bar
    -> natura join speciality
    -> order by per_lname;
ERROR 1054 (42S22): Unknown column 'per_lnamem' in 'field list'
mysql> 
mysql> 
mysql> 
mysql> select 'display view v_attorney_info' as '';
+------------------------------+
|                              |
+------------------------------+
| display view v_attorney_info |
+------------------------------+
1 row in set (0.00 sec)

mysql> select * from v_attorney_info;
ERROR 1146 (42S02): Table 'ooa19a.v_attorney_info' doesn't exist
mysql> 
mysql> drop view if exists v_attorney_info;
Query OK, 0 rows affected, 1 warning (0.01 sec)

mysql> do sleep(3);
Query OK, 0 rows affected (3.00 sec)

mysql> select 'VIEWS DISPLAY' as '';
+---------------+
|               |
+---------------+
| VIEWS DISPLAY |
+---------------+
1 row in set (0.00 sec)

mysql> do sleep(4);
Query OK, 0 rows affected (4.00 sec)

mysql> 
mysql> drop view if exists v_attorney_info;
Query OK, 0 rows affected, 1 warning (0.01 sec)

mysql> create view v_attorney_info AS
    -> 
    -> select 
    -> concat(per_lname,", ", per_fname) as name,
    -> concat(per_street, ", ", per_city, ", ", per_state, ", ", per_zip) as address,
    -> timestampdiff(year, per_dob, now()) as age,
    -> concat('$', format(aty_hourly_rate,2)) as hourly_rate,
    -> bar_name. spc_type
    -> from person
    -> natural join attorney
    -> natural join bar
    -> natura join speciality
    -> order by per_lname;
ERROR 1054 (42S22): Unknown column 'bar_name.spc_type' in 'field list'
mysql> 
mysql> 
mysql> 
mysql> select 'display view v_attorney_info' as '';
+------------------------------+
|                              |
+------------------------------+
| display view v_attorney_info |
+------------------------------+
1 row in set (0.00 sec)

mysql> select * from v_attorney_info;
ERROR 1146 (42S02): Table 'ooa19a.v_attorney_info' doesn't exist
mysql> 
mysql> drop view if exists v_attorney_info;
Query OK, 0 rows affected, 1 warning (0.00 sec)

mysql> do sleep(3);
Query OK, 0 rows affected (3.00 sec)

mysql> select 'VIEWS DISPLAY' as '';
+---------------+
|               |
+---------------+
| VIEWS DISPLAY |
+---------------+
1 row in set (0.00 sec)

mysql> do sleep(4);
Query OK, 0 rows affected (4.00 sec)

mysql> 
mysql> drop view if exists v_attorney_info;
Query OK, 0 rows affected, 1 warning (0.01 sec)

mysql> create view v_attorney_info AS
    -> 
    -> select 
    -> concat(per_lname,", ", per_fname) as name,
    -> concat(per_street, ", ", per_city, ", ", per_state, ", ", per_zip) as address,
    -> timestampdiff(year, per_dob, now()) as age,
    -> concat('$', format(aty_hourly_rate,2)) as hourly_rate,
    -> bar_name, spc_type
    -> from person
    -> natural join attorney
    -> natural join bar
    -> natura join speciality
    -> order by per_lname;
Query OK, 0 rows affected (0.01 sec)

mysql> 
mysql> 
mysql> 
mysql> select 'display view v_attorney_info' as '';
+------------------------------+
|                              |
+------------------------------+
| display view v_attorney_info |
+------------------------------+
1 row in set (0.00 sec)

mysql> select * from v_attorney_info;
+--------------+-------------------------------------------------+------+-------------+-----------------+-------------+
| name         | address                                         | age  | hourly_rate | bar_name        | spc_type    |
+--------------+-------------------------------------------------+------+-------------+-----------------+-------------+
| Avery, Ben   | 6432 Thunderbird Ln, Sioux Falls, SD, 005637932 |   36 | $50.00      | New York Bar    | criminal    |
| Avery, Ben   | 6432 Thunderbird Ln, Sioux Falls, SD, 005637932 |   36 | $50.00      | New York Bar    | real estate |
| Avery, Ben   | 6432 Thunderbird Ln, Sioux Falls, SD, 005637932 |   36 | $50.00      | New York Bar    | insurance   |
| Avery, Ben   | 6432 Thunderbird Ln, Sioux Falls, SD, 005637932 |   36 | $50.00      | New York Bar    | business    |
| Avery, Ben   | 6432 Thunderbird Ln, Sioux Falls, SD, 005637932 |   36 | $50.00      | New York Bar    | bankruptcy  |
| Best, Bob    | 4902 Avendale Ave., Scottsdale, AZ, 087263912   |   28 | $39.00      | Texas Bar       | criminal    |
| Best, Bob    | 4902 Avendale Ave., Scottsdale, AZ, 087263912   |   28 | $39.00      | Texas Bar       | real estate |
| Best, Bob    | 4902 Avendale Ave., Scottsdale, AZ, 087263912   |   28 | $39.00      | Texas Bar       | insurance   |
| Best, Bob    | 4902 Avendale Ave., Scottsdale, AZ, 087263912   |   28 | $39.00      | Texas Bar       | business    |
| Best, Bob    | 4902 Avendale Ave., Scottsdale, AZ, 087263912   |   28 | $39.00      | Texas Bar       | bankruptcy  |
| Dole, Sandra | 8732 Lawrence Ave., Atlanta, GA, 002345390      |   30 | $45.00      | New Orleans Bar | criminal    |
| Dole, Sandra | 8732 Lawrence Ave., Atlanta, GA, 002345390      |   30 | $45.00      | New Orleans Bar | real estate |
| Dole, Sandra | 8732 Lawrence Ave., Atlanta, GA, 002345390      |   30 | $45.00      | New Orleans Bar | insurance   |
| Dole, Sandra | 8732 Lawrence Ave., Atlanta, GA, 002345390      |   30 | $45.00      | New Orleans Bar | business    |
| Dole, Sandra | 8732 Lawrence Ave., Atlanta, GA, 002345390      |   30 | $45.00      | New Orleans Bar | bankruptcy  |
| Pymi, Hank   | 2355 Brown Street, Cleveland, OH, 022342390     |   39 | $45.00      | Alabama Bar     | criminal    |
| Pymi, Hank   | 2355 Brown Street, Cleveland, OH, 022342390     |   39 | $45.00      | Alabama Bar     | real estate |
| Pymi, Hank   | 2355 Brown Street, Cleveland, OH, 022342390     |   39 | $45.00      | Alabama Bar     | insurance   |
| Pymi, Hank   | 2355 Brown Street, Cleveland, OH, 022342390     |   39 | $45.00      | Alabama Bar     | business    |
| Pymi, Hank   | 2355 Brown Street, Cleveland, OH, 022342390     |   39 | $45.00      | Alabama Bar     | bankruptcy  |
| Stark, Tony  | 332 Palm Avenue, Malibu, CA, 902638832          |   47 | $40.00      | Florida Bar     | criminal    |
| Stark, Tony  | 332 Palm Avenue, Malibu, CA, 902638832          |   47 | $40.00      | Florida Bar     | real estate |
| Stark, Tony  | 332 Palm Avenue, Malibu, CA, 902638832          |   47 | $40.00      | Florida Bar     | insurance   |
| Stark, Tony  | 332 Palm Avenue, Malibu, CA, 902638832          |   47 | $40.00      | Florida Bar     | business    |
| Stark, Tony  | 332 Palm Avenue, Malibu, CA, 902638832          |   47 | $40.00      | Florida Bar     | bankruptcy  |
+--------------+-------------------------------------------------+------+-------------+-----------------+-------------+
25 rows in set (0.00 sec)

mysql> 
mysql> drop view if exists v_attorney_info;
Query OK, 0 rows affected (0.01 sec)

mysql> do sleep(3);
Query OK, 0 rows affected (3.00 sec)

mysql> select 'Stored procedure to display month numbers, month names, and how many judges were born each month' as'';
+--------------------------------------------------------------------------------------------------+
|                                                                                                  |
+--------------------------------------------------------------------------------------------------+
| Stored procedure to display month numbers, month names, and how many judges were born each month |
+--------------------------------------------------------------------------------------------------+
1 row in set (0.00 sec)

mysql> do sleep(2)
    -> drop procedure if exists sp_num_judges_born_by_month;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'drop procedure if exists sp_num_judges_born_by_month' at line 2
mysql> 
mysql> Delimiter //
mysql> create procedure sp_num_judges_born_by_month()
    -> 
    -> begin 
    -> select month(per_dob) as month, monthname(per_dob) as "Month name" ,count(*) as count
    -> from person
    -> natural join judge
    -> group by month_name
    -> order by month;
    -> end //
Query OK, 0 rows affected (0.02 sec)

mysql> delimiter ;
mysql> 
mysql> select 'calling sp_num_judges_born_by_month()' as'';
+---------------------------------------+
|                                       |
+---------------------------------------+
| calling sp_num_judges_born_by_month() |
+---------------------------------------+
1 row in set (0.00 sec)

mysql> 
mysql> call sp_num_judges_born_by_month();
ERROR 1054 (42S22): Unknown column 'month_name' in 'group statement'
mysql> do sleep(3);
Query OK, 0 rows affected (3.00 sec)

mysql> 
mysql> drop procedure if exists sp_num_judges_born_by_month;
Query OK, 0 rows affected (0.02 sec)

mysql> select 'Stored procedure to display month numbers, month names, and how many judges were born each month' as'';
+--------------------------------------------------------------------------------------------------+
|                                                                                                  |
+--------------------------------------------------------------------------------------------------+
| Stored procedure to display month numbers, month names, and how many judges were born each month |
+--------------------------------------------------------------------------------------------------+
1 row in set (0.00 sec)

mysql> do sleep(2);
Query OK, 0 rows affected (2.00 sec)

mysql> drop procedure if exists sp_num_judges_born_by_month;
Query OK, 0 rows affected, 1 warning (0.01 sec)

mysql> 
mysql> Delimiter //
mysql> create procedure sp_num_judges_born_by_month()
    -> 
    -> begin 
    -> select month(per_dob) as month, monthname(per_dob) as "Month name" ,count(*) as count
    -> from person
    -> natural join judge
    -> group by "Month name"
    -> order by month;
    -> end //
Query OK, 0 rows affected (0.01 sec)

mysql> delimiter ;
mysql> 
mysql> select 'calling sp_num_judges_born_by_month()' as'';
+---------------------------------------+
|                                       |
+---------------------------------------+
| calling sp_num_judges_born_by_month() |
+---------------------------------------+
1 row in set (0.00 sec)

mysql> 
mysql> call sp_num_judges_born_by_month();
ERROR 1055 (42000): Expression #1 of SELECT list is not in GROUP BY clause and contains nonaggregated column 'ooa19a.person.per_dob' which is not functionally dependent on columns in GROUP BY clause; this is incompatible with sql_mode=only_full_group_by
mysql> do sleep(3);
Query OK, 0 rows affected (3.00 sec)

mysql> 
mysql> drop procedure if exists sp_num_judges_born_by_month;
Query OK, 0 rows affected (0.01 sec)

mysql> select 'Stored procedure to display month numbers, month names, and how many judges were born each month' as'';
+--------------------------------------------------------------------------------------------------+
|                                                                                                  |
+--------------------------------------------------------------------------------------------------+
| Stored procedure to display month numbers, month names, and how many judges were born each month |
+--------------------------------------------------------------------------------------------------+
1 row in set (0.00 sec)

mysql> do sleep(2);
Query OK, 0 rows affected (2.00 sec)

mysql> drop procedure if exists sp_num_judges_born_by_month;
Query OK, 0 rows affected, 1 warning (0.01 sec)

mysql> 
mysql> Delimiter //
mysql> create procedure sp_num_judges_born_by_month()
    -> 
    -> begin 
    -> select month(per_dob) as month, monthname(per_dob) as Month_name ,count(*) as count
    -> from person
    -> natural join judge
    -> group by Month_name
    -> order by month;
    -> end //
Query OK, 0 rows affected (0.01 sec)

mysql> delimiter ;
mysql> 
mysql> select 'calling sp_num_judges_born_by_month()' as'';
+---------------------------------------+
|                                       |
+---------------------------------------+
| calling sp_num_judges_born_by_month() |
+---------------------------------------+
1 row in set (0.00 sec)

mysql> 
mysql> call sp_num_judges_born_by_month();
ERROR 1055 (42000): Expression #1 of SELECT list is not in GROUP BY clause and contains nonaggregated column 'ooa19a.person.per_dob' which is not functionally dependent on columns in GROUP BY clause; this is incompatible with sql_mode=only_full_group_by
mysql> do sleep(3);
Query OK, 0 rows affected (3.00 sec)

mysql> 
mysql> drop procedure if exists sp_num_judges_born_by_month;
Query OK, 0 rows affected (0.01 sec)

mysql> select * from person;
+--------+------------------------------------------------------------------+------------------------------------------------------------------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
| per_id | per_ssn                                                          | per_sslt                                                         | per_fname | per_lname  | per_street             | per_city    | per_state | per_zip    | per_email               | per_dob    | per_type | per_notes |
+--------+------------------------------------------------------------------+------------------------------------------------------------------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
|      1 | V������KvF�6�4*��A��-����`*��ғ�e���+m�g[Ai�������                                    | Q<�/�N;1�) �B_.!��R^�+7J�$[�v��W�p�/��7����wm�,�`���1                         | Steve     | Rogers     | 437 Southern Drive     | Rochester   | NY        |  032443022 | srogers@comcast.net     | 1923-10-03 | c        | NULL      |
|      2 | 6\ 5����/7gڃ-G_+ػ+�i+�0�yZۭ��{�]�,��|�A؛Gıqk,��-���b� B                          | �+�[�����=�+UTcS
+�|KlRW�V��Mr%Έ���}0��p?�w�*���AMe��UQ                           | Bruce     | Wayne      | 1007 Mountain Drive    | Gotham      | NY        |  003243440 | bwayne@knology.net      | 1968-03-20 | c        | NULL      |
|      3 | 982���+D֘L����`B�&�JԘFҍ9TVum��X �`")��^�u�jf�ul����j�R                          | �6����d��ˉ���F�f���h�3M���/��s�x%7����fz��Ba�|`���                                   | Peter     | Parker     | 20 Ingram Street       | New York    | NY        |  102862341 | pparker@msn.net         | 1923-10-03 | c        | NULL      |
|      4 | ~/c���/�)�f�w�`���3|�ᠰ%ȫVs�Rq���٪�#cuob�)���d�W��{                              | �Ӭ9��m�:��K.V\5{<7>�P��nΑ.1*����'k�o���B��Q�GeB� tI�P��                            | Jane      | Thompson   | 13563 Ocean View Drive | Seattle     | WA        |  032084409 | jthompson@gmail.net     | 1973-05-08 | c        | NULL      |
|      5 | ���:�����c��y�;Ҷ�WL��e7;��I���9�Z��j�O��5�)�>�a���\�                                    | *��K�{Y����F�L>�r	,3)y"{��M6��[��f�a��d{`3���O?lg�4��ݧU��                            | Debra     | Steele     | 543 Oak Ln             | Milwaukee   | WI        | 2862374478 | dsteele@verizon.net     | 1994-07-19 | c        | NULL      |
|      6 | Jb����Z,r���@Kg� @!'�&o0P�nk��7�a4��%�&�Zm�������;VӺ&                           | }�8Z���t$�3D 	�
Ε�iv3j+�cq�訞p`c����� E�4�W��oK� ��6��A�/�                            | Tony      | Stark      | 332 Palm Avenue        | Malibu      | CA        |  902638832 | tstark@yahoo.com        | 1972-05-04 | a        | NULL      |
|      7 | "*��r�dG�dV�.�Dy�ÖKѾ��y��Q�^����|�����tMHS��-~V5�y��z�z���Y                                 | �@cJ�koZb�]�p=\�=�M���,��9�#\�Q�N\�5�<�%�G��PU����Z���                            | Hank      | Pymi       | 2355 Brown Street      | Cleveland   | OH        |  022342390 | hpym@aol.net            | 1980-08-28 | a        | NULL      |
|      8 | �D��`-QB��e� e|*��8��tI!/�m���	OG�tO�8<�q�G 	�1PH�}��� �                         | ���ȃ��{�[�'�)�e#��\�z9�z�})�׼��Z/ sE��3Åȹ�r�N��?�r<K�                                | Bob       | Best       | 4902 Avendale Ave.     | Scottsdale  | AZ        |  087263912 | bbest@yahoo.com         | 1992-02-10 | a        | NULL      |
|      9 | v���;�������Q�t���T7�f��3]�G�
� �<�}�9�'����E���������v�>                                         | �7�/P������S�LM�����
Ӎ�H#����	�vp!WӁZ��@�vm�tu&����G��                                   | Sandra    | Dole       | 8732 Lawrence Ave.     | Atlanta     | GA        |  002345390 | sdole@gmail.net         | 1990-01-26 | a        | NULL      |
|     10 | ޳{�@��7�e}��4�?a�E,vD]����r��2�1�ů��9f�j�_���p�$3��                              | uˈܻ��Y1C�d��L麳�s�Mʳ���lJ#	~����T�d&!S�y��/x�]xZ��!                           | Ben       | Avery      | 6432 Thunderbird Ln    | Sioux Falls | SD        |  005637932 | bavery@hotmail.net      | 1983-12-24 | a        | NULL      |
|     11 | �:0��Z�Z���,�6��� V���}�DA�a�f�7�b;�ɛ��m'J52��9DS��c                             | ���E�jJ3�`���A݊k�A�F��x�b����MDO�_�7x��Y	+�-��>�%��*h�g���0                                   | Arthur    | Curry      | 3302 Euclid Avenue     | Miami       | FL        |  000219932 | acurry@gmail.com        | 1975-12-15 | j        | NULL      |
|     12 | �N�>�50W!��z�; =�ɒ`�3��ek��~q�/�u�A_���
J�~�{*T�A��nF\�j                           | k��#+�0��
�ԪTG�9�;�5IR�m�q9*�:�L��)B�!��k�2�tH�7A��������                                | Diana     | Price      | 944 Green Street       | Las vegas   | NV        |  033235223 | dprice@aol.net          | 1980-08-22 | j        | NULL      |
|     13 | +�%r��6dS�KVTD�;}~�t�L�F��_�4�j�g��S7�L�L�S}޸��`h-#l;���(�                           | ��'������V��
#.�y��$�v�+8�l�P�S���R)��������9�oE�/��[[�AdG                                     | Adam      | Jurris     | 98435 Valencia Dr.     | Gulf Shore  | AL        |  870639912 | ajurris@yahoo.com       | 1995-01-31 | j        | NULL      |
|     14 | �Rb�������XT���p�c������Қ��Oq�K�By�b,���O��g�}1ޅK                                 | ���z��=Py	��£�\@���X���\���r�d.��r"�:����F9��dn��a                                | Judy      | Sleen      | 56343 Rover Ct.        | Billings    | MT        |  067204823 | jsleen@symaptico.com    | 1995-01-31 | j        | NULL      |
|     15 | �T*o�1�L�����n��0��K��z$�@'N��x2 ��e���Vp��?H��	�O�{c                             | e1b��=�{Ф���(!EH�ē���%U#���t)�!=��Mr�	'G�%����2���R$��D                                | Bill      | Neiderheim | 43567 Netherland Blvd  | South Bend  | IN        |  320219932 | bneiderheim@comcast.net | 1982-03-24 | j        | NULL      |
+--------+------------------------------------------------------------------+------------------------------------------------------------------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
15 rows in set (0.00 sec)

mysql> select concat(per_fname, ' ',per_lname) as Name, length(per_ssn), per_dob from person;
+-----------------+-----------------+------------+
| Name            | length(per_ssn) | per_dob    |
+-----------------+-----------------+------------+
| Steve Rogers    |              64 | 1923-10-03 |
| Bruce Wayne     |              64 | 1968-03-20 |
| Peter Parker    |              64 | 1923-10-03 |
| Jane Thompson   |              64 | 1973-05-08 |
| Debra Steele    |              64 | 1994-07-19 |
| Tony Stark      |              64 | 1972-05-04 |
| Hank Pymi       |              64 | 1980-08-28 |
| Bob Best        |              64 | 1992-02-10 |
| Sandra Dole     |              64 | 1990-01-26 |
| Ben Avery       |              64 | 1983-12-24 |
| Arthur Curry    |              64 | 1975-12-15 |
| Diana Price     |              64 | 1980-08-22 |
| Adam Jurris     |              64 | 1995-01-31 |
| Judy Sleen      |              64 | 1995-01-31 |
| Bill Neiderheim |              64 | 1982-03-24 |
+-----------------+-----------------+------------+
15 rows in set (0.00 sec)

mysql> select month(per_don), monthname(per_dob), count(*) from person;
ERROR 1054 (42S22): Unknown column 'per_don' in 'field list'
mysql> select month(per_dob), monthname(per_dob), count(*) from person;
ERROR 1140 (42000): In aggregated query without GROUP BY, expression #1 of SELECT list contains nonaggregated column 'ooa19a.person.per_dob'; this is incompatible with sql_mode=only_full_group_by
mysql> select month(per_dob), monthname(per_dob), count(*) from person
    -> group by month(per_dob);
ERROR 1055 (42000): Expression #2 of SELECT list is not in GROUP BY clause and contains nonaggregated column 'ooa19a.person.per_dob' which is not functionally dependent on columns in GROUP BY clause; this is incompatible with sql_mode=only_full_group_by
mysql> select month(per_dob), monthname(per_dob), count(*) from person
    -> group by month(per_dob);
ERROR 1055 (42000): Expression #2 of SELECT list is not in GROUP BY clause and contains nonaggregated column 'ooa19a.person.per_dob' which is not functionally dependent on columns in GROUP BY clause; this is incompatible with sql_mode=only_full_group_by
mysql> select concat(per_fname, ' ',per_lname) as Name, length(per_ssn), per_dob from person;
+-----------------+-----------------+------------+
| Name            | length(per_ssn) | per_dob    |
+-----------------+-----------------+------------+
| Steve Rogers    |              64 | 1923-10-03 |
| Bruce Wayne     |              64 | 1968-03-20 |
| Peter Parker    |              64 | 1923-10-03 |
| Jane Thompson   |              64 | 1973-05-08 |
| Debra Steele    |              64 | 1994-07-19 |
| Tony Stark      |              64 | 1972-05-04 |
| Hank Pymi       |              64 | 1980-08-28 |
| Bob Best        |              64 | 1992-02-10 |
| Sandra Dole     |              64 | 1990-01-26 |
| Ben Avery       |              64 | 1983-12-24 |
| Arthur Curry    |              64 | 1975-12-15 |
| Diana Price     |              64 | 1980-08-22 |
| Adam Jurris     |              64 | 1995-01-31 |
| Judy Sleen      |              64 | 1995-01-31 |
| Bill Neiderheim |              64 | 1982-03-24 |
+-----------------+-----------------+------------+
15 rows in set (0.00 sec)

mysql> select concat(per_fname, ' ',per_lname) as "Name (Firstname first)", length(per_ssn), per_dob from person;
+------------------------+-----------------+------------+
| Name (Firstname first) | length(per_ssn) | per_dob    |
+------------------------+-----------------+------------+
| Steve Rogers           |              64 | 1923-10-03 |
| Bruce Wayne            |              64 | 1968-03-20 |
| Peter Parker           |              64 | 1923-10-03 |
| Jane Thompson          |              64 | 1973-05-08 |
| Debra Steele           |              64 | 1994-07-19 |
| Tony Stark             |              64 | 1972-05-04 |
| Hank Pymi              |              64 | 1980-08-28 |
| Bob Best               |              64 | 1992-02-10 |
| Sandra Dole            |              64 | 1990-01-26 |
| Ben Avery              |              64 | 1983-12-24 |
| Arthur Curry           |              64 | 1975-12-15 |
| Diana Price            |              64 | 1980-08-22 |
| Adam Jurris            |              64 | 1995-01-31 |
| Judy Sleen             |              64 | 1995-01-31 |
| Bill Neiderheim        |              64 | 1982-03-24 |
+------------------------+-----------------+------------+
15 rows in set (0.00 sec)

mysql> select month(per_dob), monthname(per_dob), count(*) from person
    -> natural join judge
    -> froup by month_name
    -> order by month;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'by month_name
order by month' at line 3
mysql> select month(per_dob), monthname(per_dob), count(*) from person
    -> natural join judge
    -> group by month_name
    -> order by month(per_dob);
ERROR 1054 (42S22): Unknown column 'month_name' in 'group statement'
mysql> select month(per_dob), monthname(per_dob), count(*) from person
    -> natural join judge
    -> group by monthname(per_dob)
    -> order by month(per_dob);
ERROR 1055 (42000): Expression #1 of SELECT list is not in GROUP BY clause and contains nonaggregated column 'ooa19a.person.per_dob' which is not functionally dependent on columns in GROUP BY clause; this is incompatible with sql_mode=only_full_group_by
mysql> select month(per_dob), monthname(per_dob), count(*) from person
    -> natural join judge
    -> group by monthname(per_dob), count(*)
    -> order by month(per_dob);
ERROR 1056 (42000): Can't group on 'count(*)'
mysql> SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));
Query OK, 0 rows affected (0.00 sec)

mysql> select month(per_dob) as month, monthname(per_dob) as Month_name ,count(*) as count
    -> from person
    -> natural join judge
    -> group by Month_name
    -> order by month;
ERROR 1055 (42000): Expression #1 of SELECT list is not in GROUP BY clause and contains nonaggregated column 'ooa19a.person.per_dob' which is not functionally dependent on columns in GROUP BY clause; this is incompatible with sql_mode=only_full_group_by
mysql> SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));select 'Stored procedure to display month numbers, month names, and how many judges were born each month' as'';do sleep(2);drop procedure if exists sp_num_judges_born_by_month;Delimiter //create procedure sp_num_judges_born_by_month()begin select month(per_dob) as month, monthname(per_dob) as Month_name ,count(*) as countfrom personnatural join judgegroup by Month_nameorder by month;end //delimiter ;select 'calling sp_num_judges_born_by_month()' as'';call sp_num_judges_born_by_month();
Query OK, 0 rows affected (0.00 sec)

+--------------------------------------------------------------------------------------------------+
|                                                                                                  |
+--------------------------------------------------------------------------------------------------+
| Stored procedure to display month numbers, month names, and how many judges were born each month |
+--------------------------------------------------------------------------------------------------+
1 row in set (0.00 sec)

Query OK, 0 rows affected (2.00 sec)

Query OK, 0 rows affected (0.01 sec)

    ->  \c
mysql> c
    -> \c
mysql> exit
mysql> select 'Stored procedure to display month numbers, month names, and how many judges were born each month' as'';
+--------------------------------------------------------------------------------------------------+
|                                                                                                  |
+--------------------------------------------------------------------------------------------------+
| Stored procedure to display month numbers, month names, and how many judges were born each month |
+--------------------------------------------------------------------------------------------------+
1 row in set (0.00 sec)

mysql> do sleep(2);
Query OK, 0 rows affected (2.00 sec)

mysql> drop procedure if exists sp_num_judges_born_by_month;
Query OK, 0 rows affected, 1 warning (0.01 sec)

mysql> 
mysql> Delimiter //
mysql> create procedure sp_num_judges_born_by_month()
    -> 
    -> begin 
    -> select month(per_dob) as month, monthname(per_dob) as Month_name ,count(*) as count
    -> from person
    -> natural join judge
    -> group by Month_name
    -> order by month;
    -> end //
Query OK, 0 rows affected (0.01 sec)

mysql> delimiter ;
mysql> 
mysql> select 'calling sp_num_judges_born_by_month()' as'';
+---------------------------------------+
|                                       |
+---------------------------------------+
| calling sp_num_judges_born_by_month() |
+---------------------------------------+
1 row in set (0.00 sec)

mysql> 
mysql> call sp_num_judges_born_by_month();
+-------+------------+-------+
| month | Month_name | count |
+-------+------------+-------+
|     1 | January    |     2 |
|     3 | March      |     1 |
|     8 | August     |     1 |
|    12 | December   |     1 |
+-------+------------+-------+
4 rows in set (0.00 sec)

Query OK, 0 rows affected (0.04 sec)

mysql> do sleep(3);
Query OK, 0 rows affected (3.00 sec)

mysql> 
mysql> 
mysql> 
mysql> 
mysql> 
mysql> 
mysql> drop procedure if exists sp_num_judges_born_by_month;
Query OK, 0 rows affected (0.01 sec)

mysql> select 'Stored procedure to display month numbers, month names, and how many judges were born each month' as'';
+--------------------------------------------------------------------------------------------------+
|                                                                                                  |
+--------------------------------------------------------------------------------------------------+
| Stored procedure to display month numbers, month names, and how many judges were born each month |
+--------------------------------------------------------------------------------------------------+
1 row in set (0.00 sec)

mysql> do sleep(2);
Query OK, 0 rows affected (2.00 sec)

mysql> drop procedure if exists sp_num_judges_born_by_month;
Query OK, 0 rows affected, 1 warning (0.01 sec)

mysql> 
mysql> Delimiter //
mysql> create procedure sp_num_judges_born_by_month()
    -> 
    -> begin 
    -> select month(per_dob) as month, monthname(per_dob) as Month_name ,count(*) as count
    -> from person
    -> natural join judge
    -> group by Month_name
    -> order by month;
    -> end //
Query OK, 0 rows affected (0.01 sec)

mysql> delimiter ;
mysql> 
mysql> select 'calling sp_num_judges_born_by_month()' as'';
+---------------------------------------+
|                                       |
+---------------------------------------+
| calling sp_num_judges_born_by_month() |
+---------------------------------------+
1 row in set (0.00 sec)

mysql> 
mysql> call sp_num_judges_born_by_month();
+-------+------------+-------+
| month | Month_name | count |
+-------+------------+-------+
|     1 | January    |     2 |
|     3 | March      |     1 |
|     8 | August     |     1 |
|    12 | December   |     1 |
+-------+------------+-------+
4 rows in set (0.00 sec)

Query OK, 0 rows affected (0.01 sec)

mysql> do sleep(3);
Query OK, 0 rows affected (3.00 sec)

mysql> 
mysql> drop procedure if exists sp_num_judges_born_by_month;
Query OK, 0 rows affected (0.01 sec)

mysql> select month(per_dob) as month, monthname(per_dob) as Month_name ,count(*) as count
    -> from person
    -> natural join attorney
    -> group by Month_name
    -> order by month;
+-------+------------+-------+
| month | Month_name | count |
+-------+------------+-------+
|     1 | January    |     1 |
|     2 | February   |     1 |
|     5 | May        |     1 |
|     8 | August     |     1 |
|    12 | December   |     1 |
+-------+------------+-------+
5 rows in set (0.00 sec)

mysql> select * from client;
+--------+-----------+
| per_id | cli_notes |
+--------+-----------+
|      1 | NULL      |
|      2 | NULL      |
|      3 | NULL      |
|      4 | NULL      |
|      5 | NULL      |
+--------+-----------+
5 rows in set (0.00 sec)

mysql> select * from attorney;
+--------+----------------+--------------+-----------------+-----------------------+------------+
| per_id | aty_start_date | aty_end_date | aty_hourly_rate | aty_years_in_practice | aty_notes  |
+--------+----------------+--------------+-----------------+-----------------------+------------+
|      6 | 1995-02-03     | 2019-01-12   |           40.00 |                    24 | attorney 1 |
|      7 | 1980-06-05     | 2018-09-12   |           45.00 |                    38 | attorney 2 |
|      8 | 1975-05-04     | 2017-12-01   |           39.00 |                    42 | attorney 3 |
|      9 | 1970-09-08     | 2012-12-03   |           45.00 |                    42 | attorney 4 |
|     10 | 2000-08-12     | NULL         |           50.00 |                    20 | attorney 5 |
+--------+----------------+--------------+-----------------+-----------------------+------------+
5 rows in set (0.00 sec)

mysql> select month(per_dob) as month, monthname(per_dob) as Month_name ,count(*) as count
    -> from person
    -> natural join client
    -> group by Month_name
    -> order by month;
+-------+------------+-------+
| month | Month_name | count |
+-------+------------+-------+
|     3 | March      |     1 |
|     5 | May        |     1 |
|     7 | July       |     1 |
|    10 | October    |     2 |
+-------+------------+-------+
4 rows in set (0.01 sec)

mysql> select * from judge;
+--------+--------+------------+-----------------------+-----------+
| per_id | crt_id | jud_salary | jud_years_in_practice | jud_notes |
+--------+--------+------------+-----------------------+-----------+
|     11 |      1 |  150000.00 |                    12 | Judge 1   |
|     12 |      2 |  140000.00 |                    10 | Judge 2   |
|     13 |      3 |  100000.00 |                    15 | Judge 3   |
|     14 |      4 |   95000.00 |                    12 | Judge 4   |
|     15 |      5 |  120000.00 |                    11 | Judge 5   |
+--------+--------+------------+-----------------------+-----------+
5 rows in set (0.00 sec)

mysql> select per_id, cse_id, cse_type, cse_description, concat('(', substring(phn_num, 1,3), substring(phn_num, 4,3), '-' substring(7,4)) as "Judge Phone Number", phn_type, jud_years_in_practice, 
    -> cse_start date, cse_end_date 
    -> from person
    -> natural join judge
    -> natural join `case`
    -> natural join phone
    -> 
    -> where per_type ='j'
    -> order by per_lname;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'substring(7,4)) as "Judge Phone Number", phn_type, jud_years_in_practice, 
cse_s' at line 1
mysql> select per_id, cse_id, cse_type, cse_description, concat('(', substring(phn_num, 1,3), substring(phn_num, 4,3), '-' substring(phn_num, 7,4)) as "Judge Phone Number", phn_type, jud_years_in_practice, 
    -> cse_start date, cse_end_date 
    -> from person
    -> natural join judge
    -> natural join `case`
    -> natural join phone
    -> 
    -> where per_type ='j'
    -> order by per_lname;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'substring(phn_num, 7,4)) as "Judge Phone Number", phn_type, jud_years_in_practic' at line 1
mysql> 
mysql> select per_id, cse_id, cse_type, cse_description, concat('(', substring(phn_num, 1,3),')', substring(phn_num, 4,3), '-' ,substring(phn_num, 7,4)) as "Judge Phone Number", phn_type, jud_years_in_practice, 
    -> cse_start date, cse_end_date 
    -> from person
    -> natural join judge
    -> natural join `case`
    -> natural join phone
    -> 
    -> where per_type ='j'
    -> order by per_lname;
ERROR 1054 (42S22): Unknown column 'cse_start' in 'field list'
mysql> select per_id, cse_id, cse_type, cse_description, concat('(', substring(phn_num, 1,3),')', substring(phn_num, 4,3), '-' ,substring(phn_num, 7,4)) as "Judge Phone Number", phn_type, jud_years_in_practice, 
    -> cse_start_date, cse_end_date 
    -> from person
    -> natural join judge
    -> natural join `case`
    -> natural join phone
    -> 
    -> where per_type ='j'
    -> order by per_lname;
+--------+--------+----------+--------------------------------------------------------------------------------------------------------------------------------------+--------------------+----------+-----------------------+----------------+--------------+
| per_id | cse_id | cse_type | cse_description                                                                                                                      | Judge Phone Number | phn_type | jud_years_in_practice | cse_start_date | cse_end_date |
+--------+--------+----------+--------------------------------------------------------------------------------------------------------------------------------------+--------------------+----------+-----------------------+----------------+--------------+
|     11 |      3 | criminal | client broke an ankle while shopping at a local grocery store. No wet floor sign was posted although the floor had jsut being mopped | (499)900-0222      | c        |                    12 | 2010-09-28     | 2010-12-24   |
|     13 |      5 | civil    | client is charged with posession of 10 grams of cocaine, allegedely found in his glove box by state police                           | (963)637-8282      | f        |                    15 | 2010-09-28     | NULL         |
|     13 |      7 | civil    | client had chosen a degree other than IT and had to decleared bankruptcy.                                                            | (963)637-8282      | f        |                    15 | 2010-09-28     | 2013-02-12   |
|     13 |      1 | civil    | client says that his logo is being used without his content to promote a rival business                                              | (963)637-8282      | f        |                    15 | 2010-09-28     | NULL         |
|     12 |      4 | civil    | client is charged with stealing of several television from his former place of employment. Client has a solid alibi                  | (863)637-8282      | w        |                    10 | 2011-09-28     | NULL         |
+--------+--------+----------+--------------------------------------------------------------------------------------------------------------------------------------+--------------------+----------+-----------------------+----------------+--------------+
5 rows in set (0.01 sec)

mysql> select per_id, concat(per_fname, ' ', per_lname) as "Name (Firstname first)" cse_id, cse_type, cse_description, concat('(', substring(phn_num, 1,3),')', substring(phn_num, 4,3), '-' ,substring(phn_num, 7,4)) as "Judge Phone Number", phn_type, jud_years_in_practice, 
    -> cse_start_date, cse_end_date 
    -> from person
    -> natural join judge
    -> natural join `case`
    -> natural join phone
    -> 
    -> where per_type ='j'
    -> order by per_lname;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'cse_id, cse_type, cse_description, concat('(', substring(phn_num, 1,3),')', subs' at line 1
mysql> select per_id, concat(per_fname, ' ', per_lname) as "Name (Firstname first)", cse_id, cse_type, cse_description, concat('(', substring(phn_num, 1,3),')', substring(phn_num, 4,3), '-' ,substring(phn_num, 7,4)) as "Judge Phone Number", phn_type, jud_years_in_practice, 
    -> cse_start_date, cse_end_date 
    -> from person
    -> natural join judge
    -> natural join `case`
    -> natural join phone
    -> 
    -> where per_type ='j'
    -> order by per_lname;
+--------+------------------------+--------+----------+--------------------------------------------------------------------------------------------------------------------------------------+--------------------+----------+-----------------------+----------------+--------------+
| per_id | Name (Firstname first) | cse_id | cse_type | cse_description                                                                                                                      | Judge Phone Number | phn_type | jud_years_in_practice | cse_start_date | cse_end_date |
+--------+------------------------+--------+----------+--------------------------------------------------------------------------------------------------------------------------------------+--------------------+----------+-----------------------+----------------+--------------+
|     11 | Arthur Curry           |      3 | criminal | client broke an ankle while shopping at a local grocery store. No wet floor sign was posted although the floor had jsut being mopped | (499)900-0222      | c        |                    12 | 2010-09-28     | 2010-12-24   |
|     13 | Adam Jurris            |      1 | civil    | client says that his logo is being used without his content to promote a rival business                                              | (963)637-8282      | f        |                    15 | 2010-09-28     | NULL         |
|     13 | Adam Jurris            |      5 | civil    | client is charged with posession of 10 grams of cocaine, allegedely found in his glove box by state police                           | (963)637-8282      | f        |                    15 | 2010-09-28     | NULL         |
|     13 | Adam Jurris            |      7 | civil    | client had chosen a degree other than IT and had to decleared bankruptcy.                                                            | (963)637-8282      | f        |                    15 | 2010-09-28     | 2013-02-12   |
|     12 | Diana Price            |      4 | civil    | client is charged with stealing of several television from his former place of employment. Client has a solid alibi                  | (863)637-8282      | w        |                    10 | 2011-09-28     | NULL         |
+--------+------------------------+--------+----------+--------------------------------------------------------------------------------------------------------------------------------------+--------------------+----------+-----------------------+----------------+--------------+
5 rows in set (0.00 sec)

mysql> select * from `case`;
+--------+--------+----------+--------------------------------------------------------------------------------------------------------------------------------------+----------------+--------------+------------------------+
| cse_id | per_id | cse_type | cse_description                                                                                                                      | cse_start_date | cse_end_date | cse_notes              |
+--------+--------+----------+--------------------------------------------------------------------------------------------------------------------------------------+----------------+--------------+------------------------+
|      1 |     13 | civil    | client says that his logo is being used without his content to promote a rival business                                              | 2010-09-28     | NULL         | copyright infringement |
|      2 |     14 | criminal | client is charged of assulting her husband                                                                                           | 2010-12-28     | NULL         | Assult                 |
|      3 |     11 | criminal | client broke an ankle while shopping at a local grocery store. No wet floor sign was posted although the floor had jsut being mopped | 2010-09-28     | 2010-12-24   | slip and fall          |
|      4 |     12 | civil    | client is charged with stealing of several television from his former place of employment. Client has a solid alibi                  | 2011-09-28     | NULL         | grand theft            |
|      5 |     13 | civil    | client is charged with posession of 10 grams of cocaine, allegedely found in his glove box by state police                           | 2010-09-28     | NULL         | posession of narcotics |
|      6 |     15 | criminal | client is charged with the murder of his co-worker over a lovers fued. Client has no alibi                                           | 2010-03-12     | NULL         | murder                 |
|      7 |     13 | civil    | client had chosen a degree other than IT and had to decleared bankruptcy.                                                            | 2010-09-28     | 2013-02-12   | Bankruptcy             |
+--------+--------+----------+--------------------------------------------------------------------------------------------------------------------------------------+----------------+--------------+------------------------+
7 rows in set (0.00 sec)

mysql> drop procedure if exists sp_case_and_judges;
Query OK, 0 rows affected, 1 warning (0.02 sec)

mysql> delimiter //
mysql> create procedure sp_case_and_judges()
    -> begin
    -> 
    -> select per_id, concat(per_fname, ' ', per_lname) as "Name (Firstname first)", cse_id, cse_type, cse_description, concat('(', substring(phn_num, 1,3),')', substring(phn_num, 4,3), '-' ,substring(phn_num, 7,4)) as "Judge Phone Number", phn_type, jud_years_in_practice, 
    -> cse_start_date, cse_end_date 
    -> from person
    -> natural join judge
    -> natural join `case`
    -> natural join phone
    -> 
    -> where per_type ='j'
    -> order by per_lname;
    -> 
    -> end //
Query OK, 0 rows affected (0.01 sec)

mysql> delimiter ;
mysql> select per_id, concat(per_fname, ' ', per_lname) as "Name (Firstname first)", cse_id, cse_type, cse_description, concat('(', substring(phn_num, 1,3),')', substring(phn_num, 4,3), '-' ,substring(phn_num, 7,4)) as "Judge Phone Number", phn_type, jud_years_in_practice, 
    -> cse_start_date, cse_end_date 
    -> from person
    -> natural join judge
    -> natural join `case`
    -> natural join phone
    -> 
    -> where per_type ='j'
    -> order by per_lname;
+--------+------------------------+--------+----------+--------------------------------------------------------------------------------------------------------------------------------------+--------------------+----------+-----------------------+----------------+--------------+
| per_id | Name (Firstname first) | cse_id | cse_type | cse_description                                                                                                                      | Judge Phone Number | phn_type | jud_years_in_practice | cse_start_date | cse_end_date |
+--------+------------------------+--------+----------+--------------------------------------------------------------------------------------------------------------------------------------+--------------------+----------+-----------------------+----------------+--------------+
|     11 | Arthur Curry           |      3 | criminal | client broke an ankle while shopping at a local grocery store. No wet floor sign was posted although the floor had jsut being mopped | (499)900-0222      | c        |                    12 | 2010-09-28     | 2010-12-24   |
|     13 | Adam Jurris            |      1 | civil    | client says that his logo is being used without his content to promote a rival business                                              | (963)637-8282      | f        |                    15 | 2010-09-28     | NULL         |
|     13 | Adam Jurris            |      5 | civil    | client is charged with posession of 10 grams of cocaine, allegedely found in his glove box by state police                           | (963)637-8282      | f        |                    15 | 2010-09-28     | NULL         |
|     13 | Adam Jurris            |      7 | civil    | client had chosen a degree other than IT and had to decleared bankruptcy.                                                            | (963)637-8282      | f        |                    15 | 2010-09-28     | 2013-02-12   |
|     12 | Diana Price            |      4 | civil    | client is charged with stealing of several television from his former place of employment. Client has a solid alibi                  | (863)637-8282      | w        |                    10 | 2011-09-28     | NULL         |
+--------+------------------------+--------+----------+--------------------------------------------------------------------------------------------------------------------------------------+--------------------+----------+-----------------------+----------------+--------------+
5 rows in set (0.00 sec)

mysql> drop procedure if exists sp_case_and_judges;
Query OK, 0 rows affected (0.02 sec)

mysql> delimiter //
mysql> create procedure sp_case_and_judges()
    -> begin
    -> 
    -> select per_id, concat(per_fname, ' ', per_lname) as "Name (Firstname first)", cse_id, cse_type, cse_description, concat('(', substring(phn_num, 1,3),')', substring(phn_num, 4,3), '-' ,substring(phn_num, 7,4)) as "Judge Phone Number", phn_type, jud_years_in_practice, 
    -> cse_start_date, cse_end_date 
    -> from person
    -> natural join judge
    -> natural join `case`
    -> natural join phone
    -> 
    -> where per_type ='j'
    -> order by per_lname;
    -> 
    -> end //
Query OK, 0 rows affected (0.01 sec)

mysql> select 'create sp_case_and_judges()'as '';
    -> do sleep(3);
    -> drop procedure if exists sp_case_and_judges;
    -> delimiter //
+-----------------------------+
|                             |
+-----------------------------+
| create sp_case_and_judges() |
+-----------------------------+
1 row in set (0.00 sec)

Query OK, 0 rows affected (3.00 sec)

Query OK, 0 rows affected (3.02 sec)

ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'delimiter' at line 1
mysql> create procedure sp_case_and_judges()
    -> begin
    -> 
    -> select per_id, concat(per_fname, ' ', per_lname) as "Name (Firstname first)", cse_id, cse_type, cse_description, concat('(', substring(phn_num, 1,3),')', substring(phn_num, 4,3), '-' ,substring(phn_num, 7,4)) as "Judge Phone Number", phn_type, jud_years_in_practice, 
    -> cse_start_date, cse_end_date 
    -> from person
    -> natural join judge
    -> natural join `case`
    -> natural join phone
    -> 
    -> where per_type ='j'
    -> order by per_lname;
    -> 
    -> end //
Query OK, 0 rows affected (0.01 sec)

mysql> delimiter ;
mysql> 
mysql> call sp_case_and_judges();
+--------+------------------------+--------+----------+--------------------------------------------------------------------------------------------------------------------------------------+--------------------+----------+-----------------------+----------------+--------------+
| per_id | Name (Firstname first) | cse_id | cse_type | cse_description                                                                                                                      | Judge Phone Number | phn_type | jud_years_in_practice | cse_start_date | cse_end_date |
+--------+------------------------+--------+----------+--------------------------------------------------------------------------------------------------------------------------------------+--------------------+----------+-----------------------+----------------+--------------+
|     11 | Arthur Curry           |      3 | criminal | client broke an ankle while shopping at a local grocery store. No wet floor sign was posted although the floor had jsut being mopped | (499)900-0222      | c        |                    12 | 2010-09-28     | 2010-12-24   |
|     13 | Adam Jurris            |      1 | civil    | client says that his logo is being used without his content to promote a rival business                                              | (963)637-8282      | f        |                    15 | 2010-09-28     | NULL         |
|     13 | Adam Jurris            |      5 | civil    | client is charged with posession of 10 grams of cocaine, allegedely found in his glove box by state police                           | (963)637-8282      | f        |                    15 | 2010-09-28     | NULL         |
|     13 | Adam Jurris            |      7 | civil    | client had chosen a degree other than IT and had to decleared bankruptcy.                                                            | (963)637-8282      | f        |                    15 | 2010-09-28     | 2013-02-12   |
|     12 | Diana Price            |      4 | civil    | client is charged with stealing of several television from his former place of employment. Client has a solid alibi                  | (863)637-8282      | w        |                    10 | 2011-09-28     | NULL         |
+--------+------------------------+--------+----------+--------------------------------------------------------------------------------------------------------------------------------------+--------------------+----------+-----------------------+----------------+--------------+
5 rows in set (0.00 sec)

Query OK, 0 rows affected (0.07 sec)

mysql> do sleep(3);
Query OK, 0 rows affected (3.00 sec)

mysql> select * from judge_hist;
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
| jhs_id | per_id | jhs_crt_id | jhs_date            | jhs_type | jhs_salary | jhs_notes                                            |
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
|      1 |     11 |          3 | 2009-01-16 00:00:00 | i        |  130000.00 | NULL                                                 |
|      2 |     12 |          3 | 2009-01-16 00:00:00 | i        |  140000.00 | NULL                                                 |
|      3 |     13 |          3 | 2009-01-16 00:00:00 | i        |  165000.00 | NULL                                                 |
|      4 |     11 |          3 | 2009-01-16 00:00:00 | i        |  135000.00 | freshman justice                                     |
|      5 |     14 |          3 | 2009-01-16 00:00:00 | i        |  165000.00 | became chief justice                                 |
|      6 |     15 |          3 | 2009-04-18 00:00:00 | i        |  170000.00 | reassigned to court based upon local area population |
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
6 rows in set (0.00 sec)

mysql>   select * from judge;
+--------+--------+------------+-----------------------+-----------+
| per_id | crt_id | jud_salary | jud_years_in_practice | jud_notes |
+--------+--------+------------+-----------------------+-----------+
|     11 |      1 |  150000.00 |                    12 | Judge 1   |
|     12 |      2 |  140000.00 |                    10 | Judge 2   |
|     13 |      3 |  100000.00 |                    15 | Judge 3   |
|     14 |      4 |   95000.00 |                    12 | Judge 4   |
|     15 |      5 |  120000.00 |                    11 | Judge 5   |
+--------+--------+------------+-----------------------+-----------+
5 rows in set (0.00 sec)

mysql> select count(per_id) from person;
+---------------+
| count(per_id) |
+---------------+
|            15 |
+---------------+
1 row in set (0.01 sec)

mysql> select per_id, per_fname, per_lname from person;
+--------+-----------+------------+
| per_id | per_fname | per_lname  |
+--------+-----------+------------+
|      1 | Steve     | Rogers     |
|      2 | Bruce     | Wayne      |
|      3 | Peter     | Parker     |
|      4 | Jane      | Thompson   |
|      5 | Debra     | Steele     |
|      6 | Tony      | Stark      |
|      7 | Hank      | Pymi       |
|      8 | Bob       | Best       |
|      9 | Sandra    | Dole       |
|     10 | Ben       | Avery      |
|     11 | Arthur    | Curry      |
|     12 | Diana     | Price      |
|     13 | Adam      | Jurris     |
|     14 | Judy      | Sleen      |
|     15 | Bill      | Neiderheim |
+--------+-----------+------------+
15 rows in set (0.00 sec)

mysql> do sleep(5);
Query OK, 0 rows affected (5.00 sec)

mysql> 
mysql> insert into person 
    -> values
    -> (null, unhex(sha2(000000000,512)), 'Bobby', 'Sue', 'J.Cole street', 'Panama City Beach','FL', 4453245678, 'bsue@fl.gov','1962-05-16', 'j', 'new district judge');
ERROR 1136 (21S01): Column count doesn't match value count at row 1
mysql> 
mysql> select 'show person table AFTER adding the new record' as '';
+-----------------------------------------------+
|                                               |
+-----------------------------------------------+
| show person table AFTER adding the new record |
+-----------------------------------------------+
1 row in set (0.00 sec)

mysql> select per_id, per_fname, per_lname from person;
+--------+-----------+------------+
| per_id | per_fname | per_lname  |
+--------+-----------+------------+
|      1 | Steve     | Rogers     |
|      2 | Bruce     | Wayne      |
|      3 | Peter     | Parker     |
|      4 | Jane      | Thompson   |
|      5 | Debra     | Steele     |
|      6 | Tony      | Stark      |
|      7 | Hank      | Pymi       |
|      8 | Bob       | Best       |
|      9 | Sandra    | Dole       |
|     10 | Ben       | Avery      |
|     11 | Arthur    | Curry      |
|     12 | Diana     | Price      |
|     13 | Adam      | Jurris     |
|     14 | Judy      | Sleen      |
|     15 | Bill      | Neiderheim |
+--------+-----------+------------+
15 rows in set (0.00 sec)

mysql> do sleep (3);
Query OK, 0 rows affected (3.00 sec)

mysql> 
mysql> 
mysql> select 'show judge/judge data before AFTER INSERT TRIGGER fires (tr_judge_history_after_insert)' as '';
+-----------------------------------------------------------------------------------------+
|                                                                                         |
+-----------------------------------------------------------------------------------------+
| show judge/judge data before AFTER INSERT TRIGGER fires (tr_judge_history_after_insert) |
+-----------------------------------------------------------------------------------------+
1 row in set (0.00 sec)

mysql> select * from judge;
+--------+--------+------------+-----------------------+-----------+
| per_id | crt_id | jud_salary | jud_years_in_practice | jud_notes |
+--------+--------+------------+-----------------------+-----------+
|     11 |      1 |  150000.00 |                    12 | Judge 1   |
|     12 |      2 |  140000.00 |                    10 | Judge 2   |
|     13 |      3 |  100000.00 |                    15 | Judge 3   |
|     14 |      4 |   95000.00 |                    12 | Judge 4   |
|     15 |      5 |  120000.00 |                    11 | Judge 5   |
+--------+--------+------------+-----------------------+-----------+
5 rows in set (0.00 sec)

mysql> select * from judge_hist;
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
| jhs_id | per_id | jhs_crt_id | jhs_date            | jhs_type | jhs_salary | jhs_notes                                            |
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
|      1 |     11 |          3 | 2009-01-16 00:00:00 | i        |  130000.00 | NULL                                                 |
|      2 |     12 |          3 | 2009-01-16 00:00:00 | i        |  140000.00 | NULL                                                 |
|      3 |     13 |          3 | 2009-01-16 00:00:00 | i        |  165000.00 | NULL                                                 |
|      4 |     11 |          3 | 2009-01-16 00:00:00 | i        |  135000.00 | freshman justice                                     |
|      5 |     14 |          3 | 2009-01-16 00:00:00 | i        |  165000.00 | became chief justice                                 |
|      6 |     15 |          3 | 2009-04-18 00:00:00 | i        |  170000.00 | reassigned to court based upon local area population |
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
6 rows in set (0.00 sec)

mysql> do sleep(5);
Query OK, 0 rows affected (5.00 sec)

mysql> 
mysql> 
mysql> 
mysql> drop trigger if exists tr_judge_history_after_insert;
Query OK, 0 rows affected, 1 warning (0.02 sec)

mysql> 
mysql> delimiter //
mysql> 
mysql> create trigger tr_judge_history_after_insert
    -> AFTER INSERT ON judge
    -> FOR EACH ROW
    -> 
    -> BEGIN
    -> 
    -> insert into judge_hist
    -> values
    -> (new.per_id, new.crt_id, current_timestamp(),'i', new.jud_salary, concat('modifying user:', new.jud_notes)
    -> );
    -> end //
Query OK, 0 rows affected (0.01 sec)

mysql> delimiter ;
mysql> 
mysql> 
mysql> -- This comment is here for easy readability of the code
mysql> 
mysql> 
mysql> 
mysql> select 'fire trigger by inserting record into judge table' as '';
+---------------------------------------------------+
|                                                   |
+---------------------------------------------------+
| fire trigger by inserting record into judge table |
+---------------------------------------------------+
1 row in set (0.00 sec)

mysql> do sleep(5);
Query OK, 0 rows affected (5.00 sec)

mysql> 
mysql> insert into judge 
    -> values
    -> ((select count(per_id) from person), 3, 143300,25,'transferred from neighboring jurisdiction');
ERROR 1062 (23000): Duplicate entry '15' for key 'PRIMARY'
mysql> 
mysql> select 'show judge/judge_hist data AFTER INSERT trigger fires (tr_judge_history_after_insert)'
    -> select * from judge;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'select * from judge' at line 2
mysql> select * from judge_hist;
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
| jhs_id | per_id | jhs_crt_id | jhs_date            | jhs_type | jhs_salary | jhs_notes                                            |
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
|      1 |     11 |          3 | 2009-01-16 00:00:00 | i        |  130000.00 | NULL                                                 |
|      2 |     12 |          3 | 2009-01-16 00:00:00 | i        |  140000.00 | NULL                                                 |
|      3 |     13 |          3 | 2009-01-16 00:00:00 | i        |  165000.00 | NULL                                                 |
|      4 |     11 |          3 | 2009-01-16 00:00:00 | i        |  135000.00 | freshman justice                                     |
|      5 |     14 |          3 | 2009-01-16 00:00:00 | i        |  165000.00 | became chief justice                                 |
|      6 |     15 |          3 | 2009-04-18 00:00:00 | i        |  170000.00 | reassigned to court based upon local area population |
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
6 rows in set (0.00 sec)

mysql> do sleep(7);
Query OK, 0 rows affected (7.00 sec)

mysql> 
mysql> 
mysql> drop trigger if exists tr_judge_history_after_insert;
Query OK, 0 rows affected (0.02 sec)

mysql> select * from judge;
+--------+--------+------------+-----------------------+-----------+
| per_id | crt_id | jud_salary | jud_years_in_practice | jud_notes |
+--------+--------+------------+-----------------------+-----------+
|     11 |      1 |  150000.00 |                    12 | Judge 1   |
|     12 |      2 |  140000.00 |                    10 | Judge 2   |
|     13 |      3 |  100000.00 |                    15 | Judge 3   |
|     14 |      4 |   95000.00 |                    12 | Judge 4   |
|     15 |      5 |  120000.00 |                    11 | Judge 5   |
+--------+--------+------------+-----------------------+-----------+
5 rows in set (0.00 sec)

mysql> select * from person;
+--------+------------------------------------------------------------------+------------------------------------------------------------------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
| per_id | per_ssn                                                          | per_sslt                                                         | per_fname | per_lname  | per_street             | per_city    | per_state | per_zip    | per_email               | per_dob    | per_type | per_notes |
+--------+------------------------------------------------------------------+------------------------------------------------------------------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
|      1 | V������KvF�6�4*��A��-����`*��ғ�e���+m�g[Ai�������                                    | Q<�/�N;1�) �B_.!��R^�+7J�$[�v��W�p�/��7����wm�,�`���1                         | Steve     | Rogers     | 437 Southern Drive     | Rochester   | NY        |  032443022 | srogers@comcast.net     | 1923-10-03 | c        | NULL      |
|      2 | 6\ 5����/7gڃ-G_+ػ+�i+�0�yZۭ��{�]�,��|�A؛Gıqk,��-���b� B                          | �+�[�����=�+UTcS
+�|KlRW�V��Mr%Έ���}0��p?�w�*���AMe��UQ                           | Bruce     | Wayne      | 1007 Mountain Drive    | Gotham      | NY        |  003243440 | bwayne@knology.net      | 1968-03-20 | c        | NULL      |
|      3 | 982���+D֘L����`B�&�JԘFҍ9TVum��X �`")��^�u�jf�ul����j�R                          | �6����d��ˉ���F�f���h�3M���/��s�x%7����fz��Ba�|`���                                   | Peter     | Parker     | 20 Ingram Street       | New York    | NY        |  102862341 | pparker@msn.net         | 1923-10-03 | c        | NULL      |
|      4 | ~/c���/�)�f�w�`���3|�ᠰ%ȫVs�Rq���٪�#cuob�)���d�W��{                              | �Ӭ9��m�:��K.V\5{<7>�P��nΑ.1*����'k�o���B��Q�GeB� tI�P��                            | Jane      | Thompson   | 13563 Ocean View Drive | Seattle     | WA        |  032084409 | jthompson@gmail.net     | 1973-05-08 | c        | NULL      |
|      5 | ���:�����c��y�;Ҷ�WL��e7;��I���9�Z��j�O��5�)�>�a���\�                                    | *��K�{Y����F�L>�r	,3)y"{��M6��[��f�a��d{`3���O?lg�4��ݧU��                            | Debra     | Steele     | 543 Oak Ln             | Milwaukee   | WI        | 2862374478 | dsteele@verizon.net     | 1994-07-19 | c        | NULL      |
|      6 | Jb����Z,r���@Kg� @!'�&o0P�nk��7�a4��%�&�Zm�������;VӺ&                           | }�8Z���t$�3D 	�
Ε�iv3j+�cq�訞p`c����� E�4�W��oK� ��6��A�/�                            | Tony      | Stark      | 332 Palm Avenue        | Malibu      | CA        |  902638832 | tstark@yahoo.com        | 1972-05-04 | a        | NULL      |
|      7 | "*��r�dG�dV�.�Dy�ÖKѾ��y��Q�^����|�����tMHS��-~V5�y��z�z���Y                                 | �@cJ�koZb�]�p=\�=�M���,��9�#\�Q�N\�5�<�%�G��PU����Z���                            | Hank      | Pymi       | 2355 Brown Street      | Cleveland   | OH        |  022342390 | hpym@aol.net            | 1980-08-28 | a        | NULL      |
|      8 | �D��`-QB��e� e|*��8��tI!/�m���	OG�tO�8<�q�G 	�1PH�}��� �                         | ���ȃ��{�[�'�)�e#��\�z9�z�})�׼��Z/ sE��3Åȹ�r�N��?�r<K�                                | Bob       | Best       | 4902 Avendale Ave.     | Scottsdale  | AZ        |  087263912 | bbest@yahoo.com         | 1992-02-10 | a        | NULL      |
|      9 | v���;�������Q�t���T7�f��3]�G�
� �<�}�9�'����E���������v�>                                         | �7�/P������S�LM�����
Ӎ�H#����	�vp!WӁZ��@�vm�tu&����G��                                   | Sandra    | Dole       | 8732 Lawrence Ave.     | Atlanta     | GA        |  002345390 | sdole@gmail.net         | 1990-01-26 | a        | NULL      |
|     10 | ޳{�@��7�e}��4�?a�E,vD]����r��2�1�ů��9f�j�_���p�$3��                              | uˈܻ��Y1C�d��L麳�s�Mʳ���lJ#	~����T�d&!S�y��/x�]xZ��!                           | Ben       | Avery      | 6432 Thunderbird Ln    | Sioux Falls | SD        |  005637932 | bavery@hotmail.net      | 1983-12-24 | a        | NULL      |
|     11 | �:0��Z�Z���,�6��� V���}�DA�a�f�7�b;�ɛ��m'J52��9DS��c                             | ���E�jJ3�`���A݊k�A�F��x�b����MDO�_�7x��Y	+�-��>�%��*h�g���0                                   | Arthur    | Curry      | 3302 Euclid Avenue     | Miami       | FL        |  000219932 | acurry@gmail.com        | 1975-12-15 | j        | NULL      |
|     12 | �N�>�50W!��z�; =�ɒ`�3��ek��~q�/�u�A_���
J�~�{*T�A��nF\�j                           | k��#+�0��
�ԪTG�9�;�5IR�m�q9*�:�L��)B�!��k�2�tH�7A��������                                | Diana     | Price      | 944 Green Street       | Las vegas   | NV        |  033235223 | dprice@aol.net          | 1980-08-22 | j        | NULL      |
|     13 | +�%r��6dS�KVTD�;}~�t�L�F��_�4�j�g��S7�L�L�S}޸��`h-#l;���(�                           | ��'������V��
#.�y��$�v�+8�l�P�S���R)��������9�oE�/��[[�AdG                                     | Adam      | Jurris     | 98435 Valencia Dr.     | Gulf Shore  | AL        |  870639912 | ajurris@yahoo.com       | 1995-01-31 | j        | NULL      |
|     14 | �Rb�������XT���p�c������Қ��Oq�K�By�b,���O��g�}1ޅK                                 | ���z��=Py	��£�\@���X���\���r�d.��r"�:����F9��dn��a                                | Judy      | Sleen      | 56343 Rover Ct.        | Billings    | MT        |  067204823 | jsleen@symaptico.com    | 1995-01-31 | j        | NULL      |
|     15 | �T*o�1�L�����n��0��K��z$�@'N��x2 ��e���Vp��?H��	�O�{c                             | e1b��=�{Ф���(!EH�ē���%U#���t)�!=��Mr�	'G�%����2���R$��D                                | Bill      | Neiderheim | 43567 Netherland Blvd  | South Bend  | IN        |  320219932 | bneiderheim@comcast.net | 1982-03-24 | j        | NULL      |
+--------+------------------------------------------------------------------+------------------------------------------------------------------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
15 rows in set (0.00 sec)

mysql> select length(per_ssn) from person;
+-----------------+
| length(per_ssn) |
+-----------------+
|              64 |
|              64 |
|              64 |
|              64 |
|              64 |
|              64 |
|              64 |
|              64 |
|              64 |
|              64 |
|              64 |
|              64 |
|              64 |
|              64 |
|              64 |
+-----------------+
15 rows in set (0.00 sec)

mysql> select length(per_ssn), per_id from person;
+-----------------+--------+
| length(per_ssn) | per_id |
+-----------------+--------+
|              64 |      7 |
|              64 |     13 |
|              64 |      2 |
|              64 |      3 |
|              64 |      6 |
|              64 |      1 |
|              64 |      9 |
|              64 |      4 |
|              64 |     11 |
|              64 |     14 |
|              64 |     15 |
|              64 |     12 |
|              64 |     10 |
|              64 |      5 |
|              64 |      8 |
+-----------------+--------+
15 rows in set (0.00 sec)

mysql> select length(per_ssn), per_id from person order by per_id asc;
+-----------------+--------+
| length(per_ssn) | per_id |
+-----------------+--------+
|              64 |      1 |
|              64 |      2 |
|              64 |      3 |
|              64 |      4 |
|              64 |      5 |
|              64 |      6 |
|              64 |      7 |
|              64 |      8 |
|              64 |      9 |
|              64 |     10 |
|              64 |     11 |
|              64 |     12 |
|              64 |     13 |
|              64 |     14 |
|              64 |     15 |
+-----------------+--------+
15 rows in set (0.00 sec)

mysql> select per_id, per_fname, per_lname from person;
+--------+-----------+------------+
| per_id | per_fname | per_lname  |
+--------+-----------+------------+
|      1 | Steve     | Rogers     |
|      2 | Bruce     | Wayne      |
|      3 | Peter     | Parker     |
|      4 | Jane      | Thompson   |
|      5 | Debra     | Steele     |
|      6 | Tony      | Stark      |
|      7 | Hank      | Pymi       |
|      8 | Bob       | Best       |
|      9 | Sandra    | Dole       |
|     10 | Ben       | Avery      |
|     11 | Arthur    | Curry      |
|     12 | Diana     | Price      |
|     13 | Adam      | Jurris     |
|     14 | Judy      | Sleen      |
|     15 | Bill      | Neiderheim |
+--------+-----------+------------+
15 rows in set (0.00 sec)

mysql> do sleep(5);
Query OK, 0 rows affected (5.00 sec)

mysql> 
mysql> insert into person 
    -> values
    -> (null, unhex(sha2(000000000,512)), 'Bobby', 'Sue', 'J.Cole street', 'Panama City Beach','FL', 4453245678, 'bsue@fl.gov','1962-05-16', 'j', 'new district judge');
ERROR 1136 (21S01): Column count doesn't match value count at row 1
mysql> 
mysql> select 'show person table AFTER adding the new record' as '';
+-----------------------------------------------+
|                                               |
+-----------------------------------------------+
| show person table AFTER adding the new record |
+-----------------------------------------------+
1 row in set (0.00 sec)

mysql> select per_id, per_fname, per_lname from person;
+--------+-----------+------------+
| per_id | per_fname | per_lname  |
+--------+-----------+------------+
|      1 | Steve     | Rogers     |
|      2 | Bruce     | Wayne      |
|      3 | Peter     | Parker     |
|      4 | Jane      | Thompson   |
|      5 | Debra     | Steele     |
|      6 | Tony      | Stark      |
|      7 | Hank      | Pymi       |
|      8 | Bob       | Best       |
|      9 | Sandra    | Dole       |
|     10 | Ben       | Avery      |
|     11 | Arthur    | Curry      |
|     12 | Diana     | Price      |
|     13 | Adam      | Jurris     |
|     14 | Judy      | Sleen      |
|     15 | Bill      | Neiderheim |
+--------+-----------+------------+
15 rows in set (0.00 sec)

mysql> do sleep (3);
Query OK, 0 rows affected (3.00 sec)

mysql> 
mysql> 
mysql> select 'show judge/judge data before AFTER INSERT TRIGGER fires (tr_judge_history_after_insert)' as '';
+-----------------------------------------------------------------------------------------+
|                                                                                         |
+-----------------------------------------------------------------------------------------+
| show judge/judge data before AFTER INSERT TRIGGER fires (tr_judge_history_after_insert) |
+-----------------------------------------------------------------------------------------+
1 row in set (0.00 sec)

mysql> select * from judge;
+--------+--------+------------+-----------------------+-----------+
| per_id | crt_id | jud_salary | jud_years_in_practice | jud_notes |
+--------+--------+------------+-----------------------+-----------+
|     11 |      1 |  150000.00 |                    12 | Judge 1   |
|     12 |      2 |  140000.00 |                    10 | Judge 2   |
|     13 |      3 |  100000.00 |                    15 | Judge 3   |
|     14 |      4 |   95000.00 |                    12 | Judge 4   |
|     15 |      5 |  120000.00 |                    11 | Judge 5   |
+--------+--------+------------+-----------------------+-----------+
5 rows in set (0.00 sec)

mysql> select * from judge_hist;
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
| jhs_id | per_id | jhs_crt_id | jhs_date            | jhs_type | jhs_salary | jhs_notes                                            |
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
|      1 |     11 |          3 | 2009-01-16 00:00:00 | i        |  130000.00 | NULL                                                 |
|      2 |     12 |          3 | 2009-01-16 00:00:00 | i        |  140000.00 | NULL                                                 |
|      3 |     13 |          3 | 2009-01-16 00:00:00 | i        |  165000.00 | NULL                                                 |
|      4 |     11 |          3 | 2009-01-16 00:00:00 | i        |  135000.00 | freshman justice                                     |
|      5 |     14 |          3 | 2009-01-16 00:00:00 | i        |  165000.00 | became chief justice                                 |
|      6 |     15 |          3 | 2009-04-18 00:00:00 | i        |  170000.00 | reassigned to court based upon local area population |
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
6 rows in set (0.00 sec)

mysql> do sleep(5);
Query OK, 0 rows affected (5.00 sec)

mysql> 
mysql> 
mysql> 
mysql> drop trigger if exists tr_judge_history_after_insert;
Query OK, 0 rows affected, 1 warning (0.01 sec)

mysql> 
mysql> delimiter //
mysql> 
mysql> create trigger tr_judge_history_after_insert
    -> AFTER INSERT ON judge
    -> FOR EACH ROW
    -> 
    -> BEGIN
    -> 
    -> insert into judge_hist
    -> values
    -> (new.per_id, new.crt_id, current_timestamp(),'i', new.jud_salary, concat('modifying user:', new.jud_notes)
    -> );
    -> end //
Query OK, 0 rows affected (0.01 sec)

mysql> delimiter ;
mysql> 
mysql> 
mysql> -- This comment is here for easy readability of the code
mysql> 
mysql> 
mysql> 
mysql> select 'fire trigger by inserting record into judge table' as '';
+---------------------------------------------------+
|                                                   |
+---------------------------------------------------+
| fire trigger by inserting record into judge table |
+---------------------------------------------------+
1 row in set (0.00 sec)

mysql> do sleep(5);
Query OK, 0 rows affected (5.00 sec)

mysql> 
mysql> insert into judge 
    -> values
    -> ((select count(per_id) from person), 3, 143300,25,'transferred from neighboring jurisdiction');
ERROR 1062 (23000): Duplicate entry '15' for key 'PRIMARY'
mysql> 
mysql> select 'show judge/judge_hist data AFTER INSERT trigger fires (tr_judge_history_after_insert)' as '';
+---------------------------------------------------------------------------------------+
|                                                                                       |
+---------------------------------------------------------------------------------------+
| show judge/judge_hist data AFTER INSERT trigger fires (tr_judge_history_after_insert) |
+---------------------------------------------------------------------------------------+
1 row in set (0.00 sec)

mysql> select * from judge;
+--------+--------+------------+-----------------------+-----------+
| per_id | crt_id | jud_salary | jud_years_in_practice | jud_notes |
+--------+--------+------------+-----------------------+-----------+
|     11 |      1 |  150000.00 |                    12 | Judge 1   |
|     12 |      2 |  140000.00 |                    10 | Judge 2   |
|     13 |      3 |  100000.00 |                    15 | Judge 3   |
|     14 |      4 |   95000.00 |                    12 | Judge 4   |
|     15 |      5 |  120000.00 |                    11 | Judge 5   |
+--------+--------+------------+-----------------------+-----------+
5 rows in set (0.00 sec)

mysql> select * from judge_hist;
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
| jhs_id | per_id | jhs_crt_id | jhs_date            | jhs_type | jhs_salary | jhs_notes                                            |
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
|      1 |     11 |          3 | 2009-01-16 00:00:00 | i        |  130000.00 | NULL                                                 |
|      2 |     12 |          3 | 2009-01-16 00:00:00 | i        |  140000.00 | NULL                                                 |
|      3 |     13 |          3 | 2009-01-16 00:00:00 | i        |  165000.00 | NULL                                                 |
|      4 |     11 |          3 | 2009-01-16 00:00:00 | i        |  135000.00 | freshman justice                                     |
|      5 |     14 |          3 | 2009-01-16 00:00:00 | i        |  165000.00 | became chief justice                                 |
|      6 |     15 |          3 | 2009-04-18 00:00:00 | i        |  170000.00 | reassigned to court based upon local area population |
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
6 rows in set (0.00 sec)

mysql> do sleep(7);
Query OK, 0 rows affected (7.00 sec)

mysql> 
mysql> 
mysql> drop trigger if exists tr_judge_history_after_insert;
Query OK, 0 rows affected (0.02 sec)

mysql> select * from person;
+--------+------------------------------------------------------------------+------------------------------------------------------------------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
| per_id | per_ssn                                                          | per_sslt                                                         | per_fname | per_lname  | per_street             | per_city    | per_state | per_zip    | per_email               | per_dob    | per_type | per_notes |
+--------+------------------------------------------------------------------+------------------------------------------------------------------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
|      1 | V������KvF�6�4*��A��-����`*��ғ�e���+m�g[Ai�������                                    | Q<�/�N;1�) �B_.!��R^�+7J�$[�v��W�p�/��7����wm�,�`���1                         | Steve     | Rogers     | 437 Southern Drive     | Rochester   | NY        |  032443022 | srogers@comcast.net     | 1923-10-03 | c        | NULL      |
|      2 | 6\ 5����/7gڃ-G_+ػ+�i+�0�yZۭ��{�]�,��|�A؛Gıqk,��-���b� B                          | �+�[�����=�+UTcS
+�|KlRW�V��Mr%Έ���}0��p?�w�*���AMe��UQ                           | Bruce     | Wayne      | 1007 Mountain Drive    | Gotham      | NY        |  003243440 | bwayne@knology.net      | 1968-03-20 | c        | NULL      |
|      3 | 982���+D֘L����`B�&�JԘFҍ9TVum��X �`")��^�u�jf�ul����j�R                          | �6����d��ˉ���F�f���h�3M���/��s�x%7����fz��Ba�|`���                                   | Peter     | Parker     | 20 Ingram Street       | New York    | NY        |  102862341 | pparker@msn.net         | 1923-10-03 | c        | NULL      |
|      4 | ~/c���/�)�f�w�`���3|�ᠰ%ȫVs�Rq���٪�#cuob�)���d�W��{                              | �Ӭ9��m�:��K.V\5{<7>�P��nΑ.1*����'k�o���B��Q�GeB� tI�P��                            | Jane      | Thompson   | 13563 Ocean View Drive | Seattle     | WA        |  032084409 | jthompson@gmail.net     | 1973-05-08 | c        | NULL      |
|      5 | ���:�����c��y�;Ҷ�WL��e7;��I���9�Z��j�O��5�)�>�a���\�                                    | *��K�{Y����F�L>�r	,3)y"{��M6��[��f�a��d{`3���O?lg�4��ݧU��                            | Debra     | Steele     | 543 Oak Ln             | Milwaukee   | WI        | 2862374478 | dsteele@verizon.net     | 1994-07-19 | c        | NULL      |
|      6 | Jb����Z,r���@Kg� @!'�&o0P�nk��7�a4��%�&�Zm�������;VӺ&                           | }�8Z���t$�3D 	�
Ε�iv3j+�cq�訞p`c����� E�4�W��oK� ��6��A�/�                            | Tony      | Stark      | 332 Palm Avenue        | Malibu      | CA        |  902638832 | tstark@yahoo.com        | 1972-05-04 | a        | NULL      |
|      7 | "*��r�dG�dV�.�Dy�ÖKѾ��y��Q�^����|�����tMHS��-~V5�y��z�z���Y                                 | �@cJ�koZb�]�p=\�=�M���,��9�#\�Q�N\�5�<�%�G��PU����Z���                            | Hank      | Pymi       | 2355 Brown Street      | Cleveland   | OH        |  022342390 | hpym@aol.net            | 1980-08-28 | a        | NULL      |
|      8 | �D��`-QB��e� e|*��8��tI!/�m���	OG�tO�8<�q�G 	�1PH�}��� �                         | ���ȃ��{�[�'�)�e#��\�z9�z�})�׼��Z/ sE��3Åȹ�r�N��?�r<K�                                | Bob       | Best       | 4902 Avendale Ave.     | Scottsdale  | AZ        |  087263912 | bbest@yahoo.com         | 1992-02-10 | a        | NULL      |
|      9 | v���;�������Q�t���T7�f��3]�G�
� �<�}�9�'����E���������v�>                                         | �7�/P������S�LM�����
Ӎ�H#����	�vp!WӁZ��@�vm�tu&����G��                                   | Sandra    | Dole       | 8732 Lawrence Ave.     | Atlanta     | GA        |  002345390 | sdole@gmail.net         | 1990-01-26 | a        | NULL      |
|     10 | ޳{�@��7�e}��4�?a�E,vD]����r��2�1�ů��9f�j�_���p�$3��                              | uˈܻ��Y1C�d��L麳�s�Mʳ���lJ#	~����T�d&!S�y��/x�]xZ��!                           | Ben       | Avery      | 6432 Thunderbird Ln    | Sioux Falls | SD        |  005637932 | bavery@hotmail.net      | 1983-12-24 | a        | NULL      |
|     11 | �:0��Z�Z���,�6��� V���}�DA�a�f�7�b;�ɛ��m'J52��9DS��c                             | ���E�jJ3�`���A݊k�A�F��x�b����MDO�_�7x��Y	+�-��>�%��*h�g���0                                   | Arthur    | Curry      | 3302 Euclid Avenue     | Miami       | FL        |  000219932 | acurry@gmail.com        | 1975-12-15 | j        | NULL      |
|     12 | �N�>�50W!��z�; =�ɒ`�3��ek��~q�/�u�A_���
J�~�{*T�A��nF\�j                           | k��#+�0��
�ԪTG�9�;�5IR�m�q9*�:�L��)B�!��k�2�tH�7A��������                                | Diana     | Price      | 944 Green Street       | Las vegas   | NV        |  033235223 | dprice@aol.net          | 1980-08-22 | j        | NULL      |
|     13 | +�%r��6dS�KVTD�;}~�t�L�F��_�4�j�g��S7�L�L�S}޸��`h-#l;���(�                           | ��'������V��
#.�y��$�v�+8�l�P�S���R)��������9�oE�/��[[�AdG                                     | Adam      | Jurris     | 98435 Valencia Dr.     | Gulf Shore  | AL        |  870639912 | ajurris@yahoo.com       | 1995-01-31 | j        | NULL      |
|     14 | �Rb�������XT���p�c������Қ��Oq�K�By�b,���O��g�}1ޅK                                 | ���z��=Py	��£�\@���X���\���r�d.��r"�:����F9��dn��a                                | Judy      | Sleen      | 56343 Rover Ct.        | Billings    | MT        |  067204823 | jsleen@symaptico.com    | 1995-01-31 | j        | NULL      |
|     15 | �T*o�1�L�����n��0��K��z$�@'N��x2 ��e���Vp��?H��	�O�{c                             | e1b��=�{Ф���(!EH�ē���%U#���t)�!=��Mr�	'G�%����2���R$��D                                | Bill      | Neiderheim | 43567 Netherland Blvd  | South Bend  | IN        |  320219932 | bneiderheim@comcast.net | 1982-03-24 | j        | NULL      |
+--------+------------------------------------------------------------------+------------------------------------------------------------------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
15 rows in set (0.00 sec)

mysql> select per_id, per_fname, per_lname from person;
+--------+-----------+------------+
| per_id | per_fname | per_lname  |
+--------+-----------+------------+
|      1 | Steve     | Rogers     |
|      2 | Bruce     | Wayne      |
|      3 | Peter     | Parker     |
|      4 | Jane      | Thompson   |
|      5 | Debra     | Steele     |
|      6 | Tony      | Stark      |
|      7 | Hank      | Pymi       |
|      8 | Bob       | Best       |
|      9 | Sandra    | Dole       |
|     10 | Ben       | Avery      |
|     11 | Arthur    | Curry      |
|     12 | Diana     | Price      |
|     13 | Adam      | Jurris     |
|     14 | Judy      | Sleen      |
|     15 | Bill      | Neiderheim |
+--------+-----------+------------+
15 rows in set (0.00 sec)

mysql> do sleep(5);
Query OK, 0 rows affected (5.00 sec)

mysql> 
mysql> set @salt = random_bytes(64); set @ran_num = FLOOR(RAND() * (999999999 - 111111111 + 1)) + 111111111; 
Query OK, 0 rows affected (0.00 sec)

Query OK, 0 rows affected (0.01 sec)

mysql> insert into person 
    -> values
    -> (null, unhex(sha2(000000000,512)), @salt, 'Bobby', 'Sue', 'J.Cole street', 'Panama City Beach','FL', 4453245678, 'bsue@fl.gov','1962-05-16', 'j', 'new district judge');
ERROR 1264 (22003): Out of range value for column 'per_zip' at row 1
mysql> 
mysql> select 'show person table AFTER adding the new record' as '';
+-----------------------------------------------+
|                                               |
+-----------------------------------------------+
| show person table AFTER adding the new record |
+-----------------------------------------------+
1 row in set (0.00 sec)

mysql> select per_id, per_fname, per_lname from person;
+--------+-----------+------------+
| per_id | per_fname | per_lname  |
+--------+-----------+------------+
|      1 | Steve     | Rogers     |
|      2 | Bruce     | Wayne      |
|      3 | Peter     | Parker     |
|      4 | Jane      | Thompson   |
|      5 | Debra     | Steele     |
|      6 | Tony      | Stark      |
|      7 | Hank      | Pymi       |
|      8 | Bob       | Best       |
|      9 | Sandra    | Dole       |
|     10 | Ben       | Avery      |
|     11 | Arthur    | Curry      |
|     12 | Diana     | Price      |
|     13 | Adam      | Jurris     |
|     14 | Judy      | Sleen      |
|     15 | Bill      | Neiderheim |
+--------+-----------+------------+
15 rows in set (0.00 sec)

mysql> do sleep (3);
Query OK, 0 rows affected (3.00 sec)

mysql> 
mysql> 
mysql> select 'show judge/judge data before AFTER INSERT TRIGGER fires (tr_judge_history_after_insert)' as '';
+-----------------------------------------------------------------------------------------+
|                                                                                         |
+-----------------------------------------------------------------------------------------+
| show judge/judge data before AFTER INSERT TRIGGER fires (tr_judge_history_after_insert) |
+-----------------------------------------------------------------------------------------+
1 row in set (0.00 sec)

mysql> select * from judge;
+--------+--------+------------+-----------------------+-----------+
| per_id | crt_id | jud_salary | jud_years_in_practice | jud_notes |
+--------+--------+------------+-----------------------+-----------+
|     11 |      1 |  150000.00 |                    12 | Judge 1   |
|     12 |      2 |  140000.00 |                    10 | Judge 2   |
|     13 |      3 |  100000.00 |                    15 | Judge 3   |
|     14 |      4 |   95000.00 |                    12 | Judge 4   |
|     15 |      5 |  120000.00 |                    11 | Judge 5   |
+--------+--------+------------+-----------------------+-----------+
5 rows in set (0.00 sec)

mysql> select * from judge_hist;
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
| jhs_id | per_id | jhs_crt_id | jhs_date            | jhs_type | jhs_salary | jhs_notes                                            |
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
|      1 |     11 |          3 | 2009-01-16 00:00:00 | i        |  130000.00 | NULL                                                 |
|      2 |     12 |          3 | 2009-01-16 00:00:00 | i        |  140000.00 | NULL                                                 |
|      3 |     13 |          3 | 2009-01-16 00:00:00 | i        |  165000.00 | NULL                                                 |
|      4 |     11 |          3 | 2009-01-16 00:00:00 | i        |  135000.00 | freshman justice                                     |
|      5 |     14 |          3 | 2009-01-16 00:00:00 | i        |  165000.00 | became chief justice                                 |
|      6 |     15 |          3 | 2009-04-18 00:00:00 | i        |  170000.00 | reassigned to court based upon local area population |
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
6 rows in set (0.00 sec)

mysql> do sleep(5);
Query OK, 0 rows affected (5.00 sec)

mysql> 
mysql> 
mysql> 
mysql> drop trigger if exists tr_judge_history_after_insert;
Query OK, 0 rows affected, 1 warning (0.01 sec)

mysql> 
mysql> delimiter //
mysql> 
mysql> create trigger tr_judge_history_after_insert
    -> AFTER INSERT ON judge
    -> FOR EACH ROW
    -> 
    -> BEGIN
    -> 
    -> insert into judge_hist
    -> values
    -> (new.per_id, new.crt_id, current_timestamp(),'i', new.jud_salary, concat('modifying user:', new.jud_notes)
    -> );
    -> end //
Query OK, 0 rows affected (0.01 sec)

mysql> delimiter ;
mysql> 
mysql> 
mysql> -- This comment is here for easy readability of the code
mysql> 
mysql> 
mysql> 
mysql> select 'fire trigger by inserting record into judge table' as '';
+---------------------------------------------------+
|                                                   |
+---------------------------------------------------+
| fire trigger by inserting record into judge table |
+---------------------------------------------------+
1 row in set (0.00 sec)

mysql> do sleep(5);
Query OK, 0 rows affected (5.00 sec)

mysql> 
mysql> insert into judge 
    -> values
    -> ((select count(per_id) from person), 3, 143300,25,'transferred from neighboring jurisdiction');
ERROR 1062 (23000): Duplicate entry '15' for key 'PRIMARY'
mysql> 
mysql> select 'show judge/judge_hist data AFTER INSERT trigger fires (tr_judge_history_after_insert)' as '';
+---------------------------------------------------------------------------------------+
|                                                                                       |
+---------------------------------------------------------------------------------------+
| show judge/judge_hist data AFTER INSERT trigger fires (tr_judge_history_after_insert) |
+---------------------------------------------------------------------------------------+
1 row in set (0.00 sec)

mysql> select * from judge;
+--------+--------+------------+-----------------------+-----------+
| per_id | crt_id | jud_salary | jud_years_in_practice | jud_notes |
+--------+--------+------------+-----------------------+-----------+
|     11 |      1 |  150000.00 |                    12 | Judge 1   |
|     12 |      2 |  140000.00 |                    10 | Judge 2   |
|     13 |      3 |  100000.00 |                    15 | Judge 3   |
|     14 |      4 |   95000.00 |                    12 | Judge 4   |
|     15 |      5 |  120000.00 |                    11 | Judge 5   |
+--------+--------+------------+-----------------------+-----------+
5 rows in set (0.00 sec)

mysql> select * from judge_hist;
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
| jhs_id | per_id | jhs_crt_id | jhs_date            | jhs_type | jhs_salary | jhs_notes                                            |
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
|      1 |     11 |          3 | 2009-01-16 00:00:00 | i        |  130000.00 | NULL                                                 |
|      2 |     12 |          3 | 2009-01-16 00:00:00 | i        |  140000.00 | NULL                                                 |
|      3 |     13 |          3 | 2009-01-16 00:00:00 | i        |  165000.00 | NULL                                                 |
|      4 |     11 |          3 | 2009-01-16 00:00:00 | i        |  135000.00 | freshman justice                                     |
|      5 |     14 |          3 | 2009-01-16 00:00:00 | i        |  165000.00 | became chief justice                                 |
|      6 |     15 |          3 | 2009-04-18 00:00:00 | i        |  170000.00 | reassigned to court based upon local area population |
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
6 rows in set (0.00 sec)

mysql> do sleep(7);
Query OK, 0 rows affected (7.00 sec)

mysql> 
mysql> 
mysql> drop trigger if exists tr_judge_history_after_insert;
Query OK, 0 rows affected (0.01 sec)

mysql> select per_id, per_fname, per_lname from person;
+--------+-----------+------------+
| per_id | per_fname | per_lname  |
+--------+-----------+------------+
|      1 | Steve     | Rogers     |
|      2 | Bruce     | Wayne      |
|      3 | Peter     | Parker     |
|      4 | Jane      | Thompson   |
|      5 | Debra     | Steele     |
|      6 | Tony      | Stark      |
|      7 | Hank      | Pymi       |
|      8 | Bob       | Best       |
|      9 | Sandra    | Dole       |
|     10 | Ben       | Avery      |
|     11 | Arthur    | Curry      |
|     12 | Diana     | Price      |
|     13 | Adam      | Jurris     |
|     14 | Judy      | Sleen      |
|     15 | Bill      | Neiderheim |
+--------+-----------+------------+
15 rows in set (0.00 sec)

mysql> do sleep(5);
Query OK, 0 rows affected (5.00 sec)

mysql> 
mysql> set @salt = random_bytes(64); set @ran_num = FLOOR(RAND() * (999999999 - 111111111 + 1)) + 111111111; 
Query OK, 0 rows affected (0.00 sec)

Query OK, 0 rows affected (0.00 sec)

mysql> insert into person 
    -> values
    -> (null, unhex(sha2(000000000,512)), @salt, 'Bobby', 'Sue', 'J.Cole street', 'Panama City Beach','FL', 32321, 'bsue@fl.gov','1962-05-16', 'j', 'new district judge');
Query OK, 1 row affected (0.00 sec)

mysql> 
mysql> select 'show person table AFTER adding the new record' as '';
+-----------------------------------------------+
|                                               |
+-----------------------------------------------+
| show person table AFTER adding the new record |
+-----------------------------------------------+
1 row in set (0.00 sec)

mysql> select per_id, per_fname, per_lname from person;
+--------+-----------+------------+
| per_id | per_fname | per_lname  |
+--------+-----------+------------+
|      1 | Steve     | Rogers     |
|      2 | Bruce     | Wayne      |
|      3 | Peter     | Parker     |
|      4 | Jane      | Thompson   |
|      5 | Debra     | Steele     |
|      6 | Tony      | Stark      |
|      7 | Hank      | Pymi       |
|      8 | Bob       | Best       |
|      9 | Sandra    | Dole       |
|     10 | Ben       | Avery      |
|     11 | Arthur    | Curry      |
|     12 | Diana     | Price      |
|     13 | Adam      | Jurris     |
|     14 | Judy      | Sleen      |
|     15 | Bill      | Neiderheim |
|     16 | Bobby     | Sue        |
+--------+-----------+------------+
16 rows in set (0.00 sec)

mysql> do sleep (3);
Query OK, 0 rows affected (3.00 sec)

mysql> 
mysql> 
mysql> select 'show judge/judge data before AFTER INSERT TRIGGER fires (tr_judge_history_after_insert)' as '';
+-----------------------------------------------------------------------------------------+
|                                                                                         |
+-----------------------------------------------------------------------------------------+
| show judge/judge data before AFTER INSERT TRIGGER fires (tr_judge_history_after_insert) |
+-----------------------------------------------------------------------------------------+
1 row in set (0.00 sec)

mysql> select * from judge;
+--------+--------+------------+-----------------------+-----------+
| per_id | crt_id | jud_salary | jud_years_in_practice | jud_notes |
+--------+--------+------------+-----------------------+-----------+
|     11 |      1 |  150000.00 |                    12 | Judge 1   |
|     12 |      2 |  140000.00 |                    10 | Judge 2   |
|     13 |      3 |  100000.00 |                    15 | Judge 3   |
|     14 |      4 |   95000.00 |                    12 | Judge 4   |
|     15 |      5 |  120000.00 |                    11 | Judge 5   |
+--------+--------+------------+-----------------------+-----------+
5 rows in set (0.00 sec)

mysql> select * from judge_hist;
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
| jhs_id | per_id | jhs_crt_id | jhs_date            | jhs_type | jhs_salary | jhs_notes                                            |
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
|      1 |     11 |          3 | 2009-01-16 00:00:00 | i        |  130000.00 | NULL                                                 |
|      2 |     12 |          3 | 2009-01-16 00:00:00 | i        |  140000.00 | NULL                                                 |
|      3 |     13 |          3 | 2009-01-16 00:00:00 | i        |  165000.00 | NULL                                                 |
|      4 |     11 |          3 | 2009-01-16 00:00:00 | i        |  135000.00 | freshman justice                                     |
|      5 |     14 |          3 | 2009-01-16 00:00:00 | i        |  165000.00 | became chief justice                                 |
|      6 |     15 |          3 | 2009-04-18 00:00:00 | i        |  170000.00 | reassigned to court based upon local area population |
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
6 rows in set (0.00 sec)

mysql> do sleep(5);
Query OK, 0 rows affected (5.00 sec)

mysql> 
mysql> 
mysql> 
mysql> drop trigger if exists tr_judge_history_after_insert;
Query OK, 0 rows affected, 1 warning (0.01 sec)

mysql> 
mysql> delimiter //
mysql> 
mysql> create trigger tr_judge_history_after_insert
    -> AFTER INSERT ON judge
    -> FOR EACH ROW
    -> 
    -> BEGIN
    -> 
    -> insert into judge_hist
    -> values
    -> (new.per_id, new.crt_id, current_timestamp(),'i', new.jud_salary, concat('modifying user:', new.jud_notes)
    -> );
    -> end //
Query OK, 0 rows affected (0.01 sec)

mysql> delimiter ;
mysql> 
mysql> 
mysql> -- This comment is here for easy readability of the code
mysql> 
mysql> 
mysql> 
mysql> select 'fire trigger by inserting record into judge table' as '';
+---------------------------------------------------+
|                                                   |
+---------------------------------------------------+
| fire trigger by inserting record into judge table |
+---------------------------------------------------+
1 row in set (0.00 sec)

mysql> do sleep(5);
Query OK, 0 rows affected (5.00 sec)

mysql> 
mysql> insert into judge 
    -> values
    -> ((select count(per_id) from person), 3, 143300,25,'transferred from neighboring jurisdiction');
ERROR 1136 (21S01): Column count doesn't match value count at row 1
mysql> 
mysql> select 'show judge/judge_hist data AFTER INSERT trigger fires (tr_judge_history_after_insert)' as '';
+---------------------------------------------------------------------------------------+
|                                                                                       |
+---------------------------------------------------------------------------------------+
| show judge/judge_hist data AFTER INSERT trigger fires (tr_judge_history_after_insert) |
+---------------------------------------------------------------------------------------+
1 row in set (0.00 sec)

mysql> select * from judge;
+--------+--------+------------+-----------------------+-----------+
| per_id | crt_id | jud_salary | jud_years_in_practice | jud_notes |
+--------+--------+------------+-----------------------+-----------+
|     11 |      1 |  150000.00 |                    12 | Judge 1   |
|     12 |      2 |  140000.00 |                    10 | Judge 2   |
|     13 |      3 |  100000.00 |                    15 | Judge 3   |
|     14 |      4 |   95000.00 |                    12 | Judge 4   |
|     15 |      5 |  120000.00 |                    11 | Judge 5   |
+--------+--------+------------+-----------------------+-----------+
5 rows in set (0.00 sec)

mysql> select * from judge_hist;
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
| jhs_id | per_id | jhs_crt_id | jhs_date            | jhs_type | jhs_salary | jhs_notes                                            |
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
|      1 |     11 |          3 | 2009-01-16 00:00:00 | i        |  130000.00 | NULL                                                 |
|      2 |     12 |          3 | 2009-01-16 00:00:00 | i        |  140000.00 | NULL                                                 |
|      3 |     13 |          3 | 2009-01-16 00:00:00 | i        |  165000.00 | NULL                                                 |
|      4 |     11 |          3 | 2009-01-16 00:00:00 | i        |  135000.00 | freshman justice                                     |
|      5 |     14 |          3 | 2009-01-16 00:00:00 | i        |  165000.00 | became chief justice                                 |
|      6 |     15 |          3 | 2009-04-18 00:00:00 | i        |  170000.00 | reassigned to court based upon local area population |
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
6 rows in set (0.00 sec)

mysql> do sleep(7);
Query OK, 0 rows affected (7.00 sec)

mysql> 
mysql> 
mysql> drop trigger if exists tr_judge_history_after_insert;
Query OK, 0 rows affected (0.02 sec)

mysql> delete from person 
    -> where per_id = 16;
Query OK, 1 row affected (0.01 sec)

mysql> select per_fname from person;
+-----------+
| per_fname |
+-----------+
| Steve     |
| Bruce     |
| Peter     |
| Jane      |
| Debra     |
| Tony      |
| Hank      |
| Bob       |
| Sandra    |
| Ben       |
| Arthur    |
| Diana     |
| Adam      |
| Judy      |
| Bill      |
+-----------+
15 rows in set (0.00 sec)

mysql> select per_id, per_fname, per_lname from person;
+--------+-----------+------------+
| per_id | per_fname | per_lname  |
+--------+-----------+------------+
|      1 | Steve     | Rogers     |
|      2 | Bruce     | Wayne      |
|      3 | Peter     | Parker     |
|      4 | Jane      | Thompson   |
|      5 | Debra     | Steele     |
|      6 | Tony      | Stark      |
|      7 | Hank      | Pymi       |
|      8 | Bob       | Best       |
|      9 | Sandra    | Dole       |
|     10 | Ben       | Avery      |
|     11 | Arthur    | Curry      |
|     12 | Diana     | Price      |
|     13 | Adam      | Jurris     |
|     14 | Judy      | Sleen      |
|     15 | Bill      | Neiderheim |
+--------+-----------+------------+
15 rows in set (0.00 sec)

mysql> do sleep(5);
Query OK, 0 rows affected (5.00 sec)

mysql> 
mysql> set @salt = random_bytes(64); set @ran_num = FLOOR(RAND() * (999999999 - 111111111 + 1)) + 111111111; 
Query OK, 0 rows affected (0.00 sec)

Query OK, 0 rows affected (0.00 sec)

mysql> insert into person 
    -> values
    -> (null, unhex(sha2(000000000,512)), @salt, 'Bobby', 'Sue', 'J.Cole street', 'Panama City Beach','FL', 32321, 'bsue@fl.gov','1962-05-16', 'j', 'new district judge');
Query OK, 1 row affected (0.00 sec)

mysql> 
mysql> select 'show person table AFTER adding the new record' as '';
+-----------------------------------------------+
|                                               |
+-----------------------------------------------+
| show person table AFTER adding the new record |
+-----------------------------------------------+
1 row in set (0.00 sec)

mysql> select per_id, per_fname, per_lname from person;
+--------+-----------+------------+
| per_id | per_fname | per_lname  |
+--------+-----------+------------+
|      1 | Steve     | Rogers     |
|      2 | Bruce     | Wayne      |
|      3 | Peter     | Parker     |
|      4 | Jane      | Thompson   |
|      5 | Debra     | Steele     |
|      6 | Tony      | Stark      |
|      7 | Hank      | Pymi       |
|      8 | Bob       | Best       |
|      9 | Sandra    | Dole       |
|     10 | Ben       | Avery      |
|     11 | Arthur    | Curry      |
|     12 | Diana     | Price      |
|     13 | Adam      | Jurris     |
|     14 | Judy      | Sleen      |
|     15 | Bill      | Neiderheim |
|     17 | Bobby     | Sue        |
+--------+-----------+------------+
16 rows in set (0.00 sec)

mysql> do sleep (3);
Query OK, 0 rows affected (3.00 sec)

mysql> 
mysql> 
mysql> select 'show judge/judge data before AFTER INSERT TRIGGER fires (tr_judge_history_after_insert)' as '';
+-----------------------------------------------------------------------------------------+
|                                                                                         |
+-----------------------------------------------------------------------------------------+
| show judge/judge data before AFTER INSERT TRIGGER fires (tr_judge_history_after_insert) |
+-----------------------------------------------------------------------------------------+
1 row in set (0.00 sec)

mysql> select * from judge;
+--------+--------+------------+-----------------------+-----------+
| per_id | crt_id | jud_salary | jud_years_in_practice | jud_notes |
+--------+--------+------------+-----------------------+-----------+
|     11 |      1 |  150000.00 |                    12 | Judge 1   |
|     12 |      2 |  140000.00 |                    10 | Judge 2   |
|     13 |      3 |  100000.00 |                    15 | Judge 3   |
|     14 |      4 |   95000.00 |                    12 | Judge 4   |
|     15 |      5 |  120000.00 |                    11 | Judge 5   |
+--------+--------+------------+-----------------------+-----------+
5 rows in set (0.00 sec)

mysql> select * from judge_hist;
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
| jhs_id | per_id | jhs_crt_id | jhs_date            | jhs_type | jhs_salary | jhs_notes                                            |
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
|      1 |     11 |          3 | 2009-01-16 00:00:00 | i        |  130000.00 | NULL                                                 |
|      2 |     12 |          3 | 2009-01-16 00:00:00 | i        |  140000.00 | NULL                                                 |
|      3 |     13 |          3 | 2009-01-16 00:00:00 | i        |  165000.00 | NULL                                                 |
|      4 |     11 |          3 | 2009-01-16 00:00:00 | i        |  135000.00 | freshman justice                                     |
|      5 |     14 |          3 | 2009-01-16 00:00:00 | i        |  165000.00 | became chief justice                                 |
|      6 |     15 |          3 | 2009-04-18 00:00:00 | i        |  170000.00 | reassigned to court based upon local area population |
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
6 rows in set (0.00 sec)

mysql> do sleep(5);
Query OK, 0 rows affected (5.00 sec)

mysql> 
mysql> 
mysql> 
mysql> drop trigger if exists tr_judge_history_after_insert;
Query OK, 0 rows affected, 1 warning (0.01 sec)

mysql> 
mysql> delimiter //
mysql> 
mysql> create trigger tr_judge_history_after_insert
    -> AFTER INSERT ON judge
    -> FOR EACH ROW
    -> 
    -> BEGIN
    -> 
    -> insert into judge_hist
    -> values
    -> (new.per_id, new.crt_id, current_timestamp(),'i', new.jud_salary, concat('modifying user:', new.jud_notes)
    -> );
    -> end //
Query OK, 0 rows affected (0.01 sec)

mysql> delimiter ;
mysql> 
mysql> 
mysql> -- This comment is here for easy readability of the code
mysql> 
mysql> 
mysql> 
mysql> select 'fire trigger by inserting record into judge table' as '';
+---------------------------------------------------+
|                                                   |
+---------------------------------------------------+
| fire trigger by inserting record into judge table |
+---------------------------------------------------+
1 row in set (0.00 sec)

mysql> do sleep(5);
Query OK, 0 rows affected (5.00 sec)

mysql> 
mysql> insert into judge 
    -> values
    -> ((select count(per_id) from person), 3, 143300,25,'transferred from neighboring jurisdiction');
ERROR 1452 (23000): Cannot add or update a child row: a foreign key constraint fails (`ooa19a`.`judge`, CONSTRAINT `fk_judge_person` FOREIGN KEY (`per_id`) REFERENCES `person` (`per_id`) ON UPDATE CASCADE)
mysql> 
mysql> select 'show judge/judge_hist data AFTER INSERT trigger fires (tr_judge_history_after_insert)' as '';
+---------------------------------------------------------------------------------------+
|                                                                                       |
+---------------------------------------------------------------------------------------+
| show judge/judge_hist data AFTER INSERT trigger fires (tr_judge_history_after_insert) |
+---------------------------------------------------------------------------------------+
1 row in set (0.00 sec)

mysql> select * from judge;
+--------+--------+------------+-----------------------+-----------+
| per_id | crt_id | jud_salary | jud_years_in_practice | jud_notes |
+--------+--------+------------+-----------------------+-----------+
|     11 |      1 |  150000.00 |                    12 | Judge 1   |
|     12 |      2 |  140000.00 |                    10 | Judge 2   |
|     13 |      3 |  100000.00 |                    15 | Judge 3   |
|     14 |      4 |   95000.00 |                    12 | Judge 4   |
|     15 |      5 |  120000.00 |                    11 | Judge 5   |
+--------+--------+------------+-----------------------+-----------+
5 rows in set (0.00 sec)

mysql> select * from judge_hist;
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
| jhs_id | per_id | jhs_crt_id | jhs_date            | jhs_type | jhs_salary | jhs_notes                                            |
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
|      1 |     11 |          3 | 2009-01-16 00:00:00 | i        |  130000.00 | NULL                                                 |
|      2 |     12 |          3 | 2009-01-16 00:00:00 | i        |  140000.00 | NULL                                                 |
|      3 |     13 |          3 | 2009-01-16 00:00:00 | i        |  165000.00 | NULL                                                 |
|      4 |     11 |          3 | 2009-01-16 00:00:00 | i        |  135000.00 | freshman justice                                     |
|      5 |     14 |          3 | 2009-01-16 00:00:00 | i        |  165000.00 | became chief justice                                 |
|      6 |     15 |          3 | 2009-04-18 00:00:00 | i        |  170000.00 | reassigned to court based upon local area population |
+--------+--------+------------+---------------------+----------+------------+------------------------------------------------------+
6 rows in set (0.00 sec)

mysql> do sleep(7);
Query OK, 0 rows affected (7.00 sec)

mysql> 
mysql> 
mysql> drop trigger if exists tr_judge_history_after_insert;
Query OK, 0 rows affected (0.02 sec)

mysql> set @salt = random_bytes(64); set @ran_num = FLOOR(RAND() * (999999999 - 111111111 + 1)) + 111111111; 
Query OK, 0 rows affected (0.00 sec)

Query OK, 0 rows affected (0.00 sec)

mysql> insert into person 
    -> values
    -> (16, unhex(sha2(000000000,512)), @salt, 'Bobby', 'Sue', 'J.Cole street', 'Panama City Beach','FL', 32321, 'bsue@fl.gov','1962-05-16', 'j', 'another district judge');
ERROR 1062 (23000): Duplicate entry '1\xBC\xA0 \x94\xEBx\x12jQ{ j\x88\xC7<\xFA\x9E\xC6\xF7\x04\xC7\x0' for key 'ux_per_ssn'
mysql> select per_fname, per_id order by per_id asc;
ERROR 1054 (42S22): Unknown column 'per_fname' in 'field list'
mysql> select per_fname, per_id from person order by per_id asc;
+-----------+--------+
| per_fname | per_id |
+-----------+--------+
| Steve     |      1 |
| Bruce     |      2 |
| Peter     |      3 |
| Jane      |      4 |
| Debra     |      5 |
| Tony      |      6 |
| Hank      |      7 |
| Bob       |      8 |
| Sandra    |      9 |
| Ben       |     10 |
| Arthur    |     11 |
| Diana     |     12 |
| Adam      |     13 |
| Judy      |     14 |
| Bill      |     15 |
| Bobby     |     17 |
+-----------+--------+
16 rows in set (0.00 sec)

mysql> select * from person where per_id = 16;
Empty set (0.00 sec)

mysql> select * from person where per_id = 17;
+--------+------------------------------------------------------------------+------------------------------------------------------------------+-----------+-----------+---------------+-------------------+-----------+-----------+-------------+------------+----------+--------------------+
| per_id | per_ssn                                                          | per_sslt                                                         | per_fname | per_lname | per_street    | per_city          | per_state | per_zip   | per_email   | per_dob    | per_type | per_notes          |
+--------+------------------------------------------------------------------+------------------------------------------------------------------+-----------+-----------+---------------+-------------------+-----------+-----------+-------------+------------+----------+--------------------+
|     17 | 1�� ��xjQ{ j��<�����!,�� �%���h���Cl�;S�{�
���}�Yз���ë�                                   | �9�,�[C�@.�yK���A�5|T��ǚ�)�z!h�ī=1���+&]�wE��m@�p��J&�                           | Bobby     | Sue       | J.Cole street | Panama City Beach | FL        | 000032321 | bsue@fl.gov | 1962-05-16 | j        | new district judge |
+--------+------------------------------------------------------------------+------------------------------------------------------------------+-----------+-----------+---------------+-------------------+-----------+-----------+-------------+------------+----------+--------------------+
1 row in set (0.00 sec)

mysql> update person
    -> set 
    ->  per_fname = 'Kola'
    -> where per_id = 16;
Query OK, 0 rows affected (0.01 sec)
Rows matched: 0  Changed: 0  Warnings: 0

mysql> select * from person where per_id = 16;
Empty set (0.00 sec)

mysql> exit
