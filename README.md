# LIS3781

## Kayode Adegbite

[Repo link to lis3781](https://bitbucket.org/ooa19a/lis3781/src/master/)

### Assignments

[Assignmet 1](https://bitbucket.org/ooa19a/lis3781/src/master/a1/)

* Install AMPPS
* Provide screenshots of installations
* Create Bitbucket repo
* Complete Bitbucket tutorials (bitbucketstationlocations and myteam quotes)
* Provide git command descriptions
* Create First Database
* Include Screenshots

[Assignmet 2](https://bitbucket.org/ooa19a/lis3781/src/master/a2/)

* Create tables company and customer on Local Server
* Create tables company and customer on CCI Server
* Create new User 3 and 4 on Server
* Take Screenshots and make Readme file

[Assignmet 3](https://bitbucket.org/ooa19a/lis3781/src/master/a3/)

* Create tables customer, commodity, and order on Oracle Database
* Provide SQL Solutions
* Tables, inserts, sequences, foreign key, commit & constraint SQL statements

[Assignmet 4](https://bitbucket.org/ooa19a/lis3781/src/master/a4/)

* Business Rules
* Questions
* SQL Code

### Projects

[Project 1](https://bitbucket.org/ooa19a/lis3781/src/master/project_1/)

> This is a project on court management system.

* Create a Database
* Create 11 tables (Bar, Specialty, Attorney, Assignment, Person, Phone, Client, Case, Judge, Court, Judge_Hist)
* Fill the tables and Forward Engineer the ERD

[Project 2](p2/README.md)

* MongoDB Create & Insert Database
* Add MongoDB Array using insert()
* Mongodb ObjectId()
* MongoDB Query Document using find()
* MongoDB cursor
* MongoDB Query Modifications using limit(), sort()
* MongoDB Count() & remove() function
* MongoDB Update() Document
* Questions
