drop sequence seq_cus_id;
create sequence seq_cus_id
start with 1
increment by 1
minvalue 1
maxvalue 10000;

drop table customer cascade CONSTRAINTS purge;

create table customer (
cus_id number(3,0) not null,
cus_fname varchar2(15) not null, 
cus_lname varchar2(30) not null,
cus_street varchar2(30) not null,
cus_city varchar2(30) not null,
cus_state char(2) not null,
cus_zip number(9) not null,
cus_phone number(10) not null,
cus_email varchar2(100) not null,
cus_balance number(7,2),
cus_notes varchar2(255),
CONSTRAINT pk_customer primary key (cus_id)
);

-----------------------------------------------------------

drop sequence sq_com_id;
create sequence sq_com_id
start with 1
increment by 1
minvalue 1
maxvalue 10000;

drop table commodity cascade CONSTRAINTS purge;
create table commodity
(
com_id number not null,
com_name varchar2(20),
com_price number(8,2) not null,
cus_notes varchar2(255),

constraint pk_commodity primary key (com_id),
constraint uq_com_name unique (com_name)
);

------------------------------------------------------------

drop sequence seq_ord_id;
create sequence seq_ord_id
start with 1 
increment by 1
minvalue 1
maxvalue 10000;

drop table "order" cascade constraints PURGE;

create table "order"
(
ord_id number(4,0) not null,
cus_id number,
com_id number,
ord_num_units number(5,0) not null,
ord_total_cost number(8,2)not null,
ord_notes varchar2(255),

constraint pk_order primary key (ord_id),
constraint fk_order_customer foreign key (cus_id) references customer (cus_id),
constraint fk_order_commodity foreign key (com_id) references commodity (com_id),

constraint check_unit check (ord_num_units >0),
constraint check_total check (ord_total_cost >0)

);

2 -------------------------------------------------------------------

insert into customer values (seq_cus_id.nextval, 'John','Snow','434 Obalende St.', 'Detriot', 'MI', 67893,53637332,'john@snow.com',53673.22, 'recently moved');
insert into customer values (seq_cus_id.nextval, 'Lakers','Oak','3 Lende St.', 'London', 'NM', 33432,7474893,'laker@oak.com',454.22, 'On the run');
insert into customer values (seq_cus_id.nextval, 'Oye','Kola','99 Martin St.', 'Atlanta', 'GA', 4563,434342,'Oye@Kola.com',2.34, null);
insert into customer values (seq_cus_id.nextval, 'Sk','Mathew','Mohammed Ali St.', 'New Paltz', 'NY', 89483,0393762,'Sk@mathew.com',4353.43, null);
insert into customer values (seq_cus_id.nextval, 'Russ','Ben','Kobe Byrant St.', 'Bronx', 'NY', 53637,684863,'Russ@ben.com', null, null);
commit;

-----------------------------

insert into commodity values (sq_com_id.nextval, 'DVD and Player',110.0,null);
insert into commodity values (sq_com_id.nextval, 'Cookies',39.4,'sugar free');
insert into commodity values (sq_com_id.nextval, 'Chess',40,'boardgame');
insert into commodity values (sq_com_id.nextval, 'Paracetamol',30.4,'medicine');
insert into commodity values (sq_com_id.nextval, 'PlayStation 5',400,null);
commit;


---------------------------------------------------------

insert into "order" values (SEQ_ORD_ID.NEXTVAL, 1,2,45,654,null);
insert into "order" values (SEQ_ORD_ID.NEXTVAL, 4,2,55,787,null);
insert into "order" values (SEQ_ORD_ID.NEXTVAL, 3,3,92,098,null);
insert into "order" values (SEQ_ORD_ID.NEXTVAL, 5,5,12,234,null);
insert into "order" values (SEQ_ORD_ID.NEXTVAL, 3,3,37,923,null);
insert into "order" values (SEQ_ORD_ID.NEXTVAL, 6,4,82,274,null);
insert into "order" values (SEQ_ORD_ID.NEXTVAL, 2,4,88,736,null);
insert into "order" values (SEQ_ORD_ID.NEXTVAL, 8,3,85,964,null);
insert into "order" values (SEQ_ORD_ID.NEXTVAL, 1,2,45,654,null);
insert into "order" values (SEQ_ORD_ID.NEXTVAL, 9,6,90,545,null);

commit;
3 -----------------------------------------
Select user from dual;  //display current user

4 --------------
select to_char ]
(sysdate, 'mm-dd-yyyy hh12:mi:ss AM')"NOW"   // show current date and time 
from dual;

5 -------------------------
select * from user_sys_privs; //display all privildges

6 ------------------------
select object_name from user_objects where object_type = 'TABLE'; //Display all the tables 

7 -----------------------------------------------------

decribe customer;
decribe commodity;
decribe "order";

----
	display data of each table 
select * from customer;
select * from commodity;
select * from "order";

8---------

select cus_phone, cus_lname, cus_fname, cus_email from customer;

9 --------------
select cus_phone, cus_lname, cus_fname, cus_email, cus_street from customer order by cus_lname asc, cus_state desc;

10------
select cus_lname || ' ' || cus_fname as "FULL NAME"
from customer 
where cus_id = 3;

11----------------------
select cus_phone, cus_lname, cus_fname, cus_balance from customer where cus_balance >1000;

12--------------------------- 
select com_name, com_price, to_char(com_price, 'L99,999.99') as "Formatted Price"
from commodity 
order by com_price;

13 ---------------------------
select (cus_lname || ' ' || cus_fname) as Name, (cus_street|| ', ' || cus_state || ' ' || cus_zip) as address
from customer
order by cus_zip desc;

14 ---------------------------------------
select * from "order" 
where com_id != (select * from commodity where lower(com_name)= 'chess');

15------------------------

select cus_fname, cus_lname, cus_phone, to_char(cus_balance, 'L99,999.99') 
from customer 
where cus_balance BETWEEN 4000 and 40000;

16----------------

select cus_fname, cus_lname, cus_phone, to_char(cus_balance, 'L99,999.99') 
from customer 
where cus_balance >(select avg(cus_balance)from customer);

17 ---------------

select (cus_fname || ' ' ||cus_lname) as Name, cus_phone, to_char(sum(ord_total_cost), 'L99,999.99') as "Total Order"
from customer
natural join "order"
group by cus_phone, (cus_fname || ' ' ||cus_lname)
order by sum(ord_total_cost) desc;

18 -----------------------
select cus_phone, cus_lname, cus_fname, (cus_street||', '||cus_city||', '||cus_state||', '||cus_zip) as Address 
from customer 
where cus_street like '%Ali%';

19-----------------------------------------
select cus_fname, cus_lname, cus_phone, to_char(sum(ord_total_cost),'L99,999.99') AS "Total order" 
from customer
natural join "order" 
group by cus_fname, cus_lname, cus_phone
having sum(ord_total_cost) >300
order by sum(ord_total_cost) desc;

20 --------------------------------------
select cus_fname, cus_lname, cus_phone, ord_num_units
from customer
natural join "order"
where ORD_NUM_UNITS in (45,37,55);

21 ------------------------


23-------------------
select cus_id from "order";

select distinct cus_id from "order";

selec distinct count(distinct cus_id) from "order";

25-----------------------

set define off

update commodity 
set com_price = 99
where com_name = 'DVD & Player'






