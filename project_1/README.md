# Project 1

## SQL REPORT

1. Create a viewthat displays attorneys’ *full* names, *full* addresses, ages, hourly rates, the bar names that they’ve passed, as well as their specialties, sort by attorneys’ last names.

2. Create a stored procedurethat displayshow many judgeswere born in each month of the year, sorted by month.

3. Create a stored procedurethat displays *all* case types and descriptions, as well as judges’ *full*names, *full* addresses, phone numbers, years in practice, for cases that they presided over, with their start and end dates, sort by judges’ last names.

4. Create a triggerthat automatically adds a record to the judge history table for every record addedto the judge table.

5. Create a triggerthat automatically adds a record to the judge history table for every record modifiedin the judge table.6)Create a one-time eventthat executes one hour following itscreation, the event should add a judge record (one more than the required five records), have the event call a stored procedurethat adds the record (name it one_time_add_judge).

Business Rules:

As the lead DBA for a local municipality, you are contacted by the city council to design a database in order to track and document the city’s court case data. Some report examples: Which attorney is assigned to what case(s)? How many unique clients have cases(be sure to add a client to more than one case)? How many cases has each attorney been assigned, and names of their clients (return number and names)?How many cases does each client have with the firm(return a name and number value)? Which types of cases does/did each client have/had and their start and end dates? Which attorney is associated to which client(s), and to which case(s)? Names of three judges with the most number of years in practice, include number of years. Also, include the following business rules:

1. An attorney is retained by (or assigned to)one or more clients, for each case.
2. A client has(or is assigned to) one or more attorneys for each case.
3. An attorney has one or more cases.
4. A client has one or more cases.
5. Each court has one or more judges adjudicating.
6. Each judge adjudicates upon exactly one court.
7. Each judge may preside over more than one case.
8. Each case that goes to court is presided over by exactly one judge.
9. A person can have more than one phone number.

### Notes

- Attorney data must include social security number, name, address, office phone, home phone,e-mail,start/end dates, dob, hourly rate, years in practice, bar(may be more than one-multivalued), specialty(may be more than one-multivalued).

- Clientdata must include social security number, name, address, phone, e-mail, dob.

- Case datamust include type, description, start/end dates.•Court data must include name, address, phone, e-mail, url.

- Judge data must include sameinformation as attorneys(except bar, specialtyand hourly rate;instead, use salary).

- Must track judge historicaldata—tenure at each court (i.e., start/end dates), and salaries.

- Also, history will track which courts judgespresided over,if they took a leave of absence, or retired.

- Alltables must have notes.NB: In some designs, the common attributes would be inherited from a “person” table.

### Additional Notes

- Social security numbers, should be unique, andmust use SHA2 hashing with salt.

- ERD MUST include relationships and cardinalities.

### Screenshots

* screenshot for the ERD
![Screenshot ERD](img/erd.png)

* screenshot for question 1
![Screenshot to Question 1 ](img/q1.png)

* screenshot for question 2
![Screenshot to Question 2 ](img/q2.png)

* screenshot for question 3
![Screenshot to Question 3 ](img/q3.png)

* screenshot for question 4a 
![Screenshot to Question 4 ](img/q4a.png)

* screenshot for question 4b
![Screenshot to Question 4 ](img/q4b.png)

* screenshot for question 5
![Screenshot to Question 4 ](img/q5.png)

### Attached below is the sql file of the program, you can download it and run it on your command line. It works perfectly

[Click me to see the code for the whole program. Starting from Creating a database, tables all the way to dropping it.](https://bitbucket.org/ooa19a/lis3781/src/master/project_1/p1solution.sql)

* MWB File
[Download .mwb file](img/wb.mwb)