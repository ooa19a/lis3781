mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mydb               |
| mysql              |
| performance_schema |
| pet_system         |
| tutorial           |
+--------------------+
6 rows in set (0.00 sec)

mysql> create database ooa19a
    -> ;
Query OK, 1 row affected (0.02 sec)

mysql> use ooa19a;
Database changed
mysql> drop table if exists person;
Query OK, 0 rows affected, 1 warning (0.01 sec)

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| pet_system         |
| tutorial           |
+--------------------+
5 rows in set (0.00 sec)

mysql> create database ooa19a;
Query OK, 1 row affected (0.01 sec)

mysql> DROP TABLE IF EXISTS `ooa19a`.`person` ;CREATE TABLE IF NOT EXISTS `ooa19a`.`person` (  `per_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,  `per_ssn` BINARY(64) NULL,  `per_ssnt` BINARY(64) NULL,  `per_fname` VARCHAR(15) NOT NULL,  `per_lname` VARCHAR(30) NOT NULL,  `per_street` VARCHAR(30) NOT NULL,  `per_city` VARCHAR(30) NOT NULL,  `per_state` CHAR(2) NOT NULL,  `per_zip` INT(9) ZEROFILL UNSIGNED NULL,  `per_email` VARCHAR(100) NOT NULL,  `per_dob` DATE NOT NULL,  `per_type` ENUM('a', 'c', 'j') NOT NULL,  `per_notes` VARCHAR(255) NULL,  PRIMARY KEY (`per_id`))ENGINE = InnoDB;
Query OK, 0 rows affected, 1 warning (0.01 sec)

Query OK, 0 rows affected, 2 warnings (0.02 sec)

mysql> use ooa19a;
Database changed
mysql> show tables;
+------------------+
| Tables_in_ooa19a |
+------------------+
| person           |
+------------------+
1 row in set (0.00 sec)

mysql> describe person;
+------------+--------------------------+------+-----+---------+----------------+
| Field      | Type                     | Null | Key | Default | Extra          |
+------------+--------------------------+------+-----+---------+----------------+
| per_id     | smallint(5) unsigned     | NO   | PRI | NULL    | auto_increment |
| per_ssn    | binary(64)               | YES  |     | NULL    |                |
| per_ssnt   | binary(64)               | YES  |     | NULL    |                |
| per_fname  | varchar(15)              | NO   |     | NULL    |                |
| per_lname  | varchar(30)              | NO   |     | NULL    |                |
| per_street | varchar(30)              | NO   |     | NULL    |                |
| per_city   | varchar(30)              | NO   |     | NULL    |                |
| per_state  | char(2)                  | NO   |     | NULL    |                |
| per_zip    | int(9) unsigned zerofill | YES  |     | NULL    |                |
| per_email  | varchar(100)             | NO   |     | NULL    |                |
| per_dob    | date                     | NO   |     | NULL    |                |
| per_type   | enum('a','c','j')        | NO   |     | NULL    |                |
| per_notes  | varchar(255)             | YES  |     | NULL    |                |
+------------+--------------------------+------+-----+---------+----------------+
13 rows in set (0.00 sec)

mysql> drop table if exists attorney;create table if not exists attorney(per_id smallint unsigned not null,aty_start_date date not null,sty_end_date date null default null,aty_hourly_rate decimal (5,2) unsigned not null,aty_years_in_practice tinyint not null,aty_notes varchar (255) null default null,primary key (per_id),index idx_per_id(per_id ASC),constraint fk_attorney_personforeign key (per_id)references person (per_id)on delete no actionon update cascade);show warnings;
Query OK, 0 rows affected, 1 warning (0.01 sec)

ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'key (per_id)references person (per_id)on delete no actionon update cascade)' at line 1
+---------+------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Level   | Code | Message                                                                                                                                                                                                                       |
+---------+------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Warning | 1681 | UNSIGNED for decimal and floating point data types is deprecated and support for it will be removed in a future release.                                                                                                      |
| Error   | 1064 | You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'key (per_id)references person (per_id)on delete no actionon update cascade)' at line 1 |
+---------+------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
2 rows in set (0.00 sec)

mysql> show tables;
+------------------+
| Tables_in_ooa19a |
+------------------+
| person           |
+------------------+
1 row in set (0.00 sec)

mysql> drop table if exists attorney;
Query OK, 0 rows affected, 1 warning (0.01 sec)

mysql> 
mysql> create table if not exists attorney
    -> (
    -> per_id smallint unsigned not null,
    -> aty_start_date date not null,
    -> aty_end_date date null default null,
    -> aty_hourly_rate decimal (5,2) unsigned not null,
    -> aty_years_in_practice tinyint not null,
    -> aty_notes varchar (255) null default null,
    -> primary key (per_id),
    -> 
    -> index idx_per_id(per_id ASC),
    -> 
    -> constraint fk_attorney_person
    -> foreign key (per_id)
    -> references person (per_id)
    -> on delete no action
    -> on update cascade
    -> 
    -> );
Query OK, 0 rows affected, 1 warning (0.04 sec)

mysql> show warnings;
+---------+------+--------------------------------------------------------------------------------------------------------------------------+
| Level   | Code | Message                                                                                                                  |
+---------+------+--------------------------------------------------------------------------------------------------------------------------+
| Warning | 1681 | UNSIGNED for decimal and floating point data types is deprecated and support for it will be removed in a future release. |
+---------+------+--------------------------------------------------------------------------------------------------------------------------+
1 row in set (0.00 sec)

mysql> describe attorney;
+-----------------------+-----------------------+------+-----+---------+-------+
| Field                 | Type                  | Null | Key | Default | Extra |
+-----------------------+-----------------------+------+-----+---------+-------+
| per_id                | smallint(5) unsigned  | NO   | PRI | NULL    |       |
| aty_start_date        | date                  | NO   |     | NULL    |       |
| aty_end_date          | date                  | YES  |     | NULL    |       |
| aty_hourly_rate       | decimal(5,2) unsigned | NO   |     | NULL    |       |
| aty_years_in_practice | tinyint(4)            | NO   |     | NULL    |       |
| aty_notes             | varchar(255)          | YES  |     | NULL    |       |
+-----------------------+-----------------------+------+-----+---------+-------+
6 rows in set (0.00 sec)

mysql> drop table if exists client;
Query OK, 0 rows affected, 1 warning (0.01 sec)

mysql> create table if not exists attorney
    -> (
    -> per_id smallint unsigned not null,
    -> cli_notes varchar(255) null default null,
    -> primary key (per_id),
    -> 
    -> index idx_per_id(per_id asc),
    -> 
    -> constraint fk_client_person
    -> foreign key (per_id)
    -> references person (per_id)
    -> on delete no action 
    -> on update cascade
    -> );
Query OK, 0 rows affected, 1 warning (0.00 sec)

mysql> 
mysql> show warnings;
+-------+------+---------------------------------+
| Level | Code | Message                         |
+-------+------+---------------------------------+
| Note  | 1050 | Table 'attorney' already exists |
+-------+------+---------------------------------+
1 row in set (0.00 sec)

mysql> show tables;
+------------------+
| Tables_in_ooa19a |
+------------------+
| attorney         |
| person           |
+------------------+
2 rows in set (0.00 sec)

mysql> describe attorney;
+-----------------------+-----------------------+------+-----+---------+-------+
| Field                 | Type                  | Null | Key | Default | Extra |
+-----------------------+-----------------------+------+-----+---------+-------+
| per_id                | smallint(5) unsigned  | NO   | PRI | NULL    |       |
| aty_start_date        | date                  | NO   |     | NULL    |       |
| aty_end_date          | date                  | YES  |     | NULL    |       |
| aty_hourly_rate       | decimal(5,2) unsigned | NO   |     | NULL    |       |
| aty_years_in_practice | tinyint(4)            | NO   |     | NULL    |       |
| aty_notes             | varchar(255)          | YES  |     | NULL    |       |
+-----------------------+-----------------------+------+-----+---------+-------+
6 rows in set (0.00 sec)

mysql> create table if not exists client
    -> (
    -> per_id smallint unsigned not null,
    -> cli_notes varchar(255) null default null,
    -> primary key (per_id),
    -> 
    -> index idx_per_id(per_id asc),
    -> 
    -> constraint fk_client_person
    -> foreign key (per_id)
    -> references person (per_id)
    -> on delete no action 
    -> on update cascade
    -> );
Query OK, 0 rows affected (0.04 sec)

mysql> 
mysql> show warnings;
Empty set (0.00 sec)

mysql> 
mysql> show tables;
+------------------+
| Tables_in_ooa19a |
+------------------+
| attorney         |
| client           |
| person           |
+------------------+
3 rows in set (0.00 sec)

mysql> describe client;
+-----------+----------------------+------+-----+---------+-------+
| Field     | Type                 | Null | Key | Default | Extra |
+-----------+----------------------+------+-----+---------+-------+
| per_id    | smallint(5) unsigned | NO   | PRI | NULL    |       |
| cli_notes | varchar(255)         | YES  |     | NULL    |       |
+-----------+----------------------+------+-----+---------+-------+
2 rows in set (0.00 sec)

mysql> CREATE TABLE IF NOT EXISTS `ooa19a`.`court` (
    ->   `crt_id` tinyint UNSIGNED NOT NULL AUTO_INCREMENT,
    ->   `crt_name` VARCHAR(40) NOT NULL,
    ->   `crt_street` VARCHAR(30) NOT NULL,
    ->   `crt_city` VARCHAR(30) NOT NULL,
    ->   `crt_state` CHAR(2) NOT NULL,
    ->   `crt_zip` INT(9) ZEROFILL UNSIGNED NULL,
    ->  `crt_phone` bigint not null,
    ->   `crt_email` VARCHAR(100) NOT NULL,
    ->   `crt_url` varchar(100) NOT NULL,
    ->   `crt_notes` VARCHAR(255) NULL,
    ->   PRIMARY KEY (`crt_id`))
    -> ENGINE = InnoDB;
Query OK, 0 rows affected, 2 warnings (0.03 sec)

mysql> show tables;
+------------------+
| Tables_in_ooa19a |
+------------------+
| attorney         |
| client           |
| court            |
| person           |
+------------------+
4 rows in set (0.00 sec)

mysql> drop table if exists judge;create table if not exists judge(per_id smallint unsigned not null,crt_id tinyint unsigned null default null,jud_salary decimal (8,2) not null,jud_years_in_practice tinyint unsigned not null,jud_notes varchar(255) null default null,primary key (per_id),index idx_per_id(per_id asc),index idx_crt_id (crt_id asc),constraint fk_judge_personforeign key (per_id)references person (per_id)on delete no action on update cascade,constraint fk_judge_courtforeign key(crt_id)references court (crt_id)on delete no actionon update cascade);
Query OK, 0 rows affected, 1 warning (0.01 sec)

ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'key (per_id)references person (per_id)on delete no action on update cascade,cons' at line 1
mysql> drop table if exists judge;create table if not exists judge(per_id smallint unsigned not null,crt_id tinyint unsigned null default null,jud_salary decimal (8,2) not null,jud_years_in_practice tinyint unsigned not null,jud_notes varchar(255) null default null,primary key (per_id),index idx_per_id(per_id asc),index idx_crt_id (crt_id asc),constraint fk_judge_personforeign key (per_id)references person (per_id)on delete no action on update cascade,constraint fk_judge_courtforeign key(crt_id)references court (crt_id)on delete no actionon update cascade);
Query OK, 0 rows affected, 1 warning (0.01 sec)

ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'key (per_id)references person (per_id)on delete no action on update cascade,cons' at line 1
mysql> drop table if exists judge;create table if not exists judge(per_id smallint unsigned not null,crt_id tinyint unsigned null default null,jud_salary decimal (8,2) not null,jud_years_in_practice tinyint unsigned not null,jud_notes varchar(255) null default null,primary key (per_id),index idx_per_id(per_id asc),index idx_crt_id (crt_id asc),constraint fk_judge_personforeign key (per_id)references person (per_id)on delete no action on update cascade,constraint fk_judge_courtforeign key(crt_id)references court (crt_id)on delete no actionon update cascade);
Query OK, 0 rows affected (0.03 sec)

ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'key (per_id)references person (per_id)on delete no action on update cascade,cons' at line 1
mysql> show tables;
+------------------+
| Tables_in_ooa19a |
+------------------+
| attorney         |
| client           |
| court            |
| person           |
+------------------+
4 rows in set (0.00 sec)

mysql> show tables;
+------------------+
| Tables_in_ooa19a |
+------------------+
| attorney         |
| client           |
| court            |
| person           |
+------------------+
4 rows in set (0.00 sec)

mysql> show tables;
+------------------+
| Tables_in_ooa19a |
+------------------+
| attorney         |
| client           |
| court            |
| judge            |
| person           |
+------------------+
5 rows in set (0.00 sec)

mysql> show tables;
+------------------+
| Tables_in_ooa19a |
+------------------+
| attorney         |
| client           |
| court            |
| judge            |
| person           |
+------------------+
5 rows in set (0.00 sec)

mysql> describe judge;
+-----------------------+----------------------+------+-----+---------+-------+
| Field                 | Type                 | Null | Key | Default | Extra |
+-----------------------+----------------------+------+-----+---------+-------+
| per_id                | smallint(5) unsigned | NO   | PRI | NULL    |       |
| crt_id                | tinyint(3) unsigned  | YES  | MUL | NULL    |       |
| jud_salary            | decimal(8,2)         | NO   |     | NULL    |       |
| jud_years_in_practice | tinyint(3) unsigned  | NO   |     | NULL    |       |
| jud_notes             | varchar(255)         | YES  |     | NULL    |       |
+-----------------------+----------------------+------+-----+---------+-------+
5 rows in set (0.00 sec)

mysql> show tables;
+------------------+
| Tables_in_ooa19a |
+------------------+
| attorney         |
| client           |
| court            |
| judge            |
| person           |
+------------------+
5 rows in set (0.00 sec)

mysql> show tables;
+------------------+
| Tables_in_ooa19a |
+------------------+
| attorney         |
| client           |
| court            |
| judge            |
| person           |
+------------------+
5 rows in set (0.00 sec)

mysql> drop table if exists judge_hist;create table if not exists judge_hist(jhs_id smallint unsigned not null auto_increment,per_id smallint unsigned not null,jhs_crt_id tinyint null,jhs_date timestamp not null default current_timestamp(),jhs_type enum('i','u','d') not null default 'i',jhs_salary decimal (8,2) not null,jhs_notes varchar(255) null,primary key(jhs_id),index idx_per_id (per_id asc),constraint fk_judge_hist_judgeforeign key (per_id)references judge (per_id)on delete no action on update cascade)
Query OK, 0 rows affected, 1 warning (0.01 sec)

    -> ;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'key (per_id)references judge (per_id)on delete no action on update cascade)' at line 1
mysql> drop table if exists judge_hist;
Query OK, 0 rows affected, 1 warning (0.02 sec)

mysql> 
mysql> create table if not exists judge_hist
    -> (
    -> jhs_id smallint unsigned not null auto_increment,
    -> per_id smallint unsigned not null,
    -> jhs_crt_id tinyint null,
    -> jhs_date timestamp not null default current_timestamp(),
    -> jhs_type enum('i','u','d') not null default 'i',
    -> jhs_salary decimal (8,2) not null,
    -> jhs_notes varchar(255) null,
    -> primary key(jhs_id),
    -> 
    -> index idx_per_id (per_id asc),
    -> 
    -> constraint fk_judge_hist_judge
    -> foreign key (per_id)
    -> references judge (per_id)
    -> on delete no action 
    -> on update cascade
    -> );
Query OK, 0 rows affected (0.05 sec)

mysql> describe judge_hist;
+------------+----------------------+------+-----+-------------------+-------------------+
| Field      | Type                 | Null | Key | Default           | Extra             |
+------------+----------------------+------+-----+-------------------+-------------------+
| jhs_id     | smallint(5) unsigned | NO   | PRI | NULL              | auto_increment    |
| per_id     | smallint(5) unsigned | NO   | MUL | NULL              |                   |
| jhs_crt_id | tinyint(4)           | YES  |     | NULL              |                   |
| jhs_date   | timestamp            | NO   |     | CURRENT_TIMESTAMP | DEFAULT_GENERATED |
| jhs_type   | enum('i','u','d')    | NO   |     | i                 |                   |
| jhs_salary | decimal(8,2)         | NO   |     | NULL              |                   |
| jhs_notes  | varchar(255)         | YES  |     | NULL              |                   |
+------------+----------------------+------+-----+-------------------+-------------------+
7 rows in set (0.00 sec)

mysql> 
mysql> create table if not exists case
    -> (
    -> cse_id smallint unsigned not null auto_increment,
    -> per_id smallint unsigned not null,
    -> cse_type varchar(45) not null,
    -> cse_description text not null,
    -> cse_start_date date not null,
    -> cse_end_date date null,
    -> cse_notes varchar(255) null,
    -> primary key (cse_id),
    -> 
    -> index idx_per_id(per_id asc),
    -> 
    -> constraint fk_court_case_judge
    -> foriegn key (per_id)
    -> references judge(per_id)
    -> on delete no action
    -> on update cascade
    -> );
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'case
(
cse_id smallint unsigned not null auto_increment,
per_id smallint unsigne' at line 1
mysql> show tables;
+------------------+
| Tables_in_ooa19a |
+------------------+
| attorney         |
| case             |
| client           |
| court            |
| judge            |
| judge_hist       |
| person           |
+------------------+
7 rows in set (0.00 sec)

mysql> create table if not exists bar
    -> (
    -> bar_id tinyint unsigned not null auto_increment,
    -> per_id smallint unsigned not null,
    -> bar_name varchar(45) not null,
    -> bar_notes varchar (255) null, 
    -> primary key (bar_id),
    -> 
    -> index idx_per_id (per_id asc),
    -> 
    -> constraint fk_bar_attorney
    -> foreign key (per_id)
    -> references attorney (per_id)
    -> on delete no action
    -> on update cascade
    -> );
Query OK, 0 rows affected (0.04 sec)

mysql> show tables;
+------------------+
| Tables_in_ooa19a |
+------------------+
| attorney         |
| bar              |
| case             |
| client           |
| court            |
| judge            |
| judge_hist       |
| person           |
+------------------+
8 rows in set (0.00 sec)

mysql> create table if not exists speciality 
    -> (
    -> spc_id tinyint unsigned not null auto_increment,
    -> per_id smallint unsigned not null,
    -> spc_type varchar (45) not null, 
    -> spc_notes varchar (255) null,
    -> primary key (spc_id),
    -> 
    -> index idx_per_id(per_id asc),
    -> 
    -> constraint fk_specialty_attorney
    -> foreign key (per_id)
    -> references attorney (per_id)
    -> on delete no action
    -> on update cascade
    -> );
Query OK, 0 rows affected (0.05 sec)

mysql> 
mysql> show tables;
+------------------+
| Tables_in_ooa19a |
+------------------+
| attorney         |
| bar              |
| case             |
| client           |
| court            |
| judge            |
| judge_hist       |
| person           |
| speciality       |
+------------------+
9 rows in set (0.00 sec)

mysql> create table if not exists assignment
    -> (
    -> asn_id smallint unsigned not null auto_increment,
    -> per_cid smallint unsigned not null,
    -> per_aid smallint unsigned not null,
    -> cse_id smallint unsigned not null,
    -> asn_notes varchar(255) null,
    -> primary key(asn_id),
    -> 
    -> index idx_per_cid (per_cid asc),
    -> index idx_per_aid (per_aid asc),
    -> index idx_cse_id (cse_id asc),
    -> 
    -> unique index ux_per_cid_per_aid_cse_id(per_cid asc, per_aid asc, cse_id asc),
    -> 
    -> constraint fk_assign_case
    -> foreign key (cse_id)
    -> references `case` (cse_id)
    -> on delete no action
    -> on update cascade,
    -> 
    -> constraint fk_assignment_case
    -> foreign key (per_id)
    -> references client (per_id)
    -> on delete no action
    -> on update cascade,
    -> 
    -> constraint fk_assignment_attorney
    -> foreign key (per_id)
    -> references attorney (per_id)
    -> on delete no action
    -> on update cascade
    -> );
ERROR 1072 (42000): Key column 'per_id' doesn't exist in table
mysql> create table if not exists assignment
    -> (
    -> asn_id smallint unsigned not null auto_increment,
    -> per_cid smallint unsigned not null,
    -> per_aid smallint unsigned not null,
    -> cse_id smallint unsigned not null,
    -> asn_notes varchar(255) null,
    -> primary key(asn_id),
    -> 
    -> index idx_per_cid (per_cid asc),
    -> index idx_per_aid (per_aid asc),
    -> index idx_cse_id (cse_id asc),
    -> 
    -> unique index ux_per_cid_per_aid_cse_id(per_cid asc, per_aid asc, cse_id asc),
    -> 
    -> constraint fk_assign_case
    -> foreign key (cse_id)
    -> references `case` (cse_id)
    -> on delete no action
    -> on update cascade,
    -> 
    -> constraint fk_assignment_case
    -> foreign key (per_cid)
    -> references client (per_id)
    -> on delete no action
    -> on update cascade,
    -> 
    -> constraint fk_assignment_attorney
    -> foreign key (per_id)
    -> references attorney (per_id)
    -> on delete no action
    -> on update cascade
    -> );
ERROR 1072 (42000): Key column 'per_id' doesn't exist in table
mysql> create table if not exists assignment
    -> (
    -> asn_id smallint unsigned not null auto_increment,
    -> per_cid smallint unsigned not null,
    -> per_aid smallint unsigned not null,
    -> cse_id smallint unsigned not null,
    -> asn_notes varchar(255) null,
    -> primary key(asn_id),
    -> 
    -> index idx_per_cid (per_cid asc),
    -> index idx_per_aid (per_aid asc),
    -> index idx_cse_id (cse_id asc),
    -> 
    -> unique index ux_per_cid_per_aid_cse_id(per_cid asc, per_aid asc, cse_id asc),
    -> 
    -> constraint fk_assign_case
    -> foreign key (cse_id)
    -> references `case` (cse_id)
    -> on delete no action
    -> on update cascade,
    -> 
    -> constraint fk_assignment_case
    -> foreign key (per_cid)
    -> references client (per_id)
    -> on delete no action
    -> on update cascade,
    -> 
    -> constraint fk_assignment_attorney
    -> foreign key (per_aid)
    -> references attorney (per_id)
    -> on delete no action
    -> on update cascade
    -> );
Query OK, 0 rows affected (0.06 sec)

mysql> show tables;
+------------------+
| Tables_in_ooa19a |
+------------------+
| assignment       |
| attorney         |
| bar              |
| case             |
| client           |
| court            |
| judge            |
| judge_hist       |
| person           |
| speciality       |
+------------------+
10 rows in set (0.00 sec)

mysql> create table if not exists phone
    -> (
    -> phn_id smallint unsigned not null auto_increment,
    -> per_id smallint unsigned not null,
    -> phn_num bigint unsigned not null,
    -> phn_type enum('h','c','w','f') not null comment 'home, cell, work,fax',
    -> phn_notes varchar (255) null,
    -> primary key (phn_id),
    -> 
    -> index idx_per_id(per_id asc),
    -> 
    -> constraint fk_phone_person
    -> foreign key (per_id)
    -> references person(per_id)
    -> on delete no action
    -> on update cascade
    -> 
    -> );
Query OK, 0 rows affected (0.05 sec)

mysql> show tables;
+------------------+
| Tables_in_ooa19a |
+------------------+
| assignment       |
| attorney         |
| bar              |
| case             |
| client           |
| court            |
| judge            |
| judge_hist       |
| person           |
| phone            |
| speciality       |
+------------------+
11 rows in set (0.00 sec)

mysql> describe person;
+------------+--------------------------+------+-----+---------+----------------+
| Field      | Type                     | Null | Key | Default | Extra          |
+------------+--------------------------+------+-----+---------+----------------+
| per_id     | smallint(5) unsigned     | NO   | PRI | NULL    | auto_increment |
| per_ssn    | binary(64)               | YES  |     | NULL    |                |
| per_ssnt   | binary(64)               | YES  |     | NULL    |                |
| per_fname  | varchar(15)              | NO   |     | NULL    |                |
| per_lname  | varchar(30)              | NO   |     | NULL    |                |
| per_street | varchar(30)              | NO   |     | NULL    |                |
| per_city   | varchar(30)              | NO   |     | NULL    |                |
| per_state  | char(2)                  | NO   |     | NULL    |                |
| per_zip    | int(9) unsigned zerofill | YES  |     | NULL    |                |
| per_email  | varchar(100)             | NO   |     | NULL    |                |
| per_dob    | date                     | NO   |     | NULL    |                |
| per_type   | enum('a','c','j')        | NO   |     | NULL    |                |
| per_notes  | varchar(255)             | YES  |     | NULL    |                |
+------------+--------------------------+------+-----+---------+----------------+
13 rows in set (0.00 sec)

mysql> alter table person drop per_ssnt;
Query OK, 0 rows affected (0.07 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> describe person;
+------------+--------------------------+------+-----+---------+----------------+
| Field      | Type                     | Null | Key | Default | Extra          |
+------------+--------------------------+------+-----+---------+----------------+
| per_id     | smallint(5) unsigned     | NO   | PRI | NULL    | auto_increment |
| per_ssn    | binary(64)               | YES  |     | NULL    |                |
| per_fname  | varchar(15)              | NO   |     | NULL    |                |
| per_lname  | varchar(30)              | NO   |     | NULL    |                |
| per_street | varchar(30)              | NO   |     | NULL    |                |
| per_city   | varchar(30)              | NO   |     | NULL    |                |
| per_state  | char(2)                  | NO   |     | NULL    |                |
| per_zip    | int(9) unsigned zerofill | YES  |     | NULL    |                |
| per_email  | varchar(100)             | NO   |     | NULL    |                |
| per_dob    | date                     | NO   |     | NULL    |                |
| per_type   | enum('a','c','j')        | NO   |     | NULL    |                |
| per_notes  | varchar(255)             | YES  |     | NULL    |                |
+------------+--------------------------+------+-----+---------+----------------+
12 rows in set (0.00 sec)

mysql> insert into person values
    -> (null, null, 'Steve','Rogers','437 Southern Drive', 'Rochester', 'NY', 32443022,'srogers@comcast.net', '1923-10-03', 'c', null);
Query OK, 1 row affected (0.01 sec)

mysql> select * from person where per_type ='c';
+--------+---------+-----------+-----------+--------------------+-----------+-----------+-----------+---------------------+------------+----------+-----------+
| per_id | per_ssn | per_fname | per_lname | per_street         | per_city  | per_state | per_zip   | per_email           | per_dob    | per_type | per_notes |
+--------+---------+-----------+-----------+--------------------+-----------+-----------+-----------+---------------------+------------+----------+-----------+
|      1 | NULL    | Steve     | Rogers    | 437 Southern Drive | Rochester | NY        | 032443022 | srogers@comcast.net | 1923-10-03 | c        | NULL      |
+--------+---------+-----------+-----------+--------------------+-----------+-----------+-----------+---------------------+------------+----------+-----------+
1 row in set (0.00 sec)

mysql> insert into person values
    -> 
    -> (null, null, 'Bruce','Wayne','1007 Mountain Drive', 'Gotham', 'NY', 003243440,'bwayne@knology.net', '1968-03-20', 'c', null),
    -> (null, null, 'Peter','Parker','20 Ingram Street', 'New York', 'NY', 102862341,'pparker@msn.net', '1923-10-03', 'c', null),
    -> (null, null, 'Jane','Thompson','13563 Ocean View Drive', 'Seattle', 'WA', 032084409,'jthompson@gmail.net', '1973-05-08', 'c', null),
    -> (null, null, 'Debra','Steele','543 Oak Ln', 'Milwaukee', 'WI', 2862374478,'dsteele@verizon.net', '1994-07-19', 'c', null);
Query OK, 4 rows affected (0.01 sec)
Records: 4  Duplicates: 0  Warnings: 0

mysql> select * from person where per_type ='c';
+--------+---------+-----------+-----------+------------------------+-----------+-----------+------------+---------------------+------------+----------+-----------+
| per_id | per_ssn | per_fname | per_lname | per_street             | per_city  | per_state | per_zip    | per_email           | per_dob    | per_type | per_notes |
+--------+---------+-----------+-----------+------------------------+-----------+-----------+------------+---------------------+------------+----------+-----------+
|      1 | NULL    | Steve     | Rogers    | 437 Southern Drive     | Rochester | NY        |  032443022 | srogers@comcast.net | 1923-10-03 | c        | NULL      |
|      2 | NULL    | Bruce     | Wayne     | 1007 Mountain Drive    | Gotham    | NY        |  003243440 | bwayne@knology.net  | 1968-03-20 | c        | NULL      |
|      3 | NULL    | Peter     | Parker    | 20 Ingram Street       | New York  | NY        |  102862341 | pparker@msn.net     | 1923-10-03 | c        | NULL      |
|      4 | NULL    | Jane      | Thompson  | 13563 Ocean View Drive | Seattle   | WA        |  032084409 | jthompson@gmail.net | 1973-05-08 | c        | NULL      |
|      5 | NULL    | Debra     | Steele    | 543 Oak Ln             | Milwaukee | WI        | 2862374478 | dsteele@verizon.net | 1994-07-19 | c        | NULL      |
+--------+---------+-----------+-----------+------------------------+-----------+-----------+------------+---------------------+------------+----------+-----------+
5 rows in set (0.00 sec)

mysql> select distinct per_type from customer;
ERROR 1146 (42S02): Table 'ooa19a.customer' doesn't exist
mysql> select distinct per_type from person;
+----------+
| per_type |
+----------+
| c        |
+----------+
1 row in set (0.01 sec)

mysql> select distinct count(distinct per_type) from person;
+--------------------------+
| count(distinct per_type) |
+--------------------------+
|                        1 |
+--------------------------+
1 row in set (0.01 sec)

mysql> insert into person values
    -> (null, null, 'Tony','Stark','332 Palm Avenue', 'Malibu', 'CA', 902638832,'tstark@yahoo.com', '1972-05-04', 'a', null),
    -> (null, null, 'Hank','Pymi','2355 Brown Street', 'Cleveland', 'OH', 022342390,'hpym@aol.net', '1980-08-28', 'a', null),
    -> (null, null, 'Bob','Best','4902 Avendale Ave.', 'Scottsdale', 'AZ', 87263912,'bbest@yahoo.com', '1992-02-10', 'a', null),
    -> (null, null, 'Sandra','Dole','8732 Lawrence Ave.', 'Atlanta', 'GA', 002345390,'sdole@gmail.net', '1990-01-26', 'a', null),
    -> (null, null, 'Ben','Avery','6432 Thunderbird Ln', 'Sioux Falls', 'SD', 5637932,'bavery@hotmail.net', '1983-12-24', 'a', null);
Query OK, 5 rows affected (0.01 sec)
Records: 5  Duplicates: 0  Warnings: 0

mysql> select distinct count(distinct per_type) from person;
+--------------------------+
| count(distinct per_type) |
+--------------------------+
|                        2 |
+--------------------------+
1 row in set (0.00 sec)

mysql> select count(distinct per_type) from person;
+--------------------------+
| count(distinct per_type) |
+--------------------------+
|                        2 |
+--------------------------+
1 row in set (0.00 sec)

mysql> insert into person values
    -> (null, null, 'Arthur','Curry','3302 Euclid Avenue', 'Miami', 'FL', 000219932,'acurry@gmail.com', '1975-12-15', 'j', null),
    -> (null, null, 'Diana','Price','944 Green Street', 'Las vegas', 'NV', 33235223,'dprice@aol.net', '1980-08-22', 'j', null),
    -> (null, null, 'Adam','Jurris','98435 Valencia Dr.', 'Gulf Shore', 'AL', 870639912,'ajurris@yahoo.com', '1995-01-32', 'j', null),
    -> (null, null, 'Judy','Sleen','56343 Rover Ct.', 'Billings', 'MT', 67204823,'jsleen@symaptico.com', '1995-01-31', 'j', null),
    -> (null, null, 'Bill','Neiderheim','43567 Netherland Blvd', 'South Bend', 'IN', 320219932,'bneiderheim@comcast.net', '1982-03-24', 'j', null);
ERROR 1292 (22007): Incorrect date value: '1995-01-32' for column 'per_dob' at row 3
mysql> insert into person values
    -> (null, null, 'Arthur','Curry','3302 Euclid Avenue', 'Miami', 'FL', 000219932,'acurry@gmail.com', '1975-12-15', 'j', null),
    -> (null, null, 'Diana','Price','944 Green Street', 'Las vegas', 'NV', 33235223,'dprice@aol.net', '1980-08-22', 'j', null),
    -> (null, null, 'Adam','Jurris','98435 Valencia Dr.', 'Gulf Shore', 'AL', 870639912,'ajurris@yahoo.com', '1995-01-31', 'j', null),
    -> (null, null, 'Judy','Sleen','56343 Rover Ct.', 'Billings', 'MT', 67204823,'jsleen@symaptico.com', '1995-01-31', 'j', null),
    -> (null, null, 'Bill','Neiderheim','43567 Netherland Blvd', 'South Bend', 'IN', 320219932,'bneiderheim@comcast.net', '1982-03-24', 'j', null);
Query OK, 5 rows affected (0.01 sec)
Records: 5  Duplicates: 0  Warnings: 0

mysql> select count(distinct per_type) from person;
+--------------------------+
| count(distinct per_type) |
+--------------------------+
|                        3 |
+--------------------------+
1 row in set (0.00 sec)

mysql> select distinct count(distinct per_type) from person;
+--------------------------+
| count(distinct per_type) |
+--------------------------+
|                        3 |
+--------------------------+
1 row in set (0.00 sec)

mysql> select * from person;
+--------+---------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
| per_id | per_ssn | per_fname | per_lname  | per_street             | per_city    | per_state | per_zip    | per_email               | per_dob    | per_type | per_notes |
+--------+---------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
|      1 | NULL    | Steve     | Rogers     | 437 Southern Drive     | Rochester   | NY        |  032443022 | srogers@comcast.net     | 1923-10-03 | c        | NULL      |
|      2 | NULL    | Bruce     | Wayne      | 1007 Mountain Drive    | Gotham      | NY        |  003243440 | bwayne@knology.net      | 1968-03-20 | c        | NULL      |
|      3 | NULL    | Peter     | Parker     | 20 Ingram Street       | New York    | NY        |  102862341 | pparker@msn.net         | 1923-10-03 | c        | NULL      |
|      4 | NULL    | Jane      | Thompson   | 13563 Ocean View Drive | Seattle     | WA        |  032084409 | jthompson@gmail.net     | 1973-05-08 | c        | NULL      |
|      5 | NULL    | Debra     | Steele     | 543 Oak Ln             | Milwaukee   | WI        | 2862374478 | dsteele@verizon.net     | 1994-07-19 | c        | NULL      |
|      6 | NULL    | Tony      | Stark      | 332 Palm Avenue        | Malibu      | CA        |  902638832 | tstark@yahoo.com        | 1972-05-04 | a        | NULL      |
|      7 | NULL    | Hank      | Pymi       | 2355 Brown Street      | Cleveland   | OH        |  022342390 | hpym@aol.net            | 1980-08-28 | a        | NULL      |
|      8 | NULL    | Bob       | Best       | 4902 Avendale Ave.     | Scottsdale  | AZ        |  087263912 | bbest@yahoo.com         | 1992-02-10 | a        | NULL      |
|      9 | NULL    | Sandra    | Dole       | 8732 Lawrence Ave.     | Atlanta     | GA        |  002345390 | sdole@gmail.net         | 1990-01-26 | a        | NULL      |
|     10 | NULL    | Ben       | Avery      | 6432 Thunderbird Ln    | Sioux Falls | SD        |  005637932 | bavery@hotmail.net      | 1983-12-24 | a        | NULL      |
|     16 | NULL    | Arthur    | Curry      | 3302 Euclid Avenue     | Miami       | FL        |  000219932 | acurry@gmail.com        | 1975-12-15 | j        | NULL      |
|     17 | NULL    | Diana     | Price      | 944 Green Street       | Las vegas   | NV        |  033235223 | dprice@aol.net          | 1980-08-22 | j        | NULL      |
|     18 | NULL    | Adam      | Jurris     | 98435 Valencia Dr.     | Gulf Shore  | AL        |  870639912 | ajurris@yahoo.com       | 1995-01-31 | j        | NULL      |
|     19 | NULL    | Judy      | Sleen      | 56343 Rover Ct.        | Billings    | MT        |  067204823 | jsleen@symaptico.com    | 1995-01-31 | j        | NULL      |
|     20 | NULL    | Bill      | Neiderheim | 43567 Netherland Blvd  | South Bend  | IN        |  320219932 | bneiderheim@comcast.net | 1982-03-24 | j        | NULL      |
+--------+---------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
15 rows in set (0.00 sec)

mysql> select distinct per_type from person;
+----------+
| per_type |
+----------+
| c        |
| a        |
| j        |
+----------+
3 rows in set (0.00 sec)

mysql> select distinct count(per_type) where per_type = 'c';
ERROR 1054 (42S22): Unknown column 'per_type' in 'field list'
mysql> select distinct count(per_type) from person where per_type = 'c';
+-----------------+
| count(per_type) |
+-----------------+
|               5 |
+-----------------+
1 row in set (0.00 sec)

mysql> select distinct count(per_type) as "Total Number of Clients" from person where per_type = 'c';
+-------------------------+
| Total Number of Clients |
+-------------------------+
|                       5 |
+-------------------------+
1 row in set (0.00 sec)

mysql> insert into client values
    -> (1, null),
    -> (2, null),
    -> (3, null),
    -> (4, null),
    -> (5, null);
Query OK, 5 rows affected (0.01 sec)
Records: 5  Duplicates: 0  Warnings: 0

mysql> start transaction;
Query OK, 0 rows affected (0.00 sec)

mysql> 
mysql> insert into phone values
    -> (null,1 ,6363782824,'c',null),
    -> (null,2 ,8283443432,'h',null),
    -> (null,3 ,2434578453,'w','has two office numbers'),
    -> (null,4 ,7812324282,'w',null),
    -> (null,5 ,5762378299,'f',null),
    -> (null,6 ,3363788209,'h','prefer cell phone calls'),
    -> (null,7 ,1094782823,'w','call during lunch'),
    -> (null,8 ,6328299339,'c','best number to reach'),
    -> (null,9 ,9993384822,'w','use for faxing legal docs'),
    -> (null,10,3874732999,'f',null),
    -> (null,11,4999000222,'c',null),
    -> (null,12,8636378282,'w',null),
    -> (null,13,9636378282,'f','call in the morning');
ERROR 1452 (23000): Cannot add or update a child row: a foreign key constraint fails (`ooa19a`.`phone`, CONSTRAINT `fk_phone_person` FOREIGN KEY (`per_id`) REFERENCES `person` (`per_id`) ON UPDATE CASCADE)
mysql> 
mysql> commit;
Query OK, 0 rows affected (0.01 sec)

mysql> insert into phone values
    -> (null,1 ,6363782824,'c',null);
Query OK, 1 row affected (0.01 sec)

mysql> insert into phone values
    -> 
    -> (null,2 ,8283443432,'h',null),
    -> (null,3 ,2434578453,'w','has two office numbers'),
    -> (null,4 ,7812324282,'w',null),
    -> (null,5 ,5762378299,'f',null),
    -> (null,6 ,3363788209,'h','prefer cell phone calls'),
    -> (null,7 ,1094782823,'w','call during lunch'),
    -> (null,8 ,6328299339,'c','best number to reach'),
    -> (null,9 ,9993384822,'w','use for faxing legal docs'),
    -> (null,10,3874732999,'f',null),
    -> (null,11,4999000222,'c',null),
    -> (null,12,8636378282,'w',null),
    -> (null,13,9636378282,'f','call in the morning');
ERROR 1452 (23000): Cannot add or update a child row: a foreign key constraint fails (`ooa19a`.`phone`, CONSTRAINT `fk_phone_person` FOREIGN KEY (`per_id`) REFERENCES `person` (`per_id`) ON UPDATE CASCADE)
mysql> insert into phone values
    -> (null,13,9636378282,'f','call in the morning');
ERROR 1452 (23000): Cannot add or update a child row: a foreign key constraint fails (`ooa19a`.`phone`, CONSTRAINT `fk_phone_person` FOREIGN KEY (`per_id`) REFERENCES `person` (`per_id`) ON UPDATE CASCADE)
mysql> insert into phone values
    -> (null,12,8636378282,'w',null);
ERROR 1452 (23000): Cannot add or update a child row: a foreign key constraint fails (`ooa19a`.`phone`, CONSTRAINT `fk_phone_person` FOREIGN KEY (`per_id`) REFERENCES `person` (`per_id`) ON UPDATE CASCADE)
mysql> insert into phone values
    -> (null,2 ,8283443432,'h',null);
Query OK, 1 row affected (0.01 sec)

mysql> insert into phone values
    -> (null,3 ,2434578453,'w','has two office numbers'),
    -> (null,4 ,7812324282,'w',null),
    -> (null,5 ,5762378299,'f',null),
    -> (null,6 ,3363788209,'h','prefer cell phone calls'),
    -> (null,7 ,1094782823,'w','call during lunch');
Query OK, 5 rows affected (0.01 sec)
Records: 5  Duplicates: 0  Warnings: 0

mysql> insert into phone values
    -> (null,8 ,6328299339,'c','best number to reach'),
    -> (null,9 ,9993384822,'w','use for faxing legal docs'),
    -> (null,10,3874732999,'f',null),
    -> (null,11,4999000222,'c',null);
ERROR 1452 (23000): Cannot add or update a child row: a foreign key constraint fails (`ooa19a`.`phone`, CONSTRAINT `fk_phone_person` FOREIGN KEY (`per_id`) REFERENCES `person` (`per_id`) ON UPDATE CASCADE)
mysql> insert into phone values
    -> (null,8 ,6328299339,'c','best number to reach'),
    -> (null,9 ,9993384822,'w','use for faxing legal docs');
Query OK, 2 rows affected (0.01 sec)
Records: 2  Duplicates: 0  Warnings: 0

mysql> insert into phone values
    -> (null,10,3874732999,'f',null);
Query OK, 1 row affected (0.01 sec)

mysql> insert into phone values
    -> (null,11,4999000222,'c',null);
ERROR 1452 (23000): Cannot add or update a child row: a foreign key constraint fails (`ooa19a`.`phone`, CONSTRAINT `fk_phone_person` FOREIGN KEY (`per_id`) REFERENCES `person` (`per_id`) ON UPDATE CASCADE)
mysql> select * from phone;
+--------+--------+------------+----------+---------------------------+
| phn_id | per_id | phn_num    | phn_type | phn_notes                 |
+--------+--------+------------+----------+---------------------------+
|     14 |      1 | 6363782824 | c        | NULL                      |
|     29 |      2 | 8283443432 | h        | NULL                      |
|     30 |      3 | 2434578453 | w        | has two office numbers    |
|     31 |      4 | 7812324282 | w        | NULL                      |
|     32 |      5 | 5762378299 | f        | NULL                      |
|     33 |      6 | 3363788209 | h        | prefer cell phone calls   |
|     34 |      7 | 1094782823 | w        | call during lunch         |
|     39 |      8 | 6328299339 | c        | best number to reach      |
|     40 |      9 | 9993384822 | w        | use for faxing legal docs |
|     41 |     10 | 3874732999 | f        | NULL                      |
+--------+--------+------------+----------+---------------------------+
10 rows in set (0.00 sec)

mysql> drop table phone;
Query OK, 0 rows affected (0.03 sec)

mysql> create table if not exists phone
    -> (
    -> phn_id smallint unsigned not null auto_increment,
    -> per_id smallint unsigned not null,
    -> phn_num bigint unsigned not null,
    -> phn_type enum('h','c','w','f') not null comment 'home, cell, work,fax',
    -> phn_notes varchar (255) null,
    -> primary key (phn_id),
    -> 
    -> index idx_per_id(per_id asc),
    -> 
    -> constraint fk_phone_person
    -> foreign key (per_id)
    -> references person(per_id)
    -> on delete no action
    -> on update cascade
    -> 
    -> );
Query OK, 0 rows affected (0.06 sec)

mysql> insert into phone values
    -> (null,1 ,6363782824,'c',null),
    -> (null,2 ,8283443432,'h',null),
    -> (null,3 ,2434578453,'w','has two office numbers'),
    -> (null,4 ,7812324282,'w',null),
    -> (null,5 ,5762378299,'f',null),
    -> (null,6 ,3363788209,'h','prefer cell phone calls'),
    -> (null,7 ,1094782823,'w','call during lunch'),
    -> (null,8 ,6328299339,'c','best number to reach'),
    -> (null,9 ,9993384822,'w','use for faxing legal docs'),
    -> (null,10,3874732999,'f',null);
Query OK, 10 rows affected (0.01 sec)
Records: 10  Duplicates: 0  Warnings: 0

mysql> select * from phone;
+--------+--------+------------+----------+---------------------------+
| phn_id | per_id | phn_num    | phn_type | phn_notes                 |
+--------+--------+------------+----------+---------------------------+
|      1 |      1 | 6363782824 | c        | NULL                      |
|      2 |      2 | 8283443432 | h        | NULL                      |
|      3 |      3 | 2434578453 | w        | has two office numbers    |
|      4 |      4 | 7812324282 | w        | NULL                      |
|      5 |      5 | 5762378299 | f        | NULL                      |
|      6 |      6 | 3363788209 | h        | prefer cell phone calls   |
|      7 |      7 | 1094782823 | w        | call during lunch         |
|      8 |      8 | 6328299339 | c        | best number to reach      |
|      9 |      9 | 9993384822 | w        | use for faxing legal docs |
|     10 |     10 | 3874732999 | f        | NULL                      |
+--------+--------+------------+----------+---------------------------+
10 rows in set (0.00 sec)

mysql> insert into phone values
    -> (null,11,4999000222,'c',null),
    -> (null,12,8636378282,'w',null),
    -> (null,13,9636378282,'f','call in the morning');
ERROR 1452 (23000): Cannot add or update a child row: a foreign key constraint fails (`ooa19a`.`phone`, CONSTRAINT `fk_phone_person` FOREIGN KEY (`per_id`) REFERENCES `person` (`per_id`) ON UPDATE CASCADE)
mysql> describe person;
+------------+--------------------------+------+-----+---------+----------------+
| Field      | Type                     | Null | Key | Default | Extra          |
+------------+--------------------------+------+-----+---------+----------------+
| per_id     | smallint(5) unsigned     | NO   | PRI | NULL    | auto_increment |
| per_ssn    | binary(64)               | YES  |     | NULL    |                |
| per_fname  | varchar(15)              | NO   |     | NULL    |                |
| per_lname  | varchar(30)              | NO   |     | NULL    |                |
| per_street | varchar(30)              | NO   |     | NULL    |                |
| per_city   | varchar(30)              | NO   |     | NULL    |                |
| per_state  | char(2)                  | NO   |     | NULL    |                |
| per_zip    | int(9) unsigned zerofill | YES  |     | NULL    |                |
| per_email  | varchar(100)             | NO   |     | NULL    |                |
| per_dob    | date                     | NO   |     | NULL    |                |
| per_type   | enum('a','c','j')        | NO   |     | NULL    |                |
| per_notes  | varchar(255)             | YES  |     | NULL    |                |
+------------+--------------------------+------+-----+---------+----------------+
12 rows in set (0.00 sec)

mysql> select * from phone;
+--------+--------+------------+----------+---------------------------+
| phn_id | per_id | phn_num    | phn_type | phn_notes                 |
+--------+--------+------------+----------+---------------------------+
|      1 |      1 | 6363782824 | c        | NULL                      |
|      2 |      2 | 8283443432 | h        | NULL                      |
|      3 |      3 | 2434578453 | w        | has two office numbers    |
|      4 |      4 | 7812324282 | w        | NULL                      |
|      5 |      5 | 5762378299 | f        | NULL                      |
|      6 |      6 | 3363788209 | h        | prefer cell phone calls   |
|      7 |      7 | 1094782823 | w        | call during lunch         |
|      8 |      8 | 6328299339 | c        | best number to reach      |
|      9 |      9 | 9993384822 | w        | use for faxing legal docs |
|     10 |     10 | 3874732999 | f        | NULL                      |
+--------+--------+------------+----------+---------------------------+
10 rows in set (0.00 sec)

mysql> insert into phone values
    -> (
    -> 11, 11,93837292,'f'null);
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'null)' at line 3
mysql> insert into phone values
    -> (
    -> 11, 11,93837292,'f',null);
ERROR 1452 (23000): Cannot add or update a child row: a foreign key constraint fails (`ooa19a`.`phone`, CONSTRAINT `fk_phone_person` FOREIGN KEY (`per_id`) REFERENCES `person` (`per_id`) ON UPDATE CASCADE)
mysql> select * from person;
Empty set (0.00 sec)

mysql> select * from phone;
Empty set (0.00 sec)

mysql> exit
mysql> CREATE TABLE IF NOT EXISTS `ooa19a`.`court` (
    ->   `crt_id` tinyint UNSIGNED NOT NULL AUTO_INCREMENT,
    ->   `crt_name` VARCHAR(40) NOT NULL,
    ->   `crt_street` VARCHAR(30) NOT NULL,
    ->   `crt_city` VARCHAR(30) NOT NULL,
    ->   `crt_state` CHAR(2) NOT NULL,
    ->   `crt_zip` INT(9) ZEROFILL UNSIGNED NULL,
    ->  `crt_phone` bigint not null,
    ->   `crt_email` VARCHAR(100) NOT NULL,
    ->   `crt_url` varchar(100) NOT NULL,
    ->   `crt_notes` VARCHAR(255) NULL,
    ->   PRIMARY KEY (`crt_id`))
    -> ENGINE = InnoDB;
Query OK, 0 rows affected, 2 warnings (0.03 sec)

mysql> drop table if exists judge;
Query OK, 0 rows affected, 1 warning (0.03 sec)

mysql> 
mysql> create table if not exists judge
    -> (
    -> per_id smallint unsigned not null,
    -> crt_id tinyint unsigned null default null,
    -> jud_salary decimal (8,2) not null,
    -> jud_years_in_practice tinyint unsigned not null,
    -> jud_notes varchar(255) null default null,
    -> primary key (per_id),
    -> 
    -> index idx_per_id(per_id asc),
    -> index idx_crt_id (crt_id asc),
    -> 
    -> constraint fk_judge_person
    -> foreign key (per_id)
    -> references person (per_id)
    -> on delete no action 
    -> on update cascade,
    -> 
    -> constraint fk_judge_court
    -> foreign key (crt_id)
    -> references court (crt_id)
    -> on delete no action
    -> on update cascade
    -> );
Query OK, 0 rows affected (0.07 sec)

mysql> drop table if exists judge_hist;
Query OK, 0 rows affected, 1 warning (0.03 sec)

mysql> 
mysql> create table if not exists judge_hist
    -> (
    -> jhs_id smallint unsigned not null auto_increment,
    -> per_id smallint unsigned not null,
    -> jhs_crt_id tinyint null,
    -> jhs_date timestamp not null default current_timestamp(),
    -> jhs_type enum('i','u','d') not null default 'i',
    -> jhs_salary decimal (8,2) not null,
    -> jhs_notes varchar(255) null,
    -> primary key(jhs_id),
    -> 
    -> index idx_per_id (per_id asc),
    -> 
    -> constraint fk_judge_hist_judge
    -> foreign key (per_id)
    -> references judge (per_id)
    -> on delete no action 
    -> on update cascade
    -> );
Query OK, 0 rows affected (0.06 sec)

mysql> create table if not exists `case`
    -> (
    -> cse_id smallint unsigned not null auto_increment,
    -> per_id smallint unsigned not null,
    -> cse_type varchar(45) not null,
    -> cse_description text not null,
    -> cse_start_date date not null,
    -> cse_end_date date null,
    -> cse_notes varchar(255) null,
    -> primary key (cse_id),
    -> 
    -> index idx_per_id(per_id asc),
    -> 
    -> constraint fk_court_case_judge
    -> foreign key (per_id)
    -> references judge(per_id)
    -> on delete no action
    -> on update cascade
    -> );
Query OK, 0 rows affected (0.05 sec)

mysql> create table if not exists bar
    -> (
    -> bar_id tinyint unsigned not null auto_increment,
    -> per_id smallint unsigned not null,
    -> bar_name varchar(45) not null,
    -> bar_notes varchar (255) null, 
    -> primary key (bar_id),
    -> 
    -> index idx_per_id (per_id asc),
    -> 
    -> constraint fk_bar_attorney
    -> foreign key (per_id)
    -> references attorney (per_id)
    -> on delete no action
    -> on update cascade
    -> );
Query OK, 0 rows affected (0.06 sec)

mysql> create table if not exists speciality 
    -> (
    -> spc_id tinyint unsigned not null auto_increment,
    -> per_id smallint unsigned not null,
    -> spc_type varchar (45) not null, 
    -> spc_notes varchar (255) null,
    -> primary key (spc_id),
    -> 
    -> index idx_per_id(per_id asc),
    -> 
    -> constraint fk_specialty_attorney
    -> foreign key (per_id)
    -> references attorney (per_id)
    -> on delete no action
    -> on update cascade
    -> );
Query OK, 0 rows affected (0.06 sec)

mysql> 
mysql> create table if not exists assignment
    -> (
    -> asn_id smallint unsigned not null auto_increment,
    -> per_cid smallint unsigned not null,
    -> per_aid smallint unsigned not null,
    -> cse_id smallint unsigned not null,
    -> asn_notes varchar(255) null,
    -> primary key(asn_id),
    -> 
    -> index idx_per_cid (per_cid asc),
    -> index idx_per_aid (per_aid asc),
    -> index idx_cse_id (cse_id asc),
    -> 
    -> unique index ux_per_cid_per_aid_cse_id(per_cid asc, per_aid asc, cse_id asc),
    -> 
    -> constraint fk_assign_case
    -> foreign key (cse_id)
    -> references `case` (cse_id)
    -> on delete no action
    -> on update cascade,
    -> 
    -> constraint fk_assignment_case
    -> foreign key (per_cid)
    -> references client (per_id)
    -> on delete no action
    -> on update cascade,
    -> 
    -> constraint fk_assignment_attorney
    -> foreign key (per_aid)
    -> references attorney (per_id)
    -> on delete no action
    -> on update cascade
    -> );
ERROR 1824 (HY000): Failed to open the referenced table 'client'
mysql> show tables;
+------------------+
| Tables_in_ooa19a |
+------------------+
| attorney         |
| bar              |
| case             |
| court            |
| judge            |
| judge_hist       |
| person           |
| speciality       |
+------------------+
8 rows in set (0.00 sec)

mysql> drop table if exists client;
Query OK, 0 rows affected, 1 warning (0.03 sec)

mysql> 
mysql> create table if not exists client
    -> (
    -> per_id smallint unsigned not null,
    -> cli_notes varchar(255) null default null,
    -> primary key (per_id),
    -> 
    -> index idx_per_id(per_id asc),
    -> 
    -> constraint fk_client_person
    -> foreign key (per_id)
    -> references person (per_id)
    -> on delete no action 
    -> on update cascade
    -> );
Query OK, 0 rows affected (0.05 sec)

mysql> 
mysql> show warnings;
Empty set (0.00 sec)

mysql> create table if not exists assignment(asn_id smallint unsigned not null auto_increment,per_cid smallint unsigned not null,per_aid smallint unsigned not null,cse_id smallint unsigned not null,asn_notes varchar(255) null,primary key(asn_id),index idx_per_cid (per_cid asc),index idx_per_aid (per_aid asc),index idx_cse_id (cse_id asc),unique index ux_per_cid_per_aid_cse_id(per_cid asc, per_aid asc, cse_id asc),constraint fk_assign_caseforeign key (cse_id)references `case` (cse_id)on delete no actionon update cascade,constraint fk_assignment_caseforeign key (per_cid)references client (per_id)on delete no actionon update cascade,constraint fk_assignment_attorneyforeign key (per_aid)references attorney (per_id)on delete no actionon update cascade);
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'key (cse_id)references `case` (cse_id)on delete no actionon update cascade,const' at line 1
mysql> show tables;
+------------------+
| Tables_in_ooa19a |
+------------------+
| assignment       |
| attorney         |
| bar              |
| case             |
| client           |
| court            |
| judge            |
| judge_hist       |
| person           |
| speciality       |
+------------------+
10 rows in set (0.00 sec)

mysql> CREATE TABLE IF NOT EXISTS `ooa19a`.`court` (
    ->   `crt_id` tinyint UNSIGNED NOT NULL AUTO_INCREMENT,
    ->   `crt_name` VARCHAR(40) NOT NULL,
    ->   `crt_street` VARCHAR(30) NOT NULL,
    ->   `crt_city` VARCHAR(30) NOT NULL,
    ->   `crt_state` CHAR(2) NOT NULL,
    ->   `crt_zip` INT(9) ZEROFILL UNSIGNED NULL,
    ->  `crt_phone` bigint not null,
    ->   `crt_email` VARCHAR(100) NOT NULL,
    ->   `crt_url` varchar(100) NOT NULL,
    ->   `crt_notes` VARCHAR(255) NULL,
    ->   PRIMARY KEY (`crt_id`))
    -> ENGINE = InnoDB;
Query OK, 0 rows affected, 3 warnings (0.01 sec)

mysql> create table if not exists phone
    -> (
    -> phn_id smallint unsigned not null auto_increment,
    -> per_id smallint unsigned not null,
    -> phn_num bigint unsigned not null,
    -> phn_type enum('h','c','w','f') not null comment 'home, cell, work,fax',
    -> phn_notes varchar (255) null,
    -> primary key (phn_id),
    -> 
    -> index idx_per_id(per_id asc),
    -> 
    -> constraint fk_phone_person
    -> foreign key (per_id)
    -> references person(per_id)
    -> on delete no action
    -> on update cascade
    -> 
    -> );
Query OK, 0 rows affected (0.06 sec)

mysql> 
mysql> insert into person values
    -> (null, null, 'Steve','Rogers','437 Southern Drive', 'Rochester', 'NY', 32443022,'srogers@comcast.net', '1923-10-03', 'c', null),
    -> (null, null, 'Bruce','Wayne','1007 Mountain Drive', 'Gotham', 'NY', 003243440,'bwayne@knology.net', '1968-03-20', 'c', null),
    -> (null, null, 'Peter','Parker','20 Ingram Street', 'New York', 'NY', 102862341,'pparker@msn.net', '1923-10-03', 'c', null),
    -> (null, null, 'Jane','Thompson','13563 Ocean View Drive', 'Seattle', 'WA', 032084409,'jthompson@gmail.net', '1973-05-08', 'c', null),
    -> (null, null, 'Debra','Steele','543 Oak Ln', 'Milwaukee', 'WI', 2862374478,'dsteele@verizon.net', '1994-07-19', 'c', null)
    -> (null, null, 'Tony','Stark','332 Palm Avenue', 'Malibu', 'CA', 902638832,'tstark@yahoo.com', '1972-05-04', 'a', null),
    -> (null, null, 'Hank','Pymi','2355 Brown Street', 'Cleveland', 'OH', 022342390,'hpym@aol.net', '1980-08-28', 'a', null),
    -> (null, null, 'Bob','Best','4902 Avendale Ave.', 'Scottsdale', 'AZ', 87263912,'bbest@yahoo.com', '1992-02-10', 'a', null),
    -> (null, null, 'Sandra','Dole','8732 Lawrence Ave.', 'Atlanta', 'GA', 002345390,'sdole@gmail.net', '1990-01-26', 'a', null),
    -> (null, null, 'Ben','Avery','6432 Thunderbird Ln', 'Sioux Falls', 'SD', 5637932,'bavery@hotmail.net', '1983-12-24', 'a', null)
    -> (null, null, 'Arthur','Curry','3302 Euclid Avenue', 'Miami', 'FL', 000219932,'acurry@gmail.com', '1975-12-15', 'j', null),
    -> (null, null, 'Diana','Price','944 Green Street', 'Las vegas', 'NV', 33235223,'dprice@aol.net', '1980-08-22', 'j', null),
    -> (null, null, 'Adam','Jurris','98435 Valencia Dr.', 'Gulf Shore', 'AL', 870639912,'ajurris@yahoo.com', '1995-01-31', 'j', null),
    -> (null, null, 'Judy','Sleen','56343 Rover Ct.', 'Billings', 'MT', 67204823,'jsleen@symaptico.com', '1995-01-31', 'j', null),
    -> (null, null, 'Bill','Neiderheim','43567 Netherland Blvd', 'South Bend', 'IN', 320219932,'bneiderheim@comcast.net', '1982-03-24', 'j', null);
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '(null, null, 'Tony','Stark','332 Palm Avenue', 'Malibu', 'CA', 902638832,'tstark' at line 7
mysql> select * from person;
Empty set (0.00 sec)

mysql> insert into person values
    -> (null, null, 'Steve','Rogers','437 Southern Drive', 'Rochester', 'NY', 32443022,'srogers@comcast.net', '1923-10-03', 'c', null),
    -> (null, null, 'Bruce','Wayne','1007 Mountain Drive', 'Gotham', 'NY', 003243440,'bwayne@knology.net', '1968-03-20', 'c', null),
    -> (null, null, 'Peter','Parker','20 Ingram Street', 'New York', 'NY', 102862341,'pparker@msn.net', '1923-10-03', 'c', null),
    -> (null, null, 'Jane','Thompson','13563 Ocean View Drive', 'Seattle', 'WA', 032084409,'jthompson@gmail.net', '1973-05-08', 'c', null),
    -> (null, null, 'Debra','Steele','543 Oak Ln', 'Milwaukee', 'WI', 2862374478,'dsteele@verizon.net', '1994-07-19', 'c', null),
    -> (null, null, 'Tony','Stark','332 Palm Avenue', 'Malibu', 'CA', 902638832,'tstark@yahoo.com', '1972-05-04', 'a', null),
    -> (null, null, 'Hank','Pymi','2355 Brown Street', 'Cleveland', 'OH', 022342390,'hpym@aol.net', '1980-08-28', 'a', null),
    -> (null, null, 'Bob','Best','4902 Avendale Ave.', 'Scottsdale', 'AZ', 87263912,'bbest@yahoo.com', '1992-02-10', 'a', null),
    -> (null, null, 'Sandra','Dole','8732 Lawrence Ave.', 'Atlanta', 'GA', 002345390,'sdole@gmail.net', '1990-01-26', 'a', null),
    -> (null, null, 'Ben','Avery','6432 Thunderbird Ln', 'Sioux Falls', 'SD', 5637932,'bavery@hotmail.net', '1983-12-24', 'a', null),
    -> (null, null, 'Arthur','Curry','3302 Euclid Avenue', 'Miami', 'FL', 000219932,'acurry@gmail.com', '1975-12-15', 'j', null),
    -> (null, null, 'Diana','Price','944 Green Street', 'Las vegas', 'NV', 33235223,'dprice@aol.net', '1980-08-22', 'j', null),
    -> (null, null, 'Adam','Jurris','98435 Valencia Dr.', 'Gulf Shore', 'AL', 870639912,'ajurris@yahoo.com', '1995-01-31', 'j', null),
    -> (null, null, 'Judy','Sleen','56343 Rover Ct.', 'Billings', 'MT', 67204823,'jsleen@symaptico.com', '1995-01-31', 'j', null),
    -> (null, null, 'Bill','Neiderheim','43567 Netherland Blvd', 'South Bend', 'IN', 320219932,'bneiderheim@comcast.net', '1982-03-24', 'j', null);
Query OK, 15 rows affected (0.01 sec)
Records: 15  Duplicates: 0  Warnings: 0

mysql> select * from person;
+--------+---------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
| per_id | per_ssn | per_fname | per_lname  | per_street             | per_city    | per_state | per_zip    | per_email               | per_dob    | per_type | per_notes |
+--------+---------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
|      1 | NULL    | Steve     | Rogers     | 437 Southern Drive     | Rochester   | NY        |  032443022 | srogers@comcast.net     | 1923-10-03 | c        | NULL      |
|      2 | NULL    | Bruce     | Wayne      | 1007 Mountain Drive    | Gotham      | NY        |  003243440 | bwayne@knology.net      | 1968-03-20 | c        | NULL      |
|      3 | NULL    | Peter     | Parker     | 20 Ingram Street       | New York    | NY        |  102862341 | pparker@msn.net         | 1923-10-03 | c        | NULL      |
|      4 | NULL    | Jane      | Thompson   | 13563 Ocean View Drive | Seattle     | WA        |  032084409 | jthompson@gmail.net     | 1973-05-08 | c        | NULL      |
|      5 | NULL    | Debra     | Steele     | 543 Oak Ln             | Milwaukee   | WI        | 2862374478 | dsteele@verizon.net     | 1994-07-19 | c        | NULL      |
|      6 | NULL    | Tony      | Stark      | 332 Palm Avenue        | Malibu      | CA        |  902638832 | tstark@yahoo.com        | 1972-05-04 | a        | NULL      |
|      7 | NULL    | Hank      | Pymi       | 2355 Brown Street      | Cleveland   | OH        |  022342390 | hpym@aol.net            | 1980-08-28 | a        | NULL      |
|      8 | NULL    | Bob       | Best       | 4902 Avendale Ave.     | Scottsdale  | AZ        |  087263912 | bbest@yahoo.com         | 1992-02-10 | a        | NULL      |
|      9 | NULL    | Sandra    | Dole       | 8732 Lawrence Ave.     | Atlanta     | GA        |  002345390 | sdole@gmail.net         | 1990-01-26 | a        | NULL      |
|     10 | NULL    | Ben       | Avery      | 6432 Thunderbird Ln    | Sioux Falls | SD        |  005637932 | bavery@hotmail.net      | 1983-12-24 | a        | NULL      |
|     11 | NULL    | Arthur    | Curry      | 3302 Euclid Avenue     | Miami       | FL        |  000219932 | acurry@gmail.com        | 1975-12-15 | j        | NULL      |
|     12 | NULL    | Diana     | Price      | 944 Green Street       | Las vegas   | NV        |  033235223 | dprice@aol.net          | 1980-08-22 | j        | NULL      |
|     13 | NULL    | Adam      | Jurris     | 98435 Valencia Dr.     | Gulf Shore  | AL        |  870639912 | ajurris@yahoo.com       | 1995-01-31 | j        | NULL      |
|     14 | NULL    | Judy      | Sleen      | 56343 Rover Ct.        | Billings    | MT        |  067204823 | jsleen@symaptico.com    | 1995-01-31 | j        | NULL      |
|     15 | NULL    | Bill      | Neiderheim | 43567 Netherland Blvd  | South Bend  | IN        |  320219932 | bneiderheim@comcast.net | 1982-03-24 | j        | NULL      |
+--------+---------+-----------+------------+------------------------+-------------+-----------+------------+-------------------------+------------+----------+-----------+
15 rows in set (0.00 sec)

mysql> insert into client values
    -> (1, null),
    -> (2, null),
    -> (3, null),
    -> (4, null),
    -> (5, null);
Query OK, 5 rows affected (0.02 sec)
Records: 5  Duplicates: 0  Warnings: 0

mysql> ^C
mysql> start transaction;
Query OK, 0 rows affected (0.00 sec)

mysql> 
mysql> 
mysql> 
mysql> insert into phone values
    -> (null,1 ,6363782824,'c',null),
    -> (null,2 ,8283443432,'h',null),
    -> (null,3 ,2434578453,'w','has two office numbers'),
    -> (null,4 ,7812324282,'w',null),
    -> (null,5 ,5762378299,'f',null),
    -> (null,6 ,3363788209,'h','prefer cell phone calls'),
    -> (null,7 ,1094782823,'w','call during lunch'),
    -> (null,8 ,6328299339,'c','best number to reach'),
    -> (null,9 ,9993384822,'w','use for faxing legal docs'),
    -> (null,10,3874732999,'f',null),
    -> 
    -> (null,11,4999000222,'c',null),
    -> (null,12,8636378282,'w',null),
    -> (null,13,9636378282,'f','call in the morning');
Query OK, 13 rows affected (0.01 sec)
Records: 13  Duplicates: 0  Warnings: 0

mysql> 
mysql> commit;
Query OK, 0 rows affected (0.00 sec)

mysql> 
mysql> select * from phone;
+--------+--------+------------+----------+---------------------------+
| phn_id | per_id | phn_num    | phn_type | phn_notes                 |
+--------+--------+------------+----------+---------------------------+
|      1 |      1 | 6363782824 | c        | NULL                      |
|      2 |      2 | 8283443432 | h        | NULL                      |
|      3 |      3 | 2434578453 | w        | has two office numbers    |
|      4 |      4 | 7812324282 | w        | NULL                      |
|      5 |      5 | 5762378299 | f        | NULL                      |
|      6 |      6 | 3363788209 | h        | prefer cell phone calls   |
|      7 |      7 | 1094782823 | w        | call during lunch         |
|      8 |      8 | 6328299339 | c        | best number to reach      |
|      9 |      9 | 9993384822 | w        | use for faxing legal docs |
|     10 |     10 | 3874732999 | f        | NULL                      |
|     11 |     11 | 4999000222 | c        | NULL                      |
|     12 |     12 | 8636378282 | w        | NULL                      |
|     13 |     13 | 9636378282 | f        | call in the morning       |
+--------+--------+------------+----------+---------------------------+
13 rows in set (0.00 sec)

mysql> describe ooa19a;
ERROR 1146 (42S02): Table 'ooa19a.ooa19a' doesn't exist
mysql> select * from `information_schema`.`Table` where table_name like person;
ERROR 1109 (42S02): Unknown table 'TABLE' in information_schema
mysql> select * from `information_schema`.`Table` where table_name like ooa19a;
ERROR 1109 (42S02): Unknown table 'TABLE' in information_schema
mysql> select * from `information_schema`.`Table` ;
ERROR 1109 (42S02): Unknown table 'TABLE' in information_schema
mysql> select * from `information_schema`
    -> ;
ERROR 1146 (42S02): Table 'ooa19a.information_schema' doesn't exist
mysql> show create table `users`;
ERROR 1146 (42S02): Table 'ooa19a.users' doesn't exist
mysql> show create table ;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '' at line 1
mysql> show create table `root`;
ERROR 1146 (42S02): Table 'ooa19a.root' doesn't exist
mysql> describe root;
ERROR 1146 (42S02): Table 'ooa19a.root' doesn't exist
mysql> mysqlshow ooa19a;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'mysqlshow ooa19a' at line 1
mysql> exit
