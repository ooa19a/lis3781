select 'DROP and CREATE DATABASE INCASE IT EXISTS' as '';

drop database ooa19a;

create database ooa19a;

use ooa19a; 

select 'creating Person table (auto_increment): per_id 1-15' as'';
do sleep (7);



DROP TABLE IF EXISTS `ooa19a`.`person` ;

CREATE TABLE IF NOT EXISTS `ooa19a`.`person` (
  `per_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `per_ssn` BINARY(64) NULL,
  `per_ssnlt` binary(64) null, -- COMMENT 'only demo purposes - do not use the world SALT in the name!'	
  `per_fname` VARCHAR(15) NOT NULL,
  `per_lname` VARCHAR(30) NOT NULL,
  `per_street` VARCHAR(30) NOT NULL,
  `per_city` VARCHAR(30) NOT NULL,
  `per_state` CHAR(2) NOT NULL,
  `per_zip` INT(9) ZEROFILL UNSIGNED NULL,
  `per_email` VARCHAR(100) NOT NULL,
  `per_dob` DATE NOT NULL,
  `per_type` ENUM('a', 'c', 'j') NOT NULL,
  `per_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`per_id`),
  unique index ux_per_ssn (per_ssn asc)
)
ENGINE = InnoDB;

select 'Inserting into Person table' as'';
do sleep (5);

insert into person values
(null, null, null,  'Steve','Rogers','437 Southern Drive', 'Rochester', 'NY', 32443022,'srogers@comcast.net', '1923-10-03', 'c', null),
(null, null, null,'Bruce','Wayne','1007 Mountain Drive', 'Gotham', 'NY', 003243440,'bwayne@knology.net', '1968-03-20', 'c', null),
(null, null, null,'Peter','Parker','20 Ingram Street', 'New York', 'NY', 102862341,'pparker@msn.net', '1923-10-03', 'c', null),
(null, null, null,'Jane','Thompson','13563 Ocean View Drive', 'Seattle', 'WA', 032084409,'jthompson@gmail.net', '1973-05-08', 'c', null),
(null, null, null, 'Debra','Steele','543 Oak Ln', 'Milwaukee', 'WI', 2862374478,'dsteele@verizon.net', '1994-07-19', 'c', null),
(null, null, null,'Tony','Stark','332 Palm Avenue', 'Malibu', 'CA', 902638832,'tstark@yahoo.com', '1972-05-04', 'a', null),
(null, null, null,'Hank','Pymi','2355 Brown Street', 'Cleveland', 'OH', 022342390,'hpym@aol.net', '1980-08-28', 'a', null),
(null, null, null,'Bob','Best','4902 Avendale Ave.', 'Scottsdale', 'AZ', 87263912,'bbest@yahoo.com', '1992-02-10', 'a', null),
(null, null,null, 'Sandra','Dole','8732 Lawrence Ave.', 'Atlanta', 'GA', 002345390,'sdole@gmail.net', '1990-01-26', 'a', null),
(null, null, null,'Ben','Avery','6432 Thunderbird Ln', 'Sioux Falls', 'SD', 5637932,'bavery@hotmail.net', '1983-12-24', 'a', null),
(null, null, null,'Arthur','Curry','3302 Euclid Avenue', 'Miami', 'FL', 000219932,'acurry@gmail.com', '1975-12-15', 'j', null),
(null, null, null,'Diana','Price','944 Green Street', 'Las vegas', 'NV', 33235223,'dprice@aol.net', '1980-08-22', 'j', null),
(null, null,null, 'Adam','Jurris','98435 Valencia Dr.', 'Gulf Shore', 'AL', 870639912,'ajurris@yahoo.com', '1995-01-31', 'j', null),
(null, null,null, 'Judy','Sleen','56343 Rover Ct.', 'Billings', 'MT', 67204823,'jsleen@symaptico.com', '1995-01-31', 'j', null),
(null, null, null,'Bill','Neiderheim','43567 Netherland Blvd', 'South Bend', 'IN', 320219932,'bneiderheim@comcast.net', '1982-03-24', 'j', null);
select 'Display the Person table data'as'';
select * from person;

-- For easy readability

select 'Creating attorney table' as'';
do sleep (3);
drop table if exists attorney;

create table if not exists attorney
(
per_id smallint unsigned not null,
aty_start_date date not null,
aty_end_date date null default null,
aty_hourly_rate decimal (5,2) unsigned not null,
aty_years_in_practice tinyint not null,
aty_notes varchar (255) null default null,
primary key (per_id),

index idx_per_id(per_id ASC),

constraint fk_attorney_person
foreign key (per_id)
references person (per_id)
on delete no action
on update cascade

);
show warnings;

-- For easy readablity

select 'Creating client table' as'';
do sleep (3);

drop table if exists client;

create table if not exists client
(
per_id smallint unsigned not null,
cli_notes varchar(255) null default null,
primary key (per_id),

index idx_per_id(per_id asc),

constraint fk_client_person
foreign key (per_id)
references person (per_id)
on delete no action 
on update cascade
);

show warnings;

-- For easy readability

select 'Creating court table' as'';
do sleep (3);


CREATE TABLE IF NOT EXISTS `ooa19a`.`court` (
  `crt_id` tinyint UNSIGNED NOT NULL AUTO_INCREMENT,
  `crt_name` VARCHAR(40) NOT NULL,
  `crt_street` VARCHAR(30) NOT NULL,
  `crt_city` VARCHAR(30) NOT NULL,
  `crt_state` CHAR(2) NOT NULL,
  `crt_zip` INT(9) ZEROFILL UNSIGNED NULL,
 `crt_phone` bigint not null,
  `crt_email` VARCHAR(100) NOT NULL,
  `crt_url` varchar(100) NOT NULL,
  `crt_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`crt_id`))
ENGINE = InnoDB;

-- For easy readabilty

select 'Creating judge table' as'';
do sleep (3);

drop table if exists judge;

create table if not exists judge
(
per_id smallint unsigned not null,
crt_id tinyint unsigned null default null,
jud_salary decimal (8,2) not null,
jud_years_in_practice tinyint unsigned not null,
jud_notes varchar(255) null default null,
primary key (per_id),

index idx_per_id(per_id asc),
index idx_crt_id (crt_id asc),

constraint fk_judge_person
foreign key (per_id)
references person (per_id)
on delete no action 
on update cascade,

constraint fk_judge_court
foreign key (crt_id)
references court (crt_id)
on delete no action
on update cascade
);

-- COMMENT
select 'Creating Judge History table' as'';
do sleep (3);
drop table if exists judge_hist;

create table if not exists judge_hist
(
jhs_id smallint unsigned not null auto_increment,
per_id smallint unsigned not null,
jhs_crt_id tinyint null,
jhs_date timestamp not null default current_timestamp(),
jhs_type enum('i','u','d') not null default 'i',
jhs_salary decimal (8,2) not null,
jhs_notes varchar(255) null,
primary key(jhs_id),

index idx_per_id (per_id asc),

constraint fk_judge_hist_judge
foreign key (per_id)
references judge (per_id)
on delete no action 
on update cascade
);
-- COMMENT 
 -- use ooa19a;
create table if not exists `case`
(
cse_id smallint unsigned not null auto_increment,
per_id smallint unsigned not null,
cse_type varchar(45) not null,
cse_description text not null,
cse_start_date date not null,
cse_end_date date null,
cse_notes varchar(255) null,
primary key (cse_id),

index idx_per_id(per_id asc),

constraint fk_court_case_judge
foreign key (per_id)
references judge(per_id)
on delete no action
on update cascade
);

show tables;
-- COMMENT
select 'Creating Bar table' as'';
do sleep (3); 

create table if not exists bar
(
bar_id tinyint unsigned not null auto_increment,
per_id smallint unsigned not null,
bar_name varchar(45) not null,
bar_notes varchar (255) null, 
primary key (bar_id),

index idx_per_id (per_id asc),

constraint fk_bar_attorney
foreign key (per_id)
references attorney (per_id)
on delete no action
on update cascade
);



select 'Creating Speciality table' as'';
do sleep (3); 

create table if not exists speciality 
(
spc_id tinyint unsigned not null auto_increment,
per_id smallint unsigned not null,
spc_type varchar (45) not null, 
spc_notes varchar (255) null,
primary key (spc_id),

index idx_per_id(per_id asc),

constraint fk_specialty_attorney
foreign key (per_id)
references attorney (per_id)
on delete no action
on update cascade
);


select 'Creating assignment table' as'';
do sleep (3);

create table if not exists assignment
(
asn_id smallint unsigned not null auto_increment,
per_cid smallint unsigned not null,
per_aid smallint unsigned not null,
cse_id smallint unsigned not null,
asn_notes varchar(255) null,
primary key(asn_id),

index idx_per_cid (per_cid asc),
index idx_per_aid (per_aid asc),
index idx_cse_id (cse_id asc),

unique index ux_per_cid_per_aid_cse_id(per_cid asc, per_aid asc, cse_id asc),

constraint fk_assign_case
foreign key (cse_id)
references `case` (cse_id)
on delete no action
on update cascade,

constraint fk_assignment_case
foreign key (per_cid)
references client (per_id)
on delete no action
on update cascade,

constraint fk_assignment_attorney
foreign key (per_aid)
references attorney (per_id)
on delete no action
on update cascade
);


-- COMMENT
create table if not exists phone
(
phn_id smallint unsigned not null auto_increment,
per_id smallint unsigned not null,
phn_num bigint unsigned not null,
phn_type enum('h','c','w','f') not null comment 'home, cell, work,fax',
phn_notes varchar (255) null,
primary key (phn_id),

index idx_per_id(per_id asc),

constraint fk_phone_person
foreign key (per_id)
references person(per_id)
on delete no action
on update cascade

);

-- COMMENT


select 'Inserting into client table' as'';
do sleep (5);

insert into client values
(1, null),
(2, null),
(3, null),
(4, null),
(5, null);


select 'Inserting into Phone table' as'';
do sleep (5);

start transaction;

insert into phone values
(null,1 ,6363782824,'c',null),
(null,2 ,8283443432,'h',null),
(null,3 ,2434578453,'w','has two office numbers'),
(null,4 ,7812324282,'w',null),
(null,5 ,5762378299,'f',null),
(null,6 ,3363788209,'h','prefer cell phone calls'),
(null,7 ,1094782823,'w','call during lunch'),
(null,8 ,6328299339,'c','best number to reach'),
(null,9 ,9993384822,'w','use for faxing legal docs'),
(null,10,3874732999,'f',null),
(null,11,4999000222,'c',null),
(null,12,8636378282,'w',null),
(null,13,9636378282,'f','call in the morning');
select * from phone;
do sleep(2);
commit;

select 'Inserting into Court table' as'';
do sleep (5);

INSERT INTO `ooa19a`.`court` (`crt_id`, `crt_name`, `crt_street`, `crt_city`, `crt_state`, `crt_zip`, `crt_phone`, `crt_email`, `crt_url`, `crt_notes`) 
VALUES 
('1', 'Leon County', 'West Pole', 'Tallahassee', 'FL', '32301', '3423243', 'leon@county.com', 'www.leoncounty.com', 'court 1'),
('2', 'Supreme court', 'South Pole', 'Tallahassee', 'FL', '32304', '45556553', 'supreme@court.com', 'www.supremecourt.com', 'court 2'),
('3', 'High court', 'East pole', 'Tallahasse', 'FL', '32342', '34540124', 'high@court.com', 'www.highcourt.com', 'court 3'),
('4', 'Tribunal', 'North Pole', 'Tallahassee', 'FL', '32345', '67892422', 'Tribunal@court.com', 'www.tribunal.com', 'court 4'),
('5', 'Snowden', 'Orange', 'Orlando', 'FL', '30922', '24364642', 'snowden@court.com', 'www.snowden.com', 'court 5');
 show warnings;
select * from court;
do sleep(3);



select 'Inserting into Judge table' as'';
do sleep (5);

INSERT INTO `ooa19a`.`judge` (`per_id`, `crt_id`, `jud_salary`, `jud_years_in_practice`, `jud_notes`) VALUES 
('11', '1', '150000', '12', 'Judge 1'),
('12', '2', '140000', '10', 'Judge 2'),
('13', '3', '100000', '15', 'Judge 3'),
('14', '4', '95000', '12', 'Judge 4'),
('15', '5', '120000', '11', 'Judge 5'); 
show warnings;
select * from judge;
do sleep(3);

select 'Inserting into Judge_hist table' as'';
do sleep (5);
start transaction;

Insert into judge_hist
values
(null, 11,3,'2009-1-16','i',130000,null),
(null, 12,3,'2009-1-16','i',140000,null),
(null, 13,3,'2009-1-16','i',165000,null),
(null, 11,3,'2009-1-16','i',135000,'freshman justice'),
(null, 14,3,'2009-1-16','i',165000, 'became chief justice'),
(null, 15,3,'2009-4-18','i',170000, 'reassigned to court based upon local area population');
commit;
select * from judge_hist;
do sleep (3);


select 'Inserting into CASE table' as'';
do sleep (5);
start transaction;

insert into `case`
values
(null, 13, 'civil', 'client says that his logo is being used without his content to promote a rival business', '2010-09-28',null,'copyright infringement'),
(null, 14, 'criminal', 'client is charged of assulting her husband', '2010-12-28',null,'Assult'),
(null, 11, 'criminal', 'client broke an ankle while shopping at a local grocery store. No wet floor sign was posted although the floor had jsut being mopped', '2010-09-28','2010-12-24','slip and fall'),
(null, 12, 'civil', 'client is charged with stealing of several television from his former place of employment. Client has a solid alibi', '2011-09-28',null,'grand theft'),
(null, 13, 'civil', 'client says that his logo is being used without his content to promote a rival business', '2010-09-28',null,'copyright infringement'),
(null, 15, 'criminal', 'client is charged with the murder of his co-worker over a lovers fued. Client has no alibi', '2010-03-12',null,'murder'),
(null, 13, 'civil', 'client had chosen a degree other than IT and had to decleared bankruptcy.', '2010-09-28','2013-02-12','Bankruptcy');

commit;
select * from `case`;
do sleep(3);



select 'Inserting into Attorney table' as'';
do sleep (3);
 insert into attorney
 values 
(6,  '1995-02-03', '2019-01-12' , 40.00, 24 , 'attorney 1' ),
(7, '1980-06-05', '2018-09-12' , 45.00 ,  38 , 'attorney 2'),
(8, '1975-05-04', '2017-12-01' ,39.00, 42 , 'attorney 3'),
(9, '1970-09-08', '2012-12-03',  45.00 , 42 , 'attorney 4' ),
(10, '2000-08-12', NULL ,  50.00 ,  20, 'attorney 5'); 
select * from attorney;
do sleep(3);


select 'Inserting into Assignment table' as'';
do sleep (5);
start transaction;
insert into assignment 
values
(null, 1,6,4, null),
(null, 2,7,1, null),
(null, 3,9,2, null),
(null, 1,10,3, null),
(null, 4,7,1, null),
(null, 3,8,5, null),
(null, 5,7,7, null),
(null, 1,9,6, null),
(null, 2,9,6, null),
(null, 1,8,7, null);
commit;
select * from assignment; 
do sleep (3);

select 'Inserting into Bar table' as'';
do sleep (3); 

insert into bar
values
(null,6,'Florida Bar',null),
(null,7, 'Alabama Bar',null),
(null,8, 'Texas Bar',null),
(null,9, 'New Orleans Bar',null),
(null,10, 'New York Bar',null);

select * from bar;



select 'Inserting and Displaying Speciality table' as'';
do sleep (3);

insert into speciality 
values
(null,6,'criminal','expert in criminal cases'),
(null,7,'real estate','expert in real estate cases'),
(null,8,'insurance','expert in insurance cases'),
(null,9,'business','expert in business cases'),
(null,10,'bankruptcy','expert in bankruptcy cases');

select * from speciality;

-- use ooa19a;


select 'show populated per_ssn table before calling stored procedure to generate ssn for everybody on the table' as'';
 
do sleep(3);

select per_id, length(per_ssn), per_ssn from person order by per_id asc;
drop procedure if exists CreatePersonSSN;
    Delimiter $$
    create procedure CreatePersonSSN() 
    BEGIN
   -- DECLARE VARIABLE IN MSQL
     DECLARE x, y INT;
    SET x = 1;
   --  COMMENT 'DYNAMICALLY SET LOOP ENDING VALUE (TOTAL NUMBER OF PERSONS)'
    select count(*) into y from person;   
    
-- comment 'count the total number of records on the table and put them in Y'
    while x <= y DO
    
    -- comment 'give each person a unique randomized salt, and hashed salted randomized ssn' 
	-- comment 'this demo is only for showing how to include salted and hashed randomized values for testing purposes!'
    
    set @salt = random_bytes(64); set @ran_num = FLOOR(RAND() * (999999999 - 111111111 + 1)) + 111111111; 
    set @ssn = unhex(sha2(concat(@salt, @ran_num), 512 ));
    -- comment 'rand([N]): Returns random floating point value v in the range 0 < = v   1.0'
    -- comment 'randomize ssn between 111111111-999999999 (note: using value 000000000 for ex. 4)'
    
    update person 
	set 
    per_ssn = @ssn, 
	per_ssnlt = @salt 
    where per_id =x; 
	set x = x + 1; 
end while; 
end$$ 

Delimiter ;
call CreatePersonSSN();

 select 'show populated per_ssn table after calling stored procedure' as'';
 
do sleep(3);
select per_id, length(per_ssn), per_ssn from person order by per_id asc;

-- 

select 'Create a trigger that automatically adds a record to the judge history table for every record added to the judge table.' as '';
do sleep(5);

select 'show person data BEFORE adding person record' as '';
select per_id, per_fname, per_lname, per_ssn from person;
do sleep(5);

-- give person a unique randomized salt then hash and salt ssn

set @salt =random_bytes(64); -- salt includes unique random bytes for each user
set @num = 000000000; -- note: already provided random SSN from 111111111 -999999999, inclusive above
set @ssn = unhex(sha2(concat(@salt, @num),512)); -- salt and hash person's ssn 000000000

insert into person 
values
(null, @ssn, @salt, 'Bobby', 'Sue', '123 Main St.', 'Panama City Beach', 'FL', 4553422,'bsu@fl.gov', '1972-04-19', 'j', 'new district judge');


select 'show person data AFTER adding new record' as '';
select per_id, per_fname, per_lname, per_ssn from person;
do sleep(5);

-- 1)

select 'Attorney VIEW DISPLAY' as '';
do sleep(4);

drop view if exists v_attorney_info;
create view v_attorney_info AS

select 
concat(per_lname,", ", per_fname) as name,
concat(per_street, ", ", per_city, ", ", per_state, ", ", per_zip) as address,
timestampdiff(year, per_dob, now()) as age,
concat('$', format(aty_hourly_rate,2)) as hourly_rate,
bar_name, spc_type
from person
natural join attorney
natural join bar
natura join speciality
order by per_lname;

select 'display view v_attorney_info' as '';
select * from v_attorney_info;

drop view if exists v_attorney_info;
do sleep(3);

-- 2)

select 'Stored procedure to display month numbers, month names, and how many judges were born each month' as'';
do sleep(2);
drop procedure if exists sp_num_judges_born_by_month;

Delimiter //
create procedure sp_num_judges_born_by_month()

begin 
select month(per_dob) as month, monthname(per_dob) as Month_name ,count(*) as count
from person
natural join judge
group by Month_name
order by month;
end //
delimiter ;

select 'calling sp_num_judges_born_by_month()' as'';

call sp_num_judges_born_by_month();
do sleep(3);  

drop procedure if exists sp_num_judges_born_by_month;

-- 3)

select 'create sp_case_and_judges()'as '';
do sleep(3);
drop procedure if exists sp_case_and_judges;

delimiter //
create procedure sp_case_and_judges()
begin

select per_id, concat(per_fname, ' ', per_lname) as "Name (Firstname first)", cse_id, cse_type, cse_description, 
concat('(', substring(phn_num, 1,3),')', substring(phn_num, 4,3), '-' ,substring(phn_num, 7,4)) as "Judge Phone Number", 
phn_type, jud_years_in_practice, cse_start_date, cse_end_date 
from person
natural join judge
natural join `case`
natural join phone

where per_type ='j'
order by per_lname;

end //
delimiter ;

call sp_case_and_judges();

drop procedure if exists sp_case_and_judges; 
do sleep(3);

select 'this is number 4 question 'as'';
do sleep (3);
-- 4) select 'show person data BEFORE adding person record' as '';

select per_id, per_fname, per_lname from person;
do sleep(5);

set @salt = random_bytes(64); 
insert into person 
values
(null, unhex(sha2(000000000,512)), @salt, 'Lord', 'Lancelot', 'J.Cole street', 'Panama City Beach','FL', 32321, 'bsue@fl.gov','1962-05-16', 'j', 'another district judge');

select 'show person table AFTER adding the new record' as '';
select per_id, per_fname, per_lname, per_ssn from person;
do sleep (3);


select 'show judge/judge data before AFTER INSERT trigger fires (tr_judge_history_after_insert)' as '';
select * from judge;
select * from judge_hist;
do sleep(5);

drop trigger if exists tr_judge_history_after_insert;
delimiter //
create trigger tr_judge_history_after_insert
AFTER INSERT ON judge
FOR EACH ROW
BEGIN

insert into judge_hist
(per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
values
(NEW.per_id, NEW.crt_id, current_timestamp(),'i', NEW.jud_salary, concat('modifying user:', user(), 'note: ', NEW.jud_notes)
);
end //
delimiter ;
-- This comment is here for easy readability of the code

select 'fire trigger by inserting record into judge table' as '';
do sleep(5);
insert into judge 
(per_id, crt_id, jud_salary, jud_years_in_practice, jud_notes)
values
((select count(per_id) from person), 3, 143300, 25,'transferred from neighboring jurisdiction');

select 'show judge/judge_hist data AFTER INSERT trigger fires (tr_judge_history_after_insert)' as '';
do sleep (3);

select * from judge;
select * from judge_hist;
do sleep(7);

drop trigger if exists tr_judge_history_after_insert;


-- 5) Question 5

drop trigger if exists tr_judge_history_after_update;
delimiter //
create trigger tr_judge_history_after_insert
after update on judge
for each row
begin

insert into judge_hist
(per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
values
(NEW.per_id, NEW.crt_id, current_timestamp(), 'u', NEW.jud_salary, concat('modifying user: ', user(),'Notes: ', NEW.jud_notes)
);
end //
delimiter ;

select 'fire trigger by updating latest judge entry (salary and notes)' as '';
do sleep(5);

-- this trigger is pulled once there's an update in the judge table 

update judge
set jud_salary = 185000, jud_notes = 'senior justice memeber'
where per_id = (select count(per_id) from person);


select 'displaying the history table after the trigger' as '';
select * from judge;
select * from judge_hist;

drop trigger if exists tr_judge_history_after_update;

select 'Dropping database' as'';
drop database ooa19a;

