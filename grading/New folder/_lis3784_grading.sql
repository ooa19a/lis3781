-- save this file in db directory on the CCI server for MySQL
-- run command: \. db/_lis3784_grading.sql
-- tee command: all query result sets displayed on screen are appended into indicated file.
-- For example: every time you run above command a report is saved in file: db/_lis3784_grading_reports.sql

-- database name, table name, table type, engine, number of records per table
-- now() adds timestamp
-- output to file
tee db/_lis3784_grading_reports.sql

-- display user database metadata

SELECT now() as timestamp, table_schema, table_name, table_type, engine, table_rows
FROM information_schema.tables
where table_schema IN ('teb16b','bkb17c','pmb18c',
'zlb19b','afb19a', 'jlc19k','cnc16j','jd19z','jed18c','pad19b','kae18c', 'cdf17c','blf18','njf17', 'cjg18', 'cwg17b', 'sag18','zah19d', 'bmh18v', 'ami17','boj17','jmk18cy', 'hck18', 'dml16j', 'rrm17b',
'cim16c','sam18c', 'mm19ch','dn19', 'akp19c','krp18', 'jip18','mhp19b', 'jkp16b','smr17c', 'sls16d', 'crs17','mps17c', 'bgs19c', 'dds16b','ts19m', 'lmt16d', 'bmt15b','mct18b','at18d','emt16c','kdt17b',
'aew16g')
ORDER BY table_schema ASC, table_name ASC;
-- close filestream
notee

/*
-- individual student
SELECT now() as timestamp, table_schema, table_name, table_type, engine, table_rows
FROM information_schema.tables
where table_schema IN('teb16b')
ORDER BY table_name ASC;
*/

