-- save this file in db directory on the CCI server for MySQL
-- run command: \. db/_lis3781_grading.sql
-- tee command: all query result sets displayed on screen are appended into indicated file.
-- For example: every time you run above command a report is saved in file: db/_lis3781_grading_reports.sql

-- database name, table name, table type, engine, number of records per table
-- now() adds timestamp
-- output to file
tee db/_lis3781_grading_reports.sql

-- display user database metadata
SELECT now() as timestamp, table_schema, table_name, table_type, engine, table_rows
FROM information_schema.tables
where table_schema IN('jda17d','spa17b','tdb17c','bpb17b','teb16b','ahb16e','pmb18c','zlb19b','esb17b','jlc19k','nrc16e','cc19p','jd19z','jed18c','pad19b','jaf8025','blf18','cjg18','cwg17b','bmh18v','ami17','ndk16','hck18','rrm17b','jhm17b','mm19ch','dn19','trn17b','jip18','mhp19b','vlp16c','rnr06','mas18u','sls16d','ajs16u','bgs19c','ts19m','ss16bm','at15e','lmt16d','bmt15b','mct18b','at18d','emt16c','kdt17b','thw17','krw16h')
ORDER BY table_schema ASC, table_name ASC;

-- close filestream
notee

/*
-- individual student
SELECT now() as timestamp, table_schema, table_name, table_type, engine, table_rows
FROM information_schema.tables
where table_schema IN('dd17h')
ORDER BY table_name ASC;
*/

