# LIS3781

## Kayode Adegbite

### LIS3781 - Advanced Database Managements

Project 2 Requirements:

* Questions
* Download MongoDB
* MongoDB Create & Insert Database
* Add MongoDB Array using insert()
* Mongodb ObjectId()
* MongoDB Query Document using find()
* MongoDB cursor
* MongoDB Query Modifications using limit(), sort()
* MongoDB Count() & remove() function
* MongoDB Update() Document
* Bitbucket repo link to this assignment
* README.md file should include the following items:
* screenshot of P2 shell command
* Screenshot of P2 Show Report
* P2 JSON file