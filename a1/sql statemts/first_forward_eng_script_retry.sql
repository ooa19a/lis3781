-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema ooa19a
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `ooa19a` ;

-- -----------------------------------------------------
-- Schema ooa19a
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ooa19a` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `ooa19a` ;

-- -----------------------------------------------------
-- Table `ooa19a`.`job`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ooa19a`.`job` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `ooa19a`.`job` (
  `job_id` TINYINT NOT NULL AUTO_INCREMENT,
  `job_title` VARCHAR(45) NULL,
  `job_note` VARCHAR(255) NULL,
  PRIMARY KEY (`job_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `ooa19a`.`employee`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ooa19a`.`employee` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `ooa19a`.`employee` (
  `emp_id` SMALLINT NOT NULL AUTO_INCREMENT,
  `job_id` TINYINT NULL,
  `emp__ssn` INT(9) NULL,
  `emp_fname` VARCHAR(15) NULL,
  `emp_lname` VARCHAR(30) NULL,
  `emp_dob` DATE NULL,
  `emp_start_date` DATE NULL,
  `emp_end_date` DATE NULL,
  `emp_salary` DECIMAL(8,2) NULL,
  `emp_street` VARCHAR(30) NULL,
  `emp_city` VARCHAR(20) NULL,
  `emp_state` CHAR(2) NULL,
  `emp_zip` INT(9) ZEROFILL NULL,
  `emp_phone` BIGINT NULL,
  `emp_email` VARCHAR(100) NULL,
  `emp_notes` VARCHAR(100) NULL,
  PRIMARY KEY (`emp_id`),
  INDEX `fk_employee_job1_idx` (`job_id` ASC),
  CONSTRAINT `fk_employee_job1`
    FOREIGN KEY (`job_id`)
    REFERENCES `ooa19a`.`job` (`job_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `ooa19a`.`dependent`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ooa19a`.`dependent` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `ooa19a`.`dependent` (
  `dep_id` SMALLINT NOT NULL AUTO_INCREMENT,
  `emp_id` SMALLINT NULL,
  `date_added` DATE NULL,
  `dep__ssn` INT(9) NULL,
  `dep_fname` VARCHAR(15) NULL,
  `dep_dob` DATE NULL,
  `dep_relation` VARCHAR(20) NULL,
  `dep_street` VARCHAR(30) NULL,
  `dep_city` VARCHAR(20) NULL,
  `dep_state` CHAR(2) NULL,
  `dep_zip` INT(9) ZEROFILL NULL,
  `dep_phone` BIGINT NULL,
  `dep_email` VARCHAR(100) NULL,
  `dep_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`dep_id`),
  INDEX `fk_dependent_employee_idx` (`emp_id` ASC),
  CONSTRAINT `fk_dependent_employee`
    FOREIGN KEY (`emp_id`)
    REFERENCES `ooa19a`.`employee` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `ooa19a`.`benefit`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ooa19a`.`benefit` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `ooa19a`.`benefit` (
  `ben_id` TINYINT NOT NULL AUTO_INCREMENT,
  `ben_name` VARCHAR(45) NULL,
  `ben_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`ben_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `ooa19a`.`plan`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ooa19a`.`plan` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `ooa19a`.`plan` (
  `pln_id` SMALLINT NOT NULL AUTO_INCREMENT,
  `emp_id` SMALLINT NULL,
  `ben_id` TINYINT NULL,
  `pln_type` ENUM("insurance", "transport", "stipend") NULL,
  PRIMARY KEY (`pln_id`),
  INDEX `fk_plan_employee1_idx` (`emp_id` ASC),
  INDEX `fk_plan_benefit1_idx` (`ben_id` ASC),
  CONSTRAINT `fk_plan_employee1`
    FOREIGN KEY (`emp_id`)
    REFERENCES `ooa19a`.`employee` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_plan_benefit1`
    FOREIGN KEY (`ben_id`)
    REFERENCES `ooa19a`.`benefit` (`ben_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `ooa19a`.`emp_hist`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ooa19a`.`emp_hist` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `ooa19a`.`emp_hist` (
  `eht_id` MEDIUMINT NOT NULL AUTO_INCREMENT,
  `emp_id` SMALLINT NULL,
  `eht_date` DATETIME NULL,
  `eht_type` ENUM("i", "u", "d") NULL,
  `eht_job_id` TINYINT NULL,
  `eht_emp_salary` DECIMAL(8,2) NULL,
  `eht_user_changed` VARCHAR(30) NULL,
  `eht_reason` VARCHAR(45) NULL,
  `eht_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`eht_id`),
  INDEX `fk_emp_hist_employee1_idx` (`emp_id` ASC),
  CONSTRAINT `fk_emp_hist_employee1`
    FOREIGN KEY (`emp_id`)
    REFERENCES `ooa19a`.`employee` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `ooa19a`.`employee`
-- -----------------------------------------------------
START TRANSACTION;
USE `ooa19a`;
INSERT INTO `ooa19a`.`employee` (`emp_id`, `job_id`, `emp__ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (001, 001, 908367123, 'Jon', 'Snow', '05/05/1993', '18/01/2011', NULL, 5000, 'Brookside', 'West Orange', 'NJ', 07052, 2347561987, 'jonsnow@got.com', 'Employee of the month');

COMMIT;

