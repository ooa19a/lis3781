# LIS3781

## Kayode Adegbite

### Teaching Assistant

Assignment 1

> These are git commands

1. git init - The git init command creates a new Git repository. It can be used to convert an existing, unversioned project to a Git repository or initialize a new, empty repository. Most other Git commands are not available outside of an initialized repository, so this is usually the first command you'll run in a new project.
2. git status - git status. The git status command displays the state of the working directory and the staging area. It lets you see which changes have been staged, which haven't, and which files aren't being tracked by Git. Status output does not show you any information regarding the committed project history.

3. git add - The git add command adds a change in the working directory to the staging area. It tells Git that you want to include updates to a particular file in the next commit. However, git add doesn't really affect the repository in any significant way—changes are not actually recorded until you run git commit.

4. git commit -  The "commit" command is used to save your changes to the local repository. Note that you have to explicitly tell Git which changes you want to include in a commit before running the "git commit" command. This means that a file won't be automatically included in the next commit just because it was changed.

5. git push - The git push command is used to upload local repository content to a remote repository. Pushing is how you transfer commits from your local repository to a remote repo. It's the counterpart to git fetch , but whereas fetching imports commits to local branches, pushing exports commits to remote branches.

6. git pull - git pull is a Git command used to update the local version of a repository from a remote. It is one of the four commands that prompts network interaction by Git. By default, git pull does two things. Updates the current local working branch (currently checked out branch)

Description: The expected norm for professional web developers, programmers, software engineers, and team design/development in general, is to be able to work within the confines of some type of version control system to store and maintain files. Understanding this process is particularly important during the various phases of the software (web) development life cycle (SDLC). For the remainder of this course, we will use the Git distributed version control system to manage assignment and project files. In this course, individually and as a team member, you will learn how to use the Bitbucket service for managing Git repositories—as well as the git command-line tool.

![Tomcat screenshot](/a1/img/localhost9999.png)

[Repo link to lis3781](https://bitbucket.org/ooa19a/lis3781/src/master/)

![ERD Image](/a1/img/eerd.PNG)

1. !

2. ![Number 2](/a1/img/number2.png)

3. ![Number 3](/a1/img/number3.png)

4. ![Number 4](/a1/img/number4_beforeChange.png)

5. ![Number 4](/a1/img/number4_afterChange.png)

6. ![Number 5](/a1/img/number5_beforechange.png)

7. ![Number 5](/a1/img/number5_afterchange.png)
