use master;
GO

if exists(select name from master.dbo.sysdatabases where name = N'ooa19a')
drop database ooa19a;
go

if not exists(select name from master.dbo.sysdatabases where name = N'ooa19a')
create database ooa19a;
go

use ooa19a;
go


-- -----------------------
-- table person
-- ----------------------------

if object_id('dbo.person', N'U') is not null
drop table dbo.person;
go

create table dbo.person
(
 per_id SMALLINT NOT NULL identity (1,1),
  per_ssn BINARY(64) NULL,
  per_salt binary (64) NULL,
  per_fname VARCHAR(15) NOT NULL,
  per_lname VARCHAR(30) NOT NULL,
  per_gender char (1) check (per_gender in ('m','f')),
  per_dob DATE NOT NULL,
  per_street VARCHAR(30) NOT NULL,
  per_city VARCHAR(30) NOT NULL,
  per_state CHAR(2) NOT NULL default 'FL',
  per_zip INT NOT NULL check (per_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
  per_email VARCHAR(100) NOT NULL,
  per_type char (1) not null check(per_type IN('c', 's')),
  per_notes VARCHAR(45) NULL,
  PRIMARY KEY (per_id),

  constraint ux_per_ssn unique nonclustered (per_ssn asc)
);

-- -----------------------
-- table phone
-- --------------------

if object_id (N'dbo.phone',N'U') is not null
drop table dbo.phone;
go

create table dbo.phone
(
phn_id smallint not null identity(1,1),
per_id smallint not null,
phn_num bigint not null check (phn_num like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
phn_type char(1) not null check (phn_type in ('h','c','w','f')),
phn_notes varchar (255) null,
primary key (phn_id),

constraint fk_phone_person
foreign key (per_id)
references dbo.person (per_id)
on delete cascade
on update cascade

);

-- -------------------------
-- table customer
-- -----------------------

if object_id(N'dbo.customer', N'U') is not null
drop table dbo.customer;
go 

create table dbo.customer
(
per_id smallint not null,
cus_balance decimal (7,2) not null check (cus_balance > =0),
cus_total_sales decimal (7,2) not null check (cus_total_sales >= 0),
cus_notes varchar (45) null,
primary key (per_id),

constraint fk_customer_person 
foreign key (per_id)
references dbo.person (per_id)
on delete cascade
on update cascade
);

-- -------------------------
-- table salesrep
-- -----------------------

if object_id(N'dbo.slsrep', N'U') is not null
drop table dbo.slsrep;
go 

create table dbo.slsrep
(
per_id smallint not null,
srp_yr_sales_goal decimal (8,2) not null check (srp_yr_sales_goal >= 0),
srp_ytd_sales decimal (8,2) not null check (srp_ytd_comm > =0),
srp_ytd_comm decimal (8,2) not null check (srp_ytd_comm >= 0),
srp_notes varchar (45) null,
primary key (per_id),

constraint fk_slsrep_person 
foreign key (per_id)
references dbo.person (per_id)
on delete cascade
on update cascade
);

-- ------------------------------
-- table sales rep history
-- -------------------------------

if object_id(N'dbo.srp_hist', N'U') is not null
drop table dbo.srp_hist;
go

create table dbo.srp_hist
(
sht_id smallint not null identity(1,1),
per_id smallint not null,
sht_type char(1) not null check (sht_type in ('i','u','d')),
sht_modified datetime not null,
sht_modifier varchar(45) not null default system_user,
sht_date date not null default getDate(),
sht_yr_sales_goal decimal (8,2) not null check (sht_yr_sales_goal >= 0),
sht_ytd_sales decimal (8,2) not null check (sht_ytd_comm > =0),
sht_ytd_comm decimal (8,2) not null check (sht_ytd_comm >= 0),
sht_notes varchar (45) null,

primary key (sht_id),

constraint fk_srp_hist_slsrep
foreign key (per_id)
references dbo.slsrep(per_id)
on delete cascade
on update cascade

);

-- ---------------------------
-- contact table
-- ------------------------

if object_id (N'dbo.contact', N'U') is not null
drop table dbo.contact;
go

create table dbo.contact
(
cnt_id int not null identity (1,1),
per_cid smallint not null,
per_sid smallint not null, 
cnt_date datetime not null, 
cnt_notes varchar(255)

primary key (cnt_id)
constraint fk_contact_customer
foreign key (per_cid)
references dbo.customer(per_id)
on delete  cascade
on update cascade,


constraint fk_contact_slsrep
foreign key (per_sid)
references dbo.slsrep (per_id)
on delete no action
on update no action

);

-- -------------------
-- order table
-- -----------------------

if object_id (N'dbo.[order]', N'U') is not null 
drop table dbo.[order];
go

create table dbo.[order]
(
ord_id int not null identity (1,1),
cnt_id int not null, 
ord_placed_date datetime not null,
ord_filled_date datetime null, 
ord_notes varchar(255) null

primary key (ord_id),

constraint fk_order_contact
foreign key (cnt_id)
references dbo.contact(cnt_id)
on delete cascade
on update cascade
); 

-- ---------------
-- table store
-- -----------------

if object_id('dbo.store', N'U') is not null
drop table dbo.store;
go

create table dbo.store
(
 str_id SMALLINT NOT NULL identity (1,1),
  str_name VARCHAR(30) NOT NULL,
  str_street VARCHAR(30) NOT NULL,
  str_city VARCHAR(30) NOT NULL,
  str_state CHAR(2) NOT NULL default 'FL',
  str_zip INT NOT NULL check (str_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
  str_num bigint not null check (str_num like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
  str_email VARCHAR(100) NOT NULL,
   str_url VARCHAR(100) NULL,
  str_notes VARCHAR(255) NULL,
  PRIMARY KEY (str_id),

);

-- table invoice

if object_id('dbo.invoice', N'U') is not null
drop table dbo.invoice;
go

create table dbo.invoice
(
inv_id int not null identity(1,1),
ord_id int not null,
str_id smallint not null,
inv_date datetime not null,
inv_total decimal (8,2) not null check (inv_total > = 0),
inv_paid bit not null,
inv_notes varchar(255) null

primary key (inv_id),

-- create 1:1 relationship with order by making ord_id unique

constraint ux_ord_id unique nonclustered (ord_id ASC),

constraint fk_invoice_order
foreign key (ord_id)
references dbo.[order](ord_id)
on delete cascade
on update cascade,

constraint fk_invoice_store
foreign key (str_id)
references dbo.store (str_id)
on delete cascade
on update cascade
);

-- --------------------------------------
-- table payment 
-- -------------------------------------------

if object_id ('dbo.payment', N'U') is not null
drop table dbo.payment;
go

create table dbo.payment
(
pay_id int not null identity(1,1),
inv_id int not null,
pay_date datetime not null,
pay_amt decimal (7,2) not null check (pay_amt >=0),
pay_notes varchar (255) null, 
primary key (pay_id),

constraint fk_payment_invoice
foreign key (inv_id)
references dbo.invoice (inv_id)
on delete cascade
on update cascade

);

-- -------------------------
-- table vendor 
-- ---------------------------

if object_id(N'dbo.vendor', N'U') is not null
drop table dbo.vendor;
go

create table dbo.vendor
(
ven_id smallint not null identity (1,1),
ven_name varchar(45) not null,
ven_street varchar (30) not null,
ven_city varchar (30) not null,
ven_state char(2) not null,
ven_zip int not null check (ven_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
ven_phone bigint not null check (ven_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
ven_email varchar(100) null, 
ven_url varchar(100) null, 
ven_notes varchar (255) null,
primary key (ven_id)

);

-- ---------------------
-- table product
-- ---------------------

if object_id (N'dbo.product', N'U') is not null
drop table dbo.product;
go 

create table dbo.product 
(
pro_id smallint not null identity (1,1),
ven_id smallint not null, 
pro_name varchar(30) not null, 
pro_descript varchar(45) null,
pro_weight float not null check (pro_weight >= 0),
pro_qoh smallint not null check (pro_qoh >= 0),
pro_cost decimal(7,2) not null check (pro_cost >= 0),
pro_price decimal (7,2) not null check (pro_price >= 0),
pro_discount decimal(3,0) null,
primary key (pro_id),

constraint fk_product_vendor
foreign key (ven_id)
references dbo.vendor (ven_id)
on delete cascade
on update cascade

); 
-- -------------------------
-- table product_hist
-- -------------------------------
if object_id (N'dbo.product_hist', N'U') is not null
drop table dbo.product_hist;
go

create table dbo.product_hist
(

pht_id int not null identity (1,1),
pro_id smallint not null, 
pht_date datetime not null,
pht_cost decimal(7,2) not null check (pht_cost >= 0),
pht_price decimal (7,2) not null check (pht_price >= 0),
pht_discount decimal (3,0) null,
pht_notes varchar(255) null, 
primary key (pht_id),

constraint fk_product_hist_product
foreign key (pro_id)
references dbo.product (pro_id)
on delete cascade
on update cascade

);

-- -------------------------
-- table order_line
-- -----------------------------

if object_id(N'dbo.orderline', N'U') is not null
drop table dbo.order_line;
go

create table dbo.order_line
(
oin_id int not null identity (1,1),
ord_id int not null,
pro_id smallint not null,
oin_qty smallint not null check (oin_qty >= 0),
oin_price decimal (7,2) not null check (oin_price >= 0),
oin_notes varchar(255) null,
primary key (oin_id),

-- must use delimiters[] on reserved words (e.g., order)

constraint fk_order_line_order
foreign key (ord_id)
references dbo.[order](ord_id)
on delete cascade
on update cascade,

constraint fk_order_line_product
foreign key (pro_id)
references dbo.product(pro_id)
on delete cascade
on update cascade


);

-- --------------------------
-- Insert into Person Table 
-- --------------------------------

INSERT INTO dbo.person
(per_ssn, per_salt, per_fname, per_lname, per_gender, per_dob, per_street, per_city, per_state, per_zip, per_email, per_type, per_notes)
VALUES
(1,NULL,'Steve','Rogers','m','1923-10-03','437 Southern Drive','Rochester','NY',324402222,'srogers@comcast.net','s',NULL),
(2,NULL,'Bruce','Wayne','m','1968-03-20','1007 Mountain Drive','Gotham','NY',983208440,'bwayne@knology.net','s',NULL),
(3,NULL,'Peter','Parker','m','1988-09-12','20 Ingram Street','New York','NY',102862341,'pparker@msn.com','s',NULL),
(4,NULL,'Jane','Thompson','f','1978-05-08','13563 Ocean View Drive','Seattle','WA',132084409,'jthompson@gmail.com','s',NULL),
(5,NULL,'Debra','Steele','f','1994-07-19','543 Oak Ln','Milwaukee','WI',286234178,'dsteele@verizon.net','s',NULL),
(6,NULL,'Tony','Smith','m','1972-05-04','332 Palm Avenue','Malibu','CA',902638332,'tstark@yahoo.com','c',NULL),
(7,NULL,'Hank','Pymil','m','1980-08-28','2355 Brown Street','Cleveland','OH',822348890,'hpym@aol.com','c',NULL),
(8,NULL,'Bob','Best','m','1992-02-10','4902 Avendale Avenue','Scottsdale','AZ',872638332,'bbest@yahoo.com','c',NULL),
(9,NULL,'Sandra','Smith','f','1990-01-26','87912 Lawrence Ave','Atlanta','GA',672348890,'sdole@gmail.com','c',NULL),
(10,NULL,'Ben','Avery','m','1983-12-24','6432 Thunderbird Ln','Sioux Falls','SD',562638332,'bavery@hotmail.com','c',NULL),
(11,NULL,'Arthur','Curry','m','1975-12-15','3304 Euclid Avenue','Miami','FL',342219932,'acurry@gmail.com','c',NULL),
(12,NULL,'Diana','Price','f','1980-08-22','944 Green Street','Las Vegas','NV',332048823,'dprice@sympatico.com','c',NULL),
(13,NULL,'Adam','Smith','m','1995-01-31','98435 Valencia Dr.','Gulf Shores','AL',870219932,'ajurris@gmx.com','c',NULL),
(14,NULL,'Judy','Sleen','f','1970-03-22','56343 Rover Court','Billings','MT',672048823,'jsleen@sympatico.com','c',NULL),
(15,NULL,'Bill','Neiderheim','m','1982-06-13','43567 Netherlan Blvd','South Bend','IN',320219932,'bneiderheim@comcast.net','c',NULL);
GO


-- -------------------------------------------------------
-- Creating Store Procedure
-- -------------------------------


--    create proc dbo.CreatePersonSSN() 
--AS
--    BEGIN

--   -- DECLARE VARIABLE IN MSQL
--     DECLARE @salt binary(64);
--     DECLARE @ran_num int;
--     DECLARE @ssn binary(64);	
--     DECLARE @x INT, @y INT;
--    SET @x = 1;
--   --  COMMENT 'DYNAMICALLY SET LOOP ENDING VALUE (TOTAL NUMBER OF PERSONS)'
    
--	SET @y = (select count(*) from dbo.person);
    
---- comment 'count the total number of records on the table and put them in Y'
    
--while (@x <= @y) 
--	BEGIN
    
--	    -- comment 'give each person a unique randomized salt, and hashed salted randomized ssn' 
--		-- comment 'this demo is only for showing how to include salted and hashed randomized values for testing purposes!'
    
--    set @salt = crypt_gen_random(64); 
--    set @ran_num = FLOOR(RAND() * (999999999 - 111111111 + 1)) + 111111111; 
--    set @ssn = HASHBYTES('sha2_512', concat(@salt, @ran_num));
   
-- 	-- comment 'rand([N]): Returns random floating point value v in the range 0 < = v   1.0'
--    	-- comment 'randomize ssn between 111111111-999999999 (note: using value 000000000 for ex. 4)'
    
--    update dbo.person 
--	set 
--    	per_ssn = @ssn, 
--	per_ssnlt = @salt 
--    where per_id = @x; 
--	set @x = @x + 1; 
--end;
 
--end; 

--exec dbo.CreatePersonSSN


-- ---------------------------
-- Inserting into Sales Rep
-- -------------------------

insert into dbo.slsrep
(per_id, srp_yr_sales_goal, srp_ytd_sales, srp_ytd_comm, srp_notes)
values
(1, 100000, 60000, 1800, null),
(2, 800000, 350000, 1800, null),
(3, 1500000, 870000, 1800, null),
(4, 1250000, 780000, 1800, null),
(5, 980000, 34000, 1800, null),
(6, 130000, 430000, 1800, null);


-- ---------------------------
-- Inserting into Customer
-- -------------------------
insert into dbo.customer
values
(6, 120, 14789, null),
(7,98.46, 234.92, null),
(8, 0, 4578, 'Customer always pay on time.'),
(9,981.73,1672.38,'High balance'),
(10, 541.23, 782.57, null),
(11, 251.02,13781.96,'Good customer'),
(12, 582.67, 963.12,'Previously paid in full'),
(13, 121.67, 1057.45,'Resent customer'),
(14, 765.43,6789.42,'Buys in bulk quantities'),
(15, 304.39, 456.81, 'Has not purchased recently');



-- ------------------------
-- Insert into Contact
-- -----------------------

insert into dbo.customer
values
(1,6,'1999-01-01', null),
(2,11,'1989-09-01', null),
(3,12,'2002-08-01' ,null),
(2,13,'2003-07-01' ,null),
(5,7,'1997-05-01' ,null),
(4,8,'1995-03-01' ,null),
(5,9,'2005-01-01' ,null),
(1,7,'1999-01-01' ,null),
(5,6,'1999-01-01' ,null),
(4,7,'1999-01-01' ,null),
(5,15,'1999-01-01', null);


-- -----------------------
-- insert into Order
-- ------------------------
insert into dbo.[order]
values
(1, '2010-11-23', '2010-12-24', null),
(2, '2011-8-23', '2011-8-29', null),
(3, '2012-3-23', '2012-4-5', null),
(4, '2013-4-23', '2013-5-2', null),
(5, '2000-5-23', '2000-5-30', null),
(6, '2009-07-23', '2009-07-24', null),
(7, '2008-03-23', '2008-4-29', null),
(8, '2007-01-23', '2007-01-24', null),
(9, '2010-12-23', '2010-12-24', null),
(10, '2010-9-23', '2010-9-24', null);



-- ---------------------
-- Insert into store table
-- ----------------------

insert into dbo.store
values
('Leon Store', 'West Pole', 'Tallahassee', 'FL', '32301', '3423243', 'leon@store.com', 'www.leoncounty.com', 'store 1'),
('Supreme Market', 'South Pole', 'Tallahassee', 'FL', '32304', '45556553', 'supreme@market.com', 'www.suprememarket.com', 'store 2'),
('High Note', 'East pole', 'Tallahasse', 'FL', '32342', '34540124', 'high@note.com', 'www.highnote.com', 'store 3'),
('Tribunal', 'North Pole', 'Tallahassee', 'FL', '32345', '67892422', 'Tribunal@ma.com', 'www.tribunal.com', null),
('Snowden', 'Orange', 'Orlando', 'FL', '30922', '24364642', 'snowden@snow.com', 'www.snowden.com', 'store 5');


-- -----------------------------
-- Insert into invoice table
-- ---------------------------------
insert into dbo.invoice
values
(5,1,'2001-05-03', 58.52, 0, null),
(5,1,'2006-05-03', 108.32, 1, null),
(5,1,'2001-05-03', 1158.32, 0, null),
(5,1,'2001-05-03', 864.32, 1, null),
(5,1,'2001-05-03', 595.43, 0, null),
(5,1,'2001-05-03', 584.72, 1, null),
(5,1,'2001-05-03', 954.02, 0, null),
(5,1,'2001-05-03', 58.32, 1, null),
(10,4,'2012-03-13', 27.45, 0, null);

-- -------------------------------
-- insert into vendor table
-- --------------------------------
insert into dbo.vendor
values
('CISCO', 'West Pole', 'Tallahassee', 'FL', '32301001', '98243243', 'cisco@network.com', 'www.CISCO.com', 'store 1'),
('HP', 'South Pole', 'Tallahassee', 'FL', '32304002', '93256553', 'hp@computer.com', 'www.hp.com', 'Good turn around'),
('Oracle', 'East pole', 'Tallahasse', 'FL', '32342003', '94840124', 'oracle@lol.com', 'www.Oracle.com', 'store 3'),
('EA', 'North Pole', 'Tallahassee', 'FL', '32345004', '948332422', 'EA@sport.com', 'www.ea.com', 'Competing well with Firestone'),
('SEGA', 'Orange', 'Orlando', 'FL', '30922005', '98364642', 'sega@nitendo.com', 'www.sega.com', 'vendor 5');


-- ----------------------
-- Inserting into Product
-- ------------------------

insert into dbo.product 
values
(1, 'hammer', '', 2.5,45,4.99,7.99,30,'No discount'),
(2, 'screwdriver','',1.8,120,1.99,3.49, null, null),
(4, 'pall', '16 Gallon', 2.8,48,3.89,7.99,40, null),
(3, 'frying pan', '', 3.5,178,8.45,13.99,50,'currently half the price'),
(5, 'cooking oil', 'peanut oil', 15,19,19.99,28.99, null, 'gallons');


-- ----------------------------
-- Insert into order_line table
-- -------------------------------  
insert into dbo.order_line
values
(1,2,10,8.0,null),
(2,3,7,9.88, null),
(3,4,3,6.99,null),
(5,1,2,12.76, null),
(4,5,13,58.99,null);


-- -----------------------------
-- insert into payment
-- ----------------------------------
insert into dbo.payment
values
(1,'2013-07-21', 4.99, null),
(5,'2011-05-11', 5.99, null),
(4,'2009-07-15', 4.59, null),
(3,'2008-03-19', 6.89, null),
(2,'2010-07-13', 3.93, null),
(6,'2018-01-22', 4.29, null),
(7,'2008-06-23', 7.56, null),
(8,'2009-03-30', 6.89, null),
(10,'2008-05-17', 5.99, null),
(9,'2007-05-22', 9.33, null);

select * from dbo.payment;

-- ----------------------------------
-- insert into product history table
-- ------------------------------------
insert into dbo.product_hist
values
(1, '2005-01-02 11:53:34',4.99,7.99,30,'Discounted only when purchased with Screwdriver set'),
(2, '2005-02-02 10:13:44',1.99,3.49, null, null),
(3, '2005-03-02 12:50:50',3.99,7.49, 40, null),
(4, '2005-01-05 13:22:04',19.99,28.49, null, 'gallons'),
(5, '2005-04-06 04:43:24',8.45,13.99,50, null);

select * from dbo.product_hist;

-- ---------------------------------------
--  insert into sales rep history table
-- --------------------------------------

insert into dbo.srp_hist
values
(1, 'i', getdate(), system_user, getdate(), 1000000, 110000, 11000, null),
(2, 'i', getdate(), system_user, getdate(), 1500000, 175000, 17500, null),
(3, 'u', getdate(), system_user, getdate(), 2000000, 185000, 18500, null),
(4, 'u', getdate(), original_login(), getdate(), 210000, 220000, 2200, null),
(5, 'i', getdate(), original_login(), getdate(), 225000, 230000, 2300, null);

select * from dbo.srp_hist;


select year(sht_date) from dbo.srp_hist;

-- ----------------
-- Insert into phone table 
-- ------------------

INSERT INTO dbo.phone
(per_id, phn_num, phn_type, phn_notes)
VALUES
(2,'1111111111','h', NULL),
(4,'2222222222','c', NULL),
(3,'3333333333','w', NULL),
(1,'4444444444','f', NULL),
(5,'5555555555','h', NULL);

select * from dbo.phone;

-- --------------------
-- 1) Create a view that displays the sumof all paidinvoice totalsfor each customer, sort by the largest invoice total sum appearing first.
-- ----------------------------
use ooa19a;
go

select * from dbo.invoice;

-- snapshot of all paid invoices
select inv_id, inv_total as paid_invoice_total
from dbo.invoice
where inv_paid != 0;

-- snapshot of sum of all paid invoices:
select sum(inv_total) as sum_paid_invoice total
from dbo.invoice 
where inv_paid !=0;

print '1) Create a viewthat displays the sumof all paidinvoice totalsfor each customer, sort by the largest invoice total sum appearing first.

';

--drop view if exists
--1st arg is object name, 2nd arg is type (V=view)
IF OBJECT_ID (N'dbo.v_paid_invoice_total', N'V') IS NOT NULL
DROP VIEW dbo.v_paid_invoice_total;
GO

--In MS SQL SERVER: do *NOT* use ORDER BY clause in * VIEWS* (non-guaranteed behavior)
create view dbo.v_paid_invoice_total as 
    select p.per_id, per_fname, per_lname, sum(inv_total) as sum_total, FORMAT(sum(inv_total),'C', 'em-us') as v_paid_invoice_total
    from dbo.person p
        join dbo.customer c on p.per_id=c.per_id=c.per_id
        join dbo.contact ct on c.per_id=ct.per_cid
        join dbo.[order] o on o.ord_id=i.ord_id
        join dbo.invoice i on o.ord_id=i.ord_id
    where inv_paid !=0

-- must be contained in group by, if not used in aggreagate function
    group by p.per_id, per_fname, per_lname
go

-- display view results (order by should be used outside of view)
select per_id, per_fname, per_lname, v_paid_invoice_total
from dbo.v_paid_invoice_total
order by sum_total desc;
go

-- double-check! Check individaul customer paid invoices!
select p.per_id, per_fname, per_lname, inv_total, inv_paid
from dbo.person p
    join dbo.customer c on p.per_id=c.per_id=c.per_id
    join dbo.contact ct on c.per_id=ct.per_cid
    join dbo.[order] o on o.ord_id=i.ord_id
    join dbo.invoice i on o.ord_id=i.ord_id;

-- compare views to base tables
SELECT * from information_schema.tables;
go

-- display definition of trigger, stored procedure, or view
sp_helptext 'dbo.v_paid_invoice_total'
go

-- remote view from server memory
drop view dbo.v_paid_invoice_total;



--2) Create a stored procedure that displays all customers� outstanding balances (unstored derived attribute based upon the difference of a 
--customer's invoice total and their respective payments). List their invoice totals, what was paid, and the difference.


-- a. individual customer
select p.per_id, per_fname, per_lname,
sum(pay_amt) as total_paid, (inv_total - sum(pay_amt)) invoice_diff
    from person p
    join dbo.customer c on p.per_id=c.per_id=c.per_id
    join dbo.contact ct on c.per_id=ct.per_cid
    join dbo.[order] o on o.ord_id=i.ord_id
    join dbo.invoice i on o.ord_id=i.ord_id
    join dbo.payment pt on i.inv_id=pt.inv_id
where p.per_id=7-- might need a ";"

-- must be contained in group by, if not used in agregate function
group by p.per_id, per_fname, per_lname, inv_total;

print'#1 Solution: create procedure (display all customers'' outstanding balances):

';

-- 1st arg is object name, 2nd arg is type (P=procedure)
IF OBJECT_ID(N'dbo.sp_all_customers_obstanding_balances',N'P') IS NOT NULL
DROP PROC dbo.sp_all_customers_obstanding_balances
GO

-- --In MS SQL SERVER: do *NOT* use ORDER BY clause in stored procedures though, *not* views
CREATE PROC dbo.sp_all_customers_obstanding_balances AS
begin
  select p.per_id, per_fname, per_lname
  sum(pay_amt) as total_paid, (inv_total - sum(pay_amt)) invoice_diff
  from person p
    join dbo.customer c on p.per_id=c.per_id=c.per_id
    join dbo.contact ct on c.per_id=ct.per_cid
    join dbo.[order] o on o.ord_id=i.ord_id
    join dbo.invoice i on o.ord_id=i.ord_id
    join dbo.payment pt on i.inv_id=pt.inv_id
-- must be contained in group by, if not used in aggreagate function
group by p.per_id, per_fname, per_lname, inv_total
order by invoice_diff desc;
END
GO

-- call stored procedure (Note: negative values mean *customer* is owed money or credit!)
exec dbo.sp_all_customers_obstanding_balances;

-- list all procedure (e.g., stored procedures or functions) for database
select * from mjowett.information_schema.routines
where routine_type = 'PROCEDURE';
go


-- 3) Create a stored procedure that populates the sales rep history table w/sales reps� data when called.

print'#3 Solution: create stored procedure to popluate history table w/sales reps'' data when called:

';

-- 1st arg is object name, 2nd arg is type (P=procedure)
IF OBJECT_ID(N'dbo.sp_populate_srp_hist_table',N'P') IS NOT NULL
DROP PROC dbo.sp_populate_srp_hist_table
GO

CREATE PROC dbo.sp_populate_srp_hist_table AS
BEGIN
    INSERT INTO dbo.srp_hist
    (per_id, sht_type, sht_modified, sht_modifer, sht_date, sht_yr_sales_goal, sht_yr_total_sales, sht_yr_total_comm, sht_notes)

-- mix dynamically generated data, with orginal sales reps' data
    Select per_id, 'i', getDate(), SYSTME_USER, getDate(), srp_yr_sales_goal, srp_yrt_sales, srp_ytd_sales, srp_ytd_comm, srp_notes
    FROM dbo.slsrep;
END
GO

print 'list table data before call:

';
select * from dbo.slsrep;

-- Purposefully delting orignal data to simulate iniially populating a "log" or history table
delete from dbo.srp_hist;

select * from dbo.srp_hist;  --show that there is no data!

-- call stored procedure (popilated srp_hist_table with slsrep table data)


--4) Create a trigger that automatically adds a record to the sales reps� history table for every record
--added to the sales rep table.

--1st arg is object name, 2nd arg is type (TR=trigger)
IF OBJECT_ID(N'dbo.trg_sales_history_insert', N'TR') IS NOT NULL
  DROP TRIGGER dbo.trg_sales_history_insert
  go

  create trigger dbo.trg_sales_history_insert
  ON dbo.slsrep
  AFTER INSERT AS
  BEGIN
  --declare
  DECLARE
  @per_id_v smallint,
  @sht_type_v char(1),
  @sht_modified_v date,
  @sht_modifier_v varchar(45),
  @sht_date_v date,
  @sht_yr_sales_goal_v decimal(8,2),
  @sht_yr_total_sales_v decimal(8,2),
  @sht_yr_total_comm_v decimal(7,2),
  @sht_notes_v varchar(255);

  SELECT
  @per_id_v = per_id,
  @sht_type_v = 'i',
  @sht_modified_v = getDate(),
  @sht_modifier_v = SYSTEM_USER,
  @sht_date_v  = getDate(),
  @sht_yr_sales_goal_v = srp_yr_sales_goal,
  @sht_yr_total_sales_v = srp_ytd_sales,
  @sht_yr_total_comm_v = srp_ytd_comm,
  @sht_notes_v = srp_notes
  from inserted;

  INSERT INTO dbo.srp_hist
  (per_id, sht_type, sht_modified, sht_modifier, sht_date, sht_yr_sales_goal, sht_yr_total_sales, sht_year_total_comm, sht_notes)
  VALUES
  (@per_id_v, @sht_type_v, @sht_modified_v, @sht_modifier_v, @sht_date_v, @sht_yr_sales_goal_v, @sht_yr_total_sales_v, @sht_yr_total_comm_v, @sht_notes_v);
  END
  GO

  print 'list table data before trigger fires:
  ';
  select * from dbo.slsrep;
 select * from dbo.srp_hist;

 INSERT INTO dbo.slsrep
 (per_id, srp_yr_sales_goal, srp_ytd_sales, srp_ytd_comm, srp_notes)
 VALUES
 (6, 98000, 43000, 8750, 'per_id values 1-5 already used');

 print 'list table data after trigger fires:
 ';
 select * from dbo.slsrep;
 select * from dbo.srp_hist;

 select * from sys.triggers;
 go


-- 5) Create a trigger that automatically adds a record to the product history table for every record
-- added to the product table.

-- 1st arg is object name, 2nd arg is type (TR=trigger)
IF OBJECT_ID(N'dbo.trg_product_history_insert', N'TR') IS NOT NULL
  DROP TRIGGER dbo.trg_product_history_insert
  go

  create trigger dbo.trg_product_history_insert
  ON dbo.product
  AFTER INSERT AS
  BEGIN
  DECLARE
  @pro_id_v smallint,
  @pht_modified_v date,
  @pht_cost_v decimal(7,2),
  @pht_price_v decimal(7,2),
  @pht_discount_v decimal(3,0),
  @pht_notes_v varchar(255);

  SELECT
   @pro_id_v = pro_id,
  @pht_modified_v = getDate(),
  @pht_cost_v = pro_cost,
  @pht_price_v = pro_price,
  @pht_discount_v = pro_discount,
  @pht_notes_v = pro_notes
  from inserted;

  INSERT INTO dbo.product_hist
(pro_id, pht_date, pht_cost, pht_price, pht_discount, pht_notes)  
  VALUES
  (@pro_id_v, @pht_modified_v, @pht_cost_v, @pht_price_v, @pht_discount_v, @pht_notes_v);
  END
  GO



  print 'list table data before trigger fires:
  ';
  select * from product;
 select * from product_hist;

 INSERT INTO dbo.product
 (ven_id, pro_name, pro_descript, pro_weight, pro_qoh, pro_cost, pro_price, pro_discount, pro_notes)
 VALUES
 (3, 'desk lamp', 'small desk lamp with led lights', 3.6, 14, 5.98, 11.99, 15, 'No discounts after sale.');

 print 'list table data after trigger fires:
 ';
 select * from product;
 select * from product_hist;

 select * from sys.triggers;
 go

--***Extra Credit***
--Create a stored procedure that updates sales reps� yearly_sales_goal in the slsrep table, based upon 8% more than their previous year�s total sales 
--(sht_yr_total_sales), name it sp_annual_salesrep_sales_goal. (See Notes above.)

--1st arg is object name, 2nd arg is type (P=procedure)
IF OBJECT_ID(N'dbo.sp_annual_salesrep_sales_goal',N'P') IS NOT NULL
DROP PROC dbo.sp_annual_salesrep_sales_goal
GO

CREATE PROC dbo.sp_annual_salesrep_sales_goal AS
BEGIN
 UPDATE slsrep
 SET srp_yr_sales_goal = sht_yr_total_sales * 1.08
 from slsrep as sr
  join srp_hist as sh
  on sr.per_id = sh.per_id
where sht_date=(select max(sht_date) from srp_hist);
END
GO

print 'list table data before call:
 ';
 select * from dbo.slsrep;
 select * from dbo.srp_hist;

 exec dbo.sp_annual_salesrep_sales_goal;

 print 'list table data after call:
 ';
select * from dbo.slsrep;

 select * from dsh16c.information_schema.routines
  where routine_type = 'PROCEDURE';
  go















